//
//  CourseDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYCourseModel.h"
#import "osceDatailCellStyle2.h"
@interface HYCourseDetailController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) HYCourseModel *model;
@end
