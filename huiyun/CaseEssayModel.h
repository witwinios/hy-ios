//
//  CaseEssayModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseEssayModel : NSObject

@property (strong, nonatomic) NSString *caseEssayModelStatus;
@property (strong, nonatomic) NSString *caseEssayModelTitle;
@property (strong, nonatomic) NSString *caseEssayModelDegree;
@property (strong, nonatomic) NSString *caseEssayModelCategory;
@property (strong, nonatomic) NSString *caseEssayModelAuthor;
@property (strong, nonatomic) NSString *caseEssayModelEdtion;
@property (strong, nonatomic) NSNumber *caseEssayModelDate;
@property (strong, nonatomic) NSString *caseEssayModelTeacher;
@property (strong, nonatomic) NSNumber *caseEssayModelTeacherId;
@property (strong, nonatomic) NSString *caseEssayModelDescription;
@property (strong, nonatomic) NSString *caseEssayModelAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@property (strong, nonatomic) NSNumber *recordId;
@end
