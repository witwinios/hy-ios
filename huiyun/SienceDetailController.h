//
//  SienceDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseSienceModel.h"
#import "BaseWitwinController.h"
@interface SienceDetailController : BaseWitwinController
@property (strong, nonatomic) CaseSienceModel *model;
@end
