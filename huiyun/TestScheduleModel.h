//
//  TestScheduleModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface TestScheduleModel : NSObject
//-----------------
@property (strong, nonatomic) NSString *testStatus;
@property (strong,nonatomic) NSString *testName;
@property (strong, nonatomic) NSNumber *pageNum;
@property (strong, nonatomic) NSString *testDes;
@property (strong, nonatomic) NSNumber *testId;
//-----------------
@end
