//
//  StudyHistoryViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/28.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "HYStudyHistoryViewController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface HYStudyHistoryViewController ()
{
    NSMutableArray *dataArray;
    UITableView *historyTable;
}
@end

@implementation HYStudyHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    //
    [historyTable headerBeginRefreshing];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"学习历史";
    [backNavigation addSubview:titleLab];
    
    dataArray = [NSMutableArray new];
    
    historyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    historyTable.delegate = self;
    historyTable.dataSource = self;
    [historyTable registerNib:[UINib nibWithNibName:@"HYCourseCell" bundle:nil] forCellReuseIdentifier:@"comCell"];
    [self.view addSubview:historyTable];
    //设置文字
    historyTable.headerPullToRefreshText = @"下拉刷新";
    historyTable.headerReleaseToRefreshText = @"松开进行刷新";
    historyTable.headerRefreshingText = @"刷新中。。。";
    [historyTable addHeaderWithTarget:self action:@selector(downRefresh)];

}
- (void)downRefresh{
    [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/studyPlans/studyRecord/%@/registeredCourses?pageStart=1&pageSize=99&studentId=%@",LocalIP,LocalUserId,LocalUserId] Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [historyTable headerEndRefreshing];
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"暂无课程!" places:0 toView:nil];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    HYCourseModel *model = [HYCourseModel new];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.courseStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.courseTime = [dictionary objectForKey:@"startTime"];
                    model.courseType = [dictionary objectForKey:@"scheduleType"];
                    model.courseCategory = [dictionary objectForKey:@"courseCategoryName"];
                    model.courseTeahcher = [dictionary objectForKey:@"teacherName"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.subjectName = [dictionary objectForKey:@"subjectName"];
                    if ([[dictionary objectForKey:@"scheduleStatus"]isEqualToString:@"completed"]){
                        [dataArray addObject:model];
                    }
                }
                [historyTable reloadData];
            }
        }
    } failed:^(NSString *result) {
        [historyTable headerEndRefreshing];
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma -Action
- (void)back:(UIButton *)backBtn{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -tableView协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseDetailController *detailVC = [HYCourseDetailController new];
    detailVC.model = [dataArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comCell"];
    [cell setProperty:[dataArray objectAtIndex:indexPath.row]];
    return cell;
}
@end
