//
//  QuesDetailController.h
//  yun
//
//  Created by MacAir on 2017/5/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherCell.h"
#import "MyQuesModel.h"
#import "TeacherController.h"
@interface QuesDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic, strong) MyQuesModel *model;

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (strong, nonatomic, readwrite) YWTribe *tribe;
@end
