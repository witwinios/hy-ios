//
//  SienceDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SienceDetailController.h"

@interface SienceDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation SienceDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak UIViewController *weakSelf = self;
    self.leftBlock = ^(){
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";

    if ([self.model.caseSienceStatus isEqualToString:@"待审核"]) {
        SienceDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            ScienceRecordController *scienceVC = [ScienceRecordController new];
            scienceVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:scienceVC animated:YES];
        };
    }
    self.titles = @"科研记录";
    titleArray = @[@"审核状态:",@"题目:",@"课题负责人:",@"参与角色:",@"参与时间:",@"带教老师:",@"完成情况:",@"指导意见:",@"附件:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    NSLog(@"%@",_model.caseSienceAdvice);
    NSLog(@"%@",_model.caseSienceStatus);
    self.dataArray = @[_model.caseSienceStatus,_model.caseSienceTitle,_model.caseSienceHeader,_model.caseSienceRole,[[Maneger shareObject]timeFormatter1:_model.caseSienceDate.stringValue],_model.caseSienceTeacher,_model.caseSienceComepleteDes,_model.caseSienceAdvice,_model.fileUrl];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 6 || indexPath.row == 7) {
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else if (indexPath.row == 8){
        
        return three;
    }else{
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
    return one;
    
}

@end
