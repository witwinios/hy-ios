//
//  RequestTools.m
//  xiaoyun
//
//  Created by MacAir on 2017/3/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RequestTools.h"
#define REQUEST_TIME 15;

@implementation RequestTools
+ (id)shareInstance{
    static RequestTools *tools = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (tools == nil) {
            tools = [[RequestTools alloc]init];
        }
    });
    return tools;
}
+ (void)autoLogin:(NSDictionary *)dic Success:(SucessBlock)success failed:(FaildBlock)faild{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10;
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    NSString *url = [NSString stringWithFormat:@"%@/accounts/login",LocalIP];
    [manager POST:url parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [MBProgressHUD hideHUDForView:nil];
        //保存token
        NSHTTPURLResponse *headerResponse = (NSHTTPURLResponse *)task.response;
        [[NSUserDefaults standardUserDefaults] setObject:[headerResponse allHeaderFields][@"Set-CTTS-Token"] forKey:@"token"];
        //
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:nil];
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        NSInteger statusCode = response.statusCode;
        faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
    }];
}
+ (void)RequestFile:(UIImage *)image andParams:(NSDictionary *)dic andUrl:(NSString *)Url Progress:(ProgressBlock)progress Success:(SucessBlock)success failed:(FaildBlock)faild{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //接收类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",
                                                         nil];
    [manager POST:Url parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        
        NSData *imageData =UIImageJPEGRepresentation(image,1);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat =@"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:imageData
                                    name:@"file"
                                fileName:fileName
                                mimeType:@"image/jpeg"];
    } progress:^(NSProgress *_Nonnull uploadProgress) {
        progress(uploadProgress);
        NSLog(@"%@",uploadProgress);
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError * _Nonnull error) {
        //通讯协议状态码
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        NSInteger statusCode = response.statusCode;
        faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
    }];
}
+ (void)RequestWithFile:(UIImage *)image andParams:(NSDictionary *)dic andUrl:(NSString *)Url Success:(SucessBlock)success failed:(FaildBlock)faild{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //接收类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",
                                                         nil];
    [manager POST:Url parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        
        NSData *imageData = UIImageJPEGRepresentation(image,1);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat =@"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:imageData
                                    name:@"file"
                                fileName:fileName
                                mimeType:@"image/jpeg"];
    } progress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"进度%f",uploadProgress.fractionCompleted);
        
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
//        [MBProgressHUD showToastAndMessage:@"成功!" places:0 toView:nil];
        [MBProgressHUD hideHUDForView:nil];
        success(responseObject);
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError * _Nonnull error) {
//        [MBProgressHUD showToastAndMessage:@"失败!" places:0 toView:nil];
        [MBProgressHUD hideHUDForView:nil];
        //通讯协议状态码
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        NSInteger statusCode = response.statusCode;
        faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
    }];
}
+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Message:(NSString *)message Success:(SucessBlock)success failed:(FaildBlock)faild{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        [MBProgressHUD showToastAndMessage:@"没有网络,请检查你的网络!" places:0 toView:nil];
        return;
    }
    //显示加载框
    [MBProgressHUD showHUDAndMessage:message toView:nil];
    //请求
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    //超时时间
    maneger.requestSerializer.timeoutInterval = 10;
    
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    
    if ([method isEqualToString:@"get"] || [method isEqualToString:@"GET"]) {
        
        [maneger GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }else if ([method isEqualToString:@"post"] || [method isEqualToString:@"POST"]){
        [maneger POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }else if ([method isEqualToString:@"put"] || [method isEqualToString:@"PUT"]){
        [maneger PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }
}
//封装的请求
+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success failed:(FaildBlock)faild{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        [MBProgressHUD showToastAndMessage:@"没有网络,请检查你的网络!" places:0 toView:nil];
        return;
    }
    //请求
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    
    //超时时间
    maneger.requestSerializer.timeoutInterval = 40;
    if ([method isEqualToString:@"get"] || [method isEqualToString:@"GET"]) {
        
        [maneger GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }else if ([method isEqualToString:@"post"] || [method isEqualToString:@"POST"]){
        [maneger POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
            NSLog(@"upload=%@",uploadProgress);
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }else if ([method isEqualToString:@"put"] || [method isEqualToString:@"PUT"]){
        [maneger PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
            //通讯协议状态码
            NSInteger statusCode = response.statusCode;
            NSLog(@"statusCode=%ld",(long)statusCode);
            faild([NSString stringWithFormat:@"%ld",(long)statusCode]);
        }];
    }
}
+ (void)simpleRequestUrl:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        [MBProgressHUD showToastAndMessage:@"没有网络,请检查你的网络!" places:0 toView:nil];
        return;
    }
    //请求
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    //超时时间
    maneger.requestSerializer.timeoutInterval = 10;
    if ([method isEqualToString:@"get"] || [method isEqualToString:@"GET"]) {
        
        [maneger GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }else if ([method isEqualToString:@"post"] || [method isEqualToString:@"POST"]){
        [maneger POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }];
    }else if ([method isEqualToString:@"put"] || [method isEqualToString:@"PUT"]){
        [maneger PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        }];
    }
}
+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        [MBProgressHUD showToastAndMessage:@"没有网络,请检查你的网络!" places:0 toView:nil];
        return;
    }
    //请求
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    //超时时间
    maneger.requestSerializer.timeoutInterval = 10;
    if ([method isEqualToString:@"get"] || [method isEqualToString:@"GET"]) {
        
        [maneger GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
//            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else if ([method isEqualToString:@"post"] || [method isEqualToString:@"POST"]){
        [maneger POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }];
    }else if ([method isEqualToString:@"put"] || [method isEqualToString:@"PUT"]){
        [maneger PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:nil];
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:nil];

            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }
}
- (void)tokenAction{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        [MBProgressHUD showToastAndMessage:@"没有网络,请检查你的网络!" places:0 toView:nil];
        return;
    }
    //请求
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    //超时时间
    maneger.requestSerializer.timeoutInterval = 10;
    
    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
    [maneger POST:[NSString stringWithFormat:@"%@/accounts/login?",LocalIP] parameters:@{@"userName":entity.userAccount,@"userPassword":entity.userPassword,@"organizationCode":LocalCODE} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)getRequest:(NSString *)url andParams:(NSDictionary *)params{
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [maneger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //申明请求的数据类型
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    //超时时间
    maneger.requestSerializer.timeoutInterval = REQUEST_TIME;
    [maneger GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)getRequest:(NSString *)url{
    AFHTTPSessionManager *maneger =[AFHTTPSessionManager manager];
    maneger.requestSerializer = [AFJSONRequestSerializer serializer];
    [maneger.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [maneger.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //申明请求的数据类型
    maneger.responseSerializer = [AFJSONResponseSerializer serializer];
    maneger.securityPolicy = [AFSecurityPolicy defaultPolicy];
    maneger.securityPolicy.allowInvalidCertificates = YES;
    maneger.securityPolicy.validatesDomainName = NO;
    //超时时间
    maneger.requestSerializer.timeoutInterval = 10;
    [maneger GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        //通讯协议状态码
        NSInteger statusCode = response.statusCode;
        self.errorBlock([NSString stringWithFormat:@"%ld",(long)statusCode]);
    }];
}
- (void)getSingleRequest:(NSString *)str{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager GET:str parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
}
- (void)postRequestPrams:(NSDictionary *)params andURL:(NSString *)url{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
        //通讯协议状态码
        NSInteger statusCode = response.statusCode;
        NSLog(@"code=%ld",(long)statusCode);
        self.errorBlock([NSString stringWithFormat:@"%ld",(long)statusCode]);
    }];
}
- (void)deleteRequest:(NSString *)url{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    [manager DELETE:url parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)putRequest:(NSDictionary *)params andURL:(NSString *)url{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    [manager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.responseBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)postFile:(UIImageView *)imgView andParams:(NSDictionary *)prams andURL:(NSString *)url{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    [manager.requestSerializer setValue:@"mobile" forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"CTTS-Token"];
    //接收类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",
                                                         nil];
    
    [manager POST:url parameters:prams constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        
        NSData *imageData =UIImageJPEGRepresentation(imgView.image,1);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat =@"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
        
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:imageData
                                    name:@"file"
                                fileName:fileName
                                mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"进度%@",uploadProgress);
        //打印下上传进度
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        self.responseBlock(responseObject);
        //上传成功
        NSLog(@"上传response%@",responseObject);
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError * _Nonnull error) {
        //上传失败
        NSLog(@"error%@",error);
    }];
}
@end

