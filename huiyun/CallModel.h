//
//  CallModel.h
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CallModel : NSObject
//0 人工 1 随机
@property (strong, nonatomic) NSString *callModelType;
@property (strong, nonatomic) NSNumber *callModelUserNo;
@property (strong, nonatomic) NSNumber *callModelUserId;
@property (strong, nonatomic) NSArray *callModelTeacherId;
@property (strong, nonatomic) NSNumber *callModelStationId;
@property (strong, nonatomic) NSNumber *callModelRoomId;
@property (strong, nonatomic) NSString *callModelImage;
@property (strong, nonatomic) NSString *callModelTeacher;
@property (strong, nonatomic) NSString *callModelStationName;
@property (strong, nonatomic) NSString *callModelRoomName;
@property (strong, nonatomic) NSString *callModelUserName;
//必选考点数
@property (strong, nonatomic) NSNumber *selectOsceStationNums;
@end

