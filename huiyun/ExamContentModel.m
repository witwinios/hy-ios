//
//  ExamContentModel.m
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamContentModel.h"

@implementation ExamContentModel
- (void)setExamContentModelStationName:(NSString *)ExamContentModelStationName{
    if ([ExamContentModelStationName isKindOfClass:[NSNull class]]) {
        _ExamContentModelStationName = @"暂无";
    }else{
        _ExamContentModelStationName = ExamContentModelStationName;
    }
}
- (void)setExamContentModelPurpose:(NSString *)ExamContentModelPurpose{
    if ([ExamContentModelPurpose isKindOfClass:[NSNull class]]) {
        _ExamContentModelPurpose = @"暂无";
    }else{
        _ExamContentModelPurpose = ExamContentModelPurpose;
    }
}
- (void)setExamContentModelContent:(NSString *)ExamContentModelContent{
    if ([ExamContentModelContent isKindOfClass:[NSNull class]]) {
        _ExamContentModelContent = @"暂无";
    }else{
        _ExamContentModelContent = ExamContentModelContent;
    }
}
@end
