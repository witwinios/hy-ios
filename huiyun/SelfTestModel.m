//
//  SelfTestModel.m
//  xiaoyun
//
//  Created by MacAir on 17/2/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SelfTestModel.h"

@implementation SelfTestModel
- (void)setTestStatus:(NSString *)testStatus{
    if ([testStatus isEqualToString:@"STARTED"]) {
        _testStatus = @"已开始";
    }else if ([testStatus isEqualToString:@"NOT_START"]){
        _testStatus = @"计划中";
    }else if ([testStatus isEqualToString:@"COMPLETED"]){
        _testStatus = @"已完成";
    }
}
- (void)setTestName:(NSString *)testName{
    if ([testName isKindOfClass:[NSNull class]]) {
        _testName = @"暂无";
    }else{
        _testName = testName;
    }
}
- (void)setTestDes:(NSString *)testDes{
    if ([testDes isKindOfClass:[NSNull class]] || [testDes isEqualToString:@""]) {
        _testDes = @"暂无描述";
    }else{
        _testDes = testDes;
    }
}
@end
