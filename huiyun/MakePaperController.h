//
//  MakePaperController.h
//  yun
//
//  Created by MacAir on 2017/6/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMJDropdownMenu.h"
#import "MainViewController.h"
#import "UIImageView+WebCache.h"
#import "CheckButton.h"
@interface MakePaperController : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitAction:(id)sender;

- (IBAction)backAction:(id)sender;
- (IBAction)selectAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *navigaView;
@property (weak, nonatomic) IBOutlet UILabel *typeQues;
@property (weak, nonatomic) IBOutlet UILabel *indexQues;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollview;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *leftImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightImage;

@property (weak, nonatomic) IBOutlet UILabel *paperTitle;
//试卷
@property (strong, nonatomic) PaperCellModel *paperModel;
//安排
@property (strong, nonatomic) SelfTestModel *testModel;
//
@property (copy, nonatomic) NSString *fromVC;
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSNumber *paperId;
@property (strong, nonatomic) NSNumber *testId;
@end
