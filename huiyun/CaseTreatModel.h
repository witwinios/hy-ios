//
//  CaseTreatModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseTreatModel : NSObject
@property (strong, nonatomic) NSString *caseTreatModelStatus;
@property (strong, nonatomic) NSString *caseTreatModelCategory;
@property (strong, nonatomic) NSNumber *caseTreatModelDate;
@property (strong, nonatomic) NSString *caseTreatModelTeacher;
@property (strong, nonatomic) NSNumber *caseTreatModelTeacherId;
@property (strong, nonatomic) NSString *caseTreatModelDescription;
@property (strong, nonatomic) NSString *caseTreatModelAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@property (strong, nonatomic) NSNumber *recordId;
@end
