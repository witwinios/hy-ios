//
//  MyCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MyCell.h"

@implementation MyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.CourseBtn.layer.cornerRadius = 5;
    self.CourseBtn.layer.masksToBounds = YES;
    self.CourseBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    
}

- (void)setProperty:(TCourseModel *)model{
    
    self.CourseName.text=model.courseName;
    
    self.CourseState.text=model.scheduleStatus;
    
    self.CourseTime.text=[[Maneger shareObject] timeFormatter:model.startTime.stringValue];
    
    self.CourseLocation.text=model.coursePlace;
    
    if([model.scheduleStatus isEqualToString:@"计划中"]){
        self.CourseBtn.tag=0;
        [self.CourseBtn setTitle:@"发布" forState:UIControlStateNormal];
    }else if([model.scheduleStatus isEqualToString:@"已开始"]){
        self.CourseBtn.tag=1;
        [self.CourseBtn setTitle:@"开始上课" forState:UIControlStateNormal];
    }else if([model.scheduleStatus isEqualToString:@"开始上课"]){
        self.CourseBtn.tag=2;
        [self.CourseBtn setTitle:@"结束课程" forState:UIControlStateNormal];
    }else if([model.scheduleStatus isEqualToString:@"结束课程"]){
        self.CourseBtn.tag=3;
        [self.CourseBtn setTitle:@"查看" forState:UIControlStateNormal];
    }
    
}

- (IBAction)CourseBtnAction:(id)sender {
    
    if(self.CourseBtn.tag==0){
        [self.CourseBtn setTitle:@"开始上课" forState:UIControlStateNormal];
        self.CourseState.text=@"已发布";

        self.CourseBtn.tag=1;
    }else if(self.CourseBtn.tag==1){
        [self.CourseBtn setTitle:@"结束课程" forState:UIControlStateNormal];
        self.CourseState.text=@"开始上课";
        
        self.CourseBtn.tag=2;
    }else if(self.CourseBtn.tag==2){
        [self.CourseBtn setTitle:@"查看" forState:UIControlStateNormal];
        self.CourseState.text=@"结束课程";
        
        self.CourseBtn.tag=3;
    }else if(self.CourseBtn.tag==3){
        self.CourseState.text=@"查看";
    }
    
    
    
    
}
@end
