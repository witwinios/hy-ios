//
//  TMainController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TMainController.h"

@interface TMainController ()
{
    NSArray *mainArray;
    NSArray *imageArray;
    NSString *imgUrl;
}
@end

@implementation TMainController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
    UIImage *lastImage = [NSuserDefaultManager readHeadImageByAccount:entity.userID];
    self.headImage.layer.cornerRadius = 5;
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.borderWidth = 1;
    self.headImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    NSString *imageStr = [NSString stringWithFormat:@"%@%@",SimpleIp,entity.fileUrl];
    NSLog(@"---%@---%@---",entity.fileUrl,entity.userID);
    
    if (lastImage == nil) {
        
        [self.headImage sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"head-place"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //保存
            [NSuserDefaultManager saveHeadImage:image byAccount:entity.userID];
        }];
    }else{
        [self.headImage setImage:lastImage];
    }
}
+ (id)shareObject{
    static TMainController *mainVC = nil;
    if (mainVC == nil) {
        mainVC = [TMainController new];
    }
    return mainVC;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    mainArray = @[@"课程安排",@"考试安排",@"审批请假",@"在线评分",@"科室轮转"];
    imageArray = @[@"课程安排",@"考试安排",@"请假",@"自测安排",@"自测安排"];
    //
    [self setUI];
    [self getCurrentVersion];
}
- (void)getCurrentVersion{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *current_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    [RequestTools simpleRequestUrl:@"http://test.hzwitwin.cn:9010/witwin-cspt-web/appVersion/latestIOSAppVersion" Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSString *last_Version = result[@"responseBody"][@"appVersionNumber"];
        NSString *des_version = result[@"responseBody"][@"appVersionDescription"];
        NSString *fomatStr = [des_version stringByReplacingOccurrencesOfString:@"," withString:@"\n"];
        BOOL s = ([current_Version compare:last_Version] == NSOrderedAscending);
        if (s > 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"发现新版本,是否更新?" message:fomatStr preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/app/id1227386925"]];
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"下次在说" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}
- (void)setUI{
    self.headName.text = self.entity.userFullName;
    
    self.headBackView.backgroundColor = UIColorFromHex(0x20B2AA);
    self.mainTable.delegate =self;
    self.mainTable.dataSource = self;
    self.mainTable.bounces = NO;
    self.mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTable registerNib:[UINib nibWithNibName:@"MainVcCell" bundle:nil] forCellReuseIdentifier:@"main"];
}

#pragma -protocal
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            self.tcourseVC = [TCourseController new];
            [self.navigationController pushViewController:self.tcourseVC animated:YES];
            break;
        case 1:
            self.texamVC = [TExamController new];
            [self.navigationController pushViewController:self.texamVC animated:YES];
            break;
        case 2:
            self.tleaveVC = [TLeaveController new];
            [self.navigationController pushViewController:self.tleaveVC animated:YES];
            break;
        case 3:
            self.tmarkVC = [TMarkController new];
            [self.navigationController pushViewController:self.tmarkVC animated:YES];
            break;
        case 4:
            self.departVC = [DepartController new];
            [self.navigationController pushViewController:self.departVC animated:YES];
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainVcCell *cell = [tableView dequeueReusableCellWithIdentifier:@"main"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title.text = mainArray[indexPath.row];
    cell.imgView.image = [UIImage imageNamed:imageArray[indexPath.row]];
    return cell;
}
- (IBAction)accountAction:(id)sender {
    TeacherAccountController *teacherVC = [TeacherAccountController new];
    teacherVC.imageView = self.headImage;
    [self.navigationController pushViewController:teacherVC animated:YES
     ];
}
@end

