//
//  AllStationController.m
//  yun
//
//  Created by MacAir on 2017/8/1.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "AllStationController.h"

@interface AllStationController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    NSMutableArray *selectArray;
    NSMutableArray *heightArray;
    
    
    UILabel *mustStation;
}
@end

@implementation AllStationController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    selectArray = [NSMutableArray new];
    heightArray = [NSMutableArray new];
    [self setUI];
    [self loadStation];
}
- (void)loadStation{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/osceStations?pageSize=999",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId];
    
    NSLog(@"all=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取考点中。。。" Success:^(NSDictionary *result) {
        NSArray *array = result[@"responseBody"][@"result"];
        
        for (NSDictionary *dic in array) {
            ExamPointModel *pointModel = [ExamPointModel new];
            pointModel.ExamPointModelSubject = dic[@"subjectName"];
            pointModel.ExamPointModelCategory = dic[@"categoryName"];
            pointModel.ExamPointModelUseds = dic[@"usageCount"];
            pointModel.ExamPointModelInternal = dic[@"testDuration"];
            pointModel.ExamPointModelStationId = dic[@"stationId"];
            pointModel.ExamPointModelStationName = dic[@"stationName"];
            pointModel.ExamPointModelStatus = @"未评";
            CGFloat height = [Maneger getPonentH:pointModel.ExamPointModelStationName andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-188];
            [heightArray addObject:[NSString stringWithFormat:@"%f",height]];
            [dataArray addObject:pointModel];
        }
        
        [tabView reloadData];
    } failed:^(NSString *result) {
        
    }];
}
- (void)postStation{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/start/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,self.callModel.callModelUserId];
    
    NSMutableArray *array = [NSMutableArray new];
    for (int s=0; s<selectArray.count; s++) {
        ExamPointModel *model = selectArray[s];
        [array addObject:model.ExamPointModelStationId];
    }
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:array Message:@"选取中" Success:^(NSDictionary *result) {
        if ([result[@"errorCode"] isKindOfClass:[NSNull class]]) {
            ControlManeger *VCManeger = [ControlManeger share];
            VCManeger.PointVC = [ExamPointController new];
            VCManeger.PointVC.skillModel = self.skillModel;
            VCManeger.PointVC.callModel = self.callModel;
            [self.navigationController pushViewController:VCManeger.PointVC animated:YES];
            return ;
        }
        if ([result[@"errorCode"] isEqualToString:@"nextStudent_is_not_equal"]) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您已选取过考站,是否继续?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView show];
        }
      
    } failed:^(NSString *result) {
        
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"考站列表";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    UIButton *switchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    switchBtn.frame = CGRectMake(Swidth-80, 27, 80, 30);
    [switchBtn setTitle:@"确定" forState:0];
    switchBtn.layer.cornerRadius = 5;
    switchBtn.layer.masksToBounds = YES;
    switchBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    switchBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    switchBtn.layer.borderWidth = 1;
    [switchBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [switchBtn setTitleColor:[UIColor whiteColor] forState:0];
    [navigationView addSubview:switchBtn];
    
    [self.view addSubview:navigationView];
    UIView *view  = [[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 46)];
    view.backgroundColor = [UIColor lightGrayColor];
    UILabel *totalStation = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 120, 36)];
    
    totalStation.text = [NSString stringWithFormat:@"可选站点: %@",self.callModel.selectOsceStationNums];
    totalStation.textAlignment = 1;
    totalStation.font = [UIFont systemFontOfSize:20];
    mustStation = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, 5, 120, 36)];
    mustStation.text = @"已选站点: 0";
    mustStation.textAlignment = 1;
    mustStation.font = [UIFont systemFontOfSize:20];
    [view addSubview:totalStation];
    [view addSubview:mustStation];
    
    [self.view addSubview:view];
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 110, Swidth, Sheight-110) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"ExamPointCell" bundle:nil] forCellReuseIdentifier:@"pointCell"];
    tabView.tableFooterView.frame = CGRectZero;
//    tabView.editing = YES;
    [tabView setEditing:YES animated:YES];
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
//    [tabView addHeaderWithTarget:self action:@selector(pullStudent)];
//    tabView.headerPullToRefreshText = @"下拉刷新";
//    tabView.headerReleaseToRefreshText = @"松开进行刷新";
//    tabView.headerRefreshingText = @"刷新中。。。";
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)sureAction:(UIButton *)btn{
    if (selectArray.count<self.callModel.selectOsceStationNums.integerValue) {
        [MBProgressHUD showToastAndMessage:@"选取考站数不足!" places:0 toView:nil];
        return;
    }
    [self postStation];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 149+[heightArray[indexPath.row] floatValue];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectArray.count == self.callModel.selectOsceStationNums.integerValue) {
        [MBProgressHUD showToastAndMessage:[NSString stringWithFormat:@"最多选%@个",self.callModel.selectOsceStationNums] places:0 toView:nil];
        return;
    }
    
    [selectArray addObject:dataArray[indexPath.row]];
    mustStation.text = [NSString stringWithFormat:@"已选站点: %ld",(unsigned long)selectArray.count];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    ExamPointModel *pointModel = dataArray[indexPath.row];
    for (int s=0; s<selectArray.count; s++) {
        ExamPointModel *model = selectArray[s];
        if (model.ExamPointModelStationId.integerValue == pointModel.ExamPointModelStationId.integerValue) {
            [selectArray removeObjectAtIndex:s];
        }
    }
    mustStation.text = [NSString stringWithFormat:@"已选站点: %ld",(unsigned long)selectArray.count];
}
//多选
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExamPointCell *cell = [tabView dequeueReusableCellWithIdentifier:@"pointCell"];
    ExamPointModel *pointModel = dataArray[indexPath.row];
    
    cell.stationName.text = pointModel.ExamPointModelStationName;
    cell.subject.text = pointModel.ExamPointModelSubject;
    cell.category.text = pointModel.ExamPointModelCategory;
    cell.uesds.text = pointModel.ExamPointModelUseds.stringValue;
    cell.internal.text = pointModel.ExamPointModelInternal.stringValue;
    cell.pointStatus.text = pointModel.ExamPointModelStatus;
    return cell;
}
#pragma -alertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.PointVC = [ExamPointController new];
        VCManeger.PointVC.skillModel = self.skillModel;
        VCManeger.PointVC.callModel = self.callModel;
        [self.navigationController pushViewController:VCManeger.PointVC animated:YES];
    }
}
@end
