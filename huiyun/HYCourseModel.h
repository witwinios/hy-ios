//
//  CourseModel.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Maneger.h"
@interface HYCourseModel : NSObject
@property (strong, nonatomic) NSString *courseName;
@property (strong, nonatomic) NSString *courseType;
@property (strong, nonatomic) NSString *coursePlace;
@property (strong, nonatomic) NSNumber *courseTime;
@property (strong, nonatomic) NSString *courseStatus;
@property (strong, nonatomic) NSString *courseCategory;
@property (strong, nonatomic) NSString *courseTeahcher;
@property (strong, nonatomic) NSString *courseDes;
@property (strong, nonatomic) NSString *subjectName;
- (NSString *)display;
@end
