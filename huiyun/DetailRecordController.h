//
//  DetailRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseMedicalModel.h"
@interface DetailRecordController : UIViewController
@property (strong, nonatomic) CaseMedicalModel *model;
@end
