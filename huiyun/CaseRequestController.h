//
//  CaseRequestController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseRequestCell.h"
#import "CaseHistoryController.h"
#import "CaseRequestOperateCell.h"
#import "ResultSubmitedController.h"
#import "OperateRecordController.h"
#import "ActivityRecordController.h"
#import "TeachRecordController.h"
#import "ScienceRecordController.h"
#import "RewardRecordController.h"
#import "PaperRecordController.h"
#import "SCaseRecordController.h"
#import "RescueRecordController.h"
#import "VisitRecordController.h"
#import "MistakeRecordController.h"
#import "CaseRequestController.h"
#import "CaseRequestActivityCell.h"
@interface CaseRequestController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) SDepartRecordModel *model;

@property (strong, nonatomic) OperateRecordController *operateVC;
@property (strong, nonatomic) ActivityRecordController *activityVC;
@property (strong, nonatomic) TeachRecordController *teachVC;
@property (strong, nonatomic) ScienceRecordController *scienceVC;
@property (strong, nonatomic) RewardRecordController *rewardVC;
@property (strong, nonatomic) PaperRecordController *paperVC;
@property (strong, nonatomic) RescueRecordController *rescueVC;
@property (strong, nonatomic) VisitRecordController *visitVC;
@property (strong, nonatomic) MistakeRecordController *mistakeVC;
@property (strong, nonatomic) SCaseRecordController *scaseVC;

@end

