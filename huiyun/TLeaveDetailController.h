//
//  TLeaveDetailController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryLeaveModel.h"
@interface TLeaveDetailController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *leaveName;
@property (weak, nonatomic) IBOutlet UILabel *leaveStatus;
@property (weak, nonatomic) IBOutlet UIView *naviView;
@property (weak, nonatomic) IBOutlet UILabel *leaveTime;
@property (weak, nonatomic) IBOutlet UILabel *leaveType;
@property (weak, nonatomic) IBOutlet UITextView *leaveReason;
@property (weak, nonatomic) IBOutlet UIButton *imageBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *disagreeBtn;
- (IBAction)agreeAction:(id)sender;
- (IBAction)disagreeAction:(id)sender;

- (IBAction)backAction:(id)sender;
@property(strong,nonatomic) HistoryLeaveModel *model;
@end
