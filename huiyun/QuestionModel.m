//
//  QuestionModel.m
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "QuestionModel.h"

@implementation QuestionModel
- (void)setQuestionFile:(NSString *)questionFile{
    if ([questionFile isKindOfClass:[NSNull class]]) {
        _questionFile = @"";
    }else{
        _questionFile = questionFile;
    }
}
- (void)setQuestTitle:(NSString *)questTitle{
    if ([questTitle isKindOfClass:[NSNull class]]) {
        _questTitle = @"";
    }else{
        _questTitle = questTitle;
    }
}
- (void)setQuestionDes:(NSString *)questionDes{
    if ([questionDes isKindOfClass:[NSNull class]]||[questionDes isEqualToString:@""]) {
        _questionDes = @"暂无解析";
    }else{
        _questionDes = questionDes;
    }
}
- (void)setQuestionType:(NSString *)questionType{
    if ([questionType isEqualToString:@"b1_choice_question"]){
        _questionType = @"B1题型";
    }else if ([questionType isEqualToString:@"b2_choice_question"]){
        _questionType = @"B2题型";
    }else if ([questionType isEqualToString:@"b3_choice_question"]){
        _questionType = @"B3题型";
    }else if ([questionType isEqualToString:@"b4_choice_question"]){
        _questionType = @"B4题型";
    }else if ([questionType isEqualToString:@"a1_choice_question"]){
        _questionType = @"A1题型";
    }else if ([questionType isEqualToString:@"a2_choice_question"]){
        _questionType = @"A2题型";
    }else if ([questionType isEqualToString:@"a3_choice_question"]){
        _questionType = @"A3题型";
    }else if ([questionType isEqualToString:@"a4_choice_question"]){
        _questionType = @"A4题型";
    }else if ([questionType isEqualToString:@"choice_question"]){
        _questionType = @"A1选择题";
    }else if ([questionType isEqualToString:@"x_choice_question"]){
        _questionType = @"X: 多选题";
    }
}
- (NSComparisonResult)compareWithQuestion:(QuestionModel *)model{
    if (self.questionId.integerValue > model.questionId.integerValue) {
        return NSOrderedDescending;
    }else if (self.questionId.integerValue == model.questionId.integerValue){
        return NSOrderedSame;
    }else{
        return NSOrderedAscending;
    }
}
@end
