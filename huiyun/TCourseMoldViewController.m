//
//  TCourseMoldViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseMoldViewController.h"
#import "YKMultiLevelTableView.h"

@interface TCourseMoldViewController ()

@end

@implementation TCourseMoldViewController{
    NSString *DateString;
    
    
    NSMutableArray *TreeArray;
    YKMultiLevelTableView *mutableTable;
    
    NSMutableArray *DateID;
    NSString *DateMoldString;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];

    [self returnData];
    
   
}







- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程类型";
    [backNavigation addSubview:titleLab];
}

#pragma mark 确定：



#pragma mark 回退
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)returnData{
    NSString *UrlLocation=[NSString stringWithFormat:@"%@/subjects",LocalIP];
    [RequestTools RequestWithURL:UrlLocation Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程科目" andCurentVC:self];
            }else{
                TreeArray=[NSMutableArray new];
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    
                    
                    DateID=[NSMutableArray new];
                    YKNodeModel *model=[YKNodeModel new];
                    model.parentID=[dic objectForKey:@"parentSubjectId"];
                    model.name=[dic objectForKey:@"subjectName"];
                    model.childrenID=[dic objectForKey:@"subjectId"];

                    YKNodeModel *node=[YKNodeModel
                                       nodeWithParentID:[NSString stringWithFormat:@"%@",model.parentID]
                                       name:model.name
                                       childrenID:[NSString stringWithFormat:@"%@",model.childrenID]
                                                        isExpand:NO];

              
                    
                    
                   [DateID addObject: [dic objectForKey:@"subjectId"]];
                   [TreeArray addObject:node];
                   [mutableTable reloadData];

                }
                NSLog(@"DateID=%@",DateID);
            };

            CGRect frame = CGRectMake(0, 64, Swidth, Sheight);

            mutableTable = [[YKMultiLevelTableView alloc] initWithFrame:frame
                                                                  nodes:TreeArray
                                                             rootNodeID:@""
                                                       needPreservation:YES
                                                            selectBlock:^(YKNodeModel *node) {
                                                                DateString=node.name;
                                                                DateMoldString=node.childrenID;
                                                                [self.delegate text:DateString dateID:DateMoldString];
                                                                [self.navigationController popViewControllerAnimated:YES];
                                                                NSLog(@"--select node name=%@", node.name);

                                                            }];

            
            mutableTable.showsVerticalScrollIndicator=NO;
            
            [self.view addSubview:mutableTable];


        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
