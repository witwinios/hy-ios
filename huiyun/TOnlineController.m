//
//  OnlineDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/1/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TOnlineController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface TOnlineController ()
{
    UITableView *onlineTable;
}
@end

@implementation TOnlineController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self createUI];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"在线详情";
    [backNavigation addSubview:titleLab];
}
- (void)createUI{
    //
    onlineTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT) style:UITableViewStylePlain];
    onlineTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    onlineTable.delegate = self;
    onlineTable.dataSource = self;
    onlineTable.backgroundColor = UIColorFromHex(0xf5f5f5);
    [self.view addSubview:onlineTable];
    //注册cell
    [onlineTable registerNib:[UINib nibWithNibName:@"osceDetailCellStyle1" bundle:nil] forCellReuseIdentifier:@"style1"];
    [onlineTable registerNib:[UINib nibWithNibName:@"osceDatailCellStyle2" bundle:nil] forCellReuseIdentifier:@"style2"];
}
#pragma -protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    view.backgroundColor = UIColorFromHex(0xf5f5f5);
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 6:
            
            break;
            
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    osceDetailCellStyle1 *cell;
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
        if (indexPath.row == 0) {
            cell.biaoti.text = @"考试名称:";
            cell.content.text = [self.model onlineName];
        }else if (indexPath.row == 1){
            cell.biaoti.text = @"考试时间:";
            cell.content.text = [[Maneger shareObject] timeFormatter:[self.model onlineTime].stringValue];
        }else if (indexPath.row == 2){
            cell.biaoti.text = @"考试时长:";
            cell.content.text = [NSString stringWithFormat:@"%@分钟",[self.model onlineInternal].stringValue];
        }else if (indexPath.row == 3){
            cell.biaoti.text = @"考试地点:";
            cell.content.text = [self.model onlineRoom];
        }else if (indexPath.row == 4){
            cell.biaoti.text = @"试卷数:";
            cell.content.text = [self.model onlinePages].stringValue;
        }else if (indexPath.row == 5){
            cell.biaoti.text = @"参考人数:";
            cell.imgShow.alpha = 1;
            cell.content.text = [self.model selectNums].stringValue;
        }else if (indexPath.row == 6){
            cell.biaoti.text = @"监考老师:";
            cell.content.text = [self.model teacher];
        }else {
            cell.biaoti.text = @"考试说明:";
            cell.content.text = [self.model onlineDes];
        }
    }
    return cell;
}
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
