//
//  MakePaperController.m
//  yun
//
//  Created by MacAir on 2017/6/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MakePaperController.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface MakePaperController ()<LMJDropdownMenuDelegate>
{
    //所有父题
    NSMutableArray *parrenArray;
    //所有子题
    NSMutableArray *childArray;
    //所有单选
    NSMutableArray *singleArray;
    //单选管理
    NSMutableArray *manegerArray;
    //有父题管理
    NSMutableArray *manegerParrent;
    //答案数组
    NSMutableArray *answerArray;
    //
    NSMutableArray *totalArray;
    //题目标题
    int quesNumber;
    int pageNum;
    int isDo;
    //调取试卷的API，根据状态定
    NSString *paperUrl;
    //模糊view
    UIView *showView;
    //秒表计时器
    int seconds;
    int minutes;
    //所有问题切换View
    UIScrollView *questionView;
    //
    UIView *answerViews;
    UILabel *correctLabel;
    UILabel *answerLabel;
    UITapGestureRecognizer *hideTap;
}
@end

@implementation MakePaperController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    totalArray = [NSMutableArray new];
    //添加手势
    UITapGestureRecognizer *leftTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftScroll:)];
    leftTap.numberOfTapsRequired = 1;
    [self.leftImage addGestureRecognizer:leftTap];
    
    hideTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideAnswerView)];
    hideTap.numberOfTapsRequired = 1;
    
    self.leftImage.userInteractionEnabled = YES;
    self.leftImage.frame = CGRectMake(140, 67, 30, 30);
    self.rightImage.frame = CGRectMake(WIDTH-140, 67, 30, 30);
    
    UITapGestureRecognizer *rightTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightScroll:)];
    rightTap.numberOfTapsRequired = 1;
    [self.rightImage addGestureRecognizer:rightTap];
    self.rightImage.userInteractionEnabled = YES;
    //初始化计时器
    seconds = 0;
    minutes = 0;
    self.navigaView.backgroundColor = UIColorFromHex(0x20B2AA);
    self.timerLabel.layer.cornerRadius = 5;
    self.timerLabel.layer.masksToBounds = YES;
    self.timerLabel.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    self.timerLabel.layer.borderWidth = 1;
    //关闭右滑动
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.mainScrollview.pagingEnabled = YES;
    self.mainScrollview.showsHorizontalScrollIndicator = NO;
    //
    self.typeQues.layer.cornerRadius = 5;
    self.typeQues.layer.masksToBounds = YES;
    self.typeQues.layer.borderWidth = 1;
    self.typeQues.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    self.typeQues.textColor = UIColorFromHex(0x20B2AA);
    self.typeQues.textAlignment = 1;
    
    parrenArray = [NSMutableArray new];
    childArray = [NSMutableArray new];
    singleArray = [NSMutableArray new];
    manegerArray = [NSMutableArray new];
    answerArray = [NSMutableArray new];
    manegerParrent = [NSMutableArray new];
    showView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    showView.backgroundColor = [UIColor lightGrayColor];
    showView.alpha = 0.5;
    [self.view addSubview:showView];
    
    quesNumber = 1;
    pageNum = 0;
    isDo = 0;
    if ([self.fromVC isEqualToString:@"error"]) {
        //
        paperUrl = [NSString stringWithFormat:@"%@/testPapers/%@/questions?pageSize=999",LocalIP,self.paperId];
        //改变试卷状态
        NSString *requestUrl = [NSString stringWithFormat:@"%@/studentSelfTestRecords/%@/startQuestionResponses",LocalIP,_recordId];
        
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Success:^(NSDictionary *result) {
            NSLog(@"改变试卷状态");
        }];
        isDo = 0;
        self.submitBtn.alpha = 1;
        self.submitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        self.submitBtn.layer.borderWidth = 1;
        self.submitBtn.layer.cornerRadius = 5;
        self.submitBtn.layer.masksToBounds = YES;
        [self.submitBtn addTarget:self action:@selector(submitPage) forControlEvents:UIControlEventTouchUpInside];
        [self loadQuestion];
        
    }else{
        //根据状态请求API
        if ([self.paperModel.paperStatus isEqualToString:@"完成"]){
            paperUrl = [NSString stringWithFormat:@"%@/selfTests/%@/testPapers/%@/responseAnswers?pageSize=999",LocalIP,self.testModel.testId,self.paperModel.paperID];
            isDo = 1;
            self.submitBtn.alpha = 0;
            [self loadQuestionAnswer];
        }else{
            //
            //            paperUrl = [NSString stringWithFormat:@"%@/testPapers/%@/questions?pageSize=999",LocalIP,self.paperModel.paperID];
            paperUrl = [NSString stringWithFormat:@"%@/testPapers/%@/questions?showAnswers=true&pageSize=999",LocalIP,self.paperModel.paperID];
            //改变试卷状态
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentSelfTestRecords/%@/startQuestionResponses",LocalIP,self.paperModel.recordID];
            
            NSLog(@"requestUrl = %@",requestUrl);
            
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Success:^(NSDictionary *result) {
            }];
            self.submitBtn.alpha = 1;
            self.submitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            self.submitBtn.layer.borderWidth = 1;
            self.submitBtn.layer.cornerRadius = 5;
            self.submitBtn.layer.masksToBounds = YES;
            [self.submitBtn addTarget:self action:@selector(submitPage) forControlEvents:UIControlEventTouchUpInside];
            isDo = 0;
            [self loadQuestion];
        }
    }
    
    answerViews = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    answerViews.backgroundColor = [UIColor lightGrayColor];
    correctLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Swidth/2, 30)];
    correctLabel.font = [UIFont systemFontOfSize:15];
    
    UILabel *downLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-35, 1.5, 30, 30)];
    downLabel.text = @"V";
    downLabel.textAlignment = 1;
    downLabel.adjustsFontSizeToFitWidth = YES;
    downLabel.backgroundColor = [UIColor whiteColor];
    downLabel.textColor = UIColorFromHex(0x20B2AA);
    downLabel.layer.cornerRadius = 25;
    downLabel.layer.masksToBounds = YES;
    downLabel.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    downLabel.layer.borderWidth = 1;
    
    answerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 32, Swidth, 44)];
    answerLabel.font = [UIFont systemFontOfSize:15];
    answerLabel.numberOfLines = 0;
    
    [answerViews addSubview:downLabel];
    [answerViews addSubview:answerLabel];
    [answerViews addSubview:correctLabel];
    [self.view addSubview:answerViews];
}
//做完题目加载
- (void)loadQuestionAnswer{
    [MBProgressHUD showHUDAndMessage:@"加载中..." toView:nil];
    [RequestTools RequestWithURL:paperUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        for (int i=0; i<array.count; i++) {
            NSDictionary *dictionary = [array objectAtIndex:i];
            QuestionModel *model = [QuestionModel new];
            model.questionId = [dictionary objectForKey:@"questionId"];
            model.questionType = [dictionary objectForKey:@"questionType"];
            model.questTitle = [dictionary objectForKey:@"questionTitle"];
            model.choiceOptions = [dictionary objectForKey:@"choiceOptions"];
            if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                model.parrentId = [NSNumber numberWithInt:0];
            }else{
                model.parrentId = [dictionary objectForKey:@"parentQuestionId"];
            }
            //正确答案
            NSArray *correctArray = [dictionary objectForKey:@"correctAnswer"];
            NSMutableArray *tempCorrect = [NSMutableArray new];
            for (int s=0; s<correctArray.count; s++) {
                NSDictionary *dic = correctArray[s];
                [tempCorrect addObject:[dic objectForKey:@"answer"]];
            }
            model.correctAnswer = tempCorrect;
            //选择的答案
            
            model.responseAnswer = [dictionary objectForKey:@"responseAnswer"];
            
            model.questionDes = [dictionary objectForKey:@"analysis"];
            model.childNum = [dictionary objectForKey:@"childrenQuestionsNum"];
            //获取图片
            if (![[dictionary objectForKey:@"questionFile"] isKindOfClass:[NSNull class]]) {
                model.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,[[dictionary objectForKey:@"questionFile"] objectForKey:@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }else{
                model.questionFile = @"";
            }
            
            int childNums = [[dictionary objectForKey:@"childrenQuestionsNum"] intValue];
            //组装
            if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums == 0) {
                //单独选择题
                [singleArray addObject:model];
                
            }else if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums != 0){
                //父题
                [parrenArray addObject:model];
            }else if (![[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]){
                [childArray addObject:model];
            }
        }
        //升序
        [singleArray sortUsingSelector:@selector(compareWithQuestion:)];
        [parrenArray sortUsingSelector:@selector(compareWithQuestion:)];
        
        
        for (int s=0; s<singleArray.count; s++) {
            [totalArray addObject:singleArray[s]];
        }
        for (int k=0; k<parrenArray.count; k++) {
            [totalArray addObject:parrenArray[k]];
        }
        [MBProgressHUD hideHUDForView:nil];
        [showView removeFromSuperview];
        [self setAnswerUI];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        [showView removeFromSuperview];
    }];
}
//带答案的UI
- (void)setAnswerUI{
    self.timerLabel.alpha = 0;
    //
    self.paperTitle.text = self.paperModel.paperName;
    //设置mainScrollview
    NSInteger scroll_W = parrenArray.count + singleArray.count;
    self.mainScrollview.contentSize = CGSizeMake(Swidth*scroll_W, Sheight-111);
    self.mainScrollview.delegate = self;
    self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
    int s = self.mainScrollview.contentOffset.x/Swidth;
    QuestionModel *model = totalArray[s];
    self.typeQues.text = model.questionType;
    
    //创建单选题
    for (int i=0; i<singleArray.count; i++) {
        //每个题目的高度
        CGFloat total_H = 0;
        
        UIScrollView *pageScroll = [UIScrollView new];
        pageScroll.frame = CGRectMake(Swidth * pageNum , 0, Swidth, Sheight-101);
        
        pageScroll.backgroundColor = UIColorFromHex(0xF0F0F0);
        pageScroll.contentSize = CGSizeMake(Swidth, Sheight-101);
        [self.mainScrollview addSubview:pageScroll];
        //
        QuestionModel *quesModel = singleArray[i];
        //设置answerArray
        NSDictionary *answerDictionary = @{@"questionId":quesModel.questionId,@"responseAnswer":@[]};
        [answerArray addObject:answerDictionary];
        //        self.typeQues.text = quesModel.questionType;
        if (![quesModel.questionFile isEqualToString:@""]) {
            
            UILabel *label = [UILabel new];
            label.text = [NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle];
            label.font = [UIFont systemFontOfSize:20];
            label.numberOfLines = 0;
            CGFloat title_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
            label.textColor = UIColorFromHex(0x6C6C6C);
            label.frame = CGRectMake(0, 0, Swidth, title_H);
            [pageScroll addSubview:label];
            
            ButtonManeger *btnManeger = [ButtonManeger new];
            [manegerArray addObject:btnManeger];
            UIImageView *quesImage = [UIImageView new];
            
            [quesImage sd_setImageWithURL:[NSURL URLWithString:quesModel.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
            
            quesImage.frame = CGRectMake(Swidth-130, title_H, 130, 130);
            quesImage.backgroundColor = [UIColor blueColor];
            [pageScroll addSubview:quesImage];
            
            CGFloat result_H = 25;
            if (quesModel.choiceOptions.count != 0) {
                for (int s=0; s<quesModel.choiceOptions.count; s++) {
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = quesModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:quesModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-180];
                    
                    choiceLabel.frame = CGRectMake(45, title_H+result_H, Swidth-180, choice_H);
                    result_H += choice_H+20;
                    [pageScroll addSubview:choiceLabel];
                    //选项
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(20, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(40, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                    //
                }
                total_H = title_H + result_H;
            }
            //答案Label
            UIView *answerView = [UIView new];
            answerView.backgroundColor = UIColorFromHex(0xD0E0E0);
            //正确选项
            UILabel *correctLab = [UILabel new];
            correctLab.frame = CGRectMake(5, 2, WIDTH/2-10, 30);
            correctLab.textColor = UIColorFromHex(0x6C6C6C);
            if (quesModel.correctAnswer.count !=0) {
                NSMutableString *str = [NSMutableString new];
                for (int u=0; u<quesModel.correctAnswer.count; u++) {
                    [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[quesModel.correctAnswer[u] intValue]+65]]];
                }
                correctLab.text = [NSString stringWithFormat:@"正确答案:%@",str];
            }else{
                correctLab.text = @"";
            }
            
            correctLab.adjustsFontSizeToFitWidth = YES;
            correctLab.font = [UIFont systemFontOfSize:12];
            [answerView addSubview:correctLab];
            //你的选项
            UILabel *customLab = [UILabel new];
            customLab.frame = CGRectMake(WIDTH/2, 2, WIDTH/2-10, 30);
            if (quesModel.responseAnswer.count != 0) {
                NSMutableString *str = [NSMutableString new];
                //排序
                NSArray *sortArray = [quesModel.responseAnswer sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    return [obj1 compare:obj2];
                }];
                for (int u=0; u<sortArray.count; u++) {
                    [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                }
                customLab.text = [NSString stringWithFormat:@"你的答案:%@",str];
            }else{
                customLab.text = @"你的选择:";
            }
            customLab.textColor = [UIColor redColor];
            customLab.font = [UIFont systemFontOfSize:12];
            [answerView addSubview:customLab];
            //解析
            UILabel *analysLab = [UILabel new];
            analysLab.numberOfLines = 0;
            analysLab.font = [UIFont systemFontOfSize:15];
            
            analysLab.text = [NSString stringWithFormat:@"解析: %@",quesModel.questionDes];
            analysLab.textColor = UIColorFromHex(0x6C6C6C);
            CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",quesModel.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:WIDTH-10];
            analysLab.frame = CGRectMake(5, 32, Swidth-10, analys_H);
            [answerView addSubview:analysLab];
            answerView.frame = CGRectMake(0, pageScroll.frame.size.height-32-analys_H, Swidth, analys_H+32);
            //
            [pageScroll addSubview:answerView];
        }else{
            if (![quesModel.questTitle isEqualToString:@""]) {
                UILabel *label = [UILabel new];
                label.text = [NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle];
                label.font = [UIFont systemFontOfSize:20];
                label.numberOfLines = 0;
                label.textColor = UIColorFromHex(0x6C6C6C);
                CGFloat title_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
                label.frame = CGRectMake(0, 0, Swidth, title_H);
                [pageScroll addSubview:label];
                
                ButtonManeger *btnManeger = [ButtonManeger new];
                [manegerArray addObject:btnManeger];
                CGFloat result_H = 25;
                if (quesModel.choiceOptions.count != 0) {
                    for (int s=0; s<quesModel.choiceOptions.count; s++) {
                        
                        UILabel *choiceLabel = [UILabel new];
                        choiceLabel.text = quesModel.choiceOptions[s];
                        choiceLabel.font = [UIFont systemFontOfSize:16];
                        choiceLabel.numberOfLines = 0;
                        choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                        CGFloat choice_H = [Maneger getPonentH:quesModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-60];
                        
                        choiceLabel.frame = CGRectMake(45, title_H+result_H, Swidth-60, choice_H);
                        result_H += choice_H+20;
                        [pageScroll addSubview:choiceLabel];
                        //
                        UILabel *Alabel = [UILabel new];
                        Alabel.frame = CGRectMake(20, 0, 20, 20);
                        Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                        Alabel.center = CGPointMake(40, choiceLabel.center.y);
                        [pageScroll addSubview:Alabel];
                    }
                }
            }else{
                
            }
            //答案Label
            UIView *answerView = [UIView new];
            answerView.backgroundColor = UIColorFromHex(0xD0D0D0);
            //正确选项
            UILabel *correctLab = [UILabel new];
            correctLab.frame = CGRectMake(5, 2, WIDTH/2-10, 30);
            correctLab.textColor = UIColorFromHex(0x6C6C6C);
            if (quesModel.correctAnswer.count != 0) {
                NSMutableString *str = [NSMutableString new];
                for (int u=0; u<quesModel.correctAnswer.count; u++) {
                    [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[quesModel.correctAnswer[u] intValue]+65]]];
                }
                
                correctLab.text = [NSString stringWithFormat:@"正确答案:%@",str];
            }else{
                correctLab.text = @"";
            }
            correctLab.adjustsFontSizeToFitWidth = YES;
            correctLab.font = [UIFont systemFontOfSize:12];
            [answerView addSubview:correctLab];
            //你的选项
            UILabel *customLab = [UILabel new];
            customLab.frame = CGRectMake(WIDTH/2, 2, WIDTH/2-10, 30);
            customLab.font = [UIFont systemFontOfSize:12];
            customLab.textColor = [UIColor redColor];
            /*拼接答案*/
            
            if (quesModel.responseAnswer.count != 0) {
                NSMutableString *str = [NSMutableString new];
                NSArray *sortArray = [quesModel.responseAnswer sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    return [obj1 compare:obj2];
                }];
                for (int u=0; u<sortArray.count; u++) {
                    [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                }
                customLab.text = [NSString stringWithFormat:@"你的答案:%@",str];
            }else{
                customLab.text = @"你的选择:";
            }
            
            [answerView addSubview:customLab];
            //解析
            UILabel *analysLab = [UILabel new];
            analysLab.numberOfLines = 0;
            analysLab.font = [UIFont systemFontOfSize:15];
            analysLab.text = [NSString stringWithFormat:@"解析: %@",quesModel.questionDes];
            analysLab.textColor = UIColorFromHex(0x6C6C6C);
            CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",quesModel.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:WIDTH-10];
            analysLab.frame = CGRectMake(5, 32, Swidth-10, analys_H);
            [answerView addSubview:analysLab];
            answerView.frame = CGRectMake(0, pageScroll.frame.size.height-32-analys_H, Swidth, analys_H+32);
            //
            [pageScroll addSubview:answerView];
        }
        pageNum++;
        quesNumber++;
    }
    //创建父题
    for (int s=0; s<parrenArray.count; s++) {
        //当前的角标
        CGFloat current_H = 0;
        QuestionModel *parrentModel = parrenArray[s];
        //        self.typeQues.text = parrentModel.questionType;
        
        //创建每个页面的上下滑动视图
        UIScrollView *pageScroll = [UIScrollView new];
        pageScroll.backgroundColor = UIColorFromHex(0xF0F0F0);
        pageScroll.frame = CGRectMake(Swidth * (quesNumber-1) , 0, Swidth, Sheight-101);
        
        [self.mainScrollview addSubview:pageScroll];
        //创建标题
        UILabel *parrentTitle = [UILabel new];
        parrentTitle.font = [UIFont systemFontOfSize:20];
        parrentTitle.numberOfLines = 0;
        parrentTitle.text = [NSString stringWithFormat:@"%d  %@",quesNumber,parrentModel.questTitle];
        parrentTitle.textColor = UIColorFromHex(0x6C6C6C);
        CGFloat parrent_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d  %@",quesNumber,parrentModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
        parrentTitle.frame = CGRectMake(0, 0, Swidth, parrent_H);
        [pageScroll addSubview:parrentTitle];
        //角标增加
        current_H += parrent_H+25;
        //创建选项
        CGFloat parrent_choice_H = 0;
        if ([parrentModel.questionFile isEqualToString:@""]) {
            //无图片
            if (parrentModel.choiceOptions.count != 0) {
                //选项不为空
                for (int s=0; s<parrentModel.choiceOptions.count; s++) {
                    
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = parrentModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:parrentModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-60];
                    
                    choiceLabel.frame = CGRectMake(45, parrent_choice_H+parrent_H, Swidth-60, choice_H);
                    
                    parrent_choice_H += choice_H+20;
                    
                    [pageScroll addSubview:choiceLabel];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(20, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(40, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                }
                
            }
            
            
        }else{
            //有图片
            if (parrentModel.choiceOptions.count != 0) {
                //选项不为空
                for (int s=0; s<parrentModel.choiceOptions.count; s++) {
                    //
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = parrentModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    CGFloat choice_H = [Maneger getPonentH:parrentModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-160];
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    choiceLabel.frame = CGRectMake(45, parrent_choice_H+parrent_H, Swidth-60, choice_H);
                    parrent_choice_H += choice_H+10;
                    [pageScroll addSubview:choiceLabel];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(20, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(40, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                    //
                    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth-150, parrent_H+5+150, 150, 150)];
                    [imgView sd_setImageWithURL:[NSURL URLWithString:parrentModel.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                    [pageScroll addSubview:imgView];
                }
            }
            
        }
        
        //找出当前父类的子题
        NSMutableArray *tempChildArray = [NSMutableArray new];
        for (int i=0; i<childArray.count; i++) {
            QuestionModel *childModel = childArray[i];
            if (childModel.parrentId == parrentModel.questionId) {
                [tempChildArray addObject:childModel];
            }
        }
        //创建子题
        //当前的角标
        current_H += parrent_choice_H+25;
        for (int s=0; s<tempChildArray.count; s++) {
            QuestionModel *childModel = tempChildArray[s];
            //创建标题
            UILabel *childTitle = [UILabel new];
            childTitle.font = [UIFont systemFontOfSize:18];
            childTitle.numberOfLines = 0;
            childTitle.text = [NSString stringWithFormat:@"(%d). %@",s+1,childModel.questTitle];
            childTitle.textColor = UIColorFromHex(0x6C6C6C);
            CGFloat child_H = [Maneger getPonentH:[NSString stringWithFormat:@"(%d). %@",quesNumber,childModel.questTitle] andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-4];
            childTitle.frame = CGRectMake(2, current_H, Swidth-4, child_H);
            current_H += child_H+20;
            [pageScroll addSubview:childTitle];
            
            
            if ([parrentModel.questionType isEqualToString:@"A4题型"]) {
                for (int s=0; s<childModel.choiceOptions.count; s++) {
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = childModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:childModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-180];
                    choiceLabel.frame = CGRectMake(45, current_H, Swidth-180, choice_H);
                    current_H += choice_H+10;
                    [pageScroll addSubview:choiceLabel];
                    //选项
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(20, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.center = CGPointMake(40, choiceLabel.center.y);
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    [pageScroll addSubview:Alabel];
                }
            }
            
            //创建答案层
            UIView *answerView = [UIView new];
            answerView.backgroundColor = UIColorFromHex(0xD0D0D0);
            //正确选项
            UILabel *correctLab = [UILabel new];
            correctLab.frame = CGRectMake(5, 2, 60, 30);
            correctLab.textColor = UIColorFromHex(0x6C6C6C);
            if (childModel.correctAnswer.count != 0) {
                int intV = [childModel.correctAnswer[0] intValue];
                correctLab.text = [NSString stringWithFormat:@"正确答案:%c",intV+65];
            }else{
                correctLab.text = @"";
            }
            
            correctLab.adjustsFontSizeToFitWidth = YES;
            correctLab.font = [UIFont systemFontOfSize:12];
            [answerView addSubview:correctLab];
            //你的选项
            UILabel *customLab = [UILabel new];
            customLab.frame = CGRectMake(80, 2, Swidth-85, 30);
            if (childModel.responseAnswer.count != 0) {
                int intV = [childModel.responseAnswer[0] intValue];
                customLab.text = [NSString stringWithFormat:@"你的答案:%c",intV+65];
            }else{
                customLab.text = @"你的选择:";
            }
            customLab.textColor = [UIColor redColor];
            customLab.font = [UIFont systemFontOfSize:12];
            [answerView addSubview:customLab];
            //解析
            UILabel *analysLab = [UILabel new];
            analysLab.numberOfLines = 0;
            analysLab.font = [UIFont systemFontOfSize:15];
            
            analysLab.text = [NSString stringWithFormat:@"解析: %@",childModel.questionDes];
            CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",childModel.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:WIDTH-10];
            analysLab.frame = CGRectMake(5, 32, Swidth-10, analys_H);
            analysLab.textColor = UIColorFromHex(0x6C6C6C);
            [answerView addSubview:analysLab];
            answerView.frame = CGRectMake(0, current_H, Swidth, analys_H+32);
            //
            [pageScroll addSubview:answerView];
            current_H += (analys_H+32+20);
        }
        pageScroll.contentSize = CGSizeMake(Swidth, current_H);
        //每个题目的高度
        quesNumber++;
    }
    
}
//未做题目加载
- (void)loadQuestion{
    [MBProgressHUD showHUDAndMessage:@"制作试卷中..." toView:nil];
    [RequestTools RequestWithURL:paperUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        for (int i=0; i<array.count; i++) {
            NSDictionary *dictionary = [array objectAtIndex:i];
            QuestionModel *model = [QuestionModel new];
            model.questionId = [dictionary objectForKey:@"questionId"];
            model.questionType = [dictionary objectForKey:@"questionType"];
            model.questTitle = [dictionary objectForKey:@"questionTitle"];
            model.choiceOptions = [dictionary objectForKey:@"choiceOptions"];
            if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                model.parrentId = [NSNumber numberWithInt:0];
            }else{
                model.parrentId = [dictionary objectForKey:@"parentQuestionId"];
            }
            model.childNum = [dictionary objectForKey:@"childrenQuestionsNum"];
            //正确答案
            NSArray *correctArray = [dictionary objectForKey:@"answersBean"];
            NSMutableArray *tempCorrect = [NSMutableArray new];
            for (int s=0; s<correctArray.count; s++) {
                NSDictionary *dic = correctArray[s];
                [tempCorrect addObject:[dic objectForKey:@"answer"]];
            }
            model.correctAnswer = tempCorrect;
            //解析
            model.questionDes = [dictionary objectForKey:@"analysis"];
            //获取图片
            if (![[dictionary objectForKey:@"questionFile"] isKindOfClass:[NSNull class]]) {
                model.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,[[dictionary objectForKey:@"questionFile"] objectForKey:@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }else{
                model.questionFile = @"";
            }
            
            int childNums = [[dictionary objectForKey:@"childrenQuestionsNum"] intValue];
            //组装
            if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums == 0) {
                //单独选择题
                [singleArray addObject:model];
                
            }else if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums != 0){
                //父题
                [parrenArray addObject:model];
            }else if (![[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]){
                [childArray addObject:model];
            }
        }
        //升序
        [singleArray sortUsingSelector:@selector(compareWithQuestion:)];
        [parrenArray sortUsingSelector:@selector(compareWithQuestion:)];
        
        for (int s=0; s<singleArray.count; s++) {
            [totalArray addObject:singleArray[s]];
        }
        for (int k=0; k<parrenArray.count; k++) {
            [totalArray addObject:parrenArray[k]];
        }
        [MBProgressHUD hideHUDForView:nil];
        [showView removeFromSuperview];
        [self setUI];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
//无答案的UI
- (void)setUI{
    //开始计时
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    //
    self.paperTitle.text = self.paperModel.paperName;
    //设置mainScrollview
    NSInteger scroll_W = parrenArray.count + singleArray.count;
    self.mainScrollview.contentSize = CGSizeMake(Swidth*scroll_W, Sheight-101);
    self.mainScrollview.delegate = self;
    self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
    int s = self.mainScrollview.contentOffset.x/Swidth;
    QuestionModel *model = totalArray[s];
    self.typeQues.text = model.questionType;
    
    NSLog(@"singcout=%ld parrentCount=%lu",(unsigned long)singleArray.count,(unsigned long)parrenArray.count);
    //创建单选题
    for (int i=0; i<singleArray.count; i++) {
        UIScrollView *pageScroll = [UIScrollView new];
        pageScroll.frame = CGRectMake(Swidth * (quesNumber-1) , 0, Swidth, Sheight-101);
        pageScroll.backgroundColor = UIColorFromHex(0xF0F0F0);
        //手势
        [pageScroll addGestureRecognizer:hideTap];
        
        [self.mainScrollview addSubview:pageScroll];
        QuestionModel *quesModel = singleArray[i];
        //设置answerArray
        NSDictionary *answerDictionary = @{@"questionId":quesModel.questionId,@"responseAnswer":@[]};
        [answerArray addObject:answerDictionary];
        
        if (![quesModel.questionFile isEqualToString:@""]) {
            
            UILabel *label = [UILabel new];
            label.text = [NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle];
            label.font = [UIFont systemFontOfSize:20];
            label.numberOfLines = 0;
            label.textColor = UIColorFromHex(0x6C6C6C);
            CGFloat title_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
            label.frame = CGRectMake(0, 0, Swidth, title_H);
            [pageScroll addSubview:label];
            
            ButtonManeger *btnManeger = [ButtonManeger new];
            if ([quesModel.questionType isEqualToString:@"X: 多选题"]) {
                btnManeger.singleAnswerArray = [NSMutableArray new];
            }
            [manegerArray addObject:btnManeger];
            UIImageView *quesImage = [UIImageView new];
            [quesImage sd_setImageWithURL:[NSURL URLWithString:quesModel.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
            quesImage.frame = CGRectMake(Swidth-160, title_H, 150, 150);
            quesImage.backgroundColor = [UIColor blueColor];
            [pageScroll addSubview:quesImage];
            
            CGFloat result_H = 25;
            for (int s=0; s<quesModel.choiceOptions.count; s++) {
                UILabel *choiceLabel = [UILabel new];
                choiceLabel.text = quesModel.choiceOptions[s];
                choiceLabel.font = [UIFont systemFontOfSize:16];
                choiceLabel.numberOfLines = 0;
                choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                CGFloat choice_H = [Maneger getPonentH:quesModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-220];
                
                choiceLabel.frame = CGRectMake(65, title_H+result_H, Swidth-220, choice_H);
                result_H += choice_H+20;
                [pageScroll addSubview:choiceLabel];
                //选项
                ChoiceButton *selectBtn = [ChoiceButton buttonWithType:UIButtonTypeCustom];
                selectBtn.frame = CGRectMake(25, 0, 25, 25);
                selectBtn.layer.cornerRadius = 5;
                selectBtn.layer.masksToBounds = YES;
                [selectBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                [selectBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                if ([quesModel.questionType isEqualToString:@"X: 多选题"]) {
                    [selectBtn addTarget:self action:@selector(Xchoosed:) forControlEvents:UIControlEventTouchUpInside];
                    selectBtn.tag = 200+s;
                    
                }else{
                    [selectBtn addTarget:self action:@selector(choosed:) forControlEvents:UIControlEventTouchUpInside];
                }
                selectBtn.tag = 200+s;
                btnManeger.questionId = quesModel.questionId;
                selectBtn.questionId = quesModel.questionId;
                selectBtn.quesType = quesModel.questionType;
                selectBtn.center = CGPointMake(30, choiceLabel.center.y);
                selectBtn.pageNum = [NSNumber numberWithShort:pageNum];
                [pageScroll addSubview:selectBtn];
                //
                [btnManeger add:selectBtn];
                UILabel *Alabel = [UILabel new];
                Alabel.frame = CGRectMake(20, 0, 20, 20);
                Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                Alabel.textColor = UIColorFromHex(0x6C6C6C);
                Alabel.center = CGPointMake(60, choiceLabel.center.y);
                [pageScroll addSubview:Alabel];
            }
            
            CheckButton *answerbtn = [[CheckButton alloc]initWithFrame:CGRectMake(0, result_H+title_H, 100, 40)];
            answerbtn.center = CGPointMake(Swidth/2, answerbtn.center.y);
            answerbtn.quesModel = quesModel;
            [answerbtn addTarget:self action:@selector(showAnswerView:) forControlEvents:UIControlEventTouchUpInside];
            [answerbtn setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
            answerbtn.layer.cornerRadius = 5;
            answerbtn.layer.masksToBounds = YES;
            answerbtn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
            answerbtn.layer.borderWidth = 1;
            [answerbtn setTitle:@"查看答案" forState:0];
            [pageScroll addSubview:answerbtn];
            
            
        }else{
            UILabel *label = [UILabel new];
            label.text = [NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle];
            label.font = [UIFont systemFontOfSize:20];
            label.numberOfLines = 0;
            CGFloat title_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d. %@",quesNumber,quesModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
            label.textColor = UIColorFromHex(0x6C6C6C);
            label.frame = CGRectMake(0, 0, Swidth, title_H);
            [pageScroll addSubview:label];
            
            ButtonManeger *btnManeger = [ButtonManeger new];
            if ([quesModel.questionType isEqualToString:@"X: 多选题"]) {
                btnManeger.singleAnswerArray = [NSMutableArray new];
            }
            [manegerArray addObject:btnManeger];
            CGFloat result_H = 25;
            if (quesModel.choiceOptions.count != 0) {
                for (int s=0; s<quesModel.choiceOptions.count; s++) {
                    
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = quesModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:quesModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-70];
                    
                    choiceLabel.frame = CGRectMake(65, title_H+result_H, Swidth-70, choice_H);
                    result_H += choice_H+20;
                    [pageScroll addSubview:choiceLabel];
                    //选项
                    ChoiceButton *selectBtn = [ChoiceButton buttonWithType:UIButtonTypeCustom];
                    selectBtn.frame = CGRectMake(5, 0, 25, 25);
                    selectBtn.layer.cornerRadius = 5;
                    selectBtn.layer.masksToBounds = YES;
                    [selectBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [selectBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    if ([quesModel.questionType isEqualToString:@"X: 多选题"]) {
                        [selectBtn addTarget:self action:@selector(Xchoosed:) forControlEvents:UIControlEventTouchUpInside];
                        
                        
                    }else{
                        [selectBtn addTarget:self action:@selector(choosed:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    selectBtn.tag = 200+s;
                    btnManeger.questionId = quesModel.questionId;
                    selectBtn.quesType = quesModel.questionType;
                    selectBtn.questionId = quesModel.questionId;
                    selectBtn.center = CGPointMake(30, choiceLabel.center.y);
                    selectBtn.pageNum = [NSNumber numberWithShort:pageNum];
                    [pageScroll addSubview:selectBtn];
                    //
                    [btnManeger add:selectBtn];
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(20, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(60, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                }
                CheckButton *answerbtn = [[CheckButton alloc]initWithFrame:CGRectMake(0, result_H+title_H, 100, 40)];
                answerbtn.center = CGPointMake(Swidth/2, answerbtn.center.y);
                
                [answerbtn setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
                answerbtn.layer.cornerRadius = 5;
                answerbtn.layer.masksToBounds = YES;
                answerbtn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
                answerbtn.layer.borderWidth = 1;
                
                answerbtn.quesModel = quesModel;
                [answerbtn addTarget:self action:@selector(showAnswerView:) forControlEvents:UIControlEventTouchUpInside];
                [answerbtn setTitle:@"查看答案" forState:0];
                [pageScroll addSubview:answerbtn];
            }
        }
        pageScroll.userInteractionEnabled = YES;
        [pageScroll addGestureRecognizer:hideTap];
        quesNumber++;
    }
    //创建父题
    for (int s=0; s<parrenArray.count; s++) {
        //当前的角标
        CGFloat current_H = 0;
        QuestionModel *parrentModel = parrenArray[s];
        //        self.typeQues.text = parrentModel.questionType;
        //创建每个页面的上下滑动视图
        UIScrollView *pageScroll = [UIScrollView new];
        pageScroll.frame = CGRectMake(Swidth * (quesNumber-1) , 0, Swidth, Sheight-101);
        
        //手势
        [pageScroll addGestureRecognizer:hideTap];
        
        pageScroll.backgroundColor = UIColorFromHex(0xF0F0F0);
        pageScroll.delegate = self;
        [self.mainScrollview addSubview:pageScroll];
        //创建标题
        UILabel *parrentTitle = [UILabel new];
        parrentTitle.font = [UIFont systemFontOfSize:20];
        parrentTitle.numberOfLines = 0;
        parrentTitle.text = [NSString stringWithFormat:@"%d.%@",quesNumber,parrentModel.questTitle];
        parrentTitle.textColor = UIColorFromHex(0x6C6C6C);
        CGFloat parrent_H = [Maneger getPonentH:[NSString stringWithFormat:@"%d.%@",quesNumber,parrentModel.questTitle] andFont:[UIFont systemFontOfSize:20] andWidth:Swidth];
        parrentTitle.frame = CGRectMake(0, 0, Swidth, parrent_H);
        [pageScroll addSubview:parrentTitle];
        //角标增加
        current_H = parrent_H;
        //创建选项
        if ([parrentModel.questionFile isEqualToString:@""]) {
            //无图片
            if (parrentModel.choiceOptions.count != 0) {
                //选项不为空
                for (int s=0; s<parrentModel.choiceOptions.count; s++) {
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = parrentModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:parrentModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-80];
                    
                    choiceLabel.frame = CGRectMake(65,current_H, Swidth-80, choice_H);
                    
                    current_H += (choice_H+20);
                    
                    [pageScroll addSubview:choiceLabel];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(30, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(60, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                }
                
            }
        }
        else{
            //有图片
            if (parrentModel.choiceOptions.count != 0) {
                //选项不为空
                CGFloat height_choice = 25;
                for (int s=0; s<parrentModel.choiceOptions.count; s++) {
                    //
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = parrentModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:parrentModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-180];
                    
                    choiceLabel.frame = CGRectMake(65, height_choice+choice_H, Swidth-180, choice_H);
                    height_choice += (choice_H+20);
                    [pageScroll addSubview:choiceLabel];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(30, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.center = CGPointMake(60, choiceLabel.center.y);
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    [pageScroll addSubview:Alabel];
                    //
                    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth-160, current_H+5+150, 150, 150)];
                    [imgView sd_setImageWithURL:[NSURL URLWithString:parrentModel.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                    [pageScroll addSubview:imgView];
                }
                //判断父类的选项和图片高度
                if (height_choice > 155) {
                    current_H += height_choice;
                }else{
                    current_H += 155;
                }
            }
            
        }
        
        //找出当前父类的子题
        NSMutableArray *tempChildArray = [NSMutableArray new];
        for (int i=0; i<childArray.count; i++) {
            QuestionModel *childModel = childArray[i];
            if (childModel.parrentId == parrentModel.questionId) {
                [tempChildArray addObject:childModel];
            }
        }
        //创建子题
        if (parrentModel.choiceOptions.count != 0) {
            for (int s=0; s<tempChildArray.count; s++) {
                
                QuestionModel *childModel = tempChildArray[s];
                //设置answerArray
                NSDictionary *answerDictionary = @{@"questionId":childModel.questionId,@"responseAnswer":@[]};
                [answerArray addObject:answerDictionary];
                //创建标题
                UILabel *childTitle = [UILabel new];
                childTitle.font = [UIFont systemFontOfSize:18];
                childTitle.numberOfLines = 0;
                childTitle.text = [NSString stringWithFormat:@"(%d). %@",s+1,childModel.questTitle];
                childTitle.textColor = UIColorFromHex(0x6C6C6C);
                CGFloat child_H = [Maneger getPonentH:[NSString stringWithFormat:@"(%d). %@",s+1,childModel.questTitle] andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-4];
                childTitle.frame = CGRectMake(2, current_H, Swidth-4, child_H);
                
                current_H += child_H+20;
                [pageScroll addSubview:childTitle];
                //创建小题答题view
                //创建选项
                ButtonManeger *btnManeger = [ButtonManeger new];
                [manegerParrent addObject:btnManeger];
                for (int s=0; s<parrentModel.choiceOptions.count; s++) {
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = parrentModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:parrentModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-90];
                    
                    choiceLabel.frame = CGRectMake(65,current_H, Swidth-90, choice_H);
                    
                    current_H += (choice_H+20);
                    
                    [pageScroll addSubview:choiceLabel];
                    //
                    //选项
                    ChoiceButton *selectBtn = [ChoiceButton buttonWithType:UIButtonTypeCustom];
                    selectBtn.frame = CGRectMake(10, 0, 25, 25);
                    selectBtn.layer.cornerRadius = 5;
                    selectBtn.layer.masksToBounds = YES;
                    [selectBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [selectBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    [selectBtn addTarget:self action:@selector(moreChoosed:) forControlEvents:UIControlEventTouchUpInside];
                    selectBtn.tag = 400+s;
                    selectBtn.questionId = childModel.questionId;
                    
                    btnManeger.questionId = childModel.questionId;
                    selectBtn.center = CGPointMake(35, choiceLabel.center.y);
                    selectBtn.pageNum = [NSNumber numberWithShort:pageNum];
                    [pageScroll addSubview:selectBtn];
                    //
                    [btnManeger add:selectBtn];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(30, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(60, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                }
                CheckButton *answerbtn = [[CheckButton alloc]initWithFrame:CGRectMake(0, current_H, 100, 40)];
                answerbtn.center = CGPointMake(Swidth/2, answerbtn.center.y);
                answerbtn.quesModel = childModel;
                [answerbtn addTarget:self action:@selector(showAnswerView:) forControlEvents:UIControlEventTouchUpInside];
                [answerbtn setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
                answerbtn.layer.cornerRadius = 5;
                answerbtn.layer.masksToBounds = YES;
                answerbtn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
                answerbtn.layer.borderWidth = 1;
                [answerbtn setTitle:@"查看答案" forState:0];
                [pageScroll addSubview:answerbtn];
                current_H+=40;
            }
        }else{//父类无选项
            for (int s=0; s<tempChildArray.count; s++) {
                
                QuestionModel *childModel = tempChildArray[s];
                //设置answerArray
                NSDictionary *answerDictionary = @{@"questionId":childModel.questionId,@"responseAnswer":@[]};
                [answerArray addObject:answerDictionary];
                //创建标题
                UILabel *childTitle = [UILabel new];
                childTitle.font = [UIFont systemFontOfSize:18];
                childTitle.numberOfLines = 0;
                childTitle.textColor = UIColorFromHex(0x6C6C6C);
                childTitle.text = [NSString stringWithFormat:@"(%d). %@",s+1,childModel.questTitle];
                CGFloat child_H = [Maneger getPonentH:[NSString stringWithFormat:@"(%d). %@",s+1,childModel.questTitle] andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-4];
                childTitle.frame = CGRectMake(2, current_H, Swidth-4, child_H);
                
                current_H += child_H+20;
                [pageScroll addSubview:childTitle];
                //创建小题答题view
                //创建选项
                ButtonManeger *btnManeger = [ButtonManeger new];
                [manegerParrent addObject:btnManeger];
                for (int s=0; s<childModel.choiceOptions.count; s++) {
                    UILabel *choiceLabel = [UILabel new];
                    choiceLabel.text = childModel.choiceOptions[s];
                    choiceLabel.font = [UIFont systemFontOfSize:16];
                    choiceLabel.numberOfLines = 0;
                    choiceLabel.textColor = UIColorFromHex(0x6C6C6C);
                    CGFloat choice_H = [Maneger getPonentH:childModel.choiceOptions[s] andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-90];
                    
                    choiceLabel.frame = CGRectMake(65,current_H, Swidth-90, choice_H);
                    
                    current_H += (choice_H+20);
                    
                    [pageScroll addSubview:choiceLabel];
                    //
                    //选项
                    ChoiceButton *selectBtn = [ChoiceButton buttonWithType:UIButtonTypeCustom];
                    selectBtn.frame = CGRectMake(10, 0, 25, 25);
                    selectBtn.layer.cornerRadius = 5;
                    selectBtn.layer.masksToBounds = YES;
                    [selectBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [selectBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    [selectBtn addTarget:self action:@selector(moreChoosed:) forControlEvents:UIControlEventTouchUpInside];
                    selectBtn.tag = 400+s;
                    selectBtn.questionId = childModel.questionId;
                    btnManeger.questionId = childModel.questionId;
                    selectBtn.center = CGPointMake(35, choiceLabel.center.y);
                    selectBtn.pageNum = [NSNumber numberWithShort:pageNum];
                    [pageScroll addSubview:selectBtn];
                    //
                    [btnManeger add:selectBtn];
                    //
                    UILabel *Alabel = [UILabel new];
                    Alabel.frame = CGRectMake(30, 0, 20, 20);
                    Alabel.text = [NSString stringWithFormat:@"%c",s+65];
                    Alabel.textColor = UIColorFromHex(0x6C6C6C);
                    Alabel.center = CGPointMake(60, choiceLabel.center.y);
                    [pageScroll addSubview:Alabel];
                }
                CheckButton *answerbtn = [[CheckButton alloc]initWithFrame:CGRectMake(0, current_H, 100, 40)];
                answerbtn.center = CGPointMake(Swidth/2, answerbtn.center.y);
                answerbtn.quesModel = childModel;
                [answerbtn addTarget:self action:@selector(showAnswerView:) forControlEvents:UIControlEventTouchUpInside];
                [answerbtn setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
                answerbtn.layer.cornerRadius = 5;
                answerbtn.layer.masksToBounds = YES;
                answerbtn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
                answerbtn.layer.borderWidth = 1;
                [answerbtn setTitle:@"查看答案" forState:0];
                [pageScroll addSubview:answerbtn];
                current_H +=40;
            }
        }
        
        //设置上下滑动距离
        [pageScroll addGestureRecognizer:hideTap];
        pageScroll.contentSize = CGSizeMake(Swidth, current_H);
        quesNumber++;
    }
}
#pragma -action
- (void)timerAction{
    seconds++;
    if (seconds == 60) {
        seconds = 0;
        minutes ++;
    }
    if (seconds==60 && minutes == 60) {
        seconds = 0;
        minutes = 0;
    }
    self.timerLabel.adjustsFontSizeToFitWidth = YES;
    self.timerLabel.text = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
}
- (void)leftScroll:(UITapGestureRecognizer *)btn{
    if (self.mainScrollview.contentOffset.x == 0) {
        [MBProgressHUD showToastAndMessage:@"已经是第一页了～" places:0 toView:nil];
        return;
    }
    //
    //    NSInteger scroll_W = parrenArray.count + singleArray.count;
    //    self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScrollview.contentOffset = CGPointMake(self.mainScrollview.contentOffset.x-WIDTH, self.mainScrollview.contentOffset.y);
    } completion:^(BOOL finished) {
        int s = self.mainScrollview.contentOffset.x/Swidth;
        QuestionModel *model = totalArray[s];
        self.typeQues.text = model.questionType;
        
        NSInteger scroll_W = parrenArray.count + singleArray.count;
        self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
    }];
}
- (void)rightScroll:(UITapGestureRecognizer *)btn{
    if (self.mainScrollview.contentOffset.x == self.mainScrollview.contentSize.width-WIDTH) {
        [MBProgressHUD showToastAndMessage:@"已经是最后一页了～" places:0 toView:nil];
        return;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScrollview.contentOffset = CGPointMake(self.mainScrollview.contentOffset.x+WIDTH, self.mainScrollview.contentOffset.y);
    } completion:^(BOOL finished) {
        int s = self.mainScrollview.contentOffset.x/Swidth;
        QuestionModel *model = totalArray[s];
        self.typeQues.text = model.questionType;
        
        NSInteger scroll_W = parrenArray.count + singleArray.count;
        self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
    }];
    
}
- (void)moreChoosed:(ChoiceButton *)btn{
    btn.selected = !btn.selected;
    ButtonManeger *manager;
    for (int i=0; i<manegerParrent.count; i++) {
        ButtonManeger *maneger = manegerParrent[i];
        if (maneger.questionId == btn.questionId) {
            manager = maneger;
        }
    }
    NSDictionary *dic = @{@"":btn.questionId,@"":@[@"",@""]};
    
    //更换答案
    if (btn.selected) {
        [manager changeStatus:btn.tag-400];
        for (int s=0; s<answerArray.count; s++) {
            NSDictionary *dictionary = answerArray[s];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *submitDic = @{@"questionId":btn.questionId,@"responseAnswer":@[[NSString stringWithFormat:@"%ld",(long)btn.tag-400]]};
                [answerArray replaceObjectAtIndex:s withObject:submitDic];
            }
        }
    }else{
        for (int s=0; s<answerArray.count; s++) {
            NSDictionary *dictionary = answerArray[s];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *submitDic = @{@"questionId":btn.questionId,@"responseAnswer":@[]};
                [answerArray replaceObjectAtIndex:s withObject:submitDic];
            }
        }
        
    }
}
- (void)Xchoosed:(ChoiceButton *)btn{
    btn.selected = !btn.selected;
    ButtonManeger *manager;
    for (int i=0; i<manegerArray.count; i++) {
        ButtonManeger *maneger = manegerArray[i];
        if (maneger.questionId == btn.questionId) {
            manager = maneger;
        }
    }
    //
    NSMutableArray *array = manager.singleAnswerArray;
    if (btn.selected) {
        
        [array addObject:[NSString stringWithFormat:@"%ld",btn.tag-200]];
        for (int s=0; s<answerArray.count; s++) {
            NSDictionary *dictionary = answerArray[s];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *submitDic = @{@"questionId":btn.questionId,@"responseAnswer":array};
                [answerArray replaceObjectAtIndex:s withObject:submitDic];
            }
        }
    }else{
        for (int s=0; s<array.count; s++) {
            if ([array[s] integerValue] == btn.tag-200) {
                [array removeObjectAtIndex:s];
                break;
            }
        }
        for (int i=0; i<answerArray.count; i++) {
            NSDictionary *dictionary = answerArray[i];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *answerDictionary = @{@"questionId":btn.questionId,@"responseAnswer":array};
                [answerArray replaceObjectAtIndex:i withObject:answerDictionary];
            }
        }
    }
}
- (void)choosed:(ChoiceButton *)btn{
    btn.selected = !btn.selected;
    ButtonManeger *manager;
    for (int i=0; i<manegerArray.count; i++) {
        ButtonManeger *maneger = manegerArray[i];
        if (maneger.questionId == btn.questionId) {
            manager = maneger;
        }
    }
    //更换答案
    if (btn.selected) {
        [manager changeStatus:btn.tag-200];
        for (int s=0; s<answerArray.count; s++) {
            NSDictionary *dictionary = answerArray[s];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *submitDic = @{@"questionId":btn.questionId,@"responseAnswer":@[[NSString stringWithFormat:@"%ld",(long)btn.tag-200]]};
                [answerArray replaceObjectAtIndex:s withObject:submitDic];
            }
        }
        //继续滚动
        [UIView animateWithDuration:0.7 animations:^{
            if (self.mainScrollview.contentOffset.x != self.mainScrollview.contentSize.width-WIDTH) {
                self.mainScrollview.contentOffset = CGPointMake(self.mainScrollview.contentOffset.x+WIDTH, self.mainScrollview.contentOffset.y);
                //
                NSInteger scroll_W = parrenArray.count + singleArray.count;
                self.indexQues.text = [NSString stringWithFormat:@"%.0f/%ld",self.mainScrollview.contentOffset.x/Swidth+1,(long)scroll_W];
            }
        }];
        //隐藏
        [self hideAnswerView];
        int s = self.mainScrollview.contentOffset.x/Swidth;
        QuestionModel *model = totalArray[s];
        self.typeQues.text = model.questionType;
        
        self.indexQues.text = [NSString stringWithFormat:@"%.0f/%.0f",self.mainScrollview.contentOffset.x/Swidth+1,self.mainScrollview.contentSize.width/Swidth];
    }else{
        for (int s=0; s<answerArray.count; s++) {
            NSDictionary *dictionary = answerArray[s];
            if (btn.questionId == dictionary[@"questionId"]) {
                NSDictionary *answerDictionary = @{@"questionId":btn.questionId,@"responseAnswer":@[]};
                [answerArray replaceObjectAtIndex:s withObject:answerDictionary];
            }
        }
    }
}
- (IBAction)submitAction:(id)sender {
}

- (IBAction)backAction:(id)sender {
    if (isDo == 0) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"未完成，是否退出?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *canCelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertVC addAction:canCelAction];
        [alertVC addAction:sureAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)selectAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        questionView = [UIScrollView new];
        questionView.frame = CGRectMake(0, 64,WIDTH, HEIGHT-64);
        questionView.backgroundColor = [UIColor lightGrayColor];
        questionView.alpha = 0.9;
        NSLog(@"answerArray=%lu",(unsigned long)answerArray.count);
        long tags = singleArray.count+parrenArray.count;
        for (int i=0; i<tags; i++) {
            int j = i/4;
            UIView *view = [UIView new];
            CGPoint point;
            view.frame = CGRectMake(WIDTH/4*(i%4), WIDTH/4*j, WIDTH/4, WIDTH/4);
            if (i==0 && j==0) {
                point = view.center;
            }
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(0, 0, 30, 30);
            button.center = point;
            button.tag = 300+j*4+(i%4)+1;
            [button setBackgroundImage:[UIImage imageNamed:@"weizuo"] forState:UIControlStateNormal];
            [button setTitle:[NSString stringWithFormat:@"%d",j*4+(i%4)+1] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            //
            [button addTarget:self action:@selector(moveTo:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:button];
            [questionView addSubview:view];
        }
        if (tags%4 == 0){
            questionView.contentSize = CGSizeMake(Swidth, tags/4*(Swidth/4));
        }else{
            questionView.contentSize = CGSizeMake(Swidth, (tags/4+1)*(Swidth/4));
        }
        [self.view addSubview:questionView];
        return;
    }
    [questionView removeFromSuperview];
}
- (void)moveTo:(UIButton *)btn{
    int tag = [btn.titleLabel.text intValue];
    [questionView removeFromSuperview];
    NSInteger scroll_W = parrenArray.count + singleArray.count;
    self.indexQues.text = [NSString stringWithFormat:@"%d/%ld",tag,(long)scroll_W];
    [UIView animateWithDuration:0.6 animations:^{
        self.mainScrollview.contentOffset = CGPointMake((tag-1)*Swidth, 0);
    }];
}
//显示答案
- (void)showAnswerView:(CheckButton *)btn{
    QuestionModel *quesModel = btn.quesModel;
    //正确答案
    if (quesModel.correctAnswer.count !=0) {
        NSMutableString *str = [NSMutableString new];
        for (int u=0; u<quesModel.correctAnswer.count; u++) {
            [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[quesModel.correctAnswer[u] intValue]+65]]];
        }
        correctLabel.text = [NSString stringWithFormat:@"正确答案:%@",str];
    }else{
        correctLabel.text = @"正确答案:暂无";
    }
    //解析
    CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析:%@",quesModel.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth];
    answerLabel.text = [NSString stringWithFormat:@"解析:%@",quesModel.questionDes];
    answerLabel.frame = CGRectMake(0, 32, Swidth, analys_H);
    answerViews.frame = CGRectMake(0, Sheight, Swidth, 32+analys_H);
    //
    [UIView animateWithDuration:0.5 animations:^{
        answerViews.frame = CGRectMake(0, Sheight-32-analys_H, Swidth, 32+analys_H);
    }];
}
- (void)hideAnswerView{
    [UIView animateWithDuration:0.5 animations:^{
        answerViews.frame = CGRectMake(0, Sheight, Swidth, 32);
    }];
}
//交卷
- (void)submitPage{
    [[[UIAlertView alloc]initWithTitle:@"提示" message:@"系统会自动为你交卷,确定交卷?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] show];
}
#pragma -delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self hideAnswerView];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == _mainScrollview) {
        int s = self.mainScrollview.contentOffset.x/Swidth;
        QuestionModel *model = totalArray[s];
        self.typeQues.text = model.questionType;
        
        self.indexQues.text = [NSString stringWithFormat:@"%.0f/%.0f",self.mainScrollview.contentOffset.x/Swidth+1,scrollView.contentSize.width/Swidth];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        
        if ([self.fromVC isEqualToString:@"error"]) {
            //answer
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentSelfTestRecords/%@/submitQuestionResponses",LocalIP,_recordId];
            [MBProgressHUD showHUDAndMessage:@"交卷中。。。" toView:nil];
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:answerArray Success:^(NSDictionary *result) {
                [self.navigationController popViewControllerAnimated:YES];
            } failed:^(NSString *result) {
                
            }];
            
        }else{
            //answer
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentSelfTestRecords/%@/submitQuestionResponses",LocalIP,self.paperModel.recordID];
            [MBProgressHUD showHUDAndMessage:@"交卷中。。。" toView:nil];
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:answerArray Success:^(NSDictionary *result) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } failed:^(NSString *result) {
                
            }];
            
        }
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hideAnswerView];
}
#pragma -协议
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.frame.size.height<scrollView.contentSize.height) {
        
    }
}
- (void)dropdownMenu:(LMJDropdownMenu *)menu selectedCellNumber:(NSInteger)number{
    
}
@end
