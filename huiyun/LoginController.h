//
//  LoginViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/29.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LoadingView.h"
#import "ScanIpController.h"
#import "RequestTools.h"
#import "AccountEntity.h"
#import "PersonEntity.h"
#import "TMainController.h"
#import "TIMController.h"
#import "MainViewController.h"
#import "IMController.h"
#import "SPKitExample.h"

@interface LoginController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) UITabBarController *tabarVC;
@property (strong, nonatomic) UITabBarController *ttabarVC;

@property (strong, nonatomic) MainViewController *mainVC;
@property (strong, nonatomic) IMController *imVC;

@property (strong, nonatomic) TMainController *tMainVC;
@property (strong, nonatomic) TIMController *timVC;

@property (strong, nonatomic) NSString *from;
+ (id)share;
@end

