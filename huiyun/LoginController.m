//
//  LoginViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/29.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "LoginController.h"

#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define LOGIN_TAG 301
#define JIZHU_TAG 302
@interface LoginController ()
{
    UITextField *nameField;
    UITextField *pwdField;
    UILabel *hospitalLabel;
    //
    float keyBoardHeight;
    UITextField *selectField;
    CGRect originRect;
    CGRect nameOrigin;
    CGRect pwdOrigin;
    //
    AccountEntity *accountEntity;
}
@end
@implementation LoginController
+ (id)share{
    static LoginController *login = nil;
    if (login == nil) {
        login = [LoginController new];
    }
    return login;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    //重新设置布局
    nameField.frame = nameOrigin;
    pwdField.frame = pwdOrigin;
    //给nameField，pwdField赋值
    PersonEntity *entity = [[NSuserDefaultManager share] readLastAccount];
    
    if ([self.from isEqualToString:@"delegate"]) {
        if (![entity isKindOfClass:[NSNull class]]) {
            nameField.text = entity.userAccount;
            pwdField.text = entity.userPassword;
            if (![nameField.text isEqualToString:@""] && ![pwdField.text isEqualToString:@""]) {
                //自动登录
                [self userLogin];
            }
        }
    }else if ([self.from isEqualToString:@"exit"]){
        nameField.text = @"";
        pwdField.text = @"";
    }
    HospitalEntity *hosEntity = [[NSuserDefaultManager share] readHospital];
    if ([hosEntity.hosName isEqualToString:@""]||[hosEntity.hosName isEqualToString:@"医院"]) {
        hospitalLabel.text = @"中威慧云云端";
    }else{
        hospitalLabel.text = hosEntity.hosName;
    }
    UIButton *selectBtn = [self.view viewWithTag:JIZHU_TAG];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectInteger =[[defaults objectForKey:@"witwin_select"] integerValue];
    selectBtn.selected = selectInteger;
    
    self.tabarVC.selectedIndex = 0;
    self.ttabarVC.selectedIndex = 0;
}

- (void)createUI{
    self.view.backgroundColor = [UIColor whiteColor];
    //
    UIImageView *backView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    backView.image = [UIImage imageNamed:@"mainback"];
    backView.userInteractionEnabled = YES;
    self.view = backView;
    //
    nameField = [[UITextField alloc]initWithFrame:CGRectMake(WIDTH/10, HEIGHT/1.7, WIDTH/5*4, 50)];
    
    nameOrigin = nameField.frame;
    hospitalLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 130, Swidth, 40)];
    hospitalLabel.textAlignment = 1;
    [self.view addSubview:hospitalLabel];
    
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    nameField.placeholder = @"请输入用户名";
    nameField.textAlignment = 1;
    UIImageView *nameImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    nameImage.image = [UIImage imageNamed:@"user"];
    nameField.leftViewMode=UITextFieldViewModeAlways;
    nameField.leftView = nameImage;
    nameField.delegate = self;
    nameField.returnKeyType = UIReturnKeyDone;
    nameField.background = [UIImage imageNamed:@"field"];
    //
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 24, 24)];
    UIImageView *nameRView = [[UIImageView alloc]initWithFrame:CGRectMake(6, 6, 12, 12)];
    nameRView.image = [UIImage imageNamed:@"叉叉"];
    nameRView.userInteractionEnabled = YES;
    [view addSubview:nameRView];
    
    UITapGestureRecognizer *hideTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide:)];
    hideTap.numberOfTapsRequired = 1;
    [nameRView addGestureRecognizer:hideTap];
    nameField.rightView = view;
    nameField.rightViewMode = UITextFieldViewModeAlways;
    
    [self.view addSubview:nameField];
    pwdField = [[UITextField alloc]initWithFrame:CGRectMake(WIDTH/10, HEIGHT/1.7+60, WIDTH/5*4, 50)];
    
    pwdOrigin = pwdField.frame;
    
    pwdField.borderStyle = UITextBorderStyleRoundedRect;
    pwdField.placeholder = @"请输入密码";
    pwdField.secureTextEntry = YES;
    pwdField.textAlignment = 1;
    pwdField.returnKeyType = UIReturnKeyDone;
    UIImageView *secuView = [UIImageView new];
    secuView.image = [UIImage imageNamed:@"眼睛"];
    secuView.frame = CGRectMake(0, 0, 24, 12);
    secuView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showHide:)];
    tap.numberOfTapsRequired = 1;
    [secuView addGestureRecognizer:tap];
    pwdField.rightView = secuView;
    pwdField.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *pwdImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    pwdImage.image = [UIImage imageNamed:@"pwd_img"];
    pwdField.leftView = pwdImage;
    pwdField.delegate = self;
    pwdField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:pwdField];
    
    UIButton *jizhuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    jizhuBtn.frame = CGRectMake(WIDTH/10+WIDTH/5*4-90, pwdField.frame.origin.y+pwdField.frame.size.height+5, 20, 20);
    jizhuBtn.tag = JIZHU_TAG;
    [jizhuBtn addTarget:self action:@selector(jizhu:) forControlEvents:UIControlEventTouchUpInside];
    [jizhuBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
    [jizhuBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
    [self.view addSubview:jizhuBtn];
    
    UILabel *jizhuLab = [[UILabel alloc]initWithFrame:CGRectMake(WIDTH/10+WIDTH/5*4-70, pwdField.frame.origin.y+pwdField.frame.size.height+5, 70, 20)];
    jizhuLab.text = @"记住密码";
    jizhuLab.adjustsFontSizeToFitWidth = YES;
    jizhuLab.font = [UIFont systemFontOfSize:12];
    jizhuLab.textColor = [UIColor lightGrayColor];
    [self.view addSubview:jizhuLab];
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(WIDTH/10, HEIGHT/1.7+140, WIDTH/5*4, 50);
    loginBtn.backgroundColor = UIColorFromHex(0x46bec8);
    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    loginBtn.tag = LOGIN_TAG;
    loginBtn.layer.cornerRadius = 10;
    [loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    //忘记密码
    UIButton *fogetPwd = [UIButton buttonWithType:UIButtonTypeCustom];
    fogetPwd.frame = CGRectMake(WIDTH/10, HEIGHT/1.7+190, 60, 30);
    [fogetPwd setTitle:@"忘记密码?" forState:UIControlStateNormal];
    fogetPwd.titleLabel.adjustsFontSizeToFitWidth = YES;
    [fogetPwd addTarget:self action:@selector(fogetAction:) forControlEvents:UIControlEventTouchUpInside];
    fogetPwd.titleLabel.font = [UIFont systemFontOfSize:14];
    [fogetPwd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [fogetPwd setTitleColor:UIColorFromHex(0x46bec8) forState:UIControlStateNormal];
    [self.view addSubview:fogetPwd];
    //设置IP
    UIButton *setIpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    setIpBtn.frame = CGRectMake(WIDTH-WIDTH/10-60, HEIGHT/1.7+190, 60, 30);
    [setIpBtn setTitle:@"设置地址" forState:UIControlStateNormal];
    setIpBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [setIpBtn setTitleColor:UIColorFromHex(0x46bec8) forState:UIControlStateNormal];
    [setIpBtn addTarget:self action:@selector(ipAction:) forControlEvents:UIControlEventTouchUpInside];
    setIpBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:setIpBtn];
    //键盘出现
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
    //键盘隐藏
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
//1
- (void)userLogin{
    [MBProgressHUD showHUDAndMessage:@"登录中" toView:nil];
    NSDictionary *pamas = @{@"userName":nameField.text,@"userPwd":pwdField.text,@"organizationCode":LocalCODE};
    [RequestTools autoLogin:pamas Success:^(NSDictionary *responseDic) {
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            PersonEntity *entity = [PersonEntity new];
            entity.userAccount = nameField.text;
            entity.userPassword = pwdField.text;
            entity.userID = [[responseDic objectForKey:@"responseBody"] objectForKey:@"userId"];
            entity.userName = [[responseDic objectForKey:@"responseBody"] objectForKey:@"userName"];
            entity.userFullName = [[responseDic objectForKey:@"responseBody"] objectForKey:@"fullName"];
            [[NSuserDefaultManager share] saveCurrentUser:entity];
            //保存userId
            [[NSUserDefaults standardUserDefaults] setObject:entity.userID forKey:@"LocalId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //查询登录IM
            [self searchAndLoginIM:entity.userID];
        }else {
            [MBProgressHUD showToastAndMessage:@"用户名或密码错误~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"登录请求错误~" places:0 toView:nil];
    }];
}
//2
- (void)searchAndLoginIM:(NSNumber *)userId{
    NSString *url = [NSString stringWithFormat:@"%@/instantMessaging/login/%@",LocalIP,userId];
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSString *imName = [result[@"responseBody"][@"userName"] isKindOfClass:[NSNull class]]?@"":result[@"responseBody"][@"userName"];
        NSString *imPass = [result[@"responseBody"][@"password"] isKindOfClass:[NSNull class]]?@"":result[@"responseBody"][@"password"];
        //登录IM
        [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:imName passWord:imPass preloginedBlock:nil successBlock:^{
            [[NSUserDefaults standardUserDefaults] setObject:@"succeed" forKey:@"witwin_im"];
        } failedBlock:^(NSError *aError) {
            [[NSUserDefaults standardUserDefaults] setObject:@"not_succeed" forKey:@"witwin_im"];
        }];
        //查询角色
        [self selectRoles:userId];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"获取阿里账号失败~" places:0 toView:nil];
    }];
}
//3
- (void)selectRoles:(NSNumber *)userId{
    NSString *url = [NSString stringWithFormat:@"%@/users/%@/byApp",LocalIP,userId];
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        int roleId = [result[@"responseBody"][@"roles"][0][@"roleId"] intValue];
        NSString *fileUrl = [result[@"responseBody"][@"picture"] isKindOfClass:[NSNull class]]?@"":[NSString stringWithFormat:@"%@?CTTS-Token=%@",result[@"responseBody"][@"picture"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
        PersonEntity *entity =  [[NSuserDefaultManager share] readCurrentUser];
        entity.roleId = [NSNumber numberWithShort:roleId];
        entity.fileUrl = fileUrl;
        [[NSuserDefaultManager share] saveCurrentUser:entity];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //记住账号
        UIButton *btn = [self.view viewWithTag:JIZHU_TAG];
        if (btn.selected == 1) {
            [[NSuserDefaultManager share] saveLastAccount:entity];
        }
        //
        if (roleId == 1) {
            [MBProgressHUD showToastAndMessage:@"管理员不能登录~" places:0 toView:nil];
        }else if (roleId == 2){
            self.tMainVC = [TMainController shareObject];
            self.tMainVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"事务" image:[UIImage imageNamed:@"tabbar_button_binders_normal"] tag:0];
            self.timVC = [TIMController new];
            self.timVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"讨论圈" image:[UIImage imageNamed:@"tabbar_button_updates_normal_ipad"] tag:0];
            self.ttabarVC = [UITabBarController new];
            self.ttabarVC.tabBar.tintColor = UIColorFromHex(0x20B2AA);
            self.ttabarVC.viewControllers = @[self.tMainVC,self.timVC];
            self.ttabarVC.selectedIndex = 0;
            //tabbar字体
            NSMutableDictionary *attrnor = [NSMutableDictionary dictionary];
            attrnor[NSFontAttributeName] = [UIFont systemFontOfSize:15];
            [self.tabBarItem setTitleTextAttributes:attrnor forState:0];
            [self.navigationController pushViewController:self.ttabarVC animated:YES];
        }else{
            self.mainVC = [MainViewController shareObject];
            self.mainVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"事务" image:[UIImage imageNamed:@"tabbar_button_binders_normal"] tag:0];
            self.imVC = [IMController new];
            self.imVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"讨论圈" image:[UIImage imageNamed:@"tabbar_button_updates_normal_ipad"] tag:0];
            
            self.tabarVC = [UITabBarController new];
            self.tabarVC.viewControllers = @[self.mainVC,self.imVC];
            self.tabarVC.selectedIndex = 0;
            self.tabarVC.tabBar.tintColor = UIColorFromHex(0x20B2AA);
            //tabbar字体
            NSMutableDictionary *attrnor = [NSMutableDictionary dictionary];
            attrnor[NSFontAttributeName] = [UIFont systemFontOfSize:15];
            [self.tabBarItem setTitleTextAttributes:attrnor forState:0];
            [self.navigationController pushViewController:self.tabarVC animated:YES];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"角色查询错误~" places:0 toView:nil];
    }];
}
#pragma -action
#pragma -action
- (void)loginAction:(UIButton *)button{
    if ([nameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入用户名!" places:0 toView:nil];
        return;
    }
    if ([pwdField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入密码!" places:0 toView:nil];
        return;
    }
    [self userLogin];
}
- (void)showHide:(UITapGestureRecognizer *)tap{
    pwdField.secureTextEntry = !pwdField.secureTextEntry;
}
- (void)hide:(UITapGestureRecognizer *)tap{
    nameField.text = @"";
    pwdField.text = @"";
}
- (void)fogetAction:(UIButton *)btn{
    [MBProgressHUD showToastAndMessage:@"请联系管理员~" places:0 toView:nil];
}
- (void)ipAction:(UIButton *)button{
    if ([HardTools captureEnable]) {
        ScanController *scanVC = [ScanController new];
        scanVC.btnIndex = [NSNumber numberWithLong:200];
        [self.navigationController pushViewController:scanVC animated:YES];
    }else{
        [MBProgressHUD showToastAndMessage:@"请查看相机是否打开~" places:0 toView:nil];
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [nameField resignFirstResponder];
    [pwdField resignFirstResponder];
}
#pragma -键盘监听
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    keyBoardHeight = keyboardRect.size.height;
    if (nameField == selectField) {
        if (HEIGHT - keyBoardHeight < nameField.frame.origin.y+64) {
            [UIView animateWithDuration:0.5 animations:^{
                nameField.frame = CGRectMake(nameField.frame.origin.x, HEIGHT-keyBoardHeight-64, nameField.frame.size.width, nameField.frame.size.height);
            }];
        }
    }else{
        if (HEIGHT - keyBoardHeight <pwdField.frame.origin.y+64) {
            [UIView animateWithDuration:0.5 animations:^{
                pwdField.frame = CGRectMake(pwdField.frame.origin.x, HEIGHT-keyBoardHeight-64, pwdField.frame.size.width, nameField.frame.size.height);
            }];
        }
    }
}

//当键盘退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification{
    [UIView animateWithDuration:0.5 animations:^{
        selectField.frame = originRect;
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma -协议
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    selectField = textField;
    originRect = selectField.frame;
    pwdField.text = @"";
}
#pragma -键盘done
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        selectField.frame = originRect;
    }];
    [nameField resignFirstResponder];
    [pwdField resignFirstResponder];
    return YES;
}
- (void)jizhu:(UIButton *)btn{
    btn.selected = !btn.selected;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%d",btn.selected] forKey:@"witwin_select"];
    [defaults synchronize];
}
@end

