//
//  ExamController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TExamController.h"
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface TExamController ()
{
    UIView *mohuView;
    //
    NSMutableArray *osceArray;
    NSMutableArray *onlineArray;
    NSMutableArray *skillArray;
    NSMutableArray *tempArray;
    BOOL isSearch;
    
    UITableView *modelTable;
    UISearchBar *searchBar;
    NSInteger currentIndex;
    //
    UIImageView *segImage;
    UIImageView *leftImage;
    UILabel *leftLabel;
    UIView *leftLine;
    UIButton *leftS;
    UIImageView *rightImage;
    UILabel *rightLabel;
    UIView *rightLine;
    UIButton *rightS;
    UIImageView *skillImage;
    UILabel *skillLabel;
    UIView *skillLine;
    UIButton *skillS;
    UIButton *previousBtn;
    //
    int currentPage;
    UIButton *currentBtn;
    //
    SearchUI *searchUI;
    int isFirst;
}
@end

@implementation TExamController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    currentIndex = 0;
    osceArray = [NSMutableArray new];
    onlineArray = [NSMutableArray new];
    skillArray = [NSMutableArray new];
    tempArray = [NSMutableArray new];
    isSearch = NO;
    currentPage = 1;
    //第一次进
    isFirst = 0;
    
    [self setUI];
}
#pragma -action
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (isFirst == 0) {
        
        [self setUpRefresh];
        [modelTable headerBeginRefreshing];
        isFirst = 1;
    }
}
- (void)setUI{
    UIImageView *navigationView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    navigationView.userInteractionEnabled = YES;
    [self.view addSubview:navigationView];
    UIButton   *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    UIButton  *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(timeSelect:) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 40)];
    titleLab.adjustsFontSizeToFitWidth = YES;
    titleLab.center = CGPointMake(navigationView.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.text = @"考试安排";
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    [navigationView addSubview:titleLab];
    //
    segImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 50)];
    segImage.image = [UIImage imageNamed:@"OSCE_unchecked"];
    segImage.userInteractionEnabled = YES;
    [self.view addSubview:segImage];
    
    leftS = [UIButton buttonWithType:UIButtonTypeCustom];
    leftS.frame = CGRectMake(0, 0, Swidth/3, 50);
    previousBtn = leftS;
    [segImage addSubview:leftS];
    //
    rightS = [UIButton buttonWithType:UIButtonTypeCustom];
    rightS.frame = CGRectMake(Swidth/3, 0, Swidth/3, 50);
    [segImage addSubview:rightS];
    //
    leftImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 15, 20, 20)];
    leftImage.image = [UIImage imageNamed:@"osceimgchecked"];
    leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 15, 80, 20)];
    leftLabel.text = @"OSCE考试";
    leftLabel.font = [UIFont systemFontOfSize:15];
    leftLabel.textColor = UIColorFromHex(0x46bec8);
    leftLabel.adjustsFontSizeToFitWidth = YES;
    leftLine = [[UIView alloc]initWithFrame:CGRectMake(0, 49, Swidth/3, 1)];
    leftLine.backgroundColor = UIColorFromHex(0x46bec8);
    //
    [leftS addSubview:leftImage];
    [leftS addSubview:leftLabel];
    [leftS addSubview:leftLine];
    //
    rightImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 15, 20, 20)];
    rightImage.image = [UIImage imageNamed:@"online_unckeked"];
    rightLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 15, 80, 20)];
    rightLabel.font = [UIFont systemFontOfSize:15];
    rightLabel.adjustsFontSizeToFitWidth = YES;
    rightLabel.text = @"在线考试";
    rightLabel.textColor = [UIColor blackColor];
    rightLine = [[UIView alloc]initWithFrame:CGRectMake(0, 49, Swidth/3, 1)];
    rightLine.backgroundColor = [UIColor whiteColor];
    [rightS addSubview:rightImage];
    [rightS addSubview:rightLabel];
    [rightS addSubview:rightLine];
    //技能考试模块
    skillS = [UIButton buttonWithType:UIButtonTypeCustom];
    skillS.frame = CGRectMake(Swidth/3*2, 0, Swidth/3, 50);
    //    skillS.backgroundColor = [UIColor orangeColor];
    [segImage addSubview:skillS];
    skillImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 15, 20, 20)];
    skillImage.image = [UIImage imageNamed:@"online_unckeked"];
    skillLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 15, 80, 20)];
    skillLabel.font = [UIFont systemFontOfSize:15];
    skillLabel.adjustsFontSizeToFitWidth = YES;
    skillLabel.text = @"技能考试";
    skillLine = [[UIView alloc]initWithFrame:CGRectMake(0, 49, Swidth/3, 1)];
    skillLine.backgroundColor = [UIColor whiteColor];
    [skillS addSubview:skillImage];
    [skillS addSubview:skillLabel];
    [skillS addSubview:skillLine];
    //添加搜索触发事件
    leftS.tag = 200;
    [leftS addTarget:self action:@selector(findAction:) forControlEvents:UIControlEventTouchUpInside];
    rightS.tag = 201;
    [rightS addTarget:self action:@selector(findAction:) forControlEvents:UIControlEventTouchUpInside];
    skillS.tag = 202;
    [skillS addTarget:self action:@selector(findAction:) forControlEvents:UIControlEventTouchUpInside];
    //添加search
    searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, Swidth, 44)];
    searchBar.placeholder = @"请输入考试名称";
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    //添加table
    modelTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 114, Swidth, Sheight-114) style:UITableViewStylePlain];
    modelTable.delegate = self;
    modelTable.dataSource = self;
    modelTable.tableHeaderView = searchBar;
    modelTable.showsVerticalScrollIndicator = YES;
    modelTable.separatorStyle  = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:modelTable];
    //注册cell
    [modelTable registerNib:[UINib nibWithNibName:@"OsceCell" bundle:nil] forCellReuseIdentifier:@"modelCell"];
    //
    //制作时间选择器并添加
    searchUI = [[SearchUI alloc]initWithCurrentvc:self];
    __weak typeof(self) weakSelf = self;
    searchUI.responseBlock = ^(NSString *startTime,NSString *endTime){
        [weakSelf timeSearchAction:startTime andEndTime:endTime];
    };
}
- (void)timeSearchAction:(NSString *)startTime andEndTime:(NSString *)endTime{
    NSString *requestURL;
    if (currentIndex == 0) {
        requestURL = [NSString stringWithFormat:@"%@/osceTests/listOSCETestsAPP?startTimeFrom=%@&startTimeTo=%@&teacherId=%@&pageStart=1&pageSize=999",LocalIP,startTime,endTime,LocalUserId];
        
        NSLog(@"osce=%@",requestURL);
        
        [RequestTools RequestWithURL:requestURL Method:@"get" Params:nil Message:@"加载中。。。" Success:^(NSDictionary *result) {
            
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"该段时间无OSCE考试!" places:0 toView:nil];
            }else{
                [osceArray removeAllObjects];
                for (int i=0; i<array.count; i++) {
                    OSCEModel *osceModel = [OSCEModel new];
                    NSDictionary *objectDic = array[i];
                    osceModel.osceID = [objectDic objectForKey:@"testId"];
                    osceModel.osceName = [objectDic objectForKey:@"testName"];
                    osceModel.osceTime = [objectDic objectForKey:@"startTime"];
                    osceModel.type = @"osce_test";
                    osceModel.osceStatus = [objectDic objectForKey:@"testStatus"];
                    
                    osceModel.osceCategory = [objectDic objectForKey:@"categoryName"];
                    
                    osceModel.totalScore = [objectDic objectForKey:@"totalScoreValue"];
                    
                    osceModel.des = [objectDic objectForKey:@"description"];
                    
                    osceModel.timeBlock = [objectDic objectForKey:@"timeBlocksNum"];
                    
                    osceModel.osceStationNums = [objectDic objectForKey:@"selectedStationsNum"];
                    
                    osceModel.subjectName = [objectDic objectForKey:@"subjectName"];
            
                    //
                    [osceArray addObject:osceModel];
                }
                [modelTable reloadData];
            }
        } failed:^(NSString *result) {
            
        }];
    }else if (currentIndex == 1){
        requestURL = [NSString stringWithFormat:@"%@/testSchedules?pageStart=1&pageSize=999&teacherId=%@&testTimeFrom=%@&testTimeTo=%@",LocalIP,LocalUserId,startTime,endTime];
        
        NSLog(@"online=%@",requestURL);
        
        [RequestTools RequestWithURL:requestURL Method:@"get" Params:nil Message:@"加载中。。。" Success:^(NSDictionary *result) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"该段时间无在线考试!" places:0 toView:nil];
            }else{
                [onlineArray removeAllObjects];
                
                for (int i = 0; i<array.count; i++) {
                    onlineModel *model = [onlineModel new];
                    NSDictionary *objectDic = array[i];
                    model.onlineName = [objectDic objectForKey:@"testName"];
                    model.onlineRoom = [objectDic objectForKey:@"testRoomName"];
                    model.onlineInternal = [objectDic objectForKey:@"   mkkk"];
                    model.onlineTime = [objectDic objectForKey:@"testTime"];
                    model.type = @"online_test";
                    model.onlinePages = [objectDic objectForKey:@"testPapersNum"];
                    model.onlineDes = [objectDic objectForKey:@"description"];
                    model.onlineStatus = [objectDic objectForKey:@"testStatus"];
                    model.createTime = [objectDic objectForKey:@"createdTime"];
                    model.createName = [objectDic objectForKey:@"createdBy"];
                    model.difficulty = [objectDic objectForKey:@"difficulty"];
                    model.selectNums = [objectDic objectForKey:@"registeredStudentsNum"];
                    //拼接老师
                    NSArray *teacherArray = [objectDic objectForKey:@"testTeachers"];
                    if (teacherArray.count != 0) {
                        NSString *teacherFormat = @"";
                        for (int s=0; s<teacherArray.count; s++) {
                            NSDictionary *teacherDic = teacherArray[s];
                            if (s==teacherArray.count-1) {
                                teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@",[teacherDic objectForKey:@"fullName"]]];
                            }else{
                                teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@,",[teacherDic objectForKey:@"fullName"]]];
                            }
                        }
                        model.teacher = teacherFormat;
                        NSLog(@"teahcher=%@",teacherFormat);
                    }
                    [onlineArray addObject:model];
                }
                [modelTable reloadData];
            }
            
        } failed:^(NSString *result) {
            
        }];
    }else{
        requestURL = [NSString stringWithFormat:@"%@/skillTests/listSkillTestsAPP?testTimeFrom=%@&testTimeTo=%@&teacherId=%@&pageStart=1&pageSize=15",LocalIP,startTime,endTime,LocalUserId];
        
        NSLog(@"skill=%@",requestURL);
        
        [RequestTools RequestWithURL:requestURL Method:@"get" Params:nil Message:@"加载中。。。" Success:^(NSDictionary *result) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"该段时间无技能考试!" places:0 toView:nil];
            }else{
                [skillArray removeAllObjects];
                for (int i=0; i<array.count; i++) {
                    SkillModel *model = [SkillModel new];
                    NSDictionary *objectDic = array[i];
                    model.skillStatus = [objectDic objectForKey:@"testStatus"];
                    model.skillName = [objectDic objectForKey:@"testName"];
                    
                    model.skillCategory = [objectDic objectForKey:@"categoryName"];
                    model.skillSubject = [objectDic objectForKey:@"subjectName"];
                    model.skillStartTime = [objectDic objectForKey:@"startTime"];
                    model.skillDes = [objectDic objectForKey:@"description"];
                    model.skillStationNums = [objectDic objectForKey:@"selectedStationsNum"];
                    model.timeBlock = [objectDic objectForKey:@"testTimeBlocksNum"];
                    [skillArray addObject:model];
                }
                [modelTable reloadData];
            }
            
        } failed:^(NSString *result) {
            NSLog(@"%@",result);
        }];
    }
}

- (void)setUpRefresh{
    [modelTable addHeaderWithTarget:self action:@selector(downRefresh)];
    [modelTable addFooterWithTarget:self action:@selector(loadRefresh)];
    //设置文字
    modelTable.headerPullToRefreshText = @"下拉刷新";
    modelTable.headerReleaseToRefreshText = @"松开进行刷新";
    modelTable.headerRefreshingText = @"刷新中。。。";
    
    modelTable.footerPullToRefreshText = @"上拉加载";
    modelTable.footerReleaseToRefreshText = @"松开进行加载";
    modelTable.footerRefreshingText = @"加载中。。。";
}
- (void)downRefresh{
    [self dowRefreshAction:currentIndex];
}
- (void)loadRefresh{
    [self loadMoreRefreshAction:currentIndex];
}
- (void)loadMoreRefreshAction:(NSInteger)indexBtn{
    NSString *url;
    currentPage ++;
    switch (indexBtn) {
        case 0:
            [modelTable footerEndRefreshing];
            url = [NSString stringWithFormat:@"%@/osceTests/listOSCETestsAPP?startTimeFrom=%@&startTimeTo=%@&teacherId=%@&pageStart=%d&pageSize=15",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId,currentPage];
            [self loadData:url AndIndex:indexBtn andType:0];
            break;
        case 1:
            url = [NSString stringWithFormat:@"%@/testSchedules?pageStart=%d&pageSize=15",LocalIP,currentPage];
            [self loadData:url AndIndex:indexBtn andType:0];
            break;
        case 2:
            [modelTable footerEndRefreshing];
//            [Maneger showAlert:@"暂无更多!" andCurentVC:self];
            url = [NSString stringWithFormat:@"%@/skillTests/listSkillTestsApp?testTimeFrom=%@&testTimeTo=%@&teacherId=%@&pageStart=%d&pageSize=15",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId,currentPage];
            [self loadData:url AndIndex:indexBtn andType:0];
            break;
        default:
            break;
    }
}
- (void)dowRefreshAction:(NSInteger)indexBtn{
    //
    currentPage = 1;
    NSString *url;
    switch (indexBtn) {
        case 0:
            url = [NSString stringWithFormat:@"%@/osceTests/listOSCETestsAPP?startTimeFrom=%@&startTimeTo=%@&teacherId=%@&pageStart=1&pageSize=15",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId];
            [self loadData:url AndIndex:indexBtn andType:1];
            break;
        case 1:
            url = [NSString stringWithFormat:@"%@/testSchedules?pageStart=1&pageSize=15",LocalIP];
            [self loadData:url AndIndex:indexBtn andType:1];
            break;
        case 2:
            url = [NSString stringWithFormat:@"%@/skillTests/listSkillTestsAPP?testTimeFrom=%@&testTimeTo=%@&teacherId=%@&pageStart=1&pageSize=15",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId];
            NSLog(@"url=%@",url);
            [self loadData:url AndIndex:indexBtn andType:1];
            break;
        default:
            break;
    }
}
- (void)loadData:(NSString *)url AndIndex:(NSInteger)index andType:(NSInteger)type{
    //
    RequestTools *tool = [RequestTools new];
    [tool getRequest:url];
    tool.errorBlock = ^(NSString *code){
        if (![code isEqualToString:@"200"]) {
            if (type ==0) {
                [modelTable footerEndRefreshing];
            }else{
                [modelTable headerEndRefreshing];
            }
            currentPage--;
        }
        return;
    };
    tool.responseBlock = ^(NSDictionary *response){
        NSLog(@"考试模块response=%@",response);
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[response objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            if (type ==0) {
                [modelTable footerEndRefreshing];
            }else{
                [modelTable headerEndRefreshing];
            }
            return;
        }
        if (type ==0) {
            [modelTable footerEndRefreshing];
            if (index == 0) {
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"暂无更多OSCE考试!" andCurentVC:self];
                }else{
                    NSMutableArray *newData = [NSMutableArray new];
                    for (int i=0; i<array.count; i++) {
                        OSCEModel *osceModel = [OSCEModel new];
                        NSDictionary *objectDic = array[i];
                        osceModel.osceID = [objectDic objectForKey:@"testId"];
                        osceModel.osceName = [objectDic objectForKey:@"testName"];
                        osceModel.osceTime = [objectDic objectForKey:@"startTime"];
                        osceModel.type = @"osce_test";
                        osceModel.osceStatus = [objectDic objectForKey:@"testStatus"];
                        osceModel.osceCategory = [objectDic objectForKey:@"testCategory"];
                
                        osceModel.subjectName = [objectDic objectForKey:@"subjectName"];
                
                        [newData addObject:osceModel];
                    }
                    NSRange range = NSMakeRange(0, newData.count);
                    NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                    [osceArray insertObjects:newData atIndexes:set];
                    [modelTable reloadData];
                }
            }
            else if (index == 1){
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"暂无更多在线考试!" andCurentVC:self];
                }else{
                    NSMutableArray *newData= [NSMutableArray new];
                    for (int i = 0; i<array.count; i++) {
                        onlineModel *model = [onlineModel new];
                        NSDictionary *objectDic = array[i];
                        model.onlineName = [objectDic objectForKey:@"testName"];
                        model.onlineRoom = [objectDic objectForKey:@"testRoomName"];
                        model.onlineInternal = [objectDic objectForKey:@"testDuration"];
                        model.onlineTime = [objectDic objectForKey:@"testTime"];
                        model.type = @"online_test";
                        model.onlinePages = [objectDic objectForKey:@"testPapersNum"];
                        model.onlineDes = [objectDic objectForKey:@"description"];
                        model.onlineStatus = [objectDic objectForKey:@"testStatus"];
                        model.createTime = [objectDic objectForKey:@"createdTime"];
                        model.createName = [objectDic objectForKey:@"createdBy"];
                        model.difficulty = [objectDic objectForKey:@"difficulty"];
                        model.selectNums = [objectDic objectForKey:@"registeredStudentsNum"];
                        //拼接老师
                        NSArray *teacherArray = [objectDic objectForKey:@"testTeachers"];
                        if (teacherArray.count != 0) {
                            NSString *teacherFormat = @"";
                            for (int s=0; s<teacherArray.count; s++) {
                                NSDictionary *teacherDic = teacherArray[s];
                                if (s==teacherArray.count-1) {
                                    teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@,",[teacherDic objectForKey:@"fullName"]]];
                                }else{
                                    teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@",[teacherDic objectForKey:@"fullName"]]];
                                }
                            }
                            model.teacher = teacherFormat;
                            NSLog(@"teahcher=%@",teacherFormat);
                        }
                        [newData addObject:model];
                    }
                    NSRange range = NSMakeRange(0, newData.count);
                    NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                    [onlineArray insertObjects:newData atIndexes:set];
                    [modelTable reloadData];
                }
            }else{
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"暂无更多技能考试!" andCurentVC:self];
                }else{
                    NSMutableArray *newData = [NSMutableArray new];
                    for (int i=0; i<array.count; i++) {
                        SkillModel *model = [SkillModel new];
                        NSDictionary *objectDic = array[i];
                        model.skillID = [objectDic objectForKey:@"testId"];
                        model.skillStatus = [objectDic objectForKey:@"testStatus"];
                        model.skillName = [objectDic objectForKey:@"testName"];
                        model.skillCategory = [objectDic objectForKey:@"categoryName"];
                        model.skillSubject = [objectDic objectForKey:@"subjectName"];
                        model.skillStartTime = [objectDic objectForKey:@"startTime"];
                        [newData addObject:model];
                    }
                    NSRange range = NSMakeRange(0, newData.count);
                    NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                    [skillArray insertObjects:newData atIndexes:set];
                    [modelTable reloadData];
                }
            }
        }else{
            [modelTable headerEndRefreshing];
            if (index == 0) {
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"今天暂无OSCE考试!" andCurentVC:self];
                }else{
                
                    [osceArray removeAllObjects];
                    for (int i=0; i<array.count; i++) {
                        OSCEModel *osceModel = [OSCEModel new];
                        NSDictionary *objectDic = array[i];
                        osceModel.osceID = [objectDic objectForKey:@"testId"];
                        osceModel.osceName = [objectDic objectForKey:@"testName"];
                        osceModel.osceTime = [objectDic objectForKey:@"startTime"];
                        osceModel.type = @"osce_test";
                        osceModel.osceStatus = [objectDic objectForKey:@"testStatus"];
                        
                        osceModel.osceCategory = [objectDic objectForKey:@"categoryName"];
                        
                        osceModel.totalScore = [objectDic objectForKey:@"totalScoreValue"];
                        
                        osceModel.des = [objectDic objectForKey:@"description"];
                        
                        osceModel.timeBlock = [objectDic objectForKey:@"timeBlocksNum"];
                        
                        osceModel.osceStationNums = [objectDic objectForKey:@"selectedStationsNum"];
                        
                        osceModel.subjectName = [objectDic objectForKey:@"subjectName"];
                        
                        
                        //
                        [osceArray addObject:osceModel];
                    }
                    [modelTable reloadData];
                }
            }
            else if (index == 1){
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"今天暂无在线考试!" andCurentVC:self];
                }else{
                    [onlineArray removeAllObjects];
                   
                    for (int i = 0; i<array.count; i++) {
                        onlineModel *model = [onlineModel new];
                        NSDictionary *objectDic = array[i];
                        model.onlineName = [objectDic objectForKey:@"testName"];
                        model.onlineRoom = [objectDic objectForKey:@"testRoomName"];
                        model.onlineInternal = [objectDic objectForKey:@"testDuration"];
                        model.onlineTime = [objectDic objectForKey:@"testTime"];
                        model.type = @"online_test";
                        model.onlinePages = [objectDic objectForKey:@"testPapersNum"];
                        model.onlineDes = [objectDic objectForKey:@"description"];
                        model.onlineStatus = [objectDic objectForKey:@"testStatus"];
                        model.createTime = [objectDic objectForKey:@"createdTime"];
                        model.createName = [objectDic objectForKey:@"createdBy"];
                        model.difficulty = [objectDic objectForKey:@"difficulty"];
                        model.selectNums = [objectDic objectForKey:@"registeredStudentsNum"];
                        //拼接老师
                        NSArray *teacherArray = [objectDic objectForKey:@"testTeachers"];
                        if (teacherArray.count != 0) {
                            NSString *teacherFormat = @"";
                            for (int s=0; s<teacherArray.count; s++) {
                                NSDictionary *teacherDic = teacherArray[s];
                                if (s==teacherArray.count-1) {
                                    teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@",[teacherDic objectForKey:@"fullName"]]];
                                }else{
                                    teacherFormat = [teacherFormat stringByAppendingString:[NSString stringWithFormat:@"%@,",[teacherDic objectForKey:@"fullName"]]];
                                }
                            }
                            model.teacher = teacherFormat;
                            NSLog(@"teahcher=%@",teacherFormat);
                        }
                        [onlineArray addObject:model];
                    }
                    [modelTable reloadData];
                }
            }else{
                NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"今天暂无技能考试!" andCurentVC:self];
                }else{
                    [skillArray removeAllObjects];
                    for (int i=0; i<array.count; i++) {
                        SkillModel *model = [SkillModel new];
                        NSDictionary *objectDic = array[i];
                        model.skillStatus = [objectDic objectForKey:@"testStatus"];
                        model.skillName = [objectDic objectForKey:@"testName"];
                        
                        model.skillCategory = [objectDic objectForKey:@"categoryName"];
                        model.skillSubject = [objectDic objectForKey:@"subjectName"];
                        model.skillStartTime = [objectDic objectForKey:@"startTime"];
                        model.skillDes = [objectDic objectForKey:@"description"];
                        model.skillStationNums = [objectDic objectForKey:@"selectedStationsNum"];
                        model.timeBlock = [objectDic objectForKey:@"testTimeBlocksNum"];
                        [skillArray addObject:model];
                    }
                    [modelTable reloadData];
                }
            }
        }
    };
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)timeSelect:(UIButton *)btn{
    if (searchUI.isShowSelf == [NSNumber numberWithInt:0]) {
        [searchUI showInVC];
    }else{
        [searchUI hideInVC];
    }
}
//
- (void)findAction:(UIButton *)button{
    isSearch = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    if (button == leftS) {
        //设置右边状态
        rightImage.image = [UIImage imageNamed:@"online_unckeked"];
        rightLabel.textColor = [UIColor blackColor];
        rightLine.backgroundColor = [UIColor whiteColor];
        //设置skill
        skillImage.image = [UIImage imageNamed:@"online_unckeked"];
        skillLabel.textColor = [UIColor blackColor];
        skillLine.backgroundColor = [UIColor whiteColor];
        //设置左边状态
        leftImage.image = [UIImage imageNamed:@"osceimgchecked"];
        leftLabel.textColor = UIColorFromHex(0x46bec8);
        leftLine.backgroundColor = UIColorFromHex(0x46bec8);
        //请求
        currentIndex = button.tag-200;
        if (osceArray.count == 0) {
            [modelTable reloadData];
            [modelTable headerBeginRefreshing];
        }else{
            [modelTable reloadData];
        }
    }else if(button == rightS){
        //设置左边状态
        leftImage.image = [UIImage imageNamed:@"osceimg_unchecked"];
        leftLabel.textColor = [UIColor blackColor];
        leftLine.backgroundColor = [UIColor whiteColor];
        //设置skill
        skillImage.image = [UIImage imageNamed:@"online_unckeked"];
        skillLabel.textColor = [UIColor blackColor];
        skillLine.backgroundColor = [UIColor whiteColor];
        //设置右边状态
        rightImage.image = [UIImage imageNamed:@"onlien_checked"];
        rightLabel.textColor = UIColorFromHex(0x46bec8);
        rightLine.backgroundColor = UIColorFromHex(0x46bec8);
        //请求
        currentIndex = button.tag-200;
        if (onlineArray.count == 0) {
            [modelTable reloadData];
            [modelTable headerBeginRefreshing];
        }else{
            [modelTable reloadData];
        }
    }else{
        //设置skill
        skillImage.image = [UIImage imageNamed:@"onlien_checked"];
        skillLabel.textColor = UIColorFromHex(0x46bec8);
        skillLine.backgroundColor = UIColorFromHex(0x46bec8);
        //设置左边
        leftImage.image = [UIImage imageNamed:@"osceimg_unchecked"];
        leftLabel.textColor = [UIColor blackColor];
        leftLine.backgroundColor = [UIColor whiteColor];
        //设置中间
        rightImage.image = [UIImage imageNamed:@"online_unckeked"];
        rightLabel.textColor = [UIColor blackColor];
        rightLine.backgroundColor = [UIColor whiteColor];
        //请求
        currentIndex = button.tag-200;
        if (skillArray.count == 0) {
            [modelTable reloadData];
            [modelTable headerBeginRefreshing];
        }else{
            [modelTable reloadData];
        }
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [searchBar resignFirstResponder];
}
#pragma -table协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isSearch == NO) {
        if (currentIndex == 0) {
            return osceArray.count;
        }else if (currentIndex == 1){
            return onlineArray.count;
        }else{
            return skillArray.count;
        }
    }else{
        return tempArray.count;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [searchBar resignFirstResponder];
    if (currentIndex ==0) {
        TOSCEController *osceVC = [TOSCEController new];
        osceVC.model = osceArray[indexPath.row];
        [self.navigationController pushViewController:osceVC animated:YES];
    }else if (currentIndex == 1){
        TOnlineController *onlineVC = [TOnlineController new];
        onlineVC.model = onlineArray[indexPath.row];
        [self.navigationController pushViewController:onlineVC animated:YES];
    }else{
        TSkillController *skillVC = [TSkillController new];
        skillVC.model = skillArray[indexPath.row];
        [self.navigationController pushViewController:skillVC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OsceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"modelCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (isSearch == NO) {
        if (currentIndex == 0) {
            [cell set:osceArray[indexPath.row]];
        }else if (currentIndex == 1){
            [cell set:onlineArray[indexPath.row]];
        }else{
            [cell set:skillArray[indexPath.row]];
        }
    }else{
        [cell set:tempArray[indexPath.row]];
    }
    return cell;
}
#pragma -protocal
- (void)searchBar:(UISearchBar *)search textDidChange:(NSString *)searchText{
    [tempArray removeAllObjects];
    if (currentIndex == 0) {
        for (int i=0; i<osceArray.count; i++) {
            OSCEModel *model = osceArray[i];
            if ([model.osceName containsString:search.text] ) {
                [tempArray addObject:model];
            }
        }
    }else if (currentIndex == 1){
        for (int j=0; j<onlineArray.count; j++) {
            onlineModel *model = onlineArray[j];
            if ([model.onlineName containsString:search.text]) {
                [tempArray addObject:model];
            }
        }
    }else{
        for (int k=0; k<skillArray.count; k++) {
            SkillModel *model = skillArray[k];
            if ([model.skillName containsString:search.text]) {
                [tempArray addObject:model];
            }
        }
    }
    if (![search.text isEqualToString:@""]) {
        isSearch  = YES;
    }else{
        isSearch = NO;
        [searchBar resignFirstResponder];
    }
    [modelTable reloadData];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)bar{
    [bar resignFirstResponder];
}
#pragma -action

#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
