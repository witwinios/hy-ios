//
//  SelfTestCell.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SelfTestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *selfName;
@property (weak, nonatomic) IBOutlet UILabel *selfType;
@property (weak, nonatomic) IBOutlet UILabel *selfCategory;
@property (weak, nonatomic) IBOutlet UILabel *selfDeficuty;
@end
