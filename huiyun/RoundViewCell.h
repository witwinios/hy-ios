//
//  RoundViewCell.h
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *recordName;
@property (weak, nonatomic) IBOutlet UILabel *passLab;
@property (weak, nonatomic) IBOutlet UILabel *referLab;
@property (weak, nonatomic) IBOutlet UILabel *pendLab;
@property (weak, nonatomic) IBOutlet UIView *line;

@end
