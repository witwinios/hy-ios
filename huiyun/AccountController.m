//
//  AccountController.m
//  yun
//
//  Created by MacAir on 2017/8/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "AccountController.h"

@interface AccountController ()
{
    NSArray *titleArray;
    NSArray *contentArray;
    UIImage *userDefaultImage;
    
    UIButton *rightBtn;
    Edit editStaus;
    
    NSDictionary *titileDic;
    NSMutableArray *tempTitleArray;
    
    NSMutableDictionary *updateDic;
    
    AccountEntity *accountEntity;
    
    UITableView *tableView;
}
@property (strong, nonatomic)WXPPickerView * PickerOne;

@end

@implementation AccountController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //默认非编辑
    editStaus = EditNo;
    rightBtn.selected = NO;
}
- (void)getAccountInfo:(NSNumber *)userId andRole:(int)roleId{
    [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
    //roleId=3 学生 roleId=2 教师
    NSString *requestUrl = [NSString stringWithFormat:@"%@/users/%@",LocalIP,userId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        accountEntity = [AccountEntity new];
        accountEntity.responseDic = result[@"responseBody"];
        if (roleId == 3) {
            //组装数据
            if (![result[@"responseBody"][@"picture"] isKindOfClass:[NSNull class]]) {
                accountEntity.userFileUrl = [NSString stringWithFormat:@"%@?CTTS-Token=%@",result[@"responseBody"][@"picture"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }
            accountEntity.userGrade = result[@"responseBody"][@"grade"];
            accountEntity.userFullName = result[@"responseBody"][@"fullName"];
            accountEntity.userSex = result[@"responseBody"][@"gender"];
            accountEntity.userNo = result[@"responseBody"][@"userNo"];
            accountEntity.userMajor = result[@"responseBody"][@"major"];
            accountEntity.userPhoneNo = result[@"responseBody"][@"personalPhoneNo"];
            accountEntity.userEmail = result[@"responseBody"][@"email"];
            accountEntity.userCard = result[@"responseBody"][@"identificationNo"];
            accountEntity.userTitle = result[@"responseBody"][@"titleName"];
            accountEntity.userTitleType = result[@"responseBody"][@"titleType"];
            if ([result[@"responseBody"][@"isDoctor"] isKindOfClass:[NSNull class]]) {
                accountEntity.isDoctor = @"否";
            }else{
                BOOL isDoc = [result[@"responseBody"][@"isDoctor"] boolValue];
                if (isDoc) {
                    accountEntity.isDoctor = @"是";
                }else{
                    accountEntity.isDoctor = @"否";
                }
            }
            accountEntity.userDegree = result[@"responseBody"][@"degree"];
            accountEntity.userSchool = result[@"responseBody"][@"schoolName"];
            accountEntity.userSource = result[@"responseBody"][@"source"];
            accountEntity.userDescription = result[@"responseBody"][@"description"];
            accountEntity.userID = result[@"responseBody"][@"userId"];
            accountEntity.roleId = @3;
            accountEntity.userName = result[@"responseBody"][@"userName"];
            accountEntity.userDegreeType = result[@"responseBody"][@"degreeType"];
            accountEntity.userEducationType = result[@"responseBody"][@"educationalSystem"];
            accountEntity.userHighDegree = result[@"responseBody"][@"educationType"];
            accountEntity.userBlankcardOutlets = result[@"responseBody"][@"bankOutlets"];
            accountEntity.userBlankNumber = result[@"responseBody"][@"bankCardNumber"];
            
            NSString *blankCardFomat = @"";
            NSString *blankCard = [NSString stringWithFormat:@"%@",accountEntity.userBlankNumber];
            for (int i=0; i<blankCard.length; i++) {
                if (i == 0 || i == blankCard.length-1) {
                    blankCardFomat = [blankCardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[blankCard characterAtIndex:i]]];
                }else{
                    blankCardFomat = [blankCardFomat stringByAppendingString:@"*"];
                }
            }
            accountEntity.userHideBlankCard = blankCardFomat;
            
            NSString *cardFomat= @"";
            NSString *cardStr = [NSString stringWithFormat:@"%@",accountEntity.userCard];
            if (cardStr.length !=0 && cardStr.length != 1) {
                for (int i=0; i<cardStr.length; i++) {
                    if (i < 3 || i > cardStr.length-6) {
                        cardFomat = [cardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[cardStr characterAtIndex:i]]];
                    }else{
                        cardFomat = [cardFomat stringByAppendingString:@"*"];
                    }
                }
            }else{
                cardStr = @"暂无";
            }
            NSString *phoneFomat=  @"";
            NSString *phone = [NSString stringWithFormat:@"%@",accountEntity.userPhoneNo];
            if (phone.length != 0 && phone.length != 1) {
                for (int i=0; i<phone.length; i++) {
                    if (i < 3 || i > phone.length-3) {
                        phoneFomat = [phoneFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[phone characterAtIndex:i]]];
                    }else{
                        phoneFomat = [phoneFomat stringByAppendingString:@"*"];
                    }
                }
            }else{
                phoneFomat = @"暂无";
            }
            accountEntity.userHideCard = cardFomat;
            accountEntity.userHidePhone = phoneFomat;
            //保存
            [[NSuserDefaultManager share] saveCurrentAccount:accountEntity];
        }else if (roleId == 2){
            if (![result[@"responseBody"][@"picture"] isKindOfClass:[NSNull class]]) {
                accountEntity.userFileUrl = [NSString stringWithFormat:@"%@?CTTS-Token=%@",result[@"responseBody"][@"picture"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }
            accountEntity.userFullName = result[@"responseBody"][@"fullName"];
            accountEntity.userSex = result[@"responseBody"][@"gender"];
            accountEntity.userNo = result[@"responseBody"][@"userNo"];
            accountEntity.userMajor = result[@"responseBody"][@"major"];
            accountEntity.userPhoneNo = result[@"responseBody"][@"personalPhoneNo"];
            accountEntity.userEmail = result[@"responseBody"][@"email"];
            accountEntity.userCard = result[@"responseBody"][@"identificationNo"];
            accountEntity.userTitle = result[@"responseBody"][@"titleName"];
            accountEntity.userTitleType = result[@"responseBody"][@"titleType"];
            if ([result[@"responseBody"][@"isDoctor"] isKindOfClass:[NSNull class]]) {
                accountEntity.isDoctor = @"否";
            }else{
                BOOL isDoc = [result[@"responseBody"][@"isDoctor"] boolValue];
                if (isDoc) {
                    accountEntity.isDoctor = @"是";
                }else{
                    accountEntity.isDoctor = @"否";
                }
            }
            accountEntity.userDegree = result[@"responseBody"][@"degree"];
            accountEntity.userSchool = result[@"responseBody"][@"schoolName"];
            accountEntity.userSource = result[@"responseBody"][@"source"];
            accountEntity.userCareer = result[@"responseBody"][@"teachingRecord"][@"career"];
            accountEntity.userTeacherType = result[@"responseBody"][@"teachingRecord"][@"teacherType"];
            accountEntity.userWorkStart = result[@"responseBody"][@"teachingRecord"][@"startDate"];
            accountEntity.userWorkEnd = result[@"responseBody"][@"teachingRecord"][@"endDate"];
            accountEntity.userDescription = result[@"responseBody"][@"description"];
            accountEntity.userID = result[@"responseBody"][@"userId"];
            accountEntity.roleId = @2;
            accountEntity.userName = result[@"responseBody"][@"userName"];
            NSString *cardFomat= @"";
            NSString *cardStr = [NSString stringWithFormat:@"%@",accountEntity.userCard];
            if (cardStr.length !=0 && cardStr.length != 1) {
                for (int i=0; i<cardStr.length; i++) {
                    if (i < 3 || i > cardStr.length-6) {
                        cardFomat = [cardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[cardStr characterAtIndex:i]]];
                    }else{
                        cardFomat = [cardFomat stringByAppendingString:@"*"];
                    }
                }
            }
            NSString *phoneFomat=  @"";
            NSString *phone = [NSString stringWithFormat:@"%@",accountEntity.userPhoneNo];
            if (phone.length != 0 && phone.length != 1) {
                for (int i=0; i<phone.length; i++) {
                    if (i < 3 || i > phone.length-3) {
                        phoneFomat = [phoneFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[phone characterAtIndex:i]]];
                    }else{
                        phoneFomat = [phoneFomat stringByAppendingString:@"*"];
                    }
                }
            }
            accountEntity.userHideCard = cardFomat;
            accountEntity.userHidePhone = phoneFomat;
            //保存教师账户
            [[NSuserDefaultManager share] saveCurrentAccount:accountEntity];
        }
        //
        updateDic = [NSMutableDictionary dictionaryWithDictionary:accountEntity.responseDic];
        if (![accountEntity.userTitleType isEqualToString:@"暂无"]) {
            [self getTempTitle:titileDic[accountEntity.userTitleType]];
        }
        contentArray = @[accountEntity.userFullName,accountEntity.userSex,[NSString stringWithFormat:@"%@",accountEntity.userNo],[NSString stringWithFormat:@"%@",accountEntity.userGrade],accountEntity.userMajor,[NSString stringWithFormat:@"%@",accountEntity.userHidePhone],accountEntity.userEmail,[NSString stringWithFormat:@"%@",accountEntity.userHideCard],accountEntity.userTitleType,accountEntity.userTitle,accountEntity.isDoctor,@"",@""];
        //加载缓存头像
        userDefaultImage = [NSuserDefaultManager readHeadImageByAccount:accountEntity.userID];
        [tableView reloadData];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"获取账户信息失败!" places:0 toView:nil];
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    tempTitleArray = [NSMutableArray new];
    titileDic = @{@"行政类":@"administrative",@"学校类":@"school",@"工程类":@"engineering",@"医学类":@"medicine"};
    titleArray = @[@"头像:",@"姓名:",@"性别:",@"学号:",@"年级:",@"专业:",@"联系电话:",@"电子邮箱:",@"身份证号:",@"职称类型:",@"职称:",@"职业医师:",@"学历信息:",@"银行卡:"];
    PersonEntity *person = [[NSuserDefaultManager share] readCurrentUser];
    [self getAccountInfo:person.userID andRole:person.roleId.intValue];
    [self setUI];
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.PickerOne close];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-60, 29.5, 60, 25);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn setTitle:@"保存" forState:UIControlStateSelected];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
    [rightBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"个人中心";
    [backNavigation addSubview:titleLab];
    //tableView
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-164) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView registerNib:[UINib nibWithNibName:@"AccountCell1" bundle:nil] forCellReuseIdentifier:@"account1"];
    [tableView registerNib:[UINib nibWithNibName:@"AccountCell2" bundle:nil] forCellReuseIdentifier:@"account2"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
    //退出按钮
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exitBtn.frame = CGRectMake(0, Sheight-40, Swidth, 40);
    [exitBtn setTitle:@"退出登录" forState:0];
    exitBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    exitBtn.backgroundColor = [UIColor redColor];
    [exitBtn setTitleColor:[UIColor whiteColor] forState:0];
    [exitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [exitBtn addTarget:self action:@selector(exitAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:exitBtn];
}
- (void)editAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        [MBProgressHUD showToastAndMessage:@"进入编辑模式~" places:0 toView:nil];
        editStaus = EditYes;
    }else{
        editStaus = EditNo;
        //修改
        [self updateInfo:0];
    }
}
- (void)updateInfo:(int)s{
    //s = 1 返回
    NSString *requesUrl = [NSString stringWithFormat:@"%@/users/%@",LocalIP,accountEntity.userID];
    [RequestTools RequestWithURL:requesUrl Method:@"put" Params:updateDic Message:@"修改中" Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"成功!" places:0 toView:nil];
        if (s==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败!" places:0 toView:nil];
    }];
}

#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)exitAction{
    [MBProgressHUD showHUDAndMessage:@"注销中" toView:nil];
    //IM退出
    [[SPKitExample sharedInstance] exampleLogout];
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.loginVC.from = @"exit";
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [MBProgressHUD hideHUDForView:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 60;
    }
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editStaus == EditYes) {
        AccountCell1 *cell = [tableView cellForRowAtIndexPath:indexPath];
        __block NSMutableDictionary *response = updateDic;
        __block AccountController *teachVC = self;
        
        if (indexPath.row == 0) {
            PersonEntity *person = [[NSuserDefaultManager share] readCurrentUser];
            AccountCell2 *cell = [tableView cellForRowAtIndexPath:indexPath];
            ImageChangeController *changeVC = [ImageChangeController new];
            changeVC.entity = accountEntity;
            changeVC.img = cell.imgViewName.image;
            changeVC.imageBlock = ^(UIImage *image){
                cell.imgViewName.image = image;
                [NSuserDefaultManager saveHeadImage:image byAccount:person.userID];
                userDefaultImage = image;
            };
            [self.navigationController pushViewController:changeVC animated:YES];
        }else if (indexPath.row == 12) {
            [MBProgressHUD showToastAndMessage:@"请先保存!" places:0 toView:nil];
            //            DegreeController *degreeVC = [DegreeController new];
            //            degreeVC.entity = self.entity;
            //            [self.navigationController pushViewController:degreeVC animated:YES];
        }else if (indexPath.row == 13){
            [MBProgressHUD showToastAndMessage:@"请先保存!" places:0 toView:nil];
            //            BlankCardController *blankVC = [BlankCardController new];
            //            blankVC.entity = self.entity;
            //            [self.navigationController pushViewController:blankVC animated:YES];
        }else if (indexPath.row == 1){
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userFullName = str;
                [updateDic setObject:str forKey:@"fullName"];
            };
            [self.view addSubview:alert];
            
        }else if (indexPath.row == 2){
            NSArray *array = @[@"男",@"女"];
            NSArray *content = @[@"male",@"female"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            __weak AccountEntity *entity = accountEntity;
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                entity.userSex = array[index];
                [response setObject:content[index] forKey:@"gender"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 3){
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userNo = @(str.integerValue);
                [updateDic setObject:str forKey:@"userNo"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 4){
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userNo = @(str.integerValue);
                [updateDic setObject:str forKey:@"grade"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 5){
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userNo = @(str.integerValue);
                [updateDic setObject:str forKey:@"major"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 6){
            AlertView *alert = [[AlertView alloc]init];
            alert.inType = @"phone";
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userPhoneNo = @(str.integerValue);
                [updateDic setObject:str forKey:@"personalPhoneNo"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 7){
            AlertView *alert = [[AlertView alloc]init];
            alert.inType = @"email";
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userEmail = str;
                [updateDic setObject:str forKey:@"email"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 8){
            AlertView *alert = [[AlertView alloc]init];
            alert.inType = @"card";
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                accountEntity.userCard = str;
                [updateDic setObject:str forKey:@"identificationNo"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 9){
            NSArray *array = @[@"行政类",@"学校类",@"工程类",@"医学类"];
            NSArray *content = @[@"administrative",@"school",@"engineering",@"medicine"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            __weak AccountEntity *entity = accountEntity;
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                entity.userTitleType = array[index];
                [response setObject:content[index] forKey:@"titleType"];
                [teachVC getTempTitle:content[index]];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 10){
            NSMutableArray *tempArray = [NSMutableArray new];
            NSMutableArray *IDArray = [NSMutableArray new];
            for (int i =0; i<tempTitleArray.count; i++) {
                NSDictionary *dic = tempTitleArray[i];
                [tempArray addObject:dic[@"titleName"]];
                [IDArray addObject:dic[@"titleId"]];
            }
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:tempArray]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            __weak AccountEntity *entity = accountEntity;
            self.PickerOne.index = ^(int index){
                cell.content.text = tempArray[index];
                entity.userTitle = tempArray[index];
                [response setObject:IDArray[index] forKey:@"titleId"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 1){
            NSArray *array = @[@"是",@"否"];
            NSArray *content = @[@"true",@"false"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            __weak AccountEntity *entity = accountEntity;
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                entity.isDoctor = array[index];
                [response setObject:content[index] forKey:@"isDoctor"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }
    }else{
        if (indexPath.row == 12) {
            DegreeController *degreeVC = [DegreeController new];
            degreeVC.entity = accountEntity;
            [self.navigationController pushViewController:degreeVC animated:YES];
        }else if (indexPath.row == 13){
            BlankCardController *blankVC = [BlankCardController new];
            blankVC.entity = accountEntity;
            [self.navigationController pushViewController:blankVC animated:YES];
        }
    }
}
- (void)getTempTitle:(NSString *)titleType{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/titles?pageSize=999&titleType=%@",LocalIP,titleType];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        tempTitleArray = result[@"responseBody"][@"result"];
        NSLog(@"%ld",tempTitleArray.count);
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountCell2 *cell2 = [tableView dequeueReusableCellWithIdentifier:@"account2"];
    AccountCell1 *cell1 = [tableView dequeueReusableCellWithIdentifier:@"account1"];
    PersonEntity *person = [[NSuserDefaultManager share] readCurrentUser];
    if (indexPath.row == 0) {
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        if (userDefaultImage != nil) {
            cell2.imgViewName.image = userDefaultImage;
        }else{
            NSString *imageStr = [[NSString stringWithFormat:@"%@%@",SimpleIp,person.fileUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            UIImage *img = [NSuserDefaultManager readHeadImageByAccount:person.userID];
            if (img == nil) {
                [cell2.imgViewName sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"head-place"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    //保存
                    [NSuserDefaultManager saveHeadImage:image byAccount:accountEntity.userID];
                    userDefaultImage = image;
                }];
            }else{
                cell2.imgViewName.image = img;
            }
        }
        return cell2;
    }else {
        if (indexPath.row == 1 || indexPath.row == 3 ||indexPath.row == 4 ||indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 ||indexPath.row == 8) {
            cell1.arrow.hidden = YES;
        }
        if (indexPath.row == 3 && [contentArray[2] integerValue] ==0) {
            cell1.content.text = @"暂无";
        }else{
            cell1.content.text = contentArray[indexPath.row-1];
        }
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        cell1.title.text = titleArray[indexPath.row];
        return cell1;
    }
}
@end

