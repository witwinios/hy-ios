//
//  TCourseModel.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TCourseModel.h"

@implementation TCourseModel
-(void)setScheduleTyoe:(NSString *)scheduleTyoe{
    if ([scheduleTyoe isKindOfClass:[NSNull class]]) {
        _scheduleTyoe = @"暂无";
    }else if ([scheduleTyoe isEqualToString:@"classroom_course"]){
        _scheduleTyoe = @"课堂课程";
    }else if ([scheduleTyoe isEqualToString:@"online_course"]){
        _scheduleTyoe = @"在线课程";
    }else if ([scheduleTyoe isEqualToString:@"operating_course"]){
        _scheduleTyoe = @"实操课程";
    }
}
- (void)setScheduleStatus:(NSString *)scheduleStatus{
    if ([scheduleStatus isEqualToString:@"planning"]) {
        _scheduleStatus = @"计划中";
    }else if ([scheduleStatus isEqualToString:@"started"]){
        _scheduleStatus = @"已开始";
    }else if ([scheduleStatus isEqualToString:@"released"]){
        _scheduleStatus = @"已发布";
    }else{
        _scheduleStatus  = @"已完成";
    }
}
- (void)setScheduleID:(NSNumber *)scheduleID{
    if ([scheduleID isKindOfClass:[NSNull class]]) {
        _scheduleID = [NSNumber numberWithInt:0];
    }else{
        _scheduleID = scheduleID;
    }
}
- (void)setCoursePlace:(NSString *)coursePlace{
    if ([coursePlace isKindOfClass:[NSNull class]]) {
        _coursePlace = @"暂无";
    }else{
        _coursePlace = coursePlace;
    }
}
- (void)setCourseSubjec:(NSString *)courseSubjec{
    if ([courseSubjec isKindOfClass:[NSNull class]]) {
        _courseSubjec = @"无科目";
    }else{
        _courseSubjec = courseSubjec;
    }
}
- (void)setCourseName:(NSString *)courseName{
    if ([courseName isKindOfClass:[NSNull class]]) {
        _courseName = @"暂无";
    }else{
        _courseName = courseName;
    }
}
- (void)setStartTime:(NSNumber *)startTime{
    if ([startTime isKindOfClass:[NSNull class]]) {
        _startTime = [NSNumber numberWithInt:0];
    }else{
        _startTime = startTime;
    }
}
- (void)setTeacherName:(NSString *)teacherName{
    if ([teacherName isKindOfClass:[NSNull class]]) {
        _teacherName = @"暂无";
    }else{
        _teacherName = teacherName;
    }
}
- (void)setRegistNum:(NSNumber *)registNum{
    if ([registNum isKindOfClass:[NSNull class]]) {
        _registNum = [NSNumber numberWithInt:0];
    }else{
        _registNum = registNum;
    }
}
- (void)setCourseDes:(NSString *)courseDes{
    if ([courseDes isKindOfClass:[NSNull class]]) {
        _courseDes = @"暂无";
    }else{
        _courseDes = courseDes;
    }
}
@end
