//
//  TClassController.m
//  yun
//
//  Created by MacAir on 2017/7/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TClassController.h"

@interface TClassController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    UIView *backView;
    UILabel *timeLabel;
    
    NSNumber *currentRoomId;
}
@end

@implementation TClassController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    
    [self setUI];
    [self loadClass];
}
- (void)loadClass{
    [MBProgressHUD showHUDAndMessage:@"加载中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/getRoomsByIdAndTeacherId/%@",LocalIP,self.skillModel.skillID,LocalUserId];
    
    NSLog(@"classVC=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [dataArray removeAllObjects];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂时没有教室!" places:0 toView:nil];
            return ;
        }
        for (NSDictionary *dic in array) {
            TRoomModel *roomModel = [TRoomModel new];
            roomModel.TRoomId = dic[@"roomId"];
            roomModel.TRoomName = dic[@"roomName"];
            roomModel.TRoomType = dic[@"roomType"];
            roomModel.TRoomCapacity = dic[@"capacity"];
            [dataArray addObject:roomModel];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"加载失败,请下拉刷新!" places:0 toView:nil];
    }];
}
- (void)pullRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/getRoomsByIdAndTeacherId/%@",LocalIP,self.skillModel.skillID,LocalUserId];
    
    NSLog(@"classVC-pull=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [tabView headerEndRefreshing];
        [dataArray removeAllObjects];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂时没有教室!" places:0 toView:nil];
            return ;
        }
        for (NSDictionary *dic in array) {
            TRoomModel *roomModel = [TRoomModel new];
            roomModel.TRoomId = dic[@"roomId"];
            roomModel.TRoomName = dic[@"roomName"];
            roomModel.TRoomType = dic[@"roomType"];
            roomModel.TRoomCapacity = dic[@"capacity"];
            [dataArray addObject:roomModel];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        [tabView headerEndRefreshing];
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"教室选取";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"TRoomCell" bundle:nil] forCellReuseIdentifier:@"roomCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
    [tabView addHeaderWithTarget:self action:@selector(pullRefresh)];
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
    
    //背景层
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, -Sheight, Swidth, Sheight)];
    backView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 150, Swidth, 80)];
    label.text = @"考试暂时没有开始!";
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:25];
    [backView addSubview:label];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(5, 20, 60, 30);
    cancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    cancelBtn.layer.borderWidth = 1;
    cancelBtn.layer.cornerRadius = 5;
    cancelBtn.layer.masksToBounds = YES;
    [cancelBtn setTitle:@"关闭" forState:0];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
    [cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:cancelBtn];
    
    UILabel *startLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    startLab.textColor = [UIColor whiteColor];
    startLab.text = @"开始时间:";
    startLab.font = [UIFont systemFontOfSize:14];
    startLab.center = CGPointMake(Swidth/2-40, Sheight/2-20);
    [backView addSubview:startLab];
    
    UILabel *startLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, Sheight/2-40, 100, 40)];
    startLabel.text = [[Maneger shareObject]timeFormatter:_skillModel.skillStartTime.stringValue];
    startLabel.font = [UIFont systemFontOfSize:14];
    startLabel.textColor = [UIColor whiteColor];
    startLabel.adjustsFontSizeToFitWidth = YES;
    [backView addSubview:startLabel];
    
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    timeLab.text = @"倒计时:";
    timeLab.textColor = [UIColor whiteColor];
    timeLab.font = [UIFont systemFontOfSize:14];
    timeLab.center = CGPointMake(Swidth/2-40, Sheight/2+20);
    [backView addSubview:timeLab];
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, Sheight/2, 100, 40)];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont systemFontOfSize:14];
    [backView addSubview:timeLabel];
    
    [self.view addSubview:backView];

}
- (void)showBack{
    backView.frame = CGRectMake(0, 0, Swidth, Sheight);
}
- (void)hidBack{
    backView.frame = CGRectMake(-Sheight, 0, Swidth, Sheight);
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)cancelView{
    backView.frame = CGRectMake(Swidth, 0, Swidth, Sheight);
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TRoomModel *roomModel = dataArray[indexPath.row];
    currentRoomId = roomModel.TRoomId;
    //
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:[NSString stringWithFormat:@"本房间名称:%@",roomModel.TRoomName] delegate:self cancelButtonTitle:@"更改房间" otherButtonTitles:@"进入考试", nil];
    [alertView show];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TRoomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roomCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    TRoomModel *roomModel = dataArray[indexPath.row];
    cell.room.text = roomModel.TRoomName;
    cell.type.text = roomModel.TRoomType;
    cell.capacity.text = roomModel.TRoomCapacity.stringValue;
    return cell;
}
#pragma -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self loadSelectStation];
    }
}
- (void)loadSelectStation{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/room",LocalIP,self.skillModel.skillID,currentRoomId];
    
    NSLog(@"get-selectId=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"跳转中" Success:^(NSDictionary *result) {
        NSNumber *selectId = result[@"responseBody"][@"selectedId"];
        ControlManeger *VCManeger = [ControlManeger share];
        
        VCManeger.ListSVC = [ListStudentController new];
        VCManeger.ListSVC.selectStationId = selectId;
        VCManeger.ListSVC.skillModel = self.skillModel;
        
        [NSTimer scheduledTimerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [self.navigationController pushViewController:VCManeger.ListSVC animated:YES];
        }];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"跳转失败,请重试!" places:0 toView:nil];
    }];
}
@end
