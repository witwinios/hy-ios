//
//  StudyHistoryViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/28.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYCourseCell.h"
#import "HYCourseDetailController.h"
@interface HYStudyHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *comptArray;
@end
