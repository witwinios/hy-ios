//
//  RoundRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SdepartRecordController.h"

@interface SdepartRecordController ()
{
    UITableView *table;
    NSArray *dataArray;
}
@end

@implementation SdepartRecordController
+ (id)shareObject{
    static SdepartRecordController *recordVC = nil;
    if (recordVC == nil) {
        recordVC = [SdepartRecordController new];
    }
    return recordVC;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dataArray = self.departModel.SDepartRecordArray;
    [table reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转记录";
    [backNavigation addSubview:titleLab];
    
    UILabel *keshiTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 64, Swidth/2, 50)];
    keshiTitle.text = @"所在科室:";
    [self.view addSubview:keshiTitle];
    
    UILabel *keshiContent = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-20, 64, 10, 20)];
    keshiContent.center = CGPointMake(Swidth-20, keshiTitle.center.y);
    
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64-200)];
    table.bounces = NO;
    table.delegate = self;
    table.dataSource = self;
    [table registerNib:[UINib nibWithNibName:@"RoundCell" bundle:nil] forCellReuseIdentifier:@"roundCell"];
    [table registerNib:[UINib nibWithNibName:@"RoundViewCell" bundle:nil] forCellReuseIdentifier:@"roundViewCell"];
    [self.view addSubview:table];
    //
    UIImageView *resultImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, Sheight-190, Swidth, 50)];
    resultImage.backgroundColor = [UIColor whiteColor];
    resultImage.userInteractionEnabled = YES;
    [self.view addSubview:resultImage];
    
    UILabel *resultLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 50)];
    resultLab.text = @"出科小结";
    resultLab.textColor = [UIColor lightGrayColor];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth-20, 15, 10, 20)];
    imgView.image = [UIImage imageNamed:@"youjiantou"];
    [resultImage addSubview:imgView];
    [resultImage addSubview:resultLab];
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    tap.numberOfTapsRequired = 1;
    [resultImage addGestureRecognizer:tap];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
//出科小结
- (void)tapAction:(UITapGestureRecognizer *)tap{
    //    ResultController *resultVC = [ResultController new];
    //    [self.navigationController pushViewController:resultVC animated:YES];
    ResultSubmitedController *submitVC = [ResultSubmitedController new];
    [self.navigationController pushViewController:submitVC animated:YES];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectRecordIndex = [NSNumber numberWithInteger:indexPath.row];
    if (indexPath.row > 2) {
        CaseHistoryController *hisVC = [CaseHistoryController shareObject];
        [self.navigationController pushViewController:hisVC animated:YES];
    }else{
        CaseRequestController *hisVC = [CaseRequestController new];
        hisVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:hisVC animated:YES];
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *head = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 100)];
    head.backgroundColor = [UIColor whiteColor];
    UILabel *keshiLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, Swidth/2-5, 50)];
    keshiLab.text = @"所在科室:";
    keshiLab.textColor = [UIColor lightGrayColor];
    keshiLab.textAlignment = 0;
    keshiLab.font = [UIFont systemFontOfSize:20];
    [head addSubview:keshiLab];
    
    UILabel *keshiContent = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, 0, Swidth/2-30, 50)];
    keshiContent.font = [UIFont systemFontOfSize:20];
    keshiContent.text = self.departModel.SDepartName;
    keshiContent.textColor = [UIColor lightGrayColor];
    keshiContent.textAlignment = NSTextAlignmentRight;
    [head addSubview:keshiContent];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth-20, 15, 10, 20)];
    imgView.image = [UIImage imageNamed:@"youjiantou"];
    [head addSubview:imgView];
    
    UIView *twoView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, Swidth, 50)];
    twoView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [head addSubview:twoView];
    NSArray *array = @[@"记录名称",@"已通过",@"未通过",@"待审核"];
    for (int i =0; i<4; i++) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/4*i, 0, Swidth/4, 50)];
        label.textAlignment = 1;
        label.text = array[i];
        label.font = [UIFont systemFontOfSize:18];
        label.textColor = [UIColor lightGrayColor];
        [twoView addSubview:label];
    }
    return head;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RoundViewCell *roundViewCell = [table dequeueReusableCellWithIdentifier:@"roundViewCell"];
    roundViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    SDepartRecordModel *model = dataArray[indexPath.row];
    roundViewCell.recordName.text = model.SDepartRecordName;
    roundViewCell.passLab.text = [NSString stringWithFormat:@"%@",model.SDepartRecordPassNum];
    roundViewCell.referLab.text = [NSString stringWithFormat:@"%@",model.SDepartRecordReferNum];
    roundViewCell.pendLab.text = [NSString stringWithFormat:@"%@",model.SDepartRecordPendNum];
    return roundViewCell;
}
@end

