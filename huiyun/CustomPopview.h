//
//  CustomPopview.h
//  yun
//
//  Created by MacAir on 2017/8/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol selectIndexPathDelegate <NSObject>
- (void)selectIndexPathRow:(NSInteger )index;
@end

typedef NS_ENUM(NSInteger, XTDirectionType) {
    XTTypeOfUpLeft,     // 上左
    XTTypeOfUpCenter,   // 上中
    XTTypeOfUpRight,    // 上右
    
    XTTypeOfDownLeft,   // 下左
    XTTypeOfDownCenter, // 下中
    XTTypeOfDownRight,  // 下右
    
    XTTypeOfLeftUp,     // 左上
    XTTypeOfLeftCenter, // 左中
    XTTypeOfLeftDown,   // 左下
    
    XTTypeOfRightUp,    // 右上
    XTTypeOfRightCenter,// 右中
    XTTypeOfRightDown,  // 右下
};

@interface CustomPopview : UIView<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UIView  * _Nonnull backGoundView;
// titles
@property (nonatomic, strong) NSArray * _Nonnull dataArray;
// images
@property (nonatomic, strong) NSArray * _Nonnull images;
// height
@property (nonatomic, assign) CGFloat row_height;
// font
@property (nonatomic, assign) CGFloat fontSize;
// textColor
@property (nonatomic, strong) UIColor * _Nonnull titleTextColor;

@property (nonatomic, assign) CGPoint origin;                    // 箭头位置
@property (nonatomic, assign) CGFloat height;                    // 视图的高度
@property (nonatomic, assign) CGFloat width;                     // 视图的宽度
@property (nonatomic, assign) XTDirectionType type;              // 箭头位置类型
@property (nonatomic, strong) UITableView *tableView;            // 填充的tableview
// delegate
@property (nonatomic, assign) id <selectIndexPathDelegate> _Nonnull delegate;
// 初始化方法
- (instancetype _Nonnull)initWithOrigin:(CGPoint) origin
                                  Width:(CGFloat) width
                                 Height:(CGFloat) height
                                   Type:(XTDirectionType)type
                                  Color:( UIColor * _Nonnull ) color;
- (void)popView;
@end
