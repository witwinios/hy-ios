//
//  SkillModel.h
//  xiaoyun
//
//  Created by MacAir on 17/2/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SkillModel : NSObject
@property (strong, nonatomic) NSNumber *skillID;
@property (strong, nonatomic) NSString *skillStatus;
@property (strong, nonatomic) NSString *skillName;
@property (strong, nonatomic) NSString *skillDes;
@property (strong, nonatomic) NSString *skillCategory;
@property (strong, nonatomic) NSString *skillSubject;
@property (strong, nonatomic) NSNumber *skillStartTime;
@property (strong, nonatomic) NSNumber *skillEndTime;
@property (strong, nonatomic) NSNumber *createTime;
@property (strong, nonatomic) NSString *createPerson;
@property (strong, nonatomic) NSNumber *timeBlock;
@property (strong, nonatomic) NSNumber *skillStationNums;
@end
