//
//  ExamPickView.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^MyBasicBlock)(id result);
@interface ExamPickView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePick;
@property (copy, nonatomic) MyBasicBlock dateBlock;
@property (strong,nonatomic) NSNumber *isShow;
- (IBAction)cancelAction:(id)sender;
- (IBAction)sureAction:(id)sender;
- (void)showPick;
- (void)hidePick;
- (instancetype)initWithCurrentvc:(UIViewController *)currentVC;
@end
