

#import <UIKit/UIKit.h>

@protocol THDatePickerViewDelegate <NSObject>

/**
 保存按钮代理方法
 
 @param timer 选择的数据
 */
- (void)datePickerViewSaveBtnClickDelegate:(NSString *)timer;

/**
 取消按钮代理方法
 */
- (void)datePickerViewCancelBtnClickDelegate;

@end
//传值block
typedef void (^dateBlock)(NSString *date,NSString *longDate);
typedef void (^cancelBlock)();
@interface THDatePickerView : UIView

@property (copy, nonatomic) NSString *title;
@property (weak, nonatomic) id <THDatePickerViewDelegate> delegate;
@property (strong, nonatomic) dateBlock sureBlock;
@property (strong, nonatomic) cancelBlock cancelBlock;
/// 显示
- (void)show;
- (instancetype)initWithFrame:(CGRect)frame andSureBlock:(dateBlock)sure andCancelBlock:(cancelBlock)cancel;

@end

