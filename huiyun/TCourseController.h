//
//  CourseViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HYCourseCell.h"
#import "LoadingView.h"
#import "RequestTools.h"
#import "TCourseModel.h"
#import "TCourseDetailController.h"
@interface TCourseController: UIViewController<UITableViewDataSource,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UIAlertViewDelegate,UISearchBarDelegate,UISearchResultsUpdating>


-(void)FilterContentForSearchText:(NSString *)searchText scope:(NSUInteger)scope;


@end
