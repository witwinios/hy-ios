//
//  SDepartModel.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDepartRecordModel.h"
@interface SDepartModel : NSObject
@property (strong, nonatomic) NSNumber *SdepartStudentId;
@property (strong, nonatomic) NSString *SdepartStudentName;
@property (strong, nonatomic) NSNumber *SDepartRecordId;
@property (strong, nonatomic) NSString *SDepartName;
@property (strong, nonatomic) NSString *SDepartStatus;
@property (strong, nonatomic) NSNumber *SDepartStartTime;
@property (strong, nonatomic) NSNumber *SDepartEndTime;
@property (strong, nonatomic) NSString *SDepartPlace;
@property (strong, nonatomic) NSString *SDepartRecordName;
@property (strong, nonatomic) NSArray *SDepartRecordArray;
@end
