//
//  SCaseRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SCaseRecordController.h"
#import "THDatePickerView.h"
@interface SCaseRecordController ()
{
    NSString *current_date;
    UIView *btnBack;
}

@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;
@end
@implementation SCaseRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    
    if (![self.myModel isKindOfClass:[NSNull class]] || self.model != nil) {
        self.diseaseNameField.text = self.myModel.caseOne;
        self.diseaseNumField.text = self.myModel.caseThree;
        self.mainText.text = self.myModel.mainIos;
        self.secodText.text = self.myModel.secondIos;
        if (self.myModel.caseFive.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseFive];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter:self.myModel.caseFive.stringValue] forState:0];
        }
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillShowNotification object:nil];
    //
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.mainText.layer.cornerRadius = 4;
    self.mainText.layer.masksToBounds = YES;
    self.secodText.layer.cornerRadius = 4;
    self.secodText.layer.masksToBounds = YES;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [self.dateBtn setTitle:date forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"病例记录";
    [backNavigation addSubview:titleLab];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doneButtonshow: (NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    
    btnBack = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight-keyBoardHeight-35, Swidth, 35)];
    btnBack.backgroundColor = UIColorFromHex(0xDBDBDB);
    UIButton *doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(Swidth-70, 0, 70, 35);
    [doneButton setTitle:@"完成" forState: UIControlStateNormal];
    [doneButton addTarget:self action:@selector(hideKeyboard:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor blueColor] forState:0];
    [btnBack addSubview:doneButton];
    [self.view addSubview:btnBack];
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
}
- (void)resignFirstRespond{
    [self.diseaseNameField resignFirstResponder];
    [self.diseaseNumField resignFirstResponder];
    [self.mainText resignFirstResponder];
    [self.secodText resignFirstResponder];
}
#pragma -delegate
- (void)saveAction{
    if ([_diseaseNameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名必填~" places:0 toView:nil];
        return;
    }
    if ([_diseaseNumField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号必填~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间必填~" places:0 toView:nil];
        return;
    }
    if ([self.mainText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"主要诊断必填~" places:0 toView:nil];
        return;
    }
    if ([self.secodText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"次要诊断必填~" places:0 toView:nil];
        return;
    }
    //添加请求http://www.hzwitwin.cn:8080/witwin-ctts-web/studentClinicalRotationRecords/648/medicalCaseRecords
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/medicalCaseRecords",LocalIP,departModel.SDepartRecordId];
    NSLog(@"request=%@",requestUrl);
    NSDictionary *params = @{@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"requirementId":_model.CaseReuestModelRequirementId};
    NSLog(@"params=%@",params);
    [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"创建成功~" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
    }];

}
- (void)updateAction{
    if ([_diseaseNameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名必填~" places:0 toView:nil];
        return;
    }
    if ([_diseaseNumField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号必填~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间必填~" places:0 toView:nil];
        return;
    }
    if ([self.mainText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"主要诊断必填~" places:0 toView:nil];
        return;
    }
    if ([self.secodText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"次要诊断必填~" places:0 toView:nil];
        return;
    }
    //添加请求http://www.hzwitwin.cn:8080/witwin-ctts-web/studentClinicalRotationRecords/648/medicalCaseRecords
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/medicalCaseRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
    NSLog(@"request=%@",requestUrl);
    NSDictionary *params = @{@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"requirementId":self.myModel.requestmentId};
    NSLog(@"params=%@",params);
    [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
    [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSLog(@"result=%@",result);
            [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
            self.myModel = nil;
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
    }];

}
- (IBAction)saveAction:(id)sender {
    NSLog(@"myModel=%@",self.myModel);
    if ([self.myModel isKindOfClass:[NSNull class]] || self.myModel == nil) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}

- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
}
@end
