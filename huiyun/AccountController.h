//
//  AccountController.h
//  yun
//
//  Created by MacAir on 2017/8/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountEntity.h"
#import "AccountCell1.h"
#import "AccountCell2.h"
#import "DegreeController.h"
#import "BlankCardController.h"
#import "ImageChangeController.h"
#import "UIImageView+WebCache.h"
#import "WXPPickerView.h"

typedef void (^imgBlock)(UIImage *img);
typedef enum
{
    EditYes = 0,
    EditNo
}Edit;//枚举

@interface AccountController : UIViewController<UITableViewDelegate,UITableViewDataSource,PickerViewOneDelegate>
@property (strong, nonatomic) AccountEntity *entity;
@property (strong, nonatomic) imgBlock selfBlock;
//@property (strong, nonatomic) UIImage
@end
