//
//  DetailCellTwo.h
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCellTwo : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UITextView *contentText;
@end
