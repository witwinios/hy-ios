//
//  ButtonManeger.m
//  yun
//
//  Created by MacAir on 2017/7/3.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ButtonManeger.h"

@implementation ButtonManeger
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.viewArray = [NSMutableArray new];
    }
    return self;
}
- (void)add:(UIButton *)btn{
    [self.viewArray addObject:btn];
}
- (void)changeStatus:(long)index{
    for (int i=0; i<self.viewArray.count; i++) {
        if (i != index) {
            UIButton *button = self.viewArray[i];
            button.selected = NO;
        }
    }
    
}
@end
