//
//  MainViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "MainViewController.h"
#import "MBProgressHUD.h"

#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]

@interface MainViewController ()
{
    AccountEntity *accountEntity;
    
    UITableView *table;
    //
    NSArray *indexArray;
}
@end

@implementation MainViewController
+(id)shareObject{
    static MainViewController *mainVC = nil;
    if (mainVC == nil) {
        mainVC = [MainViewController new];
    }
    return mainVC;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setObj];
    [self getCurrentVersion];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)getCurrentVersion{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *current_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    [RequestTools simpleRequestUrl:@"http://test.hzwitwin.cn:9010/witwin-cspt-web/appVersion/latestIOSAppVersion" Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSString *last_Version = result[@"responseBody"][@"appVersionNumber"];
        NSString *des_version = result[@"responseBody"][@"appVersionDescription"];
        NSString *fomatStr = [des_version stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
        BOOL s = ([current_Version compare:last_Version] == NSOrderedAscending);
        if (s > 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"发现新版本,是否更新?" message:fomatStr preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/app/id1227386925"]];
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"下次在说" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}
- (void)setObj{
    indexArray = @[@"课程安排",@"考试安排",@"自测安排",@"错题集",@"请假",@"观看直播",@"轮转记录",@"在线考试"];
    NSArray *imageArray = @[@"签到",@"witwin_qianchu"];
    NSArray *array = @[@"签到",@"签出"];
    //
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *headerView = [UIImageView new];
    headerView.userInteractionEnabled = YES;
    headerView.frame = CGRectMake(0, 0,WIDTH, HEIGHT/3);
    headerView.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:headerView];
    
    //用户
    UIButton *userBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [userBtn setImage:[UIImage imageNamed:@"user"] forState:0];
    userBtn.frame = CGRectMake(Swidth-50, 24, 40, 40);
    [userBtn addTarget:self action:@selector(accountAction) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:userBtn];
    
    UIView *backView = [UIView new];
    backView.frame = CGRectMake(0, headerView.frame.size.height/5, WIDTH, headerView.frame.size.height/5*3);
    [headerView addSubview:backView];
    for (int i = 0; i< 2; i ++) {
        UIView *view = [UIView new];
        CGPoint point;
        view.frame = CGRectMake(WIDTH/2*i, 0, WIDTH/2, backView.frame.size.height);
        [backView addSubview:view];
        if (i == 0) {
            point = view.center;
        }
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        UILabel *label = [UILabel new];
        button.frame = CGRectMake(0, 5, 50, 50);
        label.frame = CGRectMake(0, 56, 50, 20);
        button.center = point;
        button.tag = 100+i;
        [button addTarget:self action:@selector(titleClicked:) forControlEvents:UIControlEventTouchUpInside];
        label.center = CGPointMake(point.x, button.center.y+36);
        label.textAlignment = 1;
        [button setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        label.adjustsFontSizeToFitWidth = YES;
        label.textColor = [UIColor whiteColor];
        label.text = array[i];
        [view addSubview:button];
        [view addSubview:label];
    }
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, HEIGHT/3, WIDTH, HEIGHT/3*2) style:UITableViewStylePlain];
    table.delegate = self;
    table.dataSource = self;
    [table registerNib:[UINib nibWithNibName:@"MainVcCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
    table.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.bounces = NO;
    [self.view addSubview:table];
}
#pragma -Action
- (void)titleClicked:(UIButton *)button{
    // 1、 获取摄像设备
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (status == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        ScanController *scanVC = [ScanController new];
                        scanVC.btnIndex = [NSNumber numberWithLong:button.tag-100];
                        [self.navigationController pushViewController:scanVC animated:YES];
                    });
                    NSLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
                } else {
                    NSLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
                }
            }];
        } else if (status == AVAuthorizationStatusAuthorized) { // 用户允许当前应用访问相机
            ScanController *scanVC = [ScanController new];
            scanVC.btnIndex = [NSNumber numberWithLong:button.tag-100];
            [self.navigationController pushViewController:scanVC animated:YES];
        } else if (status == AVAuthorizationStatusDenied) { // 用户拒绝当前应用访问相机
            UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请去-> [设置 - 隐私 - 相机 - SGQRCodeExample] 打开访问开关" preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alertC addAction:alertA];
            [self presentViewController:alertC animated:YES completion:nil];
            
        } else if (status == AVAuthorizationStatusRestricted) {
            NSLog(@"因为系统原因, 无法访问相册");
        }
    } else {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"未检测到您的摄像头" preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertC addAction:alertA];
        [self presentViewController:alertC animated:YES completion:nil];
    }
}
- (void)accountAction{
    AccountController *accountVC = [AccountController new];
    [self.navigationController pushViewController:accountVC animated:YES];
}
//协议方法
#pragma tableview协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return indexArray.count-1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row+1) {
        case 1:
            self.courseVC = [HYCourseViewController new];
            [self.navigationController pushViewController:self.courseVC animated:YES];
            break;
        case 2:
            self.examVC = [ExamController new];
            [self.navigationController pushViewController:self.examVC animated:YES];
            break;
        case 3:
            self.selftestVC = [SelfTestViewController new];
            [self.navigationController pushViewController:self.selftestVC animated:YES];
            break;
        case 4:
            self.errorVC = [ErrorController new];
            [self.navigationController pushViewController:self.errorVC animated:YES];
            break;
        case 5:
            self.leaveVC = [LeaveController new];
            [self.navigationController pushViewController:self.leaveVC animated:YES];
            break;
        case 6:
            //拼接参数
            self.meetVC = [MeetDisplayController new];
            [self.navigationController pushViewController:self.meetVC animated:YES];
            break;
        case 7:
            self.sdepartVC = [SdepartController new];
            [self.navigationController pushViewController:self.sdepartVC animated:YES];
            break;
        case 8://在线考试
            self.testVC = [TestScheduleController new];
            [self.navigationController pushViewController:self.testVC animated:YES];
            break;
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainVcCell *mainCell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    mainCell.selectionStyle = UITableViewCellSelectionStyleNone;
    mainCell.title.text = indexArray[indexPath.row];
    if (indexPath.row == 6 || indexPath.row == 7) {
        mainCell.imgView.image = [UIImage imageNamed:indexArray[indexPath.row-2]];
    }else{
        mainCell.imgView.image = [UIImage imageNamed:indexArray[indexPath.row]];
    }
    return mainCell;
}
@end

