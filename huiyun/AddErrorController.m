//
//  AddErrorController.m
//  yun
//
//  Created by MacAir on 2017/7/2.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "AddErrorController.h"

@interface AddErrorController ()
{
    UITextField *nameField;
    UITextView *desView;
    //保存的testId
    NSNumber *testId;
    NSMutableArray *misArray;
    NSNumber *paperId;
    NSNumber *recordId;
    //
}
@end

@implementation AddErrorController

- (void)viewDidLoad {
    [super viewDidLoad];
    misArray = [NSMutableArray new];
    
    [self setNav];
    [self setUI];
}
- (void)setUI{
    UILabel *nameLab = [UILabel new];
    nameLab.frame = CGRectMake(0, 69, 80, 30);
    nameLab.text = @"测试名称:";
    nameLab.textAlignment = 1;
    nameLab.font = [UIFont systemFontOfSize:15];
    nameLab.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:nameLab];
    nameField = [UITextField new];
    nameField.frame = CGRectMake(82, 69, Swidth-84, 30);
    nameField.borderStyle = UITextBorderStyleBezel;
    nameField.layer.borderColor = [[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]CGColor];
    nameField.layer.borderWidth = 1.0;
    nameLab.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:nameField];
    //
    UILabel *desLab = [UILabel new];
    desLab.frame = CGRectMake(0, 105, 80, 30);
    desLab.text = @"测试说明:";
    desLab.textAlignment = 1;
    desLab.font = [UIFont systemFontOfSize:15];
    desLab.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:desLab];
    desView = [UITextView new];
    desView.frame = CGRectMake(82, 105, Swidth-84, 120);
    desView.layer.borderColor = [[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]CGColor];
    desView.layer.borderWidth = 1.0;
    [desView.layer setMasksToBounds:YES];
    desView.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:desView];
    //新增按钮
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-60, 27.5, 60, 35);
    [rightBtn addTarget:self action:@selector(addTest:) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.layer.cornerRadius = 5;
    rightBtn.layer.masksToBounds = YES;
    rightBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    rightBtn.layer.borderWidth = 1;
    [rightBtn setTitle:@"保存" forState:0];
    [backNavigation addSubview:rightBtn];

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"错题练习";
    [backNavigation addSubview:titleLab];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [nameField resignFirstResponder];
    [desView resignFirstResponder];
}
//创建错题安排
- (void)loadTest{
    [MBProgressHUD showHUDAndMessage:@"创建中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/selfTests",LocalIP];
    NSDictionary *params = @{@"testName":nameField.text,@"description":desView.text,@"selfType":@"MISTAKE_TEST"};
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            testId = [[result objectForKey:@"responseBody"] objectForKey:@"testId"];
            [self loadErrorArray];
        }else {
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"新增失败" places:0 toView:nil];
        }

    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"新增失败" places:0 toView:nil];
    }];
}
//制作错题试卷
- (void)loadErrorArray{
    [misArray removeAllObjects];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/mistakeCollections?studentId=%@&pageSize=99",LocalIP,LocalUserId];
    NSLog(@"----%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSArray *mistakeArray = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        if (mistakeArray.count > 50) {
            int s = (int)mistakeArray.count;
            for (int i=0; i<50; i++) {
                int k = random()% s;
                NSDictionary *misDic = mistakeArray[k];
                [misArray addObject:[misDic objectForKey:@"mistakeId"]];
            }
        }else{
            for (int i=0; i<mistakeArray.count; i++) {
                NSDictionary *misDic = mistakeArray[i];
                [misArray addObject:[misDic objectForKey:@"mistakeId"]];
            }
        }
        if (misArray.count == 0) {
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"你暂时没有错题集!" places:0 toView:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self makePaper];
        }
    }];

}
- (void)makePaper{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/mistakeCollections/selfTest/%@/createMistakeCollectionPaper",LocalIP,testId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:misArray Success:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        [MBProgressHUD hideHUDForView:nil];
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            paperId = [result objectForKey:@"responseBody"];
            [self loadRecord];
        }else{
            [MBProgressHUD showToastAndMessage:@"新增失败!" places:0 toView:nil];
        }
        
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"新增失败!" places:0 toView:nil];
    }];

}
- (void)loadRecord{
    NSString *requestUrl = [NSString stringWithFormat:[NSString stringWithFormat:@"%@/selfTests/%@/testPapers?pageStart=1&pageSize=5",LocalIP,testId],LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSDictionary *dictionary = array[0];
            recordId = [dictionary objectForKey:@"recordId"];
            //
            MakePaperController *makeVC = [MakePaperController new];
            makeVC.fromVC = @"error";
            makeVC.testId = testId;
            makeVC.paperId = paperId;
            makeVC.recordId = recordId;
            [self.navigationController pushViewController:makeVC animated:YES];
        }else{
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"新增失败!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"新增失败!" places:0 toView:nil];
    }];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)addTest:(UIButton *)btn{
    if ([nameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"测试名称不能为空!" places:0 toView:nil];
    }else {
        [nameField resignFirstResponder];
        [desView resignFirstResponder];
        //添加测试
        [self loadTest];
    }
}

@end
