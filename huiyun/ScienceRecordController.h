//
//  ScienceRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScienceRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UITextField *twoField;
@property (weak, nonatomic) IBOutlet UITextField *threeField;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)dateAction:(id)sender;
- (IBAction)fileAction:(id)sender;
- (IBAction)saveAction:(id)sender;
@property (strong, nonatomic)UIImagePickerController *imagePickController;
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;
@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseSienceModel *myModel;
@end
