//
//  SelfPaperController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SelfPaperController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface SelfPaperController ()
{
    UITableView *tablePaper;
    //数据源
    NSMutableArray *dataArray;
}
@end

@implementation SelfPaperController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [tablePaper headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self objectInit];
    [self buildNavigation];
    [self createTable];
    [self setUpRefresh];
}
- (void)buildNavigation{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if (![_fromVC isEqualToString:@"error"]) {
        CustomButton *rightBtn = [CustomButton new];
        rightBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"继续出卷" forState:0];
        [rightBtn addTarget:self action:@selector(goOnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [backNavigation addSubview:rightBtn];
    }
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"试卷";
    [backNavigation addSubview:titleLab];
    //
}
- (void)setUpRefresh{
    [tablePaper addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    tablePaper.headerPullToRefreshText = @"下拉刷新";
    tablePaper.headerReleaseToRefreshText = @"松开进行刷新";
    tablePaper.headerRefreshingText = @"刷新中。。。";
    
    tablePaper.footerPullToRefreshText = @"上拉加载";
    tablePaper.footerReleaseToRefreshText = @"松开进行加载";
    tablePaper.footerRefreshingText = @"加载中。。。";
}
- (void)objectInit{
    self.view.backgroundColor = [UIColor whiteColor];
    dataArray = [NSMutableArray new];
}
- (void)createTable{
    tablePaper = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    tablePaper.delegate = self;
    tablePaper.dataSource = self;
    tablePaper.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablePaper registerNib:[UINib nibWithNibName:@"PaperCell" bundle:nil] forCellReuseIdentifier:@"paperCell"];
    [self.view addSubview:tablePaper];
}
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:[NSString stringWithFormat:@"%@/selfTests/%@/testPapers?pageStart=1&pageSize=5",LocalIP,self.model.testId],LocalUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
        [tablePaper headerEndRefreshing];
        //移除所有元素
        [dataArray removeAllObjects];
        //获取数据源
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count == 0) {
                [MBProgressHUD showToastAndMessage:@"暂无试卷!" places:0 toView:nil];
                return ;
            }
            for (int i=0; i<array.count; i++) {
                NSDictionary *dictionary = [array objectAtIndex:i];
                PaperCellModel *model = [PaperCellModel new];
                model.paperID = [dictionary objectForKey:@"paperId"];
                model.recordID = [dictionary objectForKey:@"recordId"];
                model.paperSubject = [dictionary objectForKey:@"subjectName"];
                model.paperName = [dictionary objectForKey:@"paperName"];
                model.creatTime = [[Maneger shareObject] timeFormatter:[[dictionary objectForKey:@"createdTime"] stringValue]];
                model.paperCategory = [dictionary objectForKey:@"categoryName"];
                model.totalScore = [dictionary objectForKey:@"totalScoreValue"];
                model.paperDes = [dictionary objectForKey:@"description"];
                model.getScore = dictionary[@"totalScore"];
                model.paperStatus = [dictionary objectForKey:@"recordStatus"];
                [dataArray addObject:model];
            }
            [tablePaper reloadData];
        }
        else{
            [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
        }

    } failed:^(NSString *result) {
        [tablePaper headerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
#pragma Action
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)goOnAction:(UIButton *)btn{
    if ([self.model.testStatus isEqualToString:@"已完成"]) {
        [MBProgressHUD showToastAndMessage:@"测试已经完成，不能继续出卷!" places:0 toView:nil];
    }else{
        AddPaperController *addVC = [AddPaperController new];
        addVC.testID = self.model.testId.stringValue;
        addVC.superTab = @"selfPaperVC";
        [self.navigationController pushViewController:addVC animated:YES];
    }
}
#pragma 协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MakePaperController *detailVC = [MakePaperController new];
    detailVC.paperModel = [dataArray objectAtIndex:indexPath.row];
    detailVC.testModel = self.model;
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PaperCell *cell = [tablePaper dequeueReusableCellWithIdentifier:@"paperCell"];
    PaperCellModel *model = [dataArray objectAtIndex:indexPath.row];
    cell.paperCellName.text = model.paperName;
    cell.paperCellSubject.text = model.creatTime;
    cell.paperDes.text = model.paperDes;
    cell.paperStatus.text = model.paperStatus;
    cell.getScoreLab.text = [NSString stringWithFormat:@"得分: %@",model.getScore];
    cell.paperCellCategory.text = model.paperCategory;
    cell.totalScore.text = model.totalScore.stringValue;
    return cell;
}
#pragma -UIAlertViewDelegate
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
