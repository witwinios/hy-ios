//
//  AddStar.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AddStar.h"

@interface AddStar(){
    
    NSInteger *AddTag;
    
    NSInteger *temp;
    
}

@property(nonatomic,strong)UIView *FullMaskView;
@property(nonatomic) float starWidth;
@property(nonatomic) float mid;


@end

@implementation AddStar


-(NSNumber *)Score{
    
    if(!_Score){
        _Score=@5;
    }
    return _Score;
    
}

-(void)setScore:(NSNumber *)Score{
    
    
    _Score=Score;
    
    int starCount=Score.intValue;
    float others=Score.floatValue-starCount;
    
    float width=starCount *(self.starWidth+self.mid);
    
    if(others>0){
        width +=(others * self.starWidth);
    }
    
    if (self.animation) {
        [UIView animateWithDuration:0.3 animations:^{
            self.FullMaskView.frame=CGRectMake(self.FullMaskView.frame.origin.x,
                                               self.FullMaskView.frame.origin.y,
                                               width,
                                               self.FullMaskView.frame.size.height);
        }];
    }else{
        self.FullMaskView.frame=CGRectMake(self.FullMaskView.frame.origin.x,
                                           self.FullMaskView.frame.origin.y,
                                           width,
                                           self.FullMaskView.frame.size.height);
    }
}

-(BOOL)canBeChoose{
    return _canChoose;
}

-(void)setCanChoose:(BOOL)CanChoose{
    _canChoose=CanChoose;
    
    for (int i=901; i<=905; i++) {
        UIButton *Btn=(UIButton *)[self viewWithTag:i];
        Btn.enabled=CanChoose;
    }
}


-(BOOL)animation{
    return _canAnimation;
}

-(void)setAnimation:(BOOL)animation{
    _canAnimation=animation;
}

-(UIImage *)starImage_Full{
    if (!startImage_Full) {
        startImage_Full=[UIImage imageNamed:@"YQStarViewFull.png"];
        
    }
    return startImage_Full;
}


-(void)setStarImage_Full:(UIImage *)image{
    startImage_Full=image;
    [self setup];
}


-(UIImage *)starImage_Empty{
    if (!startImage_Empty) {
        startImage_Empty=[UIImage imageNamed:@"YQStarViewEmpty.png"];
    }
    return startImage_Empty;
}

-(void)setStarImage_Empty:(UIImage *)image{
    startImage_Empty=image;
    [self setup];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    [self setup];
    return self;
}



#pragma make 初始化:
-(void)setup{
    
    
    self.backgroundColor=[UIColor clearColor];
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    self.FullMaskView=[[UIView alloc]init];
    
    float maskWidth=0;
    
    if(self.Score.floatValue<5){
        maskWidth=self.bounds.size.width*(self.Score.floatValue/5);
    }else{
        maskWidth=self.bounds.size.width;
    }
    
    self.FullMaskView.frame=CGRectMake(0, 0, maskWidth, self.bounds.size.height);
    
    self.FullMaskView.layer.masksToBounds=YES;
    
    //确定star的大小
    if(self.bounds.size.width <= self.bounds.size.height *5){
        
        self.mid=0;
        self.starWidth=self.bounds.size.width/5;
        
    }else{
        
        self.starWidth=self.bounds.size.height;
        self.mid=(self.bounds.size.width -(self.bounds.size.height *5 ))/4;
        
    }
    
    for (int i=0; i<5; i++) {
        
        //绘制空星
        UIImageView *IMGV_Empty=[[UIImageView alloc]initWithFrame:CGRectMake(i * (self.starWidth+self.mid),
                                                                             0,
                                                                             self.starWidth,
                                                                             self.starWidth)];
        IMGV_Empty.contentMode=UIViewContentModeScaleAspectFit;
        IMGV_Empty.image=self.starImage_Empty;
        [self addSubview:IMGV_Empty];
        
        //实星
        UIImageView *IMGV_Full = [[UIImageView alloc]initWithFrame:IMGV_Empty.frame];
        IMGV_Full.contentMode = UIViewContentModeScaleAspectFill;
        IMGV_Full.image = self.starImage_Full;
        
        [self.FullMaskView addSubview:IMGV_Full];
        
  
    }
    
    [self addSubview:self.FullMaskView];
    
    //按钮:
    for (int i=0; i<5; i++) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(i * (self.starWidth+self.mid) - (self.mid/2), 0, self.starWidth+self.mid, self.starWidth);
        
        btn.tag=901+i;
        AddTag=btn.tag;
        [btn addTarget:self action:@selector(ChoosedStar:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        
        if (!self.canBeChoose) {
            btn.enabled=NO;
        }
    }
    
     NSLog(@"tag:%d",(int)AddTag);
}

//选择星星:
-(void)ChoosedStar:(UIButton *)BTN{
    
    

        NSInteger tag=BTN.tag;
        tag -=900;
        if (self.animation) {
            [UIView animateWithDuration:0.3 animations:^{
                self.FullMaskView.frame = CGRectMake(self.FullMaskView.frame.origin.x,
                                                     self.FullMaskView.frame.origin.y,
                                                     self.bounds.size.width*(tag/5.0),
                                                     self.FullMaskView.frame.size.height);
            }];
            
        }else{
            self.FullMaskView.frame = CGRectMake(self.FullMaskView.frame.origin.x,
                                                 self.FullMaskView.frame.origin.y,
                                                 self.bounds.size.width*(tag/5.0),
                                                 self.FullMaskView.frame.size.height);
        }
        _Score = [NSNumber numberWithInteger:tag];
        [self.delegate ChooseStarNum:(int)tag*2];
   
        
  
    
    

}






@end
