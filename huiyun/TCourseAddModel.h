//
//  TCourseAddModel.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCourseAddModel : NSObject


@property(strong,nonatomic)NSString *TCourseName; //课程名称
@property(strong,nonatomic)NSNumber *TCourseId;   //课程Id

@property(nonatomic,strong)NSNumber *CategoryId; //课程类别Id
@property(nonatomic,strong)NSNumber *subjectId;  //课程科目Id

@property(strong,nonatomic)NSNumber *StartTime; //上课时间
@property(strong,nonatomic)NSNumber *EndTime; //结束时间

@property (strong,nonatomic)NSString *TCourseLocation; //上课地址
@property(strong,nonatomic)NSNumber *ClassRoomId; //上课地点id

@property(strong,nonatomic)NSString *Description; //课程描述：


@property(strong,nonatomic)NSString *courseSubjec; //课程类型





@end
