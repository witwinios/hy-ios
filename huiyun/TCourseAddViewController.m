//
//  TCourseAddViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseAddViewController.h"
#import "WPhotoViewController.h"
#import "TCourseMoldViewController.h"
#import "TCourseAddModel.h"
#import "Maneger.h"
#import "RequestTools.h"

#define phoneScale [UIScreen mainScreen].bounds.size.width/720.0
@interface TCourseAddViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,PassingValueDelegate>{
    
    UIView *BackView;
    
    UIButton *okBtn;
    UIButton *noBtn;
    
    UIDatePicker *DatePick;
    UIPickerView *Picker;
    
    NSMutableArray *PickDateLocation;
    NSMutableArray *PickDateSubject;
    
    NSString *DateString;
    
    UITableView *_tableView;
    NSMutableArray *_photosArr;
    
    NSMutableArray *LocationID;
    NSString *LocationString;
    
    NSMutableArray *SubjectID;
    NSString *SubjectString;
    

    NSString *StrateTimeLong;
    NSString *EndTimeLong;
    
    NSString *MoldString;
    
    NSMutableArray *FileID;
    NSMutableArray *FileString;
}

@end

@implementation TCourseAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //加载数据
    [self loadData];
    
    
     [self createTableView];
    
    [self setNav];
 
    [self setUI];
    

    
    
   
    
}
-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.CourseName resignFirstResponder];
    [self.CourExplain resignFirstResponder];
}

#pragma  mark 加载数据
-(void)loadData{
    
    //获取上课地点：
    NSString *UrlLocation=[NSString stringWithFormat:@"%@/rooms",LocalIP];
    [RequestTools RequestWithURL:UrlLocation Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程地址" andCurentVC:self];
            }else{
                
                PickDateLocation=[NSMutableArray new];
                LocationID=[NSMutableArray new];
                
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    [PickDateLocation addObject:[dic objectForKey:@"roomName"]];
                    
                    [LocationID addObject:[dic objectForKey:@"roomId"]];
                
                    
                    
                    
                    
                    [Picker reloadAllComponents];
                }
                NSLog(@"LocationID:%@",LocationID);
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
    
    //获取课程分类:
    NSString *UrlMold=[NSString stringWithFormat:@"%@/courseCategories?pageSize=200",LocalIP];
    [RequestTools RequestWithURL:UrlMold Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程分类" andCurentVC:self];
                
            }else{
                
                PickDateSubject=[NSMutableArray new];
                SubjectID=[NSMutableArray new];
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    
                    [PickDateSubject addObject:[dic objectForKey:@"categoryName"]];
                    
                     [SubjectID addObject:[dic objectForKey:@"categoryId"]];
 
                    [Picker reloadAllComponents];
                }
                NSLog(@"SubjectID:%@",SubjectID);
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];

    
    //获取附件信息：
    NSString *FileUrl=[NSString stringWithFormat:@"%@/files",LocalIP];
    [RequestTools RequestWithURL:FileUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程分类" andCurentVC:self];
                
            }else{
                
                
                FileID=[NSMutableArray new];
                FileString=[NSMutableArray new];
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    [FileID addObject:[dic objectForKey:@"fileId"]];
                    [FileString addObject:[dic objectForKey:@"fileName"]];
                }
                NSLog(@"FileID:%@",FileID);
                NSLog(@"FileString:%@",FileString);
            };
            
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];

    
    
}


- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 60)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 25, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-100, 25, 100, 25);
   // [rightBtn setImage:[UIImage imageNamed:@"SelectBtn:"] forState:UIControlStateNormal];
    [rightBtn setTitle:@"保存" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(SelectBtn:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 22, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程安排";
    [backNavigation addSubview:titleLab];
}

-(void)setUI{
    
    //课程名称；
    
    self.CourseName.borderStyle=UITextBorderStyleRoundedRect; //边框样式
    self.CourseName.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.CourseName.clearsOnBeginEditing = YES;
    self.CourseName.keyboardType=UIKeyboardTypeDefault;
    self.CourseName.returnKeyType=UIReturnKeyDefault;
    self.CourseName.delegate = self;
    self.CourseName.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseName.layer.borderWidth=1;

    
    
    //课程科目：
    self.CourseSubject.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseSubject.layer.borderWidth=1;

    
    //课程分类；
   
    self.CourseMold.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseMold.layer.borderWidth=1;
    
    
    //课程描述：
    self.CourExplain.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourExplain.layer.borderWidth=1;
    self.CourExplain.delegate=self;
    
    //上课时间：

    self.StartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.StartTime.layer.borderWidth=1;
    
    
   
  
    self.EndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.EndTime.layer.borderWidth=1;

    
    //上课地址：

    self.CourseLocation.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseLocation.layer.borderWidth=1;
 //   self.CourseLocation.delegate = self;
    

    //附件：
}




#pragma mark 回退
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 课程名称：
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.CourseName  resignFirstResponder];    //主要是[receiver resignFirstResponder]在哪调用就能把receiver对应的键盘往下收
    return YES;
}



#pragma mark 课程分类：
- (IBAction)AddMoldAction:(id)sender {
    
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(LineX(0),LineY(430), Swidth, LineY(200))];
    BackView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    okBtn.backgroundColor=[UIColor whiteColor];
    okBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    okBtn.layer.borderWidth=1;
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn4:) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    noBtn.backgroundColor=[UIColor whiteColor];
    noBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    noBtn.layer.borderWidth=1;
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:noBtn];
    
    
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 30, BackView.frame.size.width, 150)];
    Picker.tag=0;
    Picker.dataSource=self;
    Picker.delegate=self;
    if (Picker.tag == 0) {
        DateString = PickDateSubject[0];
        SubjectString=SubjectID[0];
    }
    
    [BackView addSubview:Picker];
    
    [self.view addSubview:BackView];
    
    
}

-(void)setOkBtn4:(id)sender{
    
    if(DateString == nil){

        [self.CourseMold setTitle:@" " forState:UIControlStateNormal];
        
        
    BackView.hidden=YES;
    }else{
    
  [self.CourseMold setTitle:DateString forState:UIControlStateNormal];
    BackView.hidden=YES;
    }
}


#pragma mark 课程科目：


- (IBAction)AddSubjectAction:(id)sender {
    
    TCourseMoldViewController *Mold=[[TCourseMoldViewController alloc]init];
    Mold.delegate=self;
    [self.navigationController pushViewController:Mold animated:YES];
    
}



-(void)text:(NSString *)str dateID:(NSString *)dateID{
    NSLog(@"%@",str);
    [self.CourseSubject setTitle:str forState:UIControlStateNormal];
    MoldString=dateID;

}

#pragma mark 课程描述：
-(void)textViewDidChange:(UITextView *)textView{
    self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/255", (unsigned long)textView.text.length];
    //字数限制操作
    if (textView.text.length >= 256) {
        textView.text = [textView.text substringToIndex:256];
        self.stirngLenghLabel.text = @"255/255";
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark 上课时间:


- (IBAction)StartAction:(id)sender {
    
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    BackView=[[UIView alloc]initWithFrame:CGRectMake(LineX(0),LineY(430), Swidth, LineY(200))];
    BackView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    okBtn.backgroundColor=[UIColor whiteColor];
    okBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    okBtn.layer.borderWidth=1;
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn:) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    noBtn.backgroundColor=[UIColor whiteColor];
    noBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    noBtn.layer.borderWidth=1;
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:noBtn];
    
    DatePick=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 30, BackView.frame.size.width, 150)];
    DatePick.locale=[NSLocale localeWithLocaleIdentifier:@"zh-Hans"];
    DatePick.datePickerMode=UIDatePickerModeDateAndTime;
    [BackView addSubview:DatePick];
    
    [self.view addSubview:BackView];
    
    
}




-(void)setOkBtn:(id)sender{
    
  NSDate  *theDate=DatePick.date;
   
    StrateTimeLong=[NSString stringWithFormat:@"%ld",(long)[theDate timeIntervalSince1970] *1000];
    NSLog(@"StrateTimeLong:%@",StrateTimeLong);
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat=@"YY/MM/dd HH:mm";
    
    
    [self.StartTime setTitle:[dateFormatter stringFromDate:theDate] forState:UIControlStateNormal];

    
    if (StrateTimeLong == EndTimeLong) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"开始时间与结束时间一致，请重新选择！ "
                                                  delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
        [alert show];
    }else{
        BackView.hidden=YES;
    }

}




-(void)setNoBtn{
    BackView.hidden=YES;
}



#pragma mark 结束时间：

- (IBAction)EndAction:(id)sender {
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(LineX(0),LineY(430), Swidth, LineY(200))];
    BackView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    okBtn.backgroundColor=[UIColor whiteColor];
    okBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    okBtn.layer.borderWidth=1;
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn2:) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    noBtn.backgroundColor=[UIColor whiteColor];
    noBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    noBtn.layer.borderWidth=1;
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:noBtn];
    
    DatePick=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 30, BackView.frame.size.width, 150)];
    DatePick.locale=[NSLocale localeWithLocaleIdentifier:@"zh-Hans"];
    DatePick.datePickerMode=UIDatePickerModeDateAndTime;
    [BackView addSubview:DatePick];
    
    [self.view addSubview:BackView];
    
}



-(void)setOkBtn2:(id)sender{
    
   NSDate *theDate=DatePick.date;
    EndTimeLong=[NSString stringWithFormat:@"%ld",(long)[theDate timeIntervalSince1970] * 1000];
    NSLog(@"EndTimeLong:%@", EndTimeLong);
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat=@"YY/MM/dd HH:mm";
    [self.EndTime setTitle:[dateFormatter stringFromDate:theDate] forState:UIControlStateNormal];
    
    if (EndTimeLong == StrateTimeLong) {
       
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"结束时间与开始时间一致，请重新选择！"
                                                      delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
        [alert show];

    }else{
        BackView.hidden=YES;
    }
}


#pragma mark 上课地址：

- (IBAction)AddLocationAction:(id)sender {
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(LineX(0),LineY(430), Swidth, LineY(200))];
    BackView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    okBtn.backgroundColor=[UIColor whiteColor];
    okBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    okBtn.layer.borderWidth=1;
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn3:) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    noBtn.backgroundColor=[UIColor whiteColor];
    noBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    noBtn.layer.borderWidth=1;
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [BackView addSubview:noBtn];
    
    
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(100, 20, BackView.frame.size.width-200, 150)];
    Picker.tag=1;
    if (Picker.tag==1) {
        DateString = PickDateLocation[0];
        LocationString=LocationID[0];
    }
    Picker.delegate=self;
    Picker.dataSource=self;
    
    
    [BackView addSubview:Picker];
    
    [self.view addSubview:BackView];
}



//返回数据列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
    if(Picker.tag==0){
        return [PickDateSubject count];
    }else{
    
    return [PickDateLocation count];
    }
}
//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
     if(Picker.tag==0){
         return [PickDateSubject objectAtIndex:row];
     }else{
        return [PickDateLocation objectAtIndex:row];
     }
}
//返回值：
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    if(Picker.tag==0){
        NSLog(@"%@",[PickDateSubject objectAtIndex:row]);
        DateString = [PickDateSubject objectAtIndex:row];
        
        SubjectString=[SubjectID objectAtIndex:row];
        NSLog(@"%@",SubjectString);
    }else{
        NSLog(@"%@",[PickDateLocation objectAtIndex:row]);
        DateString = [PickDateLocation objectAtIndex:row];
        
        LocationString=[LocationID objectAtIndex:row];
        NSLog(@"%@",LocationString);
    }
}


-(void)setOkBtn3:(id)sender{

    [self.CourseLocation setTitle:DateString forState:UIControlStateNormal];
    
    BackView.hidden=YES;
}


#pragma mark 添加附件；
- (IBAction)CourseOtherBtnAction:(id)sender {
    
    WPhotoViewController *WphotoVC = [[WPhotoViewController alloc] init];
    //选择图片的最大数
    WphotoVC.selectPhotoOfMax = 9;
    [WphotoVC setSelectPhotosBack:^(NSMutableArray *phostsArr) {
        _photosArr = phostsArr;
        [_tableView reloadData];
    }];
    [self presentViewController:WphotoVC animated:YES completion:nil];
}
-(void)createTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 435, Swidth, Sheight-435)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _photosArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"cellId%ld", (long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(10, 10, 100, 100);
    imageView.image = [[_photosArr objectAtIndex:indexPath.row] objectForKey:@"image"];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    [cell addSubview:imageView];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}







#pragma mark 保存(发布课程)
-(void)SelectBtn:(id)sender{
    
    NSLog(@"开始时间:%@",StrateTimeLong);
    NSLog(@"结束时间:%@",EndTimeLong);
    NSLog(@"上课地址:%@",LocationID);
    NSLog(@"课程名称:%@",self.CourseName.text);
    NSLog(@"课程类别:%@",MoldString);
    NSLog(@"课程科目:%@",SubjectID);
    NSLog(@"课程描述:%@",self.CourExplain.text);
    
    //发布课程
    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/saveCourseAndSchedule",LocalIP];

    NSDictionary *params=@{
                           @"startTime":StrateTimeLong,
                           @"endTime":EndTimeLong,
                           @"classroomId":LocationString,
                           @"courseName":self.CourseName.text,
                           @"categoryId":SubjectString,
                           @"subjectId":MoldString,
                           @"description":self.CourExplain.text
                           };
    [RequestTools RequestWithURL:URL Method:@"post" Params:params Success:^(NSDictionary *result) {
        NSLog(@"%@",result);

        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [MBProgressHUD showToastAndMessage:@"创建课程成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }



    } failed:^(NSString *result) {
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
//
//    NSString *URLImg=[NSString stringWithFormat:@"%@/files",LocalIP];
//
//
//    [RequestTools RequestWithFile:[UIImage imageNamed:@"BT"] andParams:nil andUrl:URLImg Success:^(NSDictionary *result) {
//
//
//
//    } failed:^(NSString *result) {
//        if ([result isEqualToString:@"200"]) {
//            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
//            NSLog(@"result:%@",result);
//        }else{
//            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
//        }
//    }];
//
    
    
    
    
    
}





@end
