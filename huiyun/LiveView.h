//
//  LiveView.h
//  iOSZhongYe
//
//  Created by 郑敏捷 on 2017/5/19.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <PlayerSDK/PlayerSDK.h>

#import "AppDelegate.h"

#import "ZFPlayer.h"

#import "LiveModel.h"

@protocol liveViewDelegate <NSObject>

- (void)backBtnAction;

- (void)joinRoomResult:(NSString *)resultStr isCancel:(BOOL)bl;

- (void)resign;

@end

@interface LiveView : UIView<GSPPlayerManagerDelegate>

@property (weak, nonatomic) id<liveViewDelegate>delegate;

@property (strong, nonatomic) LiveModel    *liveModel;

@property (assign, nonatomic) BOOL          isAlert;

@end
