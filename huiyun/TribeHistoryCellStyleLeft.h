//
//  TribeHistoryCellStyleLeft.h
//  yun
//
//  Created by MacAir on 2017/6/12.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TribeHistoryCellStyleLeft : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *messageTime;
@property (weak, nonatomic) IBOutlet UILabel *messageContent;


@end
