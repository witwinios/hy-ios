//
//  CaseOperateModel.m
//  huiyun
//
//  Created by MacAir on 2017/10/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseOperateModel.h"

@implementation CaseOperateModel
//@property (strong, nonatomic) NSString *caseStatus;
- (void)setCaseStatus:(NSString *)caseStatus{
    if ([caseStatus isKindOfClass:[NSNull class]]) {
        _caseStatus = @"暂无";
    }else if ([caseStatus isEqualToString:@"waiting_approval"]){
        _caseStatus = @"待审核";
    }else if ([caseStatus isEqualToString:@"approved"]){
        _caseStatus = @"已通过";
    }else{
        _caseStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseResult;
- (void)setCaseResult:(NSString *)caseResult{
    if ([caseResult isKindOfClass:[NSNull class]]) {
        _caseResult = @"暂无";
    }else{
        _caseResult = caseResult;
    }
}
//@property (strong, nonatomic) NSString *casePatientName;
- (void)setCasePatientName:(NSString *)casePatientName{
    if ([casePatientName isKindOfClass:[NSNull class]]) {
        _casePatientName = @"暂无";
    }else{
        _casePatientName = casePatientName;
    }
}
//@property (strong, nonatomic) NSNumber *caseNo;
- (void)setCaseNo:(NSNumber *)caseNo{
    if ([caseNo isKindOfClass:[NSNull class]]) {
        _caseNo = @0;
    }else{
        _caseNo = caseNo;
    }
}
//@property (strong, nonatomic) NSNumber *caseDate;
- (void)setCaseDate:(NSNumber *)caseDate{
    if ([caseDate isKindOfClass:[NSNull class]]) {
        _caseDate = @0;
    }else{
        _caseDate = caseDate;
    }
}
//@property (strong, nonatomic) NSString *caseTeacher;
- (void)setCaseTeacher:(NSString *)caseTeacher{
    if ([caseTeacher isKindOfClass:[NSNull class]]) {
        _caseTeacher = @"暂无";
    }else{
        _caseTeacher = caseTeacher;
    }
}
//@property (strong, nonatomic) NSString *caseReason;
- (void)setCaseReason:(NSString *)caseReason{
    if ([caseReason isKindOfClass:[NSNull class]]) {
        _caseReason = @"暂无";
    }else{
        _caseReason = caseReason;
    }
}
//@property (strong, nonatomic) NSString *caseDescription;
- (void)setCaseDescription:(NSString *)caseDescription{
    if ([caseDescription isKindOfClass:[NSNull class]]) {
        _caseDescription = @"暂无";
    }else{
        _caseDescription = caseDescription;
    }
}
//@property (strong, nonatomic) NSString *caseAdvice;
- (void)setCaseAdvice:(NSString *)caseAdvice{
    if ([caseAdvice isKindOfClass:[NSNull class]]) {
        _caseAdvice = @"暂无";
    }else{
        _caseAdvice = caseAdvice;
    }
}
@end
