//
//  LeaveCellStyle2.h
//  xiaoyun
//
//  Created by MacAir on 17/2/23.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaveCellStyle2 : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *leaveReason;
@end
