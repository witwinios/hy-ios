//
//  DepartController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SdepartController.h"
#define STATE_SEARCH 0
#define STATE_NOT_SEARCH 1
@interface SdepartController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    NSMutableArray *copyArray;
    
    int currentPage;
}
@end

@implementation SdepartController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    copyArray = [NSMutableArray new];
    [self setUI];
    currentPage = 1;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转记录";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    [table registerNib:[UINib nibWithNibName:@"SDeaprtCell" bundle:nil] forCellReuseIdentifier:@"sdepartCell"];
    [self.view addSubview:table];
    //
    table.tableFooterView.frame = CGRectZero;
    [table addHeaderWithTarget:self action:@selector(downRefresh)];
    [table addFooterWithTarget:self action:@selector(moreRefresh)];
    [table headerBeginRefreshing];
}
- (void)searchAction:(NSString *)status{
    [MBProgressHUD showHUDAndMessage:@"" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageSize=999&pageStart=1&studentId=%@&rotationStatus=%@",LocalIP,LocalUserId,status];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无~" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            [dataArray removeAllObjects];
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"recordId"];
            model.SDepartName = dic[@"trainingDepartmentName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            SDepartRecordModel *medicalRecordModel = [SDepartRecordModel new];
            medicalRecordModel.SDepartRecordName = @"病例记录";
            medicalRecordModel.SDepartRecordPassNum = dic[@"approvedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordReferNum = dic[@"rejectedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMedicalCaseRecordsNum"];
            [recordArray addObject:medicalRecordModel];
            //操作记录
            SDepartRecordModel *operateRecordModel = [SDepartRecordModel new];
            operateRecordModel.SDepartRecordName = @"操作记录";
            operateRecordModel.SDepartRecordPassNum = dic[@"approvedOperationRecordsNum"];
            operateRecordModel.SDepartRecordReferNum = dic[@"rejectedOperationRecordsNum"];
            operateRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalOperationRecordsNum"];
            [recordArray addObject:operateRecordModel];
            //活动记录
            SDepartRecordModel *activityRecordModel = [SDepartRecordModel new];
            activityRecordModel.SDepartRecordName = @"活动记录";
            activityRecordModel.SDepartRecordPassNum = dic[@"approvedActivityRecordsNum"];
            activityRecordModel.SDepartRecordReferNum = dic[@"rejectedActivityRecordsNum"];
            activityRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalActivityRecordsNum"];
            [recordArray addObject:activityRecordModel];
            //教学记录
            SDepartRecordModel *teachRecordModel = [SDepartRecordModel new];
            teachRecordModel.SDepartRecordName = @"教学记录";
            teachRecordModel.SDepartRecordPassNum = dic[@"approvedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordReferNum = dic[@"rejectedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTutoringRecordsNum"];
            [recordArray addObject:teachRecordModel];
            //科研记录
            SDepartRecordModel *scienceRecordModel = [SDepartRecordModel new];
            scienceRecordModel.SDepartRecordName = @"科研记录";
            scienceRecordModel.SDepartRecordPassNum = dic[@"approvedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordReferNum = dic[@"rejectedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalResearchRecordsNum"];
            [recordArray addObject:scienceRecordModel];
            //获奖记录
            SDepartRecordModel *rewardRecordModel = [SDepartRecordModel new];
            rewardRecordModel.SDepartRecordName = @"获奖记录";
            rewardRecordModel.SDepartRecordPassNum = dic[@"approvedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordReferNum = dic[@"rejectedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalAwardRecordsNum"];
            [recordArray addObject:rewardRecordModel];
            //论文记录
            SDepartRecordModel *essayRecordModel = [SDepartRecordModel new];
            essayRecordModel.SDepartRecordName = @"论文记录";
            essayRecordModel.SDepartRecordPassNum = dic[@"approvedEssayRecordsNum"];
            essayRecordModel.SDepartRecordReferNum = dic[@"rejectedEssayRecordsNum"];
            essayRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalEssayRecordsNum"];
            [recordArray addObject:essayRecordModel];
            //抢救记录
            SDepartRecordModel *rescuRecordModel = [SDepartRecordModel new];
            rescuRecordModel.SDepartRecordName = @"抢救记录";
            rescuRecordModel.SDepartRecordPassNum = dic[@"approvedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordReferNum = dic[@"rejectedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalRescueRecordsNum"];
            [recordArray addObject:rescuRecordModel];
            //出诊记录
            SDepartRecordModel *treatRecordModel = [SDepartRecordModel new];
            treatRecordModel.SDepartRecordName = @"出诊记录";
            treatRecordModel.SDepartRecordPassNum = dic[@"approvedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordReferNum = dic[@"rejectedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTreatmentRecordsNum"];
            [recordArray addObject:treatRecordModel];
            //差错记录
            SDepartRecordModel *errorRecordModel = [SDepartRecordModel new];
            errorRecordModel.SDepartRecordName = @"差错记录";
            errorRecordModel.SDepartRecordPassNum = dic[@"approvedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordReferNum = dic[@"rejectedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMalpracticeRecordsNum"];
            [recordArray addObject:errorRecordModel];
            //赋值
            model.SDepartRecordArray = recordArray;
            [dataArray addObject:model];
        }
        copyArray = dataArray;
        [table headerEndRefreshing];
        [table reloadData];
    }];
}
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageSize=15&pageStart=1&studentId=%@",LocalIP,LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [dataArray removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"recordId"];
            model.SDepartName = dic[@"trainingDepartmentName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            SDepartRecordModel *medicalRecordModel = [SDepartRecordModel new];
            medicalRecordModel.SDepartRecordName = @"病例记录";
            medicalRecordModel.SDepartRecordPassNum = dic[@"approvedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordReferNum = dic[@"rejectedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMedicalCaseRecordsNum"];
            [recordArray addObject:medicalRecordModel];
            //操作记录
            SDepartRecordModel *operateRecordModel = [SDepartRecordModel new];
            operateRecordModel.SDepartRecordName = @"操作记录";
            operateRecordModel.SDepartRecordPassNum = dic[@"approvedOperationRecordsNum"];
            operateRecordModel.SDepartRecordReferNum = dic[@"rejectedOperationRecordsNum"];
            operateRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalOperationRecordsNum"];
            [recordArray addObject:operateRecordModel];
            //活动记录
            SDepartRecordModel *activityRecordModel = [SDepartRecordModel new];
            activityRecordModel.SDepartRecordName = @"活动记录";
            activityRecordModel.SDepartRecordPassNum = dic[@"approvedActivityRecordsNum"];
            activityRecordModel.SDepartRecordReferNum = dic[@"rejectedActivityRecordsNum"];
            activityRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalActivityRecordsNum"];
            [recordArray addObject:activityRecordModel];
            //教学记录
            SDepartRecordModel *teachRecordModel = [SDepartRecordModel new];
            teachRecordModel.SDepartRecordName = @"教学记录";
            teachRecordModel.SDepartRecordPassNum = dic[@"approvedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordReferNum = dic[@"rejectedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTutoringRecordsNum"];
            [recordArray addObject:teachRecordModel];
            //科研记录
            SDepartRecordModel *scienceRecordModel = [SDepartRecordModel new];
            scienceRecordModel.SDepartRecordName = @"科研记录";
            scienceRecordModel.SDepartRecordPassNum = dic[@"approvedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordReferNum = dic[@"rejectedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalResearchRecordsNum"];
            [recordArray addObject:scienceRecordModel];
            //获奖记录
            SDepartRecordModel *rewardRecordModel = [SDepartRecordModel new];
            rewardRecordModel.SDepartRecordName = @"获奖记录";
            rewardRecordModel.SDepartRecordPassNum = dic[@"approvedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordReferNum = dic[@"rejectedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalAwardRecordsNum"];
            [recordArray addObject:rewardRecordModel];
            //论文记录
            SDepartRecordModel *essayRecordModel = [SDepartRecordModel new];
            essayRecordModel.SDepartRecordName = @"论文记录";
            essayRecordModel.SDepartRecordPassNum = dic[@"approvedEssayRecordsNum"];
            essayRecordModel.SDepartRecordReferNum = dic[@"rejectedEssayRecordsNum"];
            essayRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalEssayRecordsNum"];
            [recordArray addObject:essayRecordModel];
            //抢救记录
            SDepartRecordModel *rescuRecordModel = [SDepartRecordModel new];
            rescuRecordModel.SDepartRecordName = @"抢救记录";
            rescuRecordModel.SDepartRecordPassNum = dic[@"approvedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordReferNum = dic[@"rejectedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalRescueRecordsNum"];
            [recordArray addObject:rescuRecordModel];
            //出诊记录
            SDepartRecordModel *treatRecordModel = [SDepartRecordModel new];
            treatRecordModel.SDepartRecordName = @"出诊记录";
            treatRecordModel.SDepartRecordPassNum = dic[@"approvedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordReferNum = dic[@"rejectedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTreatmentRecordsNum"];
            [recordArray addObject:treatRecordModel];
            //差错记录
            SDepartRecordModel *errorRecordModel = [SDepartRecordModel new];
            errorRecordModel.SDepartRecordName = @"差错记录";
            errorRecordModel.SDepartRecordPassNum = dic[@"approvedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordReferNum = dic[@"rejectedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMalpracticeRecordsNum"];
            [recordArray addObject:errorRecordModel];
            //赋值
            model.SDepartRecordArray = recordArray;
            [dataArray addObject:model];
        }
        copyArray = dataArray;
        [table headerEndRefreshing];
        [table reloadData];
    }];
}
- (void)moreRefresh{
    currentPage ++;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageSize=15&pageStart=%d&studentId=%@",LocalIP,currentPage,LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSMutableArray *newData = [NSMutableArray new];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无更多~" places:0 toView:nil];
            [table footerEndRefreshing];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"recordId"];
            model.SDepartName = dic[@"trainingDepartmentName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            SDepartRecordModel *medicalRecordModel = [SDepartRecordModel new];
            medicalRecordModel.SDepartRecordName = @"病例记录";
            medicalRecordModel.SDepartRecordPassNum = dic[@"approvedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordReferNum = dic[@"rejectedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMedicalCaseRecordsNum"];
            [recordArray addObject:medicalRecordModel];
            //操作记录
            SDepartRecordModel *operateRecordModel = [SDepartRecordModel new];
            operateRecordModel.SDepartRecordName = @"/操作记录";
            operateRecordModel.SDepartRecordPassNum = dic[@"approvedOperationRecordsNum"];
            operateRecordModel.SDepartRecordReferNum = dic[@"rejectedOperationRecordsNum"];
            operateRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalOperationRecordsNum"];
            [recordArray addObject:operateRecordModel];
            //活动记录
            SDepartRecordModel *activityRecordModel = [SDepartRecordModel new];
            activityRecordModel.SDepartRecordName = @"活动记录";
            activityRecordModel.SDepartRecordPassNum = dic[@"approvedActivityRecordsNum"];
            activityRecordModel.SDepartRecordReferNum = dic[@"rejectedActivityRecordsNum"];
            activityRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalActivityRecordsNum"];
            [recordArray addObject:activityRecordModel];
            //教学记录
            SDepartRecordModel *teachRecordModel = [SDepartRecordModel new];
            teachRecordModel.SDepartRecordName = @"教学记录";
            teachRecordModel.SDepartRecordPassNum = dic[@"approvedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordReferNum = dic[@"rejectedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTutoringRecordsNum"];
            [recordArray addObject:teachRecordModel];
            //科研记录
            SDepartRecordModel *scienceRecordModel = [SDepartRecordModel new];
            scienceRecordModel.SDepartRecordName = @"科研记录";
            scienceRecordModel.SDepartRecordPassNum = dic[@"approvedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordReferNum = dic[@"rejectedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalResearchRecordsNum"];
            [recordArray addObject:scienceRecordModel];
            //获奖记录
            SDepartRecordModel *rewardRecordModel = [SDepartRecordModel new];
            rewardRecordModel.SDepartRecordName = @"获奖记录";
            rewardRecordModel.SDepartRecordPassNum = dic[@"approvedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordReferNum = dic[@"rejectedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalAwardRecordsNum"];
            [recordArray addObject:rewardRecordModel];
            //论文记录
            SDepartRecordModel *essayRecordModel = [SDepartRecordModel new];
            essayRecordModel.SDepartRecordName = @"论文记录";
            essayRecordModel.SDepartRecordPassNum = dic[@"approvedEssayRecordsNum"];
            essayRecordModel.SDepartRecordReferNum = dic[@"rejectedEssayRecordsNum"];
            essayRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalEssayRecordsNum"];
            [recordArray addObject:essayRecordModel];
            //抢救记录
            SDepartRecordModel *rescuRecordModel = [SDepartRecordModel new];
            rescuRecordModel.SDepartRecordName = @"抢救记录";
            rescuRecordModel.SDepartRecordPassNum = dic[@"approvedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordReferNum = dic[@"rejectedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalRescueRecordsNum"];
            [recordArray addObject:rescuRecordModel];
            //出诊记录
            SDepartRecordModel *treatRecordModel = [SDepartRecordModel new];
            treatRecordModel.SDepartRecordName = @"出诊记录";
            treatRecordModel.SDepartRecordPassNum = dic[@"approvedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordReferNum = dic[@"rejectedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTreatmentRecordsNum"];
            [recordArray addObject:treatRecordModel];
            //差错记录
            SDepartRecordModel *errorRecordModel = [SDepartRecordModel new];
            errorRecordModel.SDepartRecordName = @"差错记录";
            errorRecordModel.SDepartRecordPassNum = dic[@"approvedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordReferNum = dic[@"rejectedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMalpracticeRecordsNum"];
            [recordArray addObject:errorRecordModel];
            //赋值
            model.SDepartRecordArray = recordArray;
            [newData addObject:model];
        }
        [table footerEndRefreshing];
        NSRange range = NSMakeRange(dataArray.count,newData.count);
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
        [dataArray insertObjects:newData atIndexes:set];
        copyArray = dataArray;
        [table reloadData];
    }];
}
#pragma -action
- (void)statusAction:(UIButton *)btn{
    
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    CGPoint point = CGPointMake(button.center.x, button.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:120 Type:XTTypeOfUpRight Color:[UIColor whiteColor]];
    popView.dataArray = @[@"未开始",@"在科",@"出科"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
    NSArray *indexArray = @[@"not_start",@"in_department",@"off_department"];
    [self searchAction:indexArray[index]];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    recordVC.departModel = dataArray[indexPath.row];
    [self.navigationController pushViewController:recordVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SDeaprtCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sdepartCell"];
    SDepartModel *model = dataArray[indexPath.row];
    cell.departName.text = model.SDepartName;
    cell.departStatus.text = model.SDepartStatus;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *start = [[Maneger shareObject] timeFormatter1:model.SDepartStartTime.stringValue];
    NSString *end = [[Maneger shareObject] timeFormatter1:model.SDepartEndTime.stringValue];
    cell.departTime.text = [NSString stringWithFormat:@"%@ ~ %@",start,end];
    return cell;
}
@end

