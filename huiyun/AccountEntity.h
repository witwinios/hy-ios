//
//  AccountEntity.h
//  yun
//
//  Created by MacAir on 2017/8/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountEntity : NSObject<NSCoding>

//*号银行卡
@property (strong, nonatomic) NSString *userHideBlankCard;
//*号身份证
@property (strong, nonatomic) NSString *userHideCard;
//*号手机号
@property (strong, nonatomic) NSString *userHidePhone;
//角色
@property (strong, nonatomic) NSNumber *roleId;
//账户名
@property (strong, nonatomic) NSString *userAccount;
//账户密码
@property (strong, nonatomic) NSString *userPassword;
//id
@property (strong, nonatomic) NSNumber *userID;
//用户名1
@property (strong, nonatomic) NSString *userName;
//用户全称1
@property (strong, nonatomic) NSString *userFullName;
//邮箱1
@property (strong, nonatomic) NSString *userEmail;
//手机号1
@property (strong, nonatomic) NSNumber *userPhoneNo;
//专业1
@property (strong, nonatomic) NSString *userMajor;
//学校1
@property (strong, nonatomic) NSString *userSchool;
//学位1
@property (strong, nonatomic) NSString *userDegree;
//职称1
@property (strong, nonatomic) NSString *userTitle;
//职称类型1
@property (strong, nonatomic) NSString *userTitleType;
//教师类型1
@property (strong, nonatomic) NSString *userTeacherType;
//所在单位1
@property (strong, nonatomic) NSString *userSource;
//工号1
@property (strong, nonatomic) NSNumber *userNo;
//聘用开始时间
@property (strong, nonatomic) NSNumber *userWorkStart;
//聘用结束时间
@property (strong, nonatomic) NSNumber *userWorkEnd;
//说明
@property (strong, nonatomic) NSString *userDescription;
//教龄
@property (strong, nonatomic) NSNumber *userCareer;
//教师身份1
@property (strong, nonatomic) NSString *isDoctor;
//头像
@property (strong, nonatomic) NSString *userFileUrl;
//性别1
@property (strong, nonatomic) NSString *userSex;
//身份证
@property (strong, nonatomic) NSNumber *userCard;


/*学生*/
//最高学历
@property (strong, nonatomic) NSString *userHighDegree;
//学位类型1
@property (strong, nonatomic) NSString *userDegreeType;
//学制类型1
@property (strong, nonatomic) NSString *userEducationType;
//开户地址1
@property (strong, nonatomic) NSString *userBlankcardOutlets;
//银行卡号1
@property (strong, nonatomic) NSNumber *userBlankNumber;
//response
@property (strong, nonatomic) NSDictionary *responseDic;
//年级
@property (strong, nonatomic) NSNumber *userGrade;
@end
