//
//  TeacherAccountController.h
//  yun
//
//  Created by MacAir on 2017/8/28.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountEntity.h"
#import "AccountCell1.h"
#import "AccountCell2.h"
#import "TeacherMoreController.h"
#import "WXPPickerView.h"
#import "AlertView.h"
typedef enum
{
    EditStatusYes = 0,
    EditStatusNo
}EditStatus;//枚举
typedef void (^TempBlock)();
@interface TeacherAccountController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,PickerViewOneDelegate>
@property (strong, nonatomic) AccountEntity *entity;
@property (strong, nonatomic) UIImageView *imageView;
@property (copy, nonatomic) TempBlock tempBlock;
@end
