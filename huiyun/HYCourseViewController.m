//
//  CourseViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "HYCourseViewController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface HYCourseViewController ()
{
    UITableView *courseTable;
    
    NSMutableArray *dataArray;
    NSMutableArray *completeArray;
    
    int currentPage;
}
@end

@implementation HYCourseViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
}
- (void)setObj{
    currentPage = 1;
    //
    dataArray = [NSMutableArray new];
    completeArray = [NSMutableArray new];
    //
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStyleGrouped];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    [courseTable registerNib:[UINib nibWithNibName:@"HYCourseCell" bundle:nil] forCellReuseIdentifier:@"courseCell"];
    [self.view addSubview:courseTable];
    //
    [courseTable addHeaderWithTarget:self action:@selector(downRefresh)];
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中";
    
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中";
    [courseTable headerBeginRefreshing];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"图标"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(historyAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程安排";
    [backNavigation addSubview:titleLab];
}
- (void)downRefresh{
    currentPage = 1;

    NSString *requestUrl = [NSString stringWithFormat:@"%@/studyPlans/studyRecord/%@/registeredCourses?pageStart=1&pageSize=15&studentId=%@",LocalIP,LocalUserId,LocalUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
        [dataArray removeAllObjects];
        //获取数据源
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无课程!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    HYCourseModel *model = [HYCourseModel new];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.courseStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.courseTime = [dictionary objectForKey:@"startTime"];
                    model.courseType = [dictionary objectForKey:@"scheduleType"];
                    model.courseCategory = [dictionary objectForKey:@"courseCategoryName"];
                    model.courseTeahcher = [dictionary objectForKey:@"teacherName"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.subjectName = [dictionary objectForKey:@"subjectName"];
                    if ([[dictionary objectForKey:@"scheduleStatus"]isEqualToString:@"completed"]){
                        [completeArray addObject:model];
                    }else{
                        [dataArray addObject:model];
                    }
                }
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
            }
        }
        else{
            [MBProgressHUD showToastAndMessage:@"请求数据错误!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [courseTable headerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
        }
    }];
}
- (void)loadRefresh{
    currentPage++;
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studyPlans/studyRecord/%@/registeredCourses?pageStart=%d&pageSize=15&studentId=%@",LocalIP,LocalUserId,currentPage,LocalUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
        [courseTable footerEndRefreshing];
        NSMutableArray *newData = [NSMutableArray new];
        //获取数据源
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"无更多课程!" places:0 toView:nil];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    HYCourseModel *model = [HYCourseModel new];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.courseStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.courseTime = [dictionary objectForKey:@"startTime"];
                    model.courseType = [dictionary objectForKey:@"scheduleType"];
                    model.courseCategory = [dictionary objectForKey:@"courseCategoryName"];
                    model.courseTeahcher = [dictionary objectForKey:@"teacherName"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.subjectName = [dictionary objectForKey:@"subjectName"];
                    if ([[dictionary objectForKey:@"scheduleStatus"]isEqualToString:@"completed"]){
                        [completeArray addObject:model];
                    }else{
                        [newData addObject:model];
                    }
                }
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [courseTable reloadData];
            }
        }
        else{
            [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)historyAction:(UIButton *)button{
    HYStudyHistoryViewController *studyVC = [HYStudyHistoryViewController new];
    studyVC.comptArray = completeArray;
    [self.navigationController pushViewController:studyVC animated:YES];
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -tableView协议
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135.f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseDetailController *detailVC = [HYCourseDetailController new];
    detailVC.model = [dataArray objectAtIndex:indexPath.section];
    
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"courseCell"];
    [cell setProperty:[dataArray objectAtIndex:indexPath.section]];
    return cell;
}
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
