//
//  TMainController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseController.h"
#import "TExamController.h"
#import "TLeaveController.h"
#import "TMarkController.h"
#import "TeacherAccountController.h"
#import "DepartController.h"
@interface TMainController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *headBackView;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
- (IBAction)accountAction:(id)sender;

@property(strong,nonatomic) TCourseController *tcourseVC;
@property(strong,nonatomic) TExamController *texamVC;
@property(strong,nonatomic) TLeaveController *tleaveVC;
@property(strong,nonatomic) TMarkController *tmarkVC;
@property(strong, nonatomic) DepartController *departVC;
@property (strong, nonatomic) AccountEntity *entity;
+ (id)shareObject;
@end
