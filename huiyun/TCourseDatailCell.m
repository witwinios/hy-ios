//
//  TCourseDatailCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TCourseDatailCell.h"

@implementation TCourseDatailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.content.adjustsFontSizeToFitWidth = YES;
}
@end
