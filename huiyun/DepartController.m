//
//  DepartController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DepartController.h"

@interface DepartController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    
    NSInteger currentPage;
}
@end

@implementation DepartController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    currentPage = 1;
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];

    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转记录";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    [table addHeaderWithTarget:self action:@selector(fresh)];
    [table addFooterWithTarget:self action:@selector(moreFresh)];
    [table registerNib:[UINib nibWithNibName:@"DepartCell" bundle:nil] forCellReuseIdentifier:@"departCell"];
    [self.view addSubview:table];
    //开始刷新
    [table headerBeginRefreshing];
}
- (void)fresh{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageStart=1&pageSize=15&teahcherId=%@",LocalIP,persion.userID];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result){
        [table headerEndRefreshing];
        [dataArray removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        NSLog(@"count=%ld",arr.count);
        
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SdepartStudentId = dic[@"studentId"];
            model.SdepartStudentName = dic[@"studentFullName"];
            model.SDepartRecordId = dic[@"recordId"];
            model.SDepartName = dic[@"trainingDepartmentName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            SDepartRecordModel *medicalRecordModel = [SDepartRecordModel new];
            medicalRecordModel.SDepartRecordName = @"病例记录";
            medicalRecordModel.SDepartRecordPassNum = dic[@"approvedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordReferNum = dic[@"rejectedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMedicalCaseRecordsNum"];
            [recordArray addObject:medicalRecordModel];
            //操作记录
            SDepartRecordModel *operateRecordModel = [SDepartRecordModel new];
            operateRecordModel.SDepartRecordName = @"操作记录";
            operateRecordModel.SDepartRecordPassNum = dic[@"approvedOperationRecordsNum"];
            operateRecordModel.SDepartRecordReferNum = dic[@"rejectedOperationRecordsNum"];
            operateRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalOperationRecordsNum"];
            [recordArray addObject:operateRecordModel];
            //活动记录
            SDepartRecordModel *activityRecordModel = [SDepartRecordModel new];
            activityRecordModel.SDepartRecordName = @"活动记录";
            activityRecordModel.SDepartRecordPassNum = dic[@"approvedActivityRecordsNum"];
            activityRecordModel.SDepartRecordReferNum = dic[@"rejectedActivityRecordsNum"];
            activityRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalActivityRecordsNum"];
            [recordArray addObject:activityRecordModel];
            //教学记录
            SDepartRecordModel *teachRecordModel = [SDepartRecordModel new];
            teachRecordModel.SDepartRecordName = @"教学记录";
            teachRecordModel.SDepartRecordPassNum = dic[@"approvedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordReferNum = dic[@"rejectedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTutoringRecordsNum"];
            [recordArray addObject:teachRecordModel];
            //科研记录
            SDepartRecordModel *scienceRecordModel = [SDepartRecordModel new];
            scienceRecordModel.SDepartRecordName = @"科研记录";
            scienceRecordModel.SDepartRecordPassNum = dic[@"approvedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordReferNum = dic[@"rejectedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalResearchRecordsNum"];
            [recordArray addObject:scienceRecordModel];
            //获奖记录
            SDepartRecordModel *rewardRecordModel = [SDepartRecordModel new];
            rewardRecordModel.SDepartRecordName = @"获奖记录";
            rewardRecordModel.SDepartRecordPassNum = dic[@"approvedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordReferNum = dic[@"rejectedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalAwardRecordsNum"];
            [recordArray addObject:rewardRecordModel];
            //论文记录
            SDepartRecordModel *essayRecordModel = [SDepartRecordModel new];
            essayRecordModel.SDepartRecordName = @"论文记录";
            essayRecordModel.SDepartRecordPassNum = dic[@"approvedEssayRecordsNum"];
            essayRecordModel.SDepartRecordReferNum = dic[@"rejectedEssayRecordsNum"];
            essayRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalEssayRecordsNum"];
            [recordArray addObject:essayRecordModel];
            //抢救记录
            SDepartRecordModel *rescuRecordModel = [SDepartRecordModel new];
            rescuRecordModel.SDepartRecordName = @"抢救记录";
            rescuRecordModel.SDepartRecordPassNum = dic[@"approvedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordReferNum = dic[@"rejectedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalRescueRecordsNum"];
            [recordArray addObject:rescuRecordModel];
            //出诊记录
            SDepartRecordModel *treatRecordModel = [SDepartRecordModel new];
            treatRecordModel.SDepartRecordName = @"出诊记录";
            treatRecordModel.SDepartRecordPassNum = dic[@"approvedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordReferNum = dic[@"rejectedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTreatmentRecordsNum"];
            [recordArray addObject:treatRecordModel];
            //差错记录
            SDepartRecordModel *errorRecordModel = [SDepartRecordModel new];
            errorRecordModel.SDepartRecordName = @"差错记录";
            errorRecordModel.SDepartRecordPassNum = dic[@"approvedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordReferNum = dic[@"rejectedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMalpracticeRecordsNum"];
            [recordArray addObject:errorRecordModel];
            //赋值
            model.SDepartRecordArray = recordArray;
            [dataArray addObject:model];
        }
        [table headerEndRefreshing];
        [table reloadData];
    }failed:^(NSString *result) {
        [table headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
}
- (void)moreFresh{
    currentPage ++;
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageStart=%ld&pageSize=15&teahcherId=%@",LocalIP,currentPage,persion.userID];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result){
        [table footerEndRefreshing];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            currentPage --;
            [MBProgressHUD showToastAndMessage:@"暂无更多~" places:0 toView:nil];
            return ;
        }
        NSMutableArray *newArray = [NSMutableArray new];
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SdepartStudentId = dic[@"studentId"];
            model.SdepartStudentName = dic[@"studentFullName"];
            model.SDepartRecordId = dic[@"recordId"];
            model.SDepartName = dic[@"trainingDepartmentName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            SDepartRecordModel *medicalRecordModel = [SDepartRecordModel new];
            medicalRecordModel.SDepartRecordName = @"病例记录";
            medicalRecordModel.SDepartRecordPassNum = dic[@"approvedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordReferNum = dic[@"rejectedMedicalCaseRecordsNum"];
            medicalRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMedicalCaseRecordsNum"];
            [recordArray addObject:medicalRecordModel];
            //操作记录
            SDepartRecordModel *operateRecordModel = [SDepartRecordModel new];
            operateRecordModel.SDepartRecordName = @"操作记录";
            operateRecordModel.SDepartRecordPassNum = dic[@"approvedOperationRecordsNum"];
            operateRecordModel.SDepartRecordReferNum = dic[@"rejectedOperationRecordsNum"];
            operateRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalOperationRecordsNum"];
            [recordArray addObject:operateRecordModel];
            //活动记录
            SDepartRecordModel *activityRecordModel = [SDepartRecordModel new];
            activityRecordModel.SDepartRecordName = @"活动记录";
            activityRecordModel.SDepartRecordPassNum = dic[@"approvedActivityRecordsNum"];
            activityRecordModel.SDepartRecordReferNum = dic[@"rejectedActivityRecordsNum"];
            activityRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalActivityRecordsNum"];
            [recordArray addObject:activityRecordModel];
            //教学记录
            SDepartRecordModel *teachRecordModel = [SDepartRecordModel new];
            teachRecordModel.SDepartRecordName = @"教学记录";
            teachRecordModel.SDepartRecordPassNum = dic[@"approvedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordReferNum = dic[@"rejectedTutoringRecordsNum"];
            teachRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTutoringRecordsNum"];
            [recordArray addObject:teachRecordModel];
            //科研记录
            SDepartRecordModel *scienceRecordModel = [SDepartRecordModel new];
            scienceRecordModel.SDepartRecordName = @"科研记录";
            scienceRecordModel.SDepartRecordPassNum = dic[@"approvedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordReferNum = dic[@"rejectedResearchRecordsNum"];
            scienceRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalResearchRecordsNum"];
            [recordArray addObject:scienceRecordModel];
            //获奖记录
            SDepartRecordModel *rewardRecordModel = [SDepartRecordModel new];
            rewardRecordModel.SDepartRecordName = @"获奖记录";
            rewardRecordModel.SDepartRecordPassNum = dic[@"approvedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordReferNum = dic[@"rejectedAwardRecordsNum"];
            rewardRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalAwardRecordsNum"];
            [recordArray addObject:rewardRecordModel];
            //论文记录
            SDepartRecordModel *essayRecordModel = [SDepartRecordModel new];
            essayRecordModel.SDepartRecordName = @"论文记录";
            essayRecordModel.SDepartRecordPassNum = dic[@"approvedEssayRecordsNum"];
            essayRecordModel.SDepartRecordReferNum = dic[@"rejectedEssayRecordsNum"];
            essayRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalEssayRecordsNum"];
            [recordArray addObject:essayRecordModel];
            //抢救记录
            SDepartRecordModel *rescuRecordModel = [SDepartRecordModel new];
            rescuRecordModel.SDepartRecordName = @"抢救记录";
            rescuRecordModel.SDepartRecordPassNum = dic[@"approvedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordReferNum = dic[@"rejectedRescueRecordsNum"];
            rescuRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalRescueRecordsNum"];
            [recordArray addObject:rescuRecordModel];
            //出诊记录
            SDepartRecordModel *treatRecordModel = [SDepartRecordModel new];
            treatRecordModel.SDepartRecordName = @"出诊记录";
            treatRecordModel.SDepartRecordPassNum = dic[@"approvedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordReferNum = dic[@"rejectedTreatmentRecordsNum"];
            treatRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalTreatmentRecordsNum"];
            [recordArray addObject:treatRecordModel];
            //差错记录
            SDepartRecordModel *errorRecordModel = [SDepartRecordModel new];
            errorRecordModel.SDepartRecordName = @"差错记录";
            errorRecordModel.SDepartRecordPassNum = dic[@"approvedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordReferNum = dic[@"rejectedMalpracticeRecordsNum"];
            errorRecordModel.SDepartRecordPendNum = dic[@"waiting_approvalMalpracticeRecordsNum"];
            [recordArray addObject:errorRecordModel];
            //赋值
            model.SDepartRecordArray = recordArray;
            [newArray addObject:model];
        }
        NSRange range = NSMakeRange(dataArray.count,newArray.count );
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
        [dataArray insertObjects:newArray atIndexes:set];
        table.contentOffset = CGPointMake(table.contentOffset.x, table.contentOffset.y+40);
        [table reloadData];
    }failed:^(NSString *result) {
        currentPage -- ;
        [table footerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];

}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    CGPoint point = CGPointMake(button.center.x, button.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:120 Type:XTTypeOfUpRight Color:[UIColor whiteColor]];
    popView.dataArray = @[@"未开始",@"在科",@"出科"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
//    NSMutableArray *muArray = [NSMutableArray new];
    switch (index) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        default:
            break;
    }
}
- (void)statusAction:(UIButton *)btn{
    DepartMarkController *markVC = [DepartMarkController new];
    [self.navigationController pushViewController:markVC animated:YES];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RoundRecordController *recordVC = [RoundRecordController new];
    recordVC.departModel = dataArray[indexPath.row];
    [self.navigationController pushViewController:recordVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DepartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"departCell"];
    [cell.statusBtn addTarget:self action:@selector(statusAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.tag = indexPath.row + 200;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SDepartModel *model = dataArray[indexPath.row];
    cell.studentName.text = model.SdepartStudentName;
    if ([model.SDepartStatus isEqualToString:@"在科"]) {
        cell.statusBtn.hidden = NO;
        [cell.statusBtn setTitle:@"出科" forState:0];
    }else if ([model.SDepartStatus isEqualToString:@"未开始"]) {
        cell.statusBtn.hidden = NO;
        [cell.statusBtn setTitle:@"入科" forState:0];
    }else{
        cell.statusBtn.hidden = YES;
    }
    cell.departStatus.text = model.SDepartStatus;
    cell.depart.text = model.SDepartName;
    cell.inTime.text = [NSString stringWithFormat:@"入科时间:%@",[[Maneger shareObject] timeFormatter1:model.SDepartStartTime.stringValue]];
    cell.outTime.text = [NSString stringWithFormat:@"出科时间:%@",[[Maneger shareObject] timeFormatter1:model.SDepartEndTime.stringValue]];
    return cell;
}
@end
