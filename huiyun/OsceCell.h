//
//  OsceCell.h
//  yun
//
//  Created by MacAir on 2017/5/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSCEModel.h"
#import "onlineModel.h"
#import "SkillModel.h"

@interface OsceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *testStatus;
@property (weak, nonatomic) IBOutlet UILabel *testName;
@property (weak, nonatomic) IBOutlet UILabel *oneTitle;
@property (weak, nonatomic) IBOutlet UILabel *oneContent;
@property (weak, nonatomic) IBOutlet UILabel *twoTitle;
@property (weak, nonatomic) IBOutlet UILabel *twoContent;
@property (weak, nonatomic) IBOutlet UILabel *threeTitle;
@property (weak, nonatomic) IBOutlet UILabel *threeContent;

- (void)set:(id)model;
@end
