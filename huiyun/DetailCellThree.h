//
//  DetailCellThree.h
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCellThree : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *contentBtn;

@end
