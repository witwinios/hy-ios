//
//  OsceCell.m
//  yun
//
//  Created by MacAir on 2017/5/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "OsceCell.h"

@implementation OsceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.testStatus.layer.cornerRadius = 5;
    self.testStatus.layer.masksToBounds = YES;
}
- (void)set:(id)model{
    if ([model isKindOfClass:[OSCEModel class]]) {
        //
        self.oneTitle.text = @"考试分类:";
        self.twoTitle.text = @"考试科目:";
        
        OSCEModel *osceModel = (OSCEModel *)model;
        self.testName.text = osceModel.osceName;
        self.testStatus.text = osceModel.osceStatus;
        self.oneContent.text = osceModel.osceCategory;
        self.twoContent.text = osceModel.subjectName;
        self.threeContent.text = [[Maneger shareObject] timeFormatter:osceModel.osceTime.stringValue];
        
    }else if ([model isKindOfClass:[onlineModel class]]){
        //
        self.oneTitle.text = @"教室名称:";
        self.twoTitle.text = @"考试时长:";
        
        onlineModel *onModel = (onlineModel *)model;
        self.testName.text = onModel.onlineName;
        self.testStatus.text = onModel.onlineStatus;
        self.oneContent.text = onModel.onlineRoom;
        self.twoContent.text = [NSString stringWithFormat:@"%@分钟",onModel.onlineInternal.stringValue];
        self.threeContent.text = [[Maneger shareObject] timeFormatter:onModel.onlineTime.stringValue];
    }else{
        self.oneTitle.text = @"考试分类:";
        self.twoTitle.text = @"考试科目:";
        
        SkillModel *sModel = (SkillModel *)model;
        self.testName.text = sModel.skillName;
        self.testStatus.text = sModel.skillStatus;
        self.oneContent.text = sModel.skillCategory;
        self.twoContent.text = sModel.skillSubject;
        self.threeContent.text = [[Maneger shareObject]timeFormatter:sModel.skillStartTime.stringValue];
    }
}
@end
