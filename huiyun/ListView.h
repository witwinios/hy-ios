//
//  MenuView.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/31.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BACKBlock)(id result,NSInteger selectId);
@interface ListView : UIView<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)UITableView *menuTable;
@property (strong, nonatomic)NSArray *listArray;

@property (strong, nonatomic)BACKBlock selectBlock;
@end

