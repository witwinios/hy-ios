//
//  EssayDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "EssayDetailController.h"

@interface EssayDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation EssayDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"论文记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";
    if ([self.model.caseEssayModelStatus isEqualToString:@"待审核"]) {
        EssayDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            PaperRecordController *paperVC = [PaperRecordController new];
            paperVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:paperVC animated:YES];
        };
    }

    titleArray = @[@"审核状态:",@"论文题目:",@"论文级别:",@"论文类别:",@"第几作者:",@"发表刊物:",@"发表时间:",@"带教老师:",@"说明:",@"指导意见:",@"附件:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    self.dataArray = @[_model.caseEssayModelStatus,_model.caseEssayModelTitle,_model.caseEssayModelDegree,_model.caseEssayModelCategory,_model.caseEssayModelAuthor,_model.caseEssayModelEdtion,[[Maneger shareObject] timeFormatter1:_model.caseEssayModelDate.stringValue],_model.caseEssayModelTeacher,_model.caseEssayModelDescription,_model.caseEssayModelAdvice,_model.fileUrl];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 10) {
        return three;
    }else if (indexPath.row == 9 || indexPath.row == 8){
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else{
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
@end
