//
//  OnlineDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/1/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "onlineModel.h"
#import "Maneger.h"
@interface OnlineDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) onlineModel *model;
@end
