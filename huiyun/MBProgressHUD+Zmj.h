//
//  MBProgressHUD+Zmj.h
//  ZhongYeIphone
//
//  Created by 郑敏捷 on 16/7/13.
//  Copyright © 2016年 郑敏捷. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (Zmj)

/**
 *  显示MBProgressHUD透明层遮盖
 *
 *  @return 直接返回一个MBProgressHUD，需要手动关闭
 */
+ (MBProgressHUD *)showHUDtoView:(UIView *)view;

/**
 *  显示小菊花MBProgressHUD
 *
 *  @param message 信息内容
 *  @param view    需要显示信息的视图
 *
 *  @return 直接返回一个MBProgressHUD，需要手动关闭
 */
+ (MBProgressHUD *)showHUDAndMessage:(NSString *)message toView:(UIView *)view;

/**
 *  显示Toast。MBProgressHUD
 *
 *  @param message 信息内容
 *  @param places  0 居中    1 居底部
 *  @param view    需要显示信息的视图
 *
 *  @return 直接返回一个MBProgressHUD，需要手动关闭
 */
+ (MBProgressHUD *)showToastAndMessage:(NSString *)message places:(NSInteger)places toView:(UIView *)view;

/**
 *  显示Toast。MBProgressHUD
 *
 *  @param message 信息内容
 *  @param places  0 居中    1 居底部
 *  @param delay   消失的时间
 *  @param view    需要显示信息的视图
 *
 *  @return 直接返回一个MBProgressHUD，需要手动关闭
 */
+ (MBProgressHUD *)showToastAndMessage:(NSString *)message places:(NSInteger)places afterDelay:(NSTimeInterval)delay toView:(UIView *)view;

/**
 *  关闭小菊花MBProgressHUD
 *
 *  @param view    显示MBProgressHUD的视图
 */
+ (void)hideHUDForView:(UIView *)view;

@end
