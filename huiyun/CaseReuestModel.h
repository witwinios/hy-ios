//
//  CaseReuestModel.h
//  huiyun
//
//  Created by MacAir on 2017/10/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseReuestModel : NSObject
@property (strong, nonatomic) NSNumber *CaseReuestModelRequirementId;
@property (strong, nonatomic) NSString *CaseReuestModelName;
@property (strong, nonatomic) NSString *CaseReuestModelType;
@property (strong, nonatomic) NSString *CaseReuestModelLevel;
@property (strong, nonatomic) NSNumber *CaseReuestModelNums;
@property (strong, nonatomic) NSNumber *total;
@property (strong, nonatomic) NSNumber *pass;
@property (strong, nonatomic) NSNumber *refer;
@property (strong, nonatomic) NSNumber *pend;
@end
