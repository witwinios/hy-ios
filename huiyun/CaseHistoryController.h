//
//  CaseHistoryController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPopview.h"
#import "RecordCell.h"

#import "SDepartModel.h"
#import "CaseMedicalModel.h"
#import "CaseOperateModel.h"
#import "CaseActivityModel.h"
#import "CaseReuestModel.h"
#import "CaseTeachModel.h"
#import "CaseSienceModel.h"
#import "CaseAwardModel.h"
#import "CaseEssayModel.h"
#import "CaseTreatModel.h"
#import "CaseRescueModel.h"
#import "CaseMistakeModel.h"

#import "TeachDetailController.h"
#import "ActivityDetailController.h"
#import "OperateDetailController.h"
#import "ErrorRecordController.h"
#import "CaseRequestController.h"
#import "SdepartRecordController.h"
#import "SienceDetailController.h"
#import "AwardDetailController.h"
#import "EssayDetailController.h"
#import "RescueDetailController.h"
#import "TreatDetailController.h"
#import "MistakeDetailController.h"

#import "SCaseRecordController.h"
@interface CaseHistoryController : UIViewController<selectIndexPathDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) SDepartModel *departModel;
@property (strong, nonatomic) CaseReuestModel *caseRequestModel;
+ (instancetype)shareObject;
@end

