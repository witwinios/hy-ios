//
//  StationModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StationModel : NSObject
@property (copy,nonatomic)NSString *stationName;
@property (strong, nonatomic)NSNumber *teacherNum;
@property (strong, nonatomic)NSNumber *skillNum;
@property (strong, nonatomic)NSNumber *selectStationNum;
@property (strong, nonatomic)NSNumber *answerNum;
@property (copy, nonatomic) NSString *stationPlace;
@end
