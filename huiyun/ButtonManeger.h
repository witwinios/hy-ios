//
//  ButtonManeger.h
//  yun
//
//  Created by MacAir on 2017/7/3.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ButtonManeger : NSObject
@property (strong, nonatomic) NSMutableArray *viewArray;
@property (strong, nonatomic) NSNumber *questionId;
@property (strong, nonatomic) NSMutableArray *singleAnswerArray;
+ (id)share;
- (void)add:(UIButton *)btn;
- (void)changeStatus:(long)index;
@end
