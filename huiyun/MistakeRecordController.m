//
//  MistakeRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MistakeRecordController.h"
#import "THDatePickerView.h"
@interface MistakeRecordController ()
{
    NSString *current_date;
    UIView *btnBack;

}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation MistakeRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    if (![self.myModel isKindOfClass:[NSNull class]] || self.model != nil) {
        self.oneField.text = self.myModel.caseMistakeModelCategory;
        self.twoField.text = self.myModel.caseMistakeModelDegree;
        self.contentTextview.text = self.myModel.caseMistakeModelViaDescription;
        self.contentTextview1.text = self.myModel.caseMistakeModelReason;
        self.contentTextview2.text = self.myModel.caseMistakeModelLession;
        self.contentTextview3.text  = self.myModel.caseMistakeModelLeaderAdvice;
        if (self.myModel.caseMistakeModelDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseMistakeModelDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter:self.myModel.caseMistakeModelDate.stringValue] forState:0];
        }
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardDidShowNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.dateBtn.layer.cornerRadius = 4;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [self.dateBtn setTitle:date forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    self.dateView = dateView;

    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"差错记录";
    [backNavigation addSubview:titleLab];
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
    [self.contentTextview1 resignFirstResponder];
    [self.contentTextview2 resignFirstResponder];
    [self.contentTextview3 resignFirstResponder];
}
-(void)doneButtonshow: (NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    
    if (btnBack == nil) {
        btnBack = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight-keyBoardHeight-35, Swidth, 35)];
    }
    btnBack.backgroundColor = UIColorFromHex(0xDBDBDB);
    UIButton *doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(Swidth-70, 0, 70, 35);
    [doneButton setTitle:@"完成" forState: UIControlStateNormal];
    [doneButton addTarget:self action:@selector(hideKeyboard:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor blueColor] forState:0];
    [btnBack addSubview:doneButton];
    [self.view addSubview:btnBack];
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
}
- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"等级不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"详细经过不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview1.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview2.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教训不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview3.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"处理意见不能为空~" places:0 toView:nil];
        return;
    }
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/malpracticeRecords",LocalIP,departModel.SDepartRecordId];
    NSDictionary *params = @{@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
    NSLog(@"params=%@",params);
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];

}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"等级不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"详细经过不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview1.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview2.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教训不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview3.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"处理意见不能为空~" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/malpracticeRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
    NSDictionary *params = @{@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
    NSLog(@"params=%@",params);
    
    [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];

}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];

}

- (IBAction)saveAction:(id)sender {
    if ([self.myModel isKindOfClass:[NSNull class]] || self.myModel == nil) {
        [self saveAction];
    }else{
        [self updateAction];
    }
    
}
@end
