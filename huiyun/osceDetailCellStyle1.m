//
//  osceDetailCellStyle1.m
//  xiaoyun
//
//  Created by MacAir on 17/2/7.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "osceDetailCellStyle1.h"

@implementation osceDetailCellStyle1

- (void)awakeFromNib {
    [super awakeFromNib];
    self.biaoti.adjustsFontSizeToFitWidth = YES;
    self.content.numberOfLines = 0;
    self.content.font = [UIFont systemFontOfSize:17];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
