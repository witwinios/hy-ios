//
//  RescueDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "BaseWitwinController.h"
#import "CaseRescueModel.h"
@interface RescueDetailController : BaseWitwinController
@property (strong, nonatomic) CaseRescueModel *model;
@end
