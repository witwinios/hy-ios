//
//  ListStudentController.h
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TStudentCell.h"
#import "SkillModel.h"
#import "TRoomModel.h"
#import "TStudentModel.h"
#import "CallModel.h"
#import "ContentController.h"
@interface ListStudentController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (strong, nonatomic) NSNumber *selectStationId;
@property (strong, nonatomic) SkillModel *skillModel;
@property (strong, nonatomic) TRoomModel *roomModel;

@end
