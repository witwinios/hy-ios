//
//  DesCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *detailDes;
@end
