//
//  ExamPointController.m
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamPointController.h"

@interface ExamPointController ()
{
    UITableView *tabView;

    NSMutableArray *heightArray;
}
@end

@implementation ExamPointController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [tabView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray new];
    heightArray = [NSMutableArray new];
    [self setUI];
    [self loadData];
}
//获取考点
- (void)loadStation{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId];
    
    NSLog(@"osceStation=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取考点中。。。" Success:^(NSDictionary *result) {
        [_dataArray removeAllObjects];
        NSArray *array = result[@"responseBody"][@"currentStudentOSCEStationBeans"];
        
        for (NSDictionary *dic in array) {
            ExamPointModel *pointModel = [ExamPointModel new];
            pointModel.ExamPointModelSubject = dic[@"subjectName"];
            pointModel.ExamPointModelCategory = dic[@"categoryName"];
            pointModel.ExamPointModelUseds = dic[@"usageCount"];
            pointModel.ExamPointModelInternal = dic[@"testDuration"];
            pointModel.ExamPointModelStationId = dic[@"stationId"];
            pointModel.ExamPointModelStationName = dic[@"stationName"];
            pointModel.ExamPointModelStatus = @"未评";
            CGFloat height_H = [Maneger heightWithFont:[UIFont systemFontOfSize:20] width:Swidth-196 string:pointModel.ExamPointModelStationName];
            NSLog(@"----%f",height_H);
            [heightArray addObject:[NSString stringWithFormat:@"%f",height_H]];
            [_dataArray addObject:pointModel];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        
    }];

}
//获取考点
- (void)loadData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/start/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,self.callModel.callModelUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Message:@"获取考点中。。。" Success:^(NSDictionary *result) {
        if ([result[@"responseBody"] isKindOfClass:[NSNull class]]) {
            [self loadStation];
            return ;
        }
        NSArray *array = result[@"responseBody"];
        
        for (NSDictionary *dic in array) {
            ExamPointModel *pointModel = [ExamPointModel new];
            pointModel.ExamPointModelSubject = dic[@"subjectName"];
            pointModel.ExamPointModelCategory = dic[@"categoryName"];
            pointModel.ExamPointModelUseds = dic[@"usageCount"];
            pointModel.ExamPointModelInternal = dic[@"testDuration"];
            pointModel.ExamPointModelStationId = dic[@"stationId"];
            pointModel.ExamPointModelStationName = dic[@"stationName"];
            pointModel.ExamPointModelStatus = @"未评";
            CGFloat height_H = [Maneger heightWithFont:[UIFont systemFontOfSize:20] width:Swidth-196 string:pointModel.ExamPointModelStationName];
            NSLog(@"----%f",height_H);
            [heightArray addObject:[NSString stringWithFormat:@"%f",height_H]];
            [_dataArray addObject:pointModel];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"考点选择";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"ExamPointCell" bundle:nil] forCellReuseIdentifier:@"pointCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    [self.view addSubview:tabView];

}
//是否选取过
- (void)isChoosed:(ExamPointModel *)pointModel AndIndexPath:(NSIndexPath *)indexPath{
    ExamPointCell *cell = [tabView cellForRowAtIndexPath:indexPath];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/checkSkillOperationRecord/%@/%@/student/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,pointModel.ExamPointModelStationId,LocalUserId,self.callModel.callModelUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"选取中" Success:^(NSDictionary *result) {
        if ([result[@"errorCode"] isKindOfClass:[NSNull class]]) {
            ControlManeger *VCManeger = [ControlManeger share];
            VCManeger.ExamContentVC = [ExamContentController new];

            VCManeger.ExamContentVC.skillModel = self.skillModel;
            VCManeger.ExamContentVC.callModel = self.callModel;
            VCManeger.ExamContentVC.examModel = pointModel;
            [self.navigationController pushViewController:VCManeger.ExamContentVC animated:YES];
        }else if ([result[@"errorCode"] isEqualToString:@"operationRecord_is_submit"]){
            cell.pointStatus.text = @"已评分";
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"该考点已经提交评分!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    } failed:^(NSString *result) {
        
    }];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 131+[heightArray[indexPath.row] floatValue];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //判断有没有选取过
    [self isChoosed:_dataArray[indexPath.row] AndIndexPath:indexPath];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExamPointCell *cell = [tabView dequeueReusableCellWithIdentifier:@"pointCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ExamPointModel *pointModel = _dataArray[indexPath.row];
    
    cell.stationName.text = pointModel.ExamPointModelStationName;
    cell.subject.text = pointModel.ExamPointModelSubject;
    cell.category.text = pointModel.ExamPointModelCategory;
    cell.uesds.text = pointModel.ExamPointModelUseds.stringValue;
    cell.internal.text = pointModel.ExamPointModelInternal.stringValue;
    cell.pointStatus.text = pointModel.ExamPointModelStatus;
    return cell;
}
@end
