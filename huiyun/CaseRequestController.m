//
//  CaseRequestController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseRequestController.h"
@interface CaseRequestController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
}
@end

@implementation CaseRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    [self setUI];
    [self loadData];
}
- (void)loadData{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    NSString *url;
    switch (recordVC.selectRecordIndex.intValue) {
        case 0:
            url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/medicalCaseRecords/summary?pageStart=1&pageSize=999&total=1",LocalIP,recordVC.departModel.SDepartRecordId];
            break;
        case 1:
            url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords/summary?pageStart=1&pageSize=999&total=1",LocalIP,recordVC.departModel.SDepartRecordId];
            break;
        case 2:
            url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/activityRecords/summary?pageStart=1&pageSize=999&total=1",LocalIP,recordVC.departModel.SDepartRecordId];
            break;
        default:
            break;
    }
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
        NSArray *arr = result[@"responseBody"][@"result"];
        switch (recordVC.selectRecordIndex.intValue) {
            case 0:
                for (int i=0; i<arr.count; i++) {
                    NSDictionary *resDic = arr[i];
                    CaseReuestModel *model = [CaseReuestModel new];
                    model.CaseReuestModelRequirementId = resDic[@"requirementId"];
                    model.CaseReuestModelName = resDic[@"requiredDiseaseType"];
                    model.CaseReuestModelType = resDic[@"requiredCaseType"];
                    model.CaseReuestModelLevel = resDic[@"requirementLevel"];
                    model.CaseReuestModelNums = resDic[@"requiredAmount"];
                    model.total = resDic[@"medicalCaseRecordsNum"];
                    model.pass = resDic[@"approvedMedicalCaseRecordsNum"];
                    model.refer = resDic[@"rejectedMedicalCaseRecordsNum"];
                    model.pend = resDic[@"waitingApprovalMedicalCaseRecordsNum"];
                    [dataArray addObject:model];
                }
                break;
            case 1:
                for (int i=0; i<arr.count; i++) {
                    NSDictionary *resDic = arr[i];
                    CaseReuestModel *model = [CaseReuestModel new];
                    model.CaseReuestModelRequirementId = resDic[@"requirementId"];
                    model.CaseReuestModelName = resDic[@"requiredOperationName"];
                    model.CaseReuestModelLevel = resDic[@"requirementLevel"];
                    model.CaseReuestModelNums = resDic[@"requiredAmount"];
                    model.total = resDic[@"operationRecordsNum"];
                    model.pass = resDic[@"approvedOperationRecordsNum"];
                    model.refer = resDic[@"rejectedOperationRecordsNum"];
                    model.pend = resDic[@"waitingApprovalOperationRecordsNum"];
                    [dataArray addObject:model];
                }
                break;
            case 2:
                for (int i=0; i<arr.count; i++) {
                    NSDictionary *resDic = arr[i];
                    CaseReuestModel *model = [CaseReuestModel new];
                    model.CaseReuestModelRequirementId = resDic[@"requirementId"];
                    model.CaseReuestModelType = resDic[@"requiredActivityType"];
                    model.pass = resDic[@"approvedActivityRecordsNum"];
                    model.refer = resDic[@"rejectedActivityRecordsNum"];
                    model.pend = resDic[@"waitingApprovalActivityRecordsNum"];
                    [dataArray addObject:model];
                }
                break;
            default:
                break;
        }
        [table reloadData];
        } failed:^(NSString *result) {
            
        }];
}
- (void)setUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightBtn.frame = CGRectMake(Swidth-35, 29.5, 25, 25);
//    [rightBtn setImage:[UIImage imageNamed:@"add"] forState:0];
//    [rightBtn addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
//    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"病例要求";
    [backNavigation addSubview:titleLab];
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    table.delegate = self;
    table.dataSource = self;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    [table registerNib:[UINib nibWithNibName:@"CaseRequestCell" bundle:nil] forCellReuseIdentifier:@"requestCell"];
    [table registerNib:[UINib nibWithNibName:@"CaseRequestOperateCell" bundle:nil] forCellReuseIdentifier:@"operateCell"];
    [table registerNib:[UINib nibWithNibName:@"CaseRequestActivityCell" bundle:nil] forCellReuseIdentifier:@"activityCell"];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:table];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)addAction{
        if ([_model.SDepartRecordName isEqualToString:@"病例记录"]) {
            self.scaseVC = [SCaseRecordController new];
            [self.navigationController pushViewController:self.scaseVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"操作记录"]) {
            self.operateVC = [OperateRecordController new];
            [self.navigationController pushViewController:self.operateVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"活动记录"]){
            self.activityVC = [ActivityRecordController new];
            [self.navigationController pushViewController:self.activityVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"教学记录"]){
            self.teachVC = [TeachRecordController new];
            [self.navigationController pushViewController:self.teachVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"科研记录"]){
            self.scienceVC = [ScienceRecordController new];
            [self.navigationController pushViewController:self.scienceVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"获奖记录"]){
            self.rewardVC = [RewardRecordController new];
            [self.navigationController pushViewController:self.rewardVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"论文记录"]){
            self.paperVC = [PaperRecordController new];
            [self.navigationController pushViewController:self.paperVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"抢救记录"]){
            self.rescueVC = [RescueRecordController new];
            [self.navigationController pushViewController:self.rescueVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"出诊记录"]){
            self.visitVC = [VisitRecordController new];
            [self.navigationController pushViewController:self.visitVC animated:YES];
        }else if ([_model.SDepartRecordName isEqualToString:@"差错记录"]){
            self.mistakeVC = [MistakeRecordController new];
            [self.navigationController pushViewController:self.mistakeVC animated:YES];
        }
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    int select = recordVC.selectRecordIndex.intValue;
    if (select == 0) {
        return 145.5;
    }else if (select == 1){
        return 144;
    }else{
        return 88;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CaseHistoryController *hisVC = [CaseHistoryController shareObject];
    hisVC.caseRequestModel = dataArray[indexPath.row];
    [self.navigationController pushViewController:hisVC animated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CaseRequestCell *medicalCell = [tableView dequeueReusableCellWithIdentifier:@"requestCell"];
    CaseRequestOperateCell *operateCell = [tableView dequeueReusableCellWithIdentifier:@"operateCell"];
    CaseRequestActivityCell *activityCell = [tableView dequeueReusableCellWithIdentifier:@"activityCell"];
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    int select = recordVC.selectRecordIndex.intValue;
    
    CaseReuestModel *model = dataArray[indexPath.row];
    
    if (select == 0) {
        medicalCell.CaseRequestName.text = model.CaseReuestModelName;
        medicalCell.CaseRequestType.text = model.CaseReuestModelType;
        medicalCell.CaseRequestLevel.text = model.CaseReuestModelLevel;
        medicalCell.CaseRequestNum.text = [NSString stringWithFormat:@"%@",model.CaseReuestModelNums];
        medicalCell.resultLab.text = [NSString stringWithFormat:@"通过数:%@ 未通过数:%@ 待审核数:%@",model.pass,model.refer,model.pend];
        return medicalCell;
    }else if (select == 1){
        operateCell.indexLab.text = [NSString stringWithFormat:@"序号:%ld",(long)indexPath.row+1];
        operateCell.operateName.text = model.CaseReuestModelName;
        operateCell.operateLevel.text = model.CaseReuestModelLevel;
        operateCell.leastNum.text = [NSString stringWithFormat:@"%@",model.CaseReuestModelNums];
        operateCell.resultLab.text = [NSString stringWithFormat:@"通过数:%@ 未通过数:%@ 待审核数:%@",model.pass,model.refer,model.pend];
        return operateCell;
    }else{
        activityCell.indexLab.text = [NSString stringWithFormat:@"序号:%ld",(long)indexPath.row+1];
        activityCell.activityType.text = model.CaseReuestModelType;
        activityCell.resultLab.text = [NSString stringWithFormat:@"通过数:%@ 未通过数:%@ 待审核数:%@",model.pass,model.refer,model.pend];
        return activityCell;
    }
}
- (void)statusAction:(UIButton *)btn{
    
}
@end

