//
//  CourseDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "HYCourseDetailController.h"
#import "MyAlertView.h"
#import "AddStar.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface HYCourseDetailController ()<MyAlertViewDelegate>{
    UIView * BackVc;
    
    UITableView *tableView;
    NSString *AddStarString;
    
    UIView *footView;
}

@end

@implementation HYCourseDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
    
    
    
    
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-28, 29.5, 26, 26);
    [rightBtn addTarget:self action:@selector(evaluate) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"pinjia"] forState:UIControlStateNormal];
    [backNavigation addSubview:rightBtn];
    
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程详情";
    [backNavigation addSubview:titleLab];
    
}
- (void)setUI{
    NSArray *nameArray = @[@"课程名称:",@"课程科目:",@"类型:",@"分类:",@"上课老师:"];
    for (int i=0; i<5; i++) {
        UILabel *nameLab = [UILabel new];
        nameLab.frame = CGRectMake(5, (HEIGHT-64)/10*i+64, 80, 40);
        nameLab.adjustsFontSizeToFitWidth = YES;
        nameLab.text = nameArray[i];
        [self.view addSubview:nameLab];
        UILabel *desLab = [UILabel new];
        desLab.frame = CGRectMake(86, (HEIGHT-64)/10*i+64, WIDTH-86, 40);
        desLab.adjustsFontSizeToFitWidth = YES;
        desLab.text = @"描述";
        [self.view addSubview:desLab];
        //
        UIView *line = [UIView new];
        line.backgroundColor = [UIColor lightGrayColor];
        line.frame = CGRectMake(0, (HEIGHT-64)/10*i+105, WIDTH, 1);
        [self.view addSubview:line];
        if (i==0) {
            desLab.text = self.model.courseName;
        }else if (i==1){
            desLab.text = self.model.subjectName;
        }else if (i==2){
            desLab.text = self.model.courseType;
        }else if (i==3){
            desLab.text = self.model.courseCategory;
        }else{
            desLab.text = self.model.courseTeahcher;
        }
    }
    //
    UILabel *courseDescription = [UILabel new];
    courseDescription.frame = CGRectMake(5, (HEIGHT-64)/2+64, 80, 40);
    courseDescription.text = @"课程简介:";
    [self.view addSubview:courseDescription];
    UITextView *textView = [UITextView new];
    textView.frame = CGRectMake(86, (HEIGHT-64)/2+64, WIDTH-86, 80);
    textView.text = self.model.courseDes;
    textView.font = [UIFont systemFontOfSize:15];
    textView.textColor = [UIColor lightGrayColor];
    textView.editable = NO;
    [self.view addSubview:textView];
    //
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 145+(HEIGHT-64)/2, WIDTH, 30);
    view.backgroundColor = UIColorFromHex(0xC0C0C0);
    [self.view addSubview:view];
    //
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 175+(HEIGHT-64)/2, WIDTH, 120) style:UITableViewStylePlain];
    tableView.scrollEnabled = NO;
    [tableView registerNib:[UINib nibWithNibName:@"osceDatailCellStyle2" bundle:nil] forCellReuseIdentifier:@"workCell"];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}
#pragma -Action
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)evaluate{
    
    [self loadAlertView:@"请评价课程" contentStr:nil btnNum:1 btnStrArr:[NSArray arrayWithObject:@"确认评价"] type:1];
    
    
}

- (void)loadAlertView:(NSString *)title contentStr:(NSString *)content btnNum:(NSInteger)num btnStrArr:(NSArray *)array type:(NSInteger)typeStr
{
    
    MyAlertView *alertView = [[MyAlertView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
   [alertView initWithTitle:title contentStr:content type:typeStr btnNum:num btntitleArr:array];

    alertView.delegate = self;
    
    
    UIView * keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview: alertView];
    
}

-(void)ChooseStarNum:(NSString *)ChooseStarNum{
    
    AddStarString=ChooseStarNum;
    
     NSLog(@"以选择 %@ 颗星星",AddStarString);
    
}


-(void)didClickButtonAtIndex:(NSUInteger)index password:(NSString *)password{
    switch (index) {
        case 101:
            NSLog(@"Click ok");
            break;
        case 100:
            [self setAddStar];
            break;
        default:
            break;
    }
}
-(void)setAddStar{
    
    footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 35)];
    
    UILabel *label=[[UILabel alloc]init];
    label.frame=footView.bounds;
    
    
    

 label.text=[NSString stringWithFormat:@"该课程获得 %@ 分",AddStarString];
  
    
    label.textAlignment=NSTextAlignmentCenter;
    label.textAlignment=NSTextAlignmentLeft;
    [footView addSubview:label];
    
    
    tableView.tableFooterView=footView;
}

#pragma -协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showTitile];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    osceDatailCellStyle2 *cell = [tableView dequeueReusableCellWithIdentifier:@"workCell"];
    if (indexPath.row == 0) {
        cell.work.text = @"课前作业:";
    }else{
        cell.work.text = @"课后作业:";
    }
    return cell;
}
#pragma -action
- (void)showTitile{
    NSString *title = NSLocalizedString(@"提示", nil);
    NSString *message = NSLocalizedString(@"待开发", nil);
    NSString *cancelButtonTitle = NSLocalizedString(@"确定", nil);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    //创建action
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"222");
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
