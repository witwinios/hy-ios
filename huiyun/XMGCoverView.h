//
//  XMGCoverView.h
//  XMG-画板
//
//  Created by 张强 on 16/3/22.
//  Copyright © 2016年 XMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XMGCoverView : UIView

@end
// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com