//
//  MarkTableController.h
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarkCell.h"
#import "SkillModel.h"
#import "CallModel.h"
#import "ExamPointModel.h"
#import "MarkTableModel.h"
#import "ListStudentController.h"
#import "ExamPointController.h"
#import "DrawController.h"
#import "MarkCell.h"
@interface MarkTableController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) SkillModel *skillModel;
@property (strong, nonatomic) ExamPointModel *pointModel;
@property (strong, nonatomic) CallModel *callModel;
@end
