//
//  HistoryCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryLeaveModel.h"
#import "Maneger.h"
@interface HistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *approver;
@property (weak, nonatomic) IBOutlet UILabel *approveStatus;
@property (weak, nonatomic) IBOutlet UILabel *approveType;
@property (weak, nonatomic) IBOutlet UILabel *leaveTime;
- (void)setProperty:(HistoryLeaveModel *)model;
@end
