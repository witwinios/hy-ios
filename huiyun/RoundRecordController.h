//
//  RoundRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "RoundCell.h"
#import "RoundViewCell.h"
#import "RoundModel.h"
#import "RecordController.h"
#import "SCaseRecordController.h"
#import "ResultController.h"
#import "SDepartModel.h"
#import "ResultSubmitedController.h"
#import "CaseRequestController.h"
@interface RoundRecordController : UIViewController<UITableViewDelegate,UITableViewDataSource>

+ (id)shareObject;
@property (strong, nonatomic) NSNumber *selectRecordIndex;

@property (strong, nonatomic) SDepartModel *departModel;

@end


