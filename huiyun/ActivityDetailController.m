//
//  ActivityDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ActivityDetailController.h"

@interface ActivityDetailController ()
{
    UITableView *table;
    NSArray *titleArray;
    NSArray *dataArray;
    NSArray *heightArr;
}
@end

@implementation ActivityDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    titleArray = @[@"活动名称:",@"主讲人:",@"活动时长:",@"活动时间:",@"带教老师:",@"活动内容:",@"指导意见:"];
    dataArray = @[_model.caseActivityName,_model.casePersion,[NSString stringWithFormat:@"%@",_model.caseInternal],[[Maneger shareObject] timeFormatter1:_model.caseDate.stringValue],_model.caseTeacher,_model.caseActivityContent,_model.caseAdvice];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"103",@"103"];
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if ([self.model.caseStatus isEqualToString:@"待审核"]) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"修改" forState:0];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    }

    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"活动记录";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.bounces = NO;
    table.delegate = self;
    table.dataSource = self;
    table.showsVerticalScrollIndicator = NO;
    [table registerNib:[UINib nibWithNibName:@"DetailCellOne" bundle:nil] forCellReuseIdentifier:@"one"];
    [table registerNib:[UINib nibWithNibName:@"DetailCellTwo" bundle:nil] forCellReuseIdentifier:@"two"];
    [self.view addSubview:table];
    
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)btn{
    ActivityRecordController *recordVC = [ActivityRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}

#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *oneCell = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *twoCell = [tableView dequeueReusableCellWithIdentifier:@"two"];
    if (indexPath.row == 6 || indexPath.row == 5){
        twoCell.titleText.text = titleArray[indexPath.row];
        twoCell.contentText.text = dataArray[indexPath.row];
        return twoCell;
    }else{
        oneCell.titleLab.text = titleArray[indexPath.row];
        oneCell.contentLab.text = dataArray[indexPath.row];
        return oneCell;
    }
}
@end
