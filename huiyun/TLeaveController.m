//
//  LeaveHistoryController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TLeaveController.h"

@interface TLeaveController ()
{
    UITableView *leaveTable;
    NSMutableArray *dataArray;
    LoadingView *loadView;
    int currentPage;
}
@end

@implementation TLeaveController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    currentPage = 1;
    [leaveTable headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
    [self setUpRefresh];
}
- (void)setUpRefresh{
    [leaveTable addHeaderWithTarget:self action:@selector(downRefresh)];
//    [leaveTable addFooterWithTarget:self action:@selector(loadMoreRefresh)];
    //设置文字
    leaveTable.headerPullToRefreshText = @"下拉刷新";
    leaveTable.headerReleaseToRefreshText = @"松开进行刷新";
    leaveTable.headerRefreshingText = @"刷新中。。。";
//    
//    leaveTable.footerPullToRefreshText = @"上拉加载";
//    leaveTable.footerReleaseToRefreshText = @"松开进行加载";
//    leaveTable.footerRefreshingText = @"加载中。。。";
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"审批列表";
    [backNavigation addSubview:titleLab];
}
- (void)setObj{
    currentPage = 1;
    dataArray = [NSMutableArray new];
    leaveTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    leaveTable.delegate = self;
    leaveTable.dataSource = self;
    [leaveTable registerNib:[UINib nibWithNibName:@"HistoryCell" bundle:nil] forCellReuseIdentifier:@"historyCell"];
    leaveTable.tableFooterView.frame = CGRectZero;
    [self.view addSubview:leaveTable];
    //
}
- (void)downRefresh{
    currentPage =1;
    
    RequestTools *tool = [RequestTools new];
    
    [tool getRequest:[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=1&pageSize=999",LocalIP]];
    
    tool.errorBlock=^(NSString *code){
        [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
    };
    tool.responseBlock = ^(NSDictionary *response){
        [leaveTable headerEndRefreshing];
        
        [dataArray removeAllObjects];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array= [[response objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count == 0) {
                [MBProgressHUD showToastAndMessage:@"无请假记录~" places:0 toView:nil];
            }else{
                for (int i=0; i<array.count; i++) {
                    HistoryLeaveModel *model = [HistoryLeaveModel new];
                    NSDictionary *responseDic = array[i];
                    if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
                        model.approver = @"暂无";
                    }else{
                        model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
                    }
                    if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
                        model.fileUrl = @"";
                    }else{
                        model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
                    }
                    model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
                    model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
                    model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
                    model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
                    model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
                    model.leaveType = [responseDic objectForKey:@"leaveType"];
                    model.leaveReason = [responseDic objectForKey:@"leaveReason"];
                    model.fileId = [responseDic objectForKey:@"file"];
                    model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"userName"];
                    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
                    if ([model.approver isEqualToString:entity.userFullName]) {
                        [dataArray addObject:model];
                    }
                    
                }
            }
            [leaveTable reloadData];
        }else{
            [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
        }
    };
}
- (void)loadMoreRefresh{
    currentPage ++;
    
    RequestTools *tool = [RequestTools new];
    [tool getRequest:[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=15",LocalIP,currentPage]];

    NSLog(@"请假:%@",[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=15",LocalIP,currentPage]);
    tool.errorBlock=^(NSString *code){
        if ([code isEqualToString:@"200"]) {
            currentPage--;
            [leaveTable footerEndRefreshing];
            return ;
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[response objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        [leaveTable footerEndRefreshing];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array= [[response objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"无更多请假!" andCurentVC:self];
                currentPage--;
            }else{
                NSMutableArray *newData = [NSMutableArray new];
                for (int i=0; i<array.count; i++) {
                    HistoryLeaveModel *model = [HistoryLeaveModel new];
                    NSDictionary *responseDic = array[i];
                    if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
                        model.approver = @"暂无";
                    }else{
                        model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
                    }
                    if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
                        model.fileUrl = @"";
                    }else{
                        model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
                    }
                    model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
                    model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
                    model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
                    model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
                    model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
                    model.leaveType = [responseDic objectForKey:@"leaveType"];
                    model.leaveReason = [responseDic objectForKey:@"leaveReason"];
                    model.fileId = [responseDic objectForKey:@"file"];
                    model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"userName"];
                    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
                    if ([model.approver isEqualToString:entity.userFullName]) {
                        [newData addObject:model];
                    }
                }
                NSRange range = NSMakeRange(dataArray.count, newData.count);
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [leaveTable reloadData];
            }
        }else{
            
        }
    };
}
- (void)selectAction:(NSNumber *)requestId{
    
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)deleteAction:(UIButton *)btn{
    leaveTable.editing = !leaveTable.editing;
    if (leaveTable.editing) {
        [btn setTitle:@"完成" forState:UIControlStateNormal];
    }else{
        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
}
#pragma -协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 109.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.tleaveDetailVC = [TLeaveDetailController new];
    self.tleaveDetailVC.model = dataArray[indexPath.row];
    [self.navigationController pushViewController:self.tleaveDetailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    [cell setProperty:dataArray[indexPath.row]];
    return cell;
}
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
