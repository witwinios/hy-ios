//
//  XMGButton.m
//  XMG-画板
//
//  Created by 张强 on 16/3/22.
//  Copyright © 2016年 XMG. All rights reserved.
//

#import "XMGButton.h"

// RGB颜色
#define RGBColor(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

// 随机色
#define XMGRandomColor RGBColor(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))

@implementation XMGButton


//往button里绘制圆形的颜色
- (void)drawRect:(CGRect)rect {
    // Drawing code
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:rect];
    
    self.btnColor = XMGRandomColor;
    [self.btnColor set];
    [path fill];
    
}



@end
// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com