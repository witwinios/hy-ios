//
//  LeaveHistoryController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "LeaveHistoryController.h"

@interface LeaveHistoryController ()
{
    UITableView *historyTable;
    NSMutableArray *dataArray;
    LoadingView *loadView;
    int currentPage;
}
@end

@implementation LeaveHistoryController
+ (id)shareInstance{
    static LeaveHistoryController *historyVC = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (historyVC == nil) {
            historyVC = [[LeaveHistoryController alloc]init];
        }
    });
    return historyVC;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    currentPage = 1;
    [historyTable headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
    [self setUpRefresh];
    [self setProgressBar];
}
- (void)setUpRefresh{
    [historyTable addHeaderWithTarget:self action:@selector(downRefresh)];
    [historyTable addFooterWithTarget:self action:@selector(loadMoreRefresh)];
    //设置文字
    historyTable.headerPullToRefreshText = @"下拉刷新";
    historyTable.headerReleaseToRefreshText = @"松开进行刷新";
    historyTable.headerRefreshingText = @"刷新中。。。";
    
    historyTable.footerPullToRefreshText = @"上拉加载";
    historyTable.footerReleaseToRefreshText = @"松开进行加载";
    historyTable.footerRefreshingText = @"加载中。。。";
}

- (void)setProgressBar{
    loadView = [[LoadingView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    loadView.center = CGPointMake(Swidth/2, Sheight/2);
    loadView.loadTitle = @"删除中";
    [self.view addSubview:loadView];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-70, 29.5, 70, 25);
    rightBtn.layer.cornerRadius = 5;
    rightBtn.layer.masksToBounds = YES;
    rightBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    rightBtn.layer.borderWidth = 1;
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"我的请假";
    [backNavigation addSubview:titleLab];
}
- (void)setObj{
    currentPage = 1;
    dataArray = [NSMutableArray new];
    historyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    historyTable.delegate = self;
    historyTable.dataSource = self;
    [historyTable registerNib:[UINib nibWithNibName:@"HistoryCell" bundle:nil] forCellReuseIdentifier:@"historyCell"];
    [self.view addSubview:historyTable];
    //
}
- (void)downRefresh{
    currentPage =1;
    RequestTools *tool = [RequestTools new];
    [tool getRequest:[NSString stringWithFormat:@"%@/attendance/leaveRequests?userId=%@&pageStart=1&pageSize=15",LocalIP,LocalUserId]];
    NSLog(@"请假API:%@",[NSString stringWithFormat:@"%@/attendance/leaveRequests?userId=%@&pageStart=1&pageSize=15",LocalIP,LocalUserId]);
    tool.errorBlock=^(NSString *code){
        if (![code isEqualToString:@"200"]) {
            [historyTable headerEndRefreshing];
            return ;
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[response objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        [historyTable headerEndRefreshing];
        
        [dataArray removeAllObjects];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array= [[response objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"无请假记录!" andCurentVC:self];
            }else{
                for (int i=0; i<array.count; i++) {
                    HistoryLeaveModel *model = [HistoryLeaveModel new];
                    NSDictionary *responseDic = array[i];
                    if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
                        model.approver = @"暂无";
                    }else{
                        model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
                        model.approverId = responseDic[@"reviewer"][@"userId"];
                    }
                    if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
                        model.fileUrl = @"";
                    }else{
                        model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
                    }
                    model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
                    model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
                    model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
                    model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
                    NSLog(@"startTime=%@",model.leaveStartTime);
                    model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
                    model.leaveType = [responseDic objectForKey:@"leaveType"];
                    model.leaveReason = [responseDic objectForKey:@"leaveReason"];
                    model.fileId = [responseDic objectForKey:@"file"];
                    model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"userName"];
                    [dataArray addObject:model];
                }
            }
            [historyTable reloadData];
        }else{
            
        }
    };
}
- (void)loadMoreRefresh{
    currentPage ++;
    
    RequestTools *tool = [RequestTools new];//1
    [tool getRequest:[NSString stringWithFormat:@"%@/attendance/leaveRequests?userId=%@&pageStart=%d&pageSize=99",LocalIP,LocalUserId,currentPage]];//2
    tool.errorBlock=^(NSString *code){ //3
        if ([code isEqualToString:@"200"]) {
            currentPage--;
            [historyTable footerEndRefreshing];
            return ;
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        //
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[response objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        
        [historyTable footerEndRefreshing];
        
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array= [[response objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"无更多请假!" andCurentVC:self];
            }else{
                NSMutableArray *newData = [NSMutableArray new];
                for (int i=0; i<array.count; i++) {
                    HistoryLeaveModel *model = [HistoryLeaveModel new];
                    NSDictionary *responseDic = array[i];
                    if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
                        model.approver = @"暂无";
                    }else{
                        model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
                        model.approverId = responseDic[@"reviewer"][@"userId"];
                    }
                    if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
                        model.fileUrl = @"";
                    }else{
                        model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
                    }
                    model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
                    model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
                    model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
                    model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
                    model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
                    model.leaveType = [responseDic objectForKey:@"leaveType"];
                    model.leaveReason = [responseDic objectForKey:@"leaveReason"];
                    model.fileId = [responseDic objectForKey:@"file"];
                    model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"userName"];
                    [newData addObject:model];
                }
                NSRange range = NSMakeRange(0, newData.count);
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [historyTable reloadData];
                
            }
        }else{
            
        }
        //
    };
}
- (void)deleteLeaveHistory:(NSNumber *)requestId{
    [loadView showLoadingView];
    
    RequestTools *tool = [RequestTools new];
    
    [tool deleteRequest:[NSString stringWithFormat:@"%@/attendance/leaveRequests/%@",LocalIP,requestId]];
    
    tool.responseBlock = ^(NSDictionary *response){
        
        [loadView hiddenLoadingView];
    };
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)deleteAction:(UIButton *)btn{
    historyTable.editing = !historyTable.editing;
    if (historyTable.editing) {
        [btn setTitle:@"完成" forState:UIControlStateNormal];
    }else{
        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
}
#pragma -协议
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[dataArray[indexPath.row] approveStatus] isEqualToString:@"同意"]) {
        return UITableViewCellEditingStyleNone;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [dataArray removeObjectAtIndex:indexPath.row];
        [historyTable deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        //后台删除
        [self deleteLeaveHistory:[dataArray[indexPath.row] leaveRequestId]];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 109.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UpdateLeaveController *updateVC = [UpdateLeaveController new];
    updateVC.model = dataArray[indexPath.row];
    [self.navigationController pushViewController:updateVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setProperty:dataArray[indexPath.row]];
    return cell;
}
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
