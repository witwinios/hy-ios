//
//  BaseWitwinController.h
//  huiyun
//
//  Created by MacAir on 2017/10/27.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCellOne.h"
#import "DetailCellTwo.h"
#import "DetailCellThree.h"
@interface BaseWitwinController : UIViewController<UITableViewDelegate,UITableViewDataSource>
typedef void (^NavBlock)();
@property (strong, nonatomic) NSString *leftTitle;
@property (strong, nonatomic) NSString *titles;
@property (strong, nonatomic) NSString *rightTitle;

@property (strong, nonatomic) UIButton *leftBtn;
@property (strong, nonatomic) UIButton *rightBtn;
@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UITableView *table;

@property (copy, nonatomic) NavBlock leftBlock;
@property (copy, nonatomic) NavBlock rightBlock;

@property (strong, nonatomic) NSArray *dataArray;
@end

