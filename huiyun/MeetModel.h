//
//  MeetModel.h
//  xiaoyun
//
//  Created by MacAir on 2017/3/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MeetModel : NSObject
@property (strong, nonatomic) NSString *meetDes;
@property (strong, nonatomic) NSString *meetId;
@property (strong, nonatomic) NSString *meetNumber;
@property (strong, nonatomic) NSNumber *startTime;
@end
