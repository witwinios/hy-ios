//
//  AddStar.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol AddStarViewDelegate<NSObject>

@optional

-(void)ChooseStarNum:(NSInteger *)ChooseStarNum;

@end

@interface AddStar : UIView
{
    
    NSNumber *_Score;
    
    BOOL _canChoose;
    BOOL _canAnimation;
    
    UIImage *startImage_Empty;
    UIImage *startImage_Full;
    
}

-(NSNumber *)Score;

-(void)setScore:(NSNumber *)Score;

-(BOOL)canBeChoose;
-(void)setCanChoose:(BOOL)CanChoose;

-(BOOL)animation;
-(void)setAnimation:(BOOL)animation;

-(UIImage *)starImage_Full;
-(void)setStarImage_Full:(UIImage *)image;

-(UIImage *)starImage_Empty;
-(void)setStarImage_Empty:(UIImage *)image;

@property(nonatomic,assign) id<AddStarViewDelegate>delegate;



@end

