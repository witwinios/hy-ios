//
//  AccountEntity.m
//  yun
//
//  Created by MacAir on 2017/8/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "AccountEntity.h"

@implementation AccountEntity
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userHighDegree forKey:@"userHighDegree"];
    [aCoder encodeObject:self.userBlankNumber forKey:@"userBlankNumber"];
    [aCoder encodeObject:self.userBlankcardOutlets forKey:@"userBlankcardOutlets"];
    [aCoder encodeObject:self.userEducationType forKey:@"userEducationType"];
    [aCoder encodeObject:self.userDegreeType forKey:@"userDegreeType"];
    [aCoder encodeObject:self.userHideBlankCard forKey:@"userHideBlankCard"];
    [aCoder encodeObject:self.userHideCard forKey:@"userHideCard"];
    [aCoder encodeObject:self.userHidePhone forKey:@"userHidePhone"];
    [aCoder encodeObject:self.userAccount forKey:@"userAccount"];
    [aCoder encodeObject:self.userPassword forKey:@"userPassword"];
    [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.userFullName forKey:@"userFullName"];
    [aCoder encodeObject:self.userEmail forKey:@"userEmail"];
    [aCoder encodeObject:self.roleId forKey:@"roleId"];
    [aCoder encodeObject:self.userPhoneNo forKey:@"userPhoneNo"];
    [aCoder encodeObject:self.userMajor forKey:@"userMajor"];
    [aCoder encodeObject:self.userSchool forKey:@"userSchool"];
    [aCoder encodeObject:self.userDegree forKey:@"userDegree"];
    [aCoder encodeObject:self.userTitle forKey:@"userTitle"];
    [aCoder encodeObject:self.userTitleType forKey:@"userTitleType"];
    [aCoder encodeObject:self.userTeacherType forKey:@"userTeacherType"];
    [aCoder encodeObject:self.userSource forKey:@"userSource"];
    [aCoder encodeObject:self.userNo forKey:@"userNo"];
    [aCoder encodeObject:self.userWorkStart forKey:@"userWorkStart"];
    [aCoder encodeObject:self.userWorkEnd forKey:@"userWorkEnd"];
    [aCoder encodeObject:self.userDescription forKey:@"userDescription"];
    [aCoder encodeObject:self.userCareer forKey:@"userCareer"];
    [aCoder encodeObject:self.isDoctor forKey:@"isDoctor"];
    [aCoder encodeObject:self.userFileUrl forKey:@"userFileUrl"];
    [aCoder encodeObject:self.userSex forKey:@"userSex"];
    [aCoder encodeObject:self.userCard forKey:@"userCard"];
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.userHighDegree = [aDecoder decodeObjectForKey:@"userHighDegree"];
        self.userBlankNumber = [aDecoder decodeObjectForKey:@"userBlankNumber"];
        self.userBlankcardOutlets = [aDecoder decodeObjectForKey:@"userBlankcardOutlets"];
        self.userEducationType = [aDecoder decodeObjectForKey:@"userEducationType"];
        self.userDegreeType = [aDecoder decodeObjectForKey:@"userDegreeType"];
        self.userHideBlankCard = [aDecoder decodeObjectForKey:@"userHideBlankCard"];
        self.userHidePhone = [aDecoder decodeObjectForKey:@"userHidePhone"];
        self.userHideCard = [aDecoder decodeObjectForKey:@"userHideCard"];
        self.userAccount = [aDecoder decodeObjectForKey:@"userAccount"];
        self.userPassword = [aDecoder decodeObjectForKey:@"userPassword"];
        self.userID = [aDecoder decodeObjectForKey:@"userID"];
        self.userName = [aDecoder decodeObjectForKey:@"userName"];
        self.userFullName = [aDecoder decodeObjectForKey:@"userFullName"];
        self.userEmail = [aDecoder decodeObjectForKey:@"userEmail"];
        self.roleId = [aDecoder decodeObjectForKey:@"roleId"];
        self.userPhoneNo = [aDecoder decodeObjectForKey:@"userPhoneNo"];
        self.userMajor = [aDecoder decodeObjectForKey:@"userMajor"];
        self.userSchool = [aDecoder decodeObjectForKey:@"userSchool"];
        self.userDegree = [aDecoder decodeObjectForKey:@"userDegree"];
        self.userTitle = [aDecoder decodeObjectForKey:@"userTitle"];
        self.userTitleType = [aDecoder decodeObjectForKey:@"userTitleType"];
        self.userTeacherType = [aDecoder decodeObjectForKey:@"userTeacherType"];
        self.userSource = [aDecoder decodeObjectForKey:@"userSource"];
        self.userNo = [aDecoder decodeObjectForKey:@"userNo"];
        self.userWorkStart = [aDecoder decodeObjectForKey:@"userWorkStart"];
        self.userWorkEnd = [aDecoder decodeObjectForKey:@"userWorkEnd"];
        self.userDescription = [aDecoder decodeObjectForKey:@"userDescription"];
        self.userCareer = [aDecoder decodeObjectForKey:@"userCareer"];
        self.isDoctor = [aDecoder decodeObjectForKey:@"isDoctor"];
        self.userFileUrl = [aDecoder decodeObjectForKey:@"userFileUrl"];
        self.userSex = [aDecoder decodeObjectForKey:@"userSex"];
        self.userCard = [aDecoder decodeObjectForKey:@"userCard"];
    }
    return self;
}
//性别
- (void)setUserSex:(NSString *)userSex{
    if ([userSex isKindOfClass:[NSNull class]]) {
        _userSex = @"暂无";
    }else if ([userSex isEqualToString:@"male"]) {
        _userSex = @"男";
    }else{
        _userSex = @"女";
    }
}

//专业
- (void)setUserMajor:(NSString *)userMajor{
    if ([userMajor isKindOfClass:[NSNull class]]) {
        _userMajor = @"暂无";
    }else{
        _userMajor = userMajor;
    }
}
//手机号
- (void)setUserPhoneNo:(NSNumber *)userPhoneNo{
    if ([userPhoneNo isKindOfClass:[NSNull class]]) {
        _userPhoneNo = @0;
    }else{
        _userPhoneNo = userPhoneNo;
    }
}
//邮箱
- (void)setUserEmail:(NSString *)userEmail{
    if ([userEmail isKindOfClass:[NSNull class]]) {
        _userEmail = @"暂无";
    }else{
        _userEmail = userEmail;
    }
}
- (void)setUserHighDegree:(NSString *)userHighDegree{
    if ([userHighDegree isKindOfClass:[NSNull class]]) {
        _userHighDegree = @"暂无";
    }else if ([userHighDegree isEqualToString:@"technical_secondary_school"]){
        _userHighDegree = @"中专";
    }else if ([userHighDegree isEqualToString:@"junior_college"]){
        _userHighDegree = @"大专";
    }else if ([userHighDegree isEqualToString:@"undergraduate"]){
        _userHighDegree = @"本科";
    }else if ([userHighDegree isEqualToString:@"master"]){
        _userHighDegree = @"硕士研究生";
    }else if ([userHighDegree isEqualToString:@"doctor"]){
        _userHighDegree = @"博士研究生";
    }
}
//教工身份
//- (void)setIsDoctor:(NSString *)isDoctor{
//    if ([isDoctor isKindOfClass:[NSNull class]]) {
//        _isDoctor = @"否";
//    }else if ([isDoctor isEqualToString:@"false"]){
//        _isDoctor = @"否";
//    }else{
//        _isDoctor = @"是";
//    }
//}
//教师类型
- (void)setUserTeacherType:(NSString *)userTeacherType{
    if ([userTeacherType isKindOfClass:[NSNull class]]) {
        _userTeacherType = @"暂无";
    }else if ([userTeacherType isEqualToString:@"inside"]){
        _userTeacherType = @"院内";
    }else if ([userTeacherType isEqualToString:@"outside"]){
        _userTeacherType = @"院外";
    }else{
        _userTeacherType = @"暂无";
    }
}
- (void)setUserName:(NSString *)userName{
    if ([userName isKindOfClass:[NSNull class]]) {
        _userName = @"暂无";
    }else{
        _userName = userName;
    }
}
- (void)setUserFullName:(NSString *)userFullName{
    if ([userFullName isKindOfClass:[NSNull class]]) {
        _userFullName = @"暂无";
    }else{
        _userFullName = userFullName;
    }
}
- (void)setUserSchool:(NSString *)userSchool{
    if ([userSchool isKindOfClass:[NSNull class]]) {
        _userSchool = @"暂无";
    }else{
        _userSchool = userSchool;
    }
}
- (void)setUserDegree:(NSString *)userDegree{
    if ([userDegree isKindOfClass:[NSNull class]]) {
        _userDegree = @"暂无";
    }else if ([userDegree isEqualToString:@"post_doctorate"]){
        _userDegree = @"博士后";
    }else if ([userDegree isEqualToString:@"doctorate"]){
        _userDegree = @"博士";
    }else if ([userDegree isEqualToString:@"bachelor"]){
        _userDegree = @"学士";
    }else if ([userDegree isEqualToString:@"master"]){
        _userDegree = @"硕士";
    }else if ([userDegree isEqualToString:@"doctor"]){
        _userDegree = @"博士";
    }else {
        _userDegree = userDegree;
    }
}
- (void)setUserTitle:(NSString *)userTitle{
    if ([userTitle isKindOfClass:[NSNull class]]) {
        _userTitle = @"暂无";
    }else{
        _userTitle = userTitle;
    }
}
- (void)setUserTitleType:(NSString *)userTitleType{
    if ([userTitleType isKindOfClass:[NSNull class]]) {
        _userTitleType = @"暂无";
    }else if ([userTitleType isEqualToString:@"school"]){
        _userTitleType = @"学校类";
        NSLog(@"111");
    }else if ([userTitleType isEqualToString:@"engineering"]){
        _userTitleType = @"工程类";
        NSLog(@"222");
    }else if ([userTitleType isEqualToString:@"administrative"]){
        _userTitleType = @"行政类";
        NSLog(@"333");
    }else if ([userTitleType isEqualToString:@"medicine"]){
        _userTitleType = @"医学类";
        NSLog(@"444");
    }else if([userTitleType isEqualToString:@"学校类"]){
        _userTitleType = @"学校";
        NSLog(@"555");
    }
}
- (void)setUserSource:(NSString *)userSource{
    if ([userSource isKindOfClass:[NSNull class]]) {
        _userSource = @"暂无";
    }else{
        _userSource = userSource;
    }
}
- (void)setUserNo:(NSNumber *)userNo{
    if ([userNo isKindOfClass:[NSNull class]]) {
        _userNo = @0;
    }else{
        _userNo = userNo;
    }
}
- (void)setUserDescription:(NSString *)userDescription{
    if ([userDescription isKindOfClass:[NSNull class]]) {
        _userDescription = @"暂无";
    }else{
        _userDescription = userDescription;
    }
}
- (void)setUserDegreeType:(NSString *)userDegreeType{
    if ([userDegreeType isKindOfClass:[NSNull class]]) {
        _userDegreeType = @"暂无";
    }else if ([userDegreeType isEqualToString:@"specialty"]){
        _userDegreeType = @"专业型";
    }else{
        _userDegreeType = @"科学型";
    }
}
- (void)setUserEducationType:(NSString *)userEducationType{
    if ([userEducationType isKindOfClass:[NSNull class]]) {
        _userEducationType = @"暂无";
    }else if ([userEducationType isEqualToString:@"full_time"]){
        _userEducationType = @"全日制";
    }else if ([userEducationType isEqualToString:@"part_time"]){
        _userEducationType = @"非全日制";
    }
}
- (void)setUserBlankcardOutlets:(NSString *)userBlankcardOutlets{
    if ([userBlankcardOutlets isKindOfClass:[NSNull class]]) {
        _userBlankcardOutlets = @"暂无";
    }else{
        _userBlankcardOutlets = userBlankcardOutlets;
    }
}
- (void)setUserCard:(NSNumber *)userCard{
    if ([userCard isKindOfClass:[NSNull class]]) {
        _userCard = @0;
    }else{
        _userCard = userCard;
    }
}
- (void)setUserBlankNumber:(NSNumber *)userBlankNumber{
    if ([userBlankNumber isKindOfClass:[NSNull class]]) {
        _userBlankNumber = @0;
    }else{
        _userBlankNumber = userBlankNumber;
    }
}
- (void)setUserGrade:(NSNumber *)userGrade{
    if ([userGrade isKindOfClass:[NSNull class]]) {
        _userGrade = @0;
    }else {
        _userGrade = userGrade;
    }
}
- (void)setUserWorkStart:(NSNumber *)userWorkStart{
    if ([userWorkStart isKindOfClass:[NSNull class]]) {
        _userWorkStart = @0;
    }else{
        _userWorkStart = userWorkStart;
    }
}
- (void)setIsDoctor:(NSString *)isDoctor{
    if ([isDoctor isKindOfClass:[NSNull class]]) {
        _isDoctor = @"";
    }else if ([isDoctor isEqualToString:@"doctor_tutor"]){
        _isDoctor = @"博士生导师";
    }else{
        _isDoctor = @"硕士生导师";
    }
}
- (void)setUserWorkEnd:(NSNumber *)userWorkEnd{
    if ([userWorkEnd isKindOfClass:[NSNull class]]) {
        _userWorkEnd = @0;
    }else{
        _userWorkEnd = userWorkEnd;
    }
}
@end

