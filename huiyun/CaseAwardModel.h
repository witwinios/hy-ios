//
//  CaseAwardModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseAwardModel : NSObject

@property (strong, nonatomic) NSString *caseAwardModelStatus;
@property (strong, nonatomic) NSString *caseAwardModelTitle;
@property (strong, nonatomic) NSString *caseAwardModelDegree;
@property (strong, nonatomic) NSNumber *caseAwardModelDate;
@property (strong, nonatomic) NSString *caseAwardModelTeacher;
@property (strong, nonatomic) NSNumber *caseAwardModelTeacherId;
@property (strong, nonatomic) NSString *caseAwardModelDescroption;
@property (strong, nonatomic) NSString *caseAwardModelAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@property (strong, nonnull) NSNumber *recordId;
@end
