//
//  AddPaperController.h
//  xiaoyun
//
//  Created by MacAir on 17/3/2.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuView.h"
#import "RequestTools.h"
@class MainViewController;
@interface AddPaperController : UIViewController
@property (copy, nonatomic) NSString *superTab;
@property (strong, nonatomic) NSString *testID;
@property (strong, nonatomic)MenuView *menuView;
@property (strong, nonatomic) NSString *selectCategoryId;
@property (strong, nonatomic) NSString *selectSubjectId;
@property (strong, nonatomic) NSString *selectKnowId;
@end
