//
//  AllStationController.h
//  yun
//
//  Created by MacAir on 2017/8/1.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExamPointCell.h"
#import "ControlManeger.h"
@interface AllStationController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (strong, nonatomic) CallModel *callModel;
@property (strong, nonatomic) SkillModel *skillModel;
@end
