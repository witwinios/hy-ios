//
//  ExamContentController.m
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamContentController.h"

@interface ExamContentController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    ExamContentModel *contentModel;
    
    NSMutableArray *heightArray;
}
@end

@implementation ExamContentController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    heightArray = [NSMutableArray new];
    [self setUI];
    [self loadData];
}
- (void)loadData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/currentStudent?osceStationId=%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,self.examModel.ExamPointModelStationId];
    
    NSLog(@"re%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"考试内容" Success:^(NSDictionary *result) {
        contentModel = [ExamContentModel new];
        contentModel.ExamContentModelRecordId = result[@"responseBody"][@"recordId"];
        contentModel.ExamContentModelDate = result[@"responseBody"][@"startTime"];
        contentModel.ExamContentModelStationId = result[@"responseBody"][@"osceStations"][0][@"stationId"];
        contentModel.ExamContentModelStationName = result[@"responseBody"][@"osceStations"][0][@"stationName"];
        contentModel.ExamContentModelStudentId = result[@"responseBody"][@"studentId"];
        contentModel.ExamContentModelStudentName = result[@"responseBody"][@"studentFullName"];
        contentModel.ExamContentModelContent = result[@"responseBody"][@"osceStations"][0][@"description"];
        contentModel.ExamContentModelPurpose = result[@"responseBody"][@"osceStations"][0][@"testPurpose"];
        [tabView reloadData];
    } failed:^(NSString *result) {
        
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"考试内容";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-110) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"ExamContentCell" bundle:nil] forCellReuseIdentifier:@"contentCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
    UIButton *markButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [markButton setTitle:@"打开评分表" forState:0];
    markButton.frame = CGRectMake(0, Sheight-46, Swidth, 46);
    markButton.titleLabel.textColor = [UIColor whiteColor];
    markButton.backgroundColor = UIColorFromHex(0x20B2AA);
    [markButton addTarget:self action:@selector(markAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:markButton];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)markAction{
    MarkTableController *markVC = [MarkTableController new];
    markVC.skillModel = self.skillModel;
    markVC.pointModel = self.examModel;
    markVC.callModel = self.callModel;
    [self.navigationController pushViewController:markVC animated:YES];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [Maneger getPonentH:contentModel.ExamContentModelContent andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-115]+23;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExamContentCell *cell = [tabView dequeueReusableCellWithIdentifier:@"contentCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == 0) {
        cell.title.text = @"考站名称:";
        cell.content.text = contentModel.ExamContentModelStationName;
    }else if (indexPath.row == 1){
        cell.title.text = @"考试目的:";
        cell.content.text = contentModel.ExamContentModelPurpose;
    }else if (indexPath.row == 2){
        cell.title.text = @"考试内容:";
        cell.content.text = contentModel.ExamContentModelContent;
    }else if (indexPath.row == 3){
        cell.title.text = @"考生姓名:";
        cell.content.text = self.callModel.callModelUserName;
    }else if (indexPath.row == 4){
        cell.title.text = @"考官姓名:";
        cell.content.text = self.callModel.callModelTeacher;
    }else if (indexPath.row == 5){
        cell.title.text = @"考试日期:";
        cell.content.text = [[Maneger shareObject] timeFormatter:contentModel.ExamContentModelDate.stringValue];
    }else if (indexPath.row == 6){
        cell.title.text = @"考试时长:";
        cell.content.text = self.examModel.ExamPointModelInternal.stringValue;
    }
    return cell;
}

@end
