//
//  CustomButton.m
//  yun
//
//  Created by MacAir on 2017/6/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 1;
        self.titleLabel.textAlignment=1;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return self;
}
@end
