//
//  PaperCellModel.m
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "PaperCellModel.h"

@implementation PaperCellModel
- (void)setPaperID:(NSNumber *)paperID{
    if ([paperID isKindOfClass:[NSNull class]]) {
        _paperID = 0;
    }else{
        _paperID = paperID;
    }
}
- (void)setPaperSubject:(NSString *)paperSubject{
    if ([paperSubject isKindOfClass:[NSNull class]]) {
        _paperSubject = @"空";
    }else{
        _paperSubject = paperSubject;
    }
}
- (void)setPaperCategory:(NSString *)paperCategory{
    if ([paperCategory isKindOfClass:[NSNull class]]) {
        _paperCategory = @"空";
    }else{
        _paperCategory = paperCategory;
    }
}
- (void)setTotalScore:(NSNumber *)totalScore{
    if ([totalScore isKindOfClass:[NSNull class]]) {
        _totalScore = [NSNumber numberWithInteger:0];
    }else{
        _totalScore = totalScore;
    }
}
- (void)setPaperName:(NSString *)paperName{
    if ([paperName isKindOfClass:[NSNull class]]) {
        _paperName = @"空";
    }else{
        _paperName = paperName;
    }
}
- (void)setPaperDes:(NSString *)paperDes{
    if ([paperDes isKindOfClass:[NSNull class]]) {
        _paperDes = @"暂无描述";
    }else{
        _paperDes = paperDes;
    }
}
- (void)setPaperStatus:(NSString *)paperStatus{
    if ([paperStatus isKindOfClass:[NSNull class]]) {
        _paperStatus = @"暂无";
    }else{
        if ([paperStatus isEqualToString:@"STARTED"]) {
            _paperStatus = @"开始";
        }else if ([paperStatus isEqualToString:@"COMPLETED"]){
            _paperStatus = @"完成";
        }else if ([paperStatus isEqualToString:@"NOT_START"]){
            _paperStatus = @"未开始";
        }
    }
}
@end
