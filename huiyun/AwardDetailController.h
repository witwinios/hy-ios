//
//  AwardDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseWitwinController.h"
#import "CaseAwardModel.h"
@interface AwardDetailController : BaseWitwinController
@property (strong, nonatomic) CaseAwardModel *model;
@end
