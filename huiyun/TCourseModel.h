//
//  TCourseModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCourseModel : NSObject
@property(strong,nonatomic)NSNumber *scheduleID;
@property(strong,nonatomic)NSString *scheduleStatus;
@property (strong,nonatomic)NSString *coursePlace;
@property(strong,nonatomic)NSString *courseSubjec;
@property(strong,nonatomic)NSString *courseName;
@property(strong,nonatomic)NSString *scheduleTyoe;
@property(strong,nonatomic)NSNumber *startTime;
@property (strong, nonatomic) NSNumber *endTime;
@property(strong,nonatomic)NSString *teacherName;
@property(strong,nonatomic)NSNumber *registNum;
@property(strong,nonatomic)NSString *courseDes;
@end
