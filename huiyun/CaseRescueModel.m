//
//  CaseRescueModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseRescueModel.h"

@implementation CaseRescueModel
//@property (strong, nonatomic) NSString *caseRescueModelStatus;
- (void)setCaseRescueModelStatus:(NSString *)caseRescueModelStatus{
    if ([caseRescueModelStatus isKindOfClass:[NSNull class]]) {
        _caseRescueModelStatus = @"";
    }else if ([caseRescueModelStatus isEqualToString:@"waiting_approval"]){
        _caseRescueModelStatus = @"待审核";
    }else if ([caseRescueModelStatus isEqualToString:@"approved"]){
        _caseRescueModelStatus = @"已通过";
    }else{
        _caseRescueModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelDiseaseName;
- (void)setCaseRescueModelDiseaseName:(NSString *)caseRescueModelDiseaseName{
    if ([caseRescueModelDiseaseName isKindOfClass:[NSNull class]]) {
        _caseRescueModelDiseaseName = @"";
    }else{
        _caseRescueModelDiseaseName = caseRescueModelDiseaseName;
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelDiseaserName;
- (void)setCaseRescueModelDiseaserName:(NSString *)caseRescueModelDiseaserName{
    if ([caseRescueModelDiseaserName isKindOfClass:[NSNull class]]) {
        _caseRescueModelDiseaserName = @"";
    }else{
        _caseRescueModelDiseaserName = caseRescueModelDiseaserName;
    }
}
//@property (strong, nonatomic) NSNumber *caseRescueModelDiseaseNum;
- (void)setCaseRescueModelDiseaseNum:(NSNumber *)caseRescueModelDiseaseNum{
    if ([caseRescueModelDiseaseNum isKindOfClass:[NSNull class]]) {
        _caseRescueModelDiseaseNum = @0;
    }else{
        _caseRescueModelDiseaseNum = caseRescueModelDiseaseNum;
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelBackDescription;
- (void)setCaseRescueModelBackDescription:(NSString *)caseRescueModelBackDescription{
    if ([caseRescueModelBackDescription isKindOfClass:[NSNull class]]) {
        _caseRescueModelBackDescription = @"";
    }else{
        _caseRescueModelBackDescription = caseRescueModelBackDescription;
    }
}
//@property (strong, nonatomic) NSNumber *caseRescueModelDate;
- (void)setCaseRescueModelDate:(NSNumber *)caseRescueModelDate{
    if ([caseRescueModelDate isKindOfClass:[NSNull class]]) {
        _caseRescueModelDate = @0;
    }else{
        _caseRescueModelDate = caseRescueModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelTeacher;
- (void)setCaseRescueModelTeacher:(NSString *)caseRescueModelTeacher{
    if ([caseRescueModelTeacher isKindOfClass:[NSNull class]]) {
        _caseRescueModelTeacher = @"";
    }else{
        _caseRescueModelTeacher = caseRescueModelTeacher;
    }
}
//@property (strong, nonatomic) NSNumber *caseRescueModelTeacherId;
- (void)setCaseRescueModelTeacherId:(NSNumber *)caseRescueModelTeacherId{
    if ([caseRescueModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseRescueModelTeacherId = @0;
    }else{
        _caseRescueModelTeacherId = caseRescueModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelDescription;
- (void)setCaseRescueModelDescription:(NSString *)caseRescueModelDescription{
    if ([caseRescueModelDescription isKindOfClass:[NSNull class]]) {
        _caseRescueModelDescription = @"";
    }else{
        _caseRescueModelDescription = caseRescueModelDescription;
    }
}
//@property (strong, nonatomic) NSString *caseRescueModelAdvice;
- (void)setCaseRescueModelAdvice:(NSString *)caseRescueModelAdvice{
    if ([caseRescueModelAdvice isKindOfClass:[NSNull class]]) {
        _caseRescueModelAdvice = @"";
    }else{
        _caseRescueModelAdvice = caseRescueModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
