//
//  OSCEModel.h
//  xiaoyun
//
//  Created by MacAir on 17/1/3.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "JSONModel.h"

@interface OSCEModel : NSObject<NSCoding>
@property (strong, nonatomic) NSNumber *osceID;
@property (strong, nonatomic) NSString *osceName;
@property (strong, nonatomic) NSString *osceStatus;
@property (strong, nonatomic) NSString *stationName;
@property (strong, nonatomic) NSString *osceContent;
@property (strong, nonatomic) NSNumber *osceTime;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSNumber *totalScore;
@property (strong, nonatomic) NSNumber *createTime;
@property (strong, nonatomic) NSString *subjectName;
@property (strong, nonatomic) NSString *osceCategory;
@property (strong, nonatomic) NSString *createName;
@property (strong, nonatomic) NSNumber *difficulty;
@property (strong, nonatomic) NSString *des;
@property (strong, nonatomic) NSNumber *osceStationNums;
@property (strong, nonatomic) NSNumber *timeBlock;
@end
