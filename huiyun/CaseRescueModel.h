//
//  CaseRescueModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseRescueModel : NSObject
@property (strong, nonatomic) NSString *caseRescueModelStatus;
@property (strong, nonatomic) NSString *caseRescueModelDiseaseName;
@property (strong, nonatomic) NSString *caseRescueModelDiseaserName;
@property (strong, nonatomic) NSNumber *caseRescueModelDiseaseNum;
@property (strong, nonatomic) NSString *caseRescueModelBackDescription;
@property (strong, nonatomic) NSNumber *caseRescueModelDate;
@property (strong, nonatomic) NSString *caseRescueModelTeacher;
@property (strong, nonatomic) NSNumber *caseRescueModelTeacherId;
@property (strong, nonatomic) NSString *caseRescueModelDescription;
@property (strong, nonatomic) NSString *caseRescueModelAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@property (strong, nonatomic) NSNumber *recordId;
@end
