//
//  TeachDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/10/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseWitwinController.h"
#import "CaseTeachModel.h"
@interface TeachDetailController : BaseWitwinController
@property (strong, nonatomic) CaseTeachModel *model;
@end
