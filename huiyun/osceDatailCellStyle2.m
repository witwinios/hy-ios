//
//  osceDatailCellStyle2.m
//  xiaoyun
//
//  Created by MacAir on 17/2/7.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "osceDatailCellStyle2.h"

@implementation osceDatailCellStyle2

- (void)awakeFromNib {
    self.work.adjustsFontSizeToFitWidth = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
