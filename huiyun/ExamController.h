//
//  ExamController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestTools.h"
#import "SkillModel.h"
#import "OSCEModel.h"
#import "onlineModel.h"
#import "ExamModelCell.h"
#import "LoadingView.h"
#import "OSCEDetailController.h"
#import "OnlineDetailController.h"
#import "SkillDetailController.h"
#import "SearchUI.h"
typedef void (^MyBasicBlock)(id result);
@interface ExamController : UIViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (copy, nonatomic) MyBasicBlock selfBlock;
@property(retain,nonatomic)UIView *backView;
@end
