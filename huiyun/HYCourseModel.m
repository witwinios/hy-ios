//
//  CourseModel.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "HYCourseModel.h"

@implementation HYCourseModel
- (void)setCourseName:(NSString *)courseName{
    if ([courseName isKindOfClass:[NSNull class]]) {
        _courseName = @"无";
    }else{
        _courseName = courseName;
    }
}
- (void)setCoursePlace:(NSString *)coursePlace{
    if ([coursePlace isKindOfClass:[NSNull class]]) {
        _coursePlace = @"暂无";
    }else{
        _coursePlace = coursePlace;
    }
}
- (void)setCourseStatus:(NSString *)courseStatus{
    if ([courseStatus isKindOfClass:[NSNull class]]) {
        _courseStatus = @"无";
    }else{
        if ([courseStatus isEqualToString:@"released"]) {
            _courseStatus = @"已发布";
        }else if ([courseStatus isEqualToString:@"completed"]){
            _courseStatus = @"完成";
        }else if ([courseStatus isEqualToString:@"started"]){
            _courseStatus = @"开始";
        }else{
            _courseStatus = @"计划中";
        }
    }
}
- (void)setCourseType:(NSString *)courseType{
    if ([courseType isKindOfClass:[NSNull class]]) {
        _courseType = @"无";
    }else{
        if ([courseType isEqualToString:@"online_course"]) {
            _courseType = @"网络课程";
        }else if ([courseType isEqualToString:@"classroom_course"]){
            _courseType = @"课堂课程";
        }else{
            _courseType = @"实操训练课程";
        }
    }
}
- (void)setCourseTime:(NSNumber *)courseTime{
    if ([courseTime isKindOfClass:[NSNull class]]) {
        _courseTime = 0;
    }else{
        _courseTime = courseTime;
    }
}
- (void)setCourseDes:(NSString *)courseDes{
    if ([courseDes isKindOfClass:[NSNull class]]) {
        _courseDes = @"暂无描述.";
    }else{
        _courseDes = courseDes;
    }
}
- (void)setCourseCategory:(NSString *)courseCategory{
    if ([courseCategory isKindOfClass:[NSNull class]]) {
        _courseCategory = @"暂无";
    }else{
        _courseCategory = courseCategory;
    }
}
- (void)setCourseTeahcher:(NSString *)courseTeahcher{
    if ([courseTeahcher isKindOfClass:[NSNull class]]) {
        _courseTeahcher = @"暂无教师.";
    }else{
        _courseTeahcher = courseTeahcher;
    }
}
- (void)setSubjectName:(NSString *)subjectName{
    if ([subjectName isKindOfClass:[NSNull class]]) {
        _subjectName = @"暂无";
    }else{
        _subjectName = subjectName;
    }
}
- (NSString *)display{
   return [NSString stringWithFormat:@"数据为：%@ %@ %@ %@ %@ %@ %@ %@ %@",_courseName,_courseType,_coursePlace,_courseTime,_courseStatus,_courseCategory,_courseTeahcher,_courseDes,_subjectName];
}
@end
