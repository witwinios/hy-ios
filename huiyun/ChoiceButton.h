//
//  ChoiceButton.h
//  yun
//
//  Created by MacAir on 2017/7/3.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceButton : UIButton
@property (strong, nonatomic) NSNumber *pageNum;
@property (strong, nonatomic) NSNumber *questionId;
@property (strong, nonatomic) NSString *answerIndex;
@property (strong, nonatomic) NSString *quesType;
@end
