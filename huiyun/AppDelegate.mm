//
//  AppDelegate.m
//  FastSdkDemo
//
//  Created by jiangcj on 16/10/11.
//  Copyright © 2016年 gensee. All rights reserved.
//

#import "AppDelegate.h"

#import "MainViewController.h"
#define IP @"http://www.hzwitwin.cn:81/witwin-ctts-web"
#define CODE @"witwin"
@interface AppDelegate (){
    BOOL isGoHome;
    UIBackgroundTaskIdentifier backIden;
}
@property (retain,nonatomic)  NSTimer *timer;


@property float autoSizeScaleX;
@property float autoSizeScaleY;


@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.loginVC = [LoginController share];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:self.loginVC];
    self.loginVC.from = @"delegate";
    self.window.rootViewController = navVC;
    [self.window makeKeyAndVisible];

    if ([LocalIP isEqualToString:@""] || [LocalIP isKindOfClass:[NSNull class]] || LocalIP == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:IP forKey:@"scanIP"];
        [defaults setObject:CODE forKey:@"scanCODE"];
        [defaults setObject:@"http://www.hzwitwin.cn:81" forKey:@"simpleIp"];
        [defaults synchronize];
        //保存成功
    }
    //定位代理
    [self initializeLocationService];
    //
<<<<<<< HEAD
//    [[SPKitExample sharedInstance] callThisInDidFinishLaunching];
    
    _playerManager = [GSPPlayerManager sharedManager];
=======
    [[SPKitExample sharedInstance] callThisInDidFinishLaunching];
    
    _playerManager = [GSPPlayerManager sharedManager];
    
>>>>>>> e5692741479ecdc0734dd4e8b73334f7a39e6928
    _playerManager.delegate = self;
    
    
   
    
    
    
    
    
    
    
    return YES;
}
- (void)initializeLocationService{
    // 初始化定位管理器
    _locationManager = [[CLLocationManager alloc] init];
    // 设置代理
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
    // 设置定位精确度到米
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    // 设置过滤器为无
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    // 开始定位
    // 取得定位权限，有两个方法，取决于你的定位使用情况
    // 一个是requestAlwaysAuthorization，一个是requestWhenInUseAuthorization
    [_locationManager requestAlwaysAuthorization];//这句话ios8以上版本使用。
    // 开始定位
    if ([CLLocationManager locationServicesEnabled]) {
        [_locationManager startUpdatingLocation];
    }else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请检查你的设备是否开启定位" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
#pragma -定位代理
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){
            CLPlacemark *placemark = [array objectAtIndex:0];
            //获取城市
            NSString *city = placemark.locality;
            if (!city) {
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                city = placemark.administrativeArea;
            }
            NSString *address = [placemark.addressDictionary objectForKey:@"FormattedAddressLines"][0];;
            [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"localAddress"];
        }
        else if (error == nil && [array count] == 0)
        {
            NSLog(@"无结果");
        }
        else if (error != nil)
        {
            NSLog(@"错误:%@", error);
        }
    }];
    //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
    [_locationManager stopUpdatingLocation];
}
@end
