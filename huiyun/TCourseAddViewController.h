//
//  TCourseAddViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

#import <MediaPlayer/MediaPlayer.h>
#import "Maneger.h"

#import "RequestTools.h"
typedef void (^MyBasicBlock)(id result);
@interface TCourseAddViewController : UIViewController


//课程名称：
@property (weak, nonatomic) IBOutlet UITextField *CourseName;

//课程类型：
@property (weak, nonatomic) IBOutlet UIButton *CourseMold;
- (IBAction)AddMoldAction:(id)sender;


//课程科目：
@property (weak, nonatomic) IBOutlet UIButton *CourseSubject;
- (IBAction)AddSubjectAction:(id)sender;

//课程描述：
@property (weak, nonatomic) IBOutlet UITextView *CourExplain;
@property (weak, nonatomic) IBOutlet UILabel *stirngLenghLabel;

//上课时间
@property (weak, nonatomic) IBOutlet UIButton *StartTime;
- (IBAction)StartAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *EndTime;
- (IBAction)EndAction:(id)sender;


//上课地点：
@property (weak, nonatomic) IBOutlet UIButton *CourseLocation;
- (IBAction)AddLocationAction:(id)sender;


//附件：
@property (weak, nonatomic) IBOutlet UIButton *CourseOtherBtn;
- (IBAction)CourseOtherBtnAction:(id)sender;

@property (nonatomic, copy) MyBasicBlock myBlock;

@end
