//
//  BeforeCourseController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/8.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeforeCourseCell.h"
@interface BeforeCourseController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)TCourseModel *model;
@end
