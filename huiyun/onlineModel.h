//
//  onlineModel.h
//  xiaoyun
//
//  Created by MacAir on 17/1/3.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface onlineModel : NSObject<NSCoding>
@property (strong, nonatomic) NSString *onlineName;
@property (strong, nonatomic) NSString *onlineStatus;
@property (strong, nonatomic) NSString *onlineRoom;
@property (strong, nonatomic) NSNumber *onlineTime;
@property (strong, nonatomic) NSNumber *onlineInternal;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSNumber *difficulty;
@property (strong, nonatomic) NSNumber *createTime;
@property (strong, nonatomic) NSString *createName;
@property (strong, nonatomic) NSNumber *onlinePages;
@property (strong, nonatomic) NSString *onlineDes;
@property (strong, nonatomic) NSNumber *timeBlock;
//
@property (strong, nonatomic) NSNumber *selectNums;
@property (strong, nonatomic) NSString *teacher;

@end
