//
//  CaseOperateModel.h
//  huiyun
//
//  Created by MacAir on 2017/10/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseOperateModel : NSObject
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSNumber *requestmentId;
@property (strong, nonatomic) NSString *caseStatus;
@property (strong, nonatomic) NSString *caseResult;
@property (strong, nonatomic) NSString *casePatientName;
@property (strong, nonatomic) NSNumber *caseNo;
@property (strong, nonatomic) NSNumber *caseDate;
@property (strong, nonatomic) NSString *caseTeacher;
@property (strong, nonatomic) NSString *caseReason;
@property (strong, nonatomic) NSString *caseDescription;
@property (strong, nonatomic) NSString *caseAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@end
