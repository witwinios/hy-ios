//
//  MarkCell.m
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MarkCell.h"

@implementation MarkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.koufenReason.layer.cornerRadius = 5;
    self.koufenReason.layer.masksToBounds = YES;
    self.koufenReason.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.koufenReason.layer.borderWidth = 1;
    self.koufenReason.returnKeyType = UIReturnKeyDone;
    
    self.koufenView.returnKeyType = UIReturnKeyDone;
    self.koufenView.keyboardType = UIKeyboardTypeDecimalPad;
    //布局
    float row_H = self.contentView.frame.size.width;
    
}
@end
