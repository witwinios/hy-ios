//
//  SelfTestModel.h
//  xiaoyun
//
//  Created by MacAir on 17/2/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelfTestModel : NSObject
@property (strong, nonatomic) NSString *testStatus;
@property (strong,nonatomic) NSString *testName;
@property (strong, nonatomic) NSNumber *pageNum;
@property (strong, nonatomic) NSString *testDes;
@property (strong, nonatomic) NSNumber *testId;
@end
