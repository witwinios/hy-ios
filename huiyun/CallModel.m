//
//  CallModel.m
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "CallModel.h"

@implementation CallModel
- (void)setCallModelUserNo:(NSNumber *)callModelUserNo{
    if ([callModelUserNo isKindOfClass:[NSNull class]] || [callModelUserNo isEqual:@""]) {
        _callModelUserNo = [NSNumber numberWithInt:0];
    }else{
        _callModelUserNo = callModelUserNo;
    }
}
- (void)setCallModelType:(NSString *)callModelType{
    if ([callModelType isEqualToString:@"AUTO"]) {
        _callModelType = @"自动";
    }else{
        _callModelType = @"手动";
    }
}
@end
