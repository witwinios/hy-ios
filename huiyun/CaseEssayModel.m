//
//  CaseEssayModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseEssayModel.h"

@implementation CaseEssayModel
//@property (strong, nonatomic) NSString *caseEssayModelStatus;
- (void)setCaseEssayModelStatus:(NSString *)caseEssayModelStatus{
    if ([caseEssayModelStatus isKindOfClass:[NSNull class]]) {
        _caseEssayModelStatus = @"";
    }else if([caseEssayModelStatus isEqualToString:@"waiting_approval"]){
        _caseEssayModelStatus = @"待审核";
    }else if ([caseEssayModelStatus isEqualToString:@"approved"]){
        _caseEssayModelStatus = @"已通过";
    }else{
        _caseEssayModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelTitle;
- (void)setCaseEssayModelTitle:(NSString *)caseEssayModelTitle{
    if ([caseEssayModelTitle isKindOfClass:[NSNull class]]) {
        _caseEssayModelTitle = @"";
    }else{
        _caseEssayModelTitle = caseEssayModelTitle;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelDegree;
- (void)setCaseEssayModelDegree:(NSString *)caseEssayModelDegree{
    if ([caseEssayModelDegree isKindOfClass:[NSNull class]]) {
        _caseEssayModelDegree = @"";
    }else{
        _caseEssayModelDegree = caseEssayModelDegree;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelCategory;
- (void)setCaseEssayModelCategory:(NSString *)caseEssayModelCategory{
    if ([caseEssayModelCategory isKindOfClass:[NSNull class]]) {
        _caseEssayModelCategory = @"";
    }else{
        _caseEssayModelCategory = caseEssayModelCategory;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelAuthor;
- (void)setCaseEssayModelAuthor:(NSString *)caseEssayModelAuthor{
    if ([caseEssayModelAuthor isKindOfClass:[NSNull class]]) {
        _caseEssayModelAuthor = @"";
    }else{
        _caseEssayModelAuthor = caseEssayModelAuthor;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelEdtion;
- (void)setCaseEssayModelEdtion:(NSString *)caseEssayModelEdtion{
    if ([caseEssayModelEdtion isKindOfClass:[NSNull class]]) {
        _caseEssayModelEdtion = @"";
    }else{
        _caseEssayModelEdtion = caseEssayModelEdtion;
    }
}
//@property (strong, nonatomic) NSNumber *caseEssayModelDate;
- (void)setCaseEssayModelDate:(NSNumber *)caseEssayModelDate{
    if ([caseEssayModelDate isKindOfClass:[NSNull class]]) {
        _caseEssayModelDate = @0;
    }else{
        _caseEssayModelDate = caseEssayModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelTeacher;
- (void)setCaseEssayModelTeacher:(NSString *)caseEssayModelTeacher{
    if ([caseEssayModelTeacher isKindOfClass:[NSNull class]]) {
        _caseEssayModelTeacher = @"";
    }else{
        _caseEssayModelTeacher = caseEssayModelTeacher;
    }
}
//@property (strong, nonatomic) NSNumber *caseEssayModelTeacherId;
- (void)setCaseEssayModelTeacherId:(NSNumber *)caseEssayModelTeacherId{
    if ([caseEssayModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseEssayModelTeacherId = @0;
    }else{
        _caseEssayModelTeacherId = caseEssayModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelDescription;
- (void)setCaseEssayModelDescription:(NSString *)caseEssayModelDescription{
    if ([caseEssayModelDescription isKindOfClass:[NSNull class]]) {
        _caseEssayModelDescription = @"";
    }else{
        _caseEssayModelDescription = caseEssayModelDescription;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelAdvice;
- (void)setCaseEssayModelAdvice:(NSString *)caseEssayModelAdvice{
    if ([caseEssayModelAdvice isKindOfClass:[NSNull class]]) {
        _caseEssayModelAdvice = @"";
    }else{
        _caseEssayModelAdvice = caseEssayModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
