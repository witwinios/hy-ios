//
//  MarkTableModel.h
//  yun
//
//  Created by MacAir on 2017/7/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarkTableModel : NSObject
@property (strong, nonatomic) NSNumber *MarkTableRecordId;
@property (strong, nonatomic) NSNumber *MarkTableOsceStationId;
@property (strong, nonatomic) NSString *MarkTableKey;
@property (strong, nonatomic) NSString *MarkTableKeyType;
@property (strong, nonatomic) NSString *MarkTableAnswer;
@property (strong, nonatomic) NSNumber *MarkTableTotalScore;
@property (strong, nonatomic) NSNumber *MarkTableTeacherId;
@property (strong, nonatomic) NSNumber *MarkTableTeacherName;
@property (strong, nonatomic) NSString *MarkTableDescription;

@end
