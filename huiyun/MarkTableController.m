//
//  MarkTableController.m
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MarkTableController.h"

@interface MarkTableController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    NSMutableArray *submitArray;
    NSMutableArray *heightArray;
    NSNumber *isHasSign;
    //记录每项的扣分数
    NSMutableArray *scoreIndex;
    
    ExamPointController *pointVC;
    
    UIImage *signImage;
    
    UIView *btnBack;
    UIButton *doneButton;
    UITextField *koufenField;
    UITextView *koufenReason;
    
    int table_change;
    float current_keyboard_H;
    //是否正在拉伸
    int isTrans;
    UIView *lastObject;
    BOOL isHaveDian;
}
@end

@implementation MarkTableController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    table_change = 0;
    isTrans  = 0;
    dataArray = [NSMutableArray new];
    submitArray = [NSMutableArray new];
    heightArray = [NSMutableArray new];
    isHasSign = [NSNumber new];
    scoreIndex = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyBoardHide:) name: UIKeyboardDidHideNotification object:nil];
    
    pointVC = [[ControlManeger share] PointVC];
    [self setUI];
}
- (void)getSignName{
    [MBProgressHUD showHUDAndMessage:@"获取评分表" toView:nil];
    NSString *Url = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/getSignName/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId];
    NSLog(@"req=%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseBody"] isKindOfClass:[NSNull class]]) {
            //
            isHasSign = [NSNumber numberWithInt:0];
            
            UIButton *signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            signBtn.frame = CGRectMake(0, Sheight-45, Swidth/2, 45);
            [signBtn setTitle:@"签名" forState:0];
            [signBtn setTitleColor:[UIColor whiteColor] forState:0];
            signBtn.backgroundColor = [UIColor lightGrayColor];
            [self.view addSubview:signBtn];
            [signBtn addTarget:self action:@selector(signAction:) forControlEvents:UIControlEventTouchUpInside];
            UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            submitBtn.frame = CGRectMake(Swidth/2, Sheight-45, Swidth/2, 45);
            [submitBtn setTitleColor:[UIColor whiteColor] forState:0];
            [submitBtn setTitle:@"提交" forState:0];
            [submitBtn addTarget:self action:@selector(submitTable:) forControlEvents:UIControlEventTouchUpInside];
            submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
            [self.view addSubview:submitBtn];
            [tabView reloadData];
            return ;
        }
        //nsdata转image
        UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        submitBtn.frame = CGRectMake(0, Sheight-45, Swidth, 45);
        [submitBtn setTitleColor:[UIColor whiteColor] forState:0];
        [submitBtn setTitle:@"提交" forState:0];
        submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
        [submitBtn addTarget:self action:@selector(submitTable:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:submitBtn];
        
        isHasSign = [NSNumber numberWithInt:1];
        
        NSString *imageStr = result[@"responseBody"];
        NSData *decodedImageData = [[NSData alloc]initWithBase64EncodedString:[imageStr substringFromIndex:22] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        signImage = [UIImage imageWithData:decodedImageData];
        
        [tabView reloadData];
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"评分表";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    UIButton *contentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    contentBtn.frame = CGRectMake(Swidth-100, 29.5, 95, 25);
    contentBtn.layer.cornerRadius = 5;
    contentBtn.layer.borderWidth = 1;
    contentBtn.layer.masksToBounds = YES;
    contentBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [contentBtn addTarget:self action:@selector(contentAction:) forControlEvents:UIControlEventTouchUpInside];
    [contentBtn setTitle:@"考试内容" forState:0];
    [navigationView addSubview:contentBtn];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64-45) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"MarkCell" bundle:nil] forCellReuseIdentifier:@"markTableCell"];
    tabView.bounces = NO;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
}

- (void)loadData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/operationRecords?pageStart=1&pageSize=999&teacherId=%@&studentId=%@&osceStationId=%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId,self.callModel.callModelUserId,self.pointModel.ExamPointModelStationId];
    
    NSLog(@"222%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取评分表" Success:^(NSDictionary *result) {
        NSArray *array = result[@"responseBody"][@"result"];
        [dataArray removeAllObjects];
        NSLog(@"markArrayCount=%ld",(unsigned long)array.count);
        for (NSDictionary *dic in array) {
            MarkTableModel *markModel = [MarkTableModel new];
            markModel.MarkTableRecordId = dic[@"recordId"];
            markModel.MarkTableOsceStationId = dic[@"osceStationId"];
            markModel.MarkTableKeyType = dic[@"operationKey"][@"keyType"];
            markModel.MarkTableKey = dic[@"operationKey"][@"key"];
            markModel.MarkTableAnswer = dic[@"operationKey"][@"answers"];
            markModel.MarkTableTotalScore = dic[@"scoreValue"];
            markModel.MarkTableTeacherId = dic[@"teacher"][@"userId"];
            markModel.MarkTableTeacherName = dic[@"teacher"][@"fullName"];
            markModel.MarkTableDescription = dic[@"operationKey"][@"description"];
            [dataArray addObject:markModel];
        }
        dataArray = (NSMutableArray *)[[dataArray reverseObjectEnumerator] allObjects];
        for (int s=0; s<dataArray.count; s++) {
            MarkTableModel *markModel = dataArray[s];
            CGFloat height = [Maneger getPonentH:markModel.MarkTableAnswer andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-116];
            [heightArray addObject:[NSString stringWithFormat:@"%f",height+292]];
        }
        for (int i=0; i<dataArray.count; i++) {
            MarkTableModel *tableModel = dataArray[i];
            NSLog(@"osceStationId=%@",tableModel.MarkTableOsceStationId);
            NSMutableDictionary *muDic = [NSMutableDictionary new];
            [muDic setValue:tableModel.MarkTableRecordId forKey:@"osceStationId"];
            [submitArray addObject:muDic];
            //
            NSMutableDictionary *Dic = [NSMutableDictionary new];
            [Dic setValue:@"" forKey:@"koufen"];
            [scoreIndex addObject:Dic];
        }
        NSLog(@"submitArray=%@",submitArray);
        [self getSignName];
    } failed:^(NSString *result) {
        
    }];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doneButtonshow: (NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    NSLog(@"%f--%f",tabView.contentOffset.y,tabView.contentSize.height);
    if ([lastObject isKindOfClass:[UITextField class]] && lastObject.tag - 500 + 1 == dataArray.count && isTrans == 0) {
        table_change = 1;
        isTrans = 1;
        current_keyboard_H = keyBoardHeight;
        [UIView animateWithDuration:0.5 animations:^{
            tabView.frame = CGRectMake(0, 64, Swidth, Sheight-64-45-keyBoardHeight);
            tabView.contentOffset = CGPointMake(0, tabView.contentOffset.y + keyBoardHeight);
        }];
        
    }else if ([lastObject isKindOfClass:[UITextView class]] && lastObject.tag - 300 +1 == dataArray.count && isTrans == 0){
        table_change = 1;
        isTrans = 1;
        current_keyboard_H = keyBoardHeight;
        [UIView animateWithDuration:0.5 animations:^{
            tabView.frame = CGRectMake(0, 64, Swidth, Sheight-64-45-keyBoardHeight);
            tabView.contentOffset = CGPointMake(0, tabView.contentOffset.y + keyBoardHeight);
        }];
        
    }
    if (btnBack != nil) {
        [btnBack removeFromSuperview];
    }
    btnBack = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight-keyBoardHeight-35, Swidth, 35)];
    btnBack.backgroundColor = UIColorFromHex(0xDBDBDB);
    doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(Swidth-70, 0, 70, 35);
    [doneButton setTitle:@"完成" forState: UIControlStateNormal];
    [doneButton addTarget:self action:@selector(hideKeyboard:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor blueColor] forState:0];
    [btnBack addSubview:doneButton];
    [self.view addSubview:btnBack];
}
- (void)keyBoardHide:(NSNotification *)notification{
    [btnBack removeFromSuperview];
    isTrans = 0;
    if (table_change == 1) {
        [UIView animateWithDuration:0.5 animations:^{
            tabView.frame = CGRectMake(0, 64, Swidth, Sheight-64-45);
        }];
    }
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
    isTrans = 1;
    if (table_change == 1) {
        [UIView animateWithDuration:0.5 animations:^{
            tabView.frame = CGRectMake(0, 64, Swidth, Sheight-64-45);
        }];
    }
}
- (void)resignFirstRespond{
    [koufenField resignFirstResponder];
    [koufenReason resignFirstResponder];
}

- (void)signAction:(UIButton *)btn{
    DrawController *drawVC = [DrawController new];
    [self.navigationController pushViewController:drawVC animated:YES];
}
- (void)contentAction:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)submitTable:(UIButton *)btn{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"提交后将无法修改,确定提交?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 250;
    [alertView show];
    
}
//
- (void)isMarkEnd{
    [MBProgressHUD showHUDAndMessage:@"检查中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseBody"][@"currentStudent"] isKindOfClass:[NSNull class]]) {
            //评分成功
            [MBProgressHUD showToastAndMessage:@"评分成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                //返回学生界面
                ControlManeger *VCManeger = [ControlManeger share];
                [self.navigationController popToViewController:VCManeger.ListSVC animated:YES];
            }];
            
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您已完成评分,请等待其他老师完成评分!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } failed:^(NSString *result) {
        
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (isHasSign.integerValue == 1) {
        return 50.f;
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (isHasSign.integerValue == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 50)];
        view.backgroundColor = [UIColor lightGrayColor];
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-100, 0, 100, 50)];
        lab.text = @"签名:";
        lab.textAlignment = 1;
        lab.font = [UIFont systemFontOfSize:18];
        [view addSubview:lab];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2, 5, 150, 40)];
        imageView.image = signImage;
        imageView.backgroundColor = [UIColor whiteColor];
        [view addSubview:imageView];
        return view;
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 100)];
    headView.backgroundColor = [UIColor lightGrayColor];
    UILabel *stuLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Swidth/2, 50)];
    stuLab.text = [NSString stringWithFormat:@"考生: %@",self.callModel.callModelUserName];
    stuLab.font = [UIFont systemFontOfSize:20];
    stuLab.textColor = [UIColor whiteColor];
    [headView addSubview:stuLab];
    UILabel *teacherLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, Swidth, 50)];
    teacherLab.text = [NSString stringWithFormat:@"考官: %@",self.callModel.callModelTeacher];
    teacherLab.textColor = [UIColor whiteColor];
    teacherLab.font = [UIFont systemFontOfSize:20];
    [headView addSubview:teacherLab];
    //时间
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, 0, Swidth/2-5, 50)];
    
    timeLab.text = [NSString stringWithFormat:@"日期: %@",[[Maneger shareObject] currentTimeF]];
    timeLab.textAlignment = 2;
    timeLab.textColor = [UIColor whiteColor];
    [headView addSubview:timeLab];
    
    return headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MarkCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    koufenField = cell.koufenView;
    koufenReason = cell.koufenReason;}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArray[indexPath.row] floatValue]+20.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MarkCell *markCell = [tableView dequeueReusableCellWithIdentifier:@"markTableCell"];
    if (markCell) {
        MarkTableModel *markModel = dataArray[indexPath.row];
        
        markCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        markCell.key.text = markModel.MarkTableKey;
        markCell.keyType.text = markModel.MarkTableKeyType;
        markCell.markDes.text = markModel.MarkTableDescription;
        markCell.answer.text = markModel.MarkTableAnswer;
        markCell.scoreValue.text = markModel.MarkTableTotalScore.stringValue;
        markCell.indexLabel.text = [NSString stringWithFormat:@"第%ld项",(long)indexPath.row+1];
        markCell.koufenReason.delegate = self;
        markCell.koufenReason.tag = 300+indexPath.row;
        markCell.koufenView.delegate = self;
        markCell.koufenView.tag = 500+indexPath.row;
        //
        NSMutableDictionary *muDic = submitArray[indexPath.row];
        markCell.koufenReason.text = muDic[@"comments"];
        //
        NSMutableDictionary *Dic = scoreIndex[indexPath.row];
        markCell.koufenView.text = Dic[@"koufen"];
    }else{
        
    }
    return markCell;
}
#pragma -扣分delegate
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSInteger rows = textView.tag-300;
    
    NSMutableDictionary *muDic = submitArray[rows];
    [muDic setValue:textView.text forKey:@"comments"];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    koufenField = textField;
    lastObject = textField;
}
- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text rangeOfString:@"."].location==NSNotFound) {
        isHaveDian=NO;
    }
    if ([string length]>0)
    {
        unichar single=[string characterAtIndex:0];//当前输入的字符
        if ((single >='0' && single<='9') || single=='.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length]==0){
                if(single == '.'){
                    [MBProgressHUD showToastAndMessage:@"亲，第一个数字不能为小数点" places:0 toView:nil];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
            }else if (textField.text.length == 1 && [textField.text isEqualToString:@"0"]){
                if (single != '.') {
                    [MBProgressHUD showToastAndMessage:@"亲，第一个数字不能为0~" places:0 toView:nil];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                    
                }
            }
            if (single=='.')
            {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian=YES;
                    return YES;
                }else
                {
                    [MBProgressHUD showToastAndMessage:@"亲，您已经输入过小数点了~" places:0 toView:nil];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            else
            {
                if (isHaveDian)//存在小数点
                {
                    //判断小数点的位数
                    NSRange ran=[textField.text rangeOfString:@"."];
                    NSInteger tt=range.location-ran.location;
                    if (tt <= 2){
                        return YES;
                    }else{
                        [MBProgressHUD showToastAndMessage:@"亲，您最多输入两位小数~" places:0 toView:nil];
                        return NO;
                    }
                }
                else
                {
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [MBProgressHUD showToastAndMessage:@"亲，您输入的格式不正确~" places:0 toView:nil];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger rows = textField.tag-500;
    MarkTableModel *markModel = dataArray[rows];
    NSMutableDictionary *muDic = submitArray[rows];
    NSMutableDictionary *Dic = scoreIndex[rows];
    if (markModel.MarkTableTotalScore.floatValue < textField.text.floatValue && [self isPureInt:textField.text] == YES) {
        [MBProgressHUD showToastAndMessage:@"扣分项不能大于总分!" places:0 toView:nil];
        textField.text = @"";
        return;
    }
    [Dic setValue:textField.text forKey:@"koufen"];
    
    CGFloat getScore = markModel.MarkTableTotalScore.floatValue - textField.text.floatValue;
    [muDic setValue:[NSString stringWithFormat:@"%.0f",getScore] forKey:@"score"];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    [self resignFirstRespond];
//    [btnBack removeFromSuperview];
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    lastObject = textView;
    koufenReason = textView;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 250) {
        [MBProgressHUD showToastAndMessage:@"提交评分中。。。" places:0 toView:nil];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/submitOperationGrades/%@/osceStation/%@/student/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId,self.pointModel.ExamPointModelStationId,self.callModel.callModelUserId];
        NSLog(@"333 %@",requestUrl);
        
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:submitArray Success:^(NSDictionary *result) {
            [MBProgressHUD hideHUDForView:nil];
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                //获取所有考点
                int isMark = 0;
                for (int s=0; s<pointVC.dataArray.count; s++) {
                    ExamPointModel *pointModel = pointVC.dataArray[s];
                    if (pointModel.ExamPointModelStationId.integerValue == self.pointModel.ExamPointModelStationId.integerValue) {
                        pointModel.ExamPointModelStatus = @"已评分";
                    }
                    if ([pointModel.ExamPointModelStatus isEqualToString:@"未评"]) {
                        isMark = 1;
                    }
                }
                if (isMark == 1) {
                    [MBProgressHUD showToastAndMessage:@"评分成功!" places:0 toView:nil];
                    //还有没评分考点
                    [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popToViewController:pointVC animated:YES];
                    }];
                }else{
                    [self isMarkEnd];
                }
            }else{
                [MBProgressHUD hideHUDForView:nil];
                [MBProgressHUD showToastAndMessage:@"打分失败!" places:0 toView:nil];
            }
            [tabView reloadData];
        } failed:^(NSString *result) {
            
        }];
    }else{
        [self isMarkEnd];
    }
}
@end

