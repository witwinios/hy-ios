//
//  ExamPointCell.m
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamPointCell.h"

@implementation ExamPointCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _pointStatus.layer.cornerRadius = 5;
    _pointStatus.layer.masksToBounds = YES;    
    self.layer.cornerRadius = 3;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    self.layer.borderWidth = 2;
}
@end
