//
//  HYCourseCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "HYCourseCell.h"

@implementation HYCourseCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setProperty:(HYCourseModel *)model{
    self.courseName.text = model.courseName;
    self.coursePlace.text = model.coursePlace;
    self.courseStatus.text = model.courseStatus;
    self.courseTime.text = [[Maneger shareObject] timeFormatter:model.courseTime.stringValue];
    self.courseType.text = model.courseType;
}
@end
