//
//  DesCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "DesCell.h"

@implementation DesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.detailDes.editable = NO;
}
@end
