//
//  DetailCellTwo.m
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DetailCellTwo.h"

@implementation DetailCellTwo

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentText.layer.cornerRadius = 5;
    self.contentText.layer.masksToBounds = YES;
    self.contentText.layer.borderColor = [UIColor grayColor].CGColor;
    self.contentText.layer.borderWidth = 1;
}

@end
