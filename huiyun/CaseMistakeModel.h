//
//  CaseMistakeModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseMistakeModel : NSObject
@property (strong, nonatomic) NSString *caseMistakeModelStatus;
@property (strong, nonatomic) NSString *caseMistakeModelCategory;
@property (strong, nonatomic) NSString *caseMistakeModelDegree;
@property (strong, nonatomic) NSNumber *caseMistakeModelDate;
@property (strong, nonatomic) NSString *caseMistakeModelTeacher;
@property (strong, nonatomic) NSString *caseMistakeModelTeacherId;
@property (strong, nonatomic) NSString *caseMistakeModelViaDescription;
@property (strong, nonatomic) NSString *caseMistakeModelReason;
//教训
@property (strong, nonatomic) NSString *caseMistakeModelLession;
@property (strong, nonatomic) NSString *caseMistakeModelAdvice;
@property (strong, nonatomic) NSString *caseMistakeModelLeader;
@property (strong, nonatomic) NSString *caseMistakeModelLeaderAdvice;
@property (strong, nonatomic) NSNumber *recordId;
@end
