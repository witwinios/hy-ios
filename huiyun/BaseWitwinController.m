//
//  BaseWitwinController.m
//  huiyun
//
//  Created by MacAir on 2017/10/27.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "BaseWitwinController.h"

@interface BaseWitwinController ()
@end

@implementation BaseWitwinController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUI];
}
- (NSArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSArray new];
    }
    return _dataArray;
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    _leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [_leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [_leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:_leftBtn];
    
    _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
    [_rightBtn setTitleColor:[UIColor whiteColor] forState:0];
    [_rightBtn setTitle:_rightTitle forState:0];
    [_rightBtn addTarget:self action:@selector(right) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:_rightBtn];
    
    _titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    _titleLab.center = CGPointMake(backNavigation.center.x, _leftBtn.center.y);
    _titleLab.textAlignment = 1;
    _titleLab.font = [UIFont systemFontOfSize:20];
    _titleLab.textColor = [UIColor whiteColor];
    _titleLab.text = _titles;
    [backNavigation addSubview:_titleLab];
    //
    _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    _table.separatorStyle = UITableViewCellAccessoryNone;
    _table.bounces = NO;
    _table.delegate = self;
    _table.dataSource = self;
    [_table registerNib:[UINib nibWithNibName:@"DetailCellOne" bundle:nil] forCellReuseIdentifier:@"one"];
    [_table registerNib:[UINib nibWithNibName:@"DetailCellTwo" bundle:nil] forCellReuseIdentifier:@"two"];
    [_table registerNib:[UINib nibWithNibName:@"DetailCellThree" bundle:nil] forCellReuseIdentifier:@"three"];
    [self.view addSubview:_table];
}
#pragma action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)right{
    _rightBlock();
}
#pragma delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    return cell;
}
@end

