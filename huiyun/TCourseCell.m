//
//  TCourseCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TCourseCell.h"

@implementation TCourseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.courseStatus.layer.cornerRadius = 5;
    self.courseStatus.layer.masksToBounds = YES;
}
- (void)setProperty:(TCourseModel *)model{
    self.courseName.text = model.courseName;
    self.courseStatus.text = model.scheduleStatus;
    self.coursePlace.text = model.coursePlace;
    self.startTime.text = [[Maneger shareObject] timeFormatter:model.startTime.stringValue];
    self.courseType.text = model.scheduleTyoe;
}
@end
