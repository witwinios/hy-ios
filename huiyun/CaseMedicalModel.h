//
//  CaseMedicalModel.h
//  huiyun
//
//  Created by MacAir on 2017/10/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseMedicalModel : NSObject
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSNumber *requestmentId;
@property (strong, nonatomic) NSString *caseOne;
@property (strong, nonatomic) NSString *caseTwo;
@property (strong, nonatomic) NSString *caseThree;
@property (strong, nonatomic) NSString *caseFour;
@property (strong, nonatomic) NSNumber *caseFive;

@property (strong, nonatomic) NSString *mainIos;
@property (strong, nonatomic) NSString *secondIos;
@property (strong, nonatomic) NSString *advice;
@end
