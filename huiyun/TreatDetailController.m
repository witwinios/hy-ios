//
//  TreatDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TreatDetailController.h"

@interface TreatDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation TreatDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"出诊记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";

    if ([self.model.caseTreatModelStatus isEqualToString:@"待审核"]) {
        TreatDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            VisitRecordController *visitVC = [VisitRecordController new];
            visitVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:visitVC animated:YES];
        };
    }

    titleArray = @[@"审核状态:",@"疾病种类:",@"出诊日期:",@"带教老师:",@"说明:",@"指导意见:",@"附件:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    self.dataArray = @[_model.caseTreatModelStatus,_model.caseTreatModelCategory,[[Maneger shareObject] timeFormatter1:_model.caseTreatModelDate.stringValue],_model.caseTreatModelTeacher,_model.caseTreatModelDescription,_model.caseTreatModelAdvice,_model.fileUrl];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 4 || indexPath.row == 5) {
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else if (indexPath.row == 6){
        return three;
    }else {
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}

@end
