//
//  TeachRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachRecordController.h"
#import "THDatePickerView.h"
@interface TeachRecordController ()
{
    NSString *current_date;
    NSString *current_end_date;
    UIView *btnBack;
    UIButton *currentDateBtn;
    
    NSNumber *has_image;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation TeachRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    current_end_date = @"";
    has_image = @0;
    if (![self.myModel isKindOfClass:[NSNull class]] || self.model != nil) {
        _organizationField.text = self.myModel.caseProject;
        _objectField.text = self.myModel.caseTeachObject;
        _numField.text = [NSString stringWithFormat:@"%@",self.myModel.caseNums];
        _contentTextview.text = self.myModel.caseTeachContent;
        
        if (self.myModel.caseStartTime.intValue == 0) {
            current_date = @"";
        }else if (self.myModel.caseEndTime.intValue == 0){
            current_end_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseStartTime];
            current_end_date = [NSString stringWithFormat:@"%@",self.myModel.caseEndTime];
            [self.startBtn setTitle:[[Maneger shareObject] timeFormatter:self.myModel.caseStartTime.stringValue] forState:0];
            [self.endBtn setTitle:[[Maneger shareObject] timeFormatter:self.myModel.caseEndTime.stringValue] forState:0];
        }
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardDidShowNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.startBtn.layer.cornerRadius = 4;
    self.startBtn.layer.masksToBounds = YES;
    self.startBtn.titleLabel.textAlignment = 0;
    self.startBtn.layer.cornerRadius = 4;
    self.startBtn.layer.masksToBounds = YES;
    self.startBtn.layer.cornerRadius = 4;
    self.endBtn.layer.masksToBounds = YES;
    self.endBtn.titleLabel.textAlignment = 0;
    self.endBtn.layer.cornerRadius = 4;
    self.endBtn.layer.masksToBounds = YES;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [currentDateBtn setTitle:date forState:0];
        if (currentDateBtn == _startBtn) {
            current_date = longDate;
        }else{
            current_end_date = longDate;
        }
    } andCancelBlock:^{
        self.btn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    self.dateView = dateView;

    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"教学记录";
    [backNavigation addSubview:titleLab];
    
}
- (void)resignFirstRespond{
    [self.organizationField resignFirstResponder];
    [self.numField resignFirstResponder];
    [self.objectField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
}
-(void)doneButtonshow: (NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    
    if (btnBack == nil) {
        btnBack = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight-keyBoardHeight-35, Swidth, 35)];
    }
    btnBack.backgroundColor = UIColorFromHex(0xDBDBDB);
    UIButton *doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(Swidth-70, 0, 70, 35);
    [doneButton setTitle:@"完成" forState: UIControlStateNormal];
    [doneButton addTarget:self action:@selector(hideKeyboard:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor blueColor] forState:0];
    [btnBack addSubview:doneButton];
    [self.view addSubview:btnBack];
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)startAction:(id)sender {
    currentDateBtn = (UIButton *)sender;
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];

}
- (IBAction)endAction:(id)sender {
    currentDateBtn = (UIButton *)sender;
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];

}
- (void)saveAction{
    if ([_organizationField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教学项目不能为空~" places:0 toView:nil];
        return;
    }
    if ([_objectField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"带教对象不能为空~" places:0 toView:nil];
        return;
    }
    if ([_numField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"人数不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"开始时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_end_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结束时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"内容不能为空~" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
    if (has_image.intValue == 0) {
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        SDepartModel *departModel = departVC.departModel;
        NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/tutoringRecords",LocalIP,departModel.SDepartRecordId];
        NSLog(@"request-%@",requestUrl);
        NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text};
        NSLog(@"params=%@",params);
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_current_Image.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            SDepartModel *departModel = departVC.departModel;
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/tutoringRecords",LocalIP,departModel.SDepartRecordId];
            NSLog(@"request-%@",requestUrl);
            NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"fileId":fileId};
            NSLog(@"params=%@",params);
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (void)updateAction{
    if ([_organizationField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教学项目不能为空~" places:0 toView:nil];
        return;
    }
    if ([_objectField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"带教对象不能为空~" places:0 toView:nil];
        return;
    }
    if ([_numField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"人数不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"开始时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_end_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结束时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"内容不能为空~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        SDepartModel *departModel = departVC.departModel;
        NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/tutoringRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
        NSLog(@"request-%@",requestUrl);
        NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text};
        NSLog(@"params=%@",params);
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_current_Image.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            SDepartModel *departModel = departVC.departModel;
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/tutoringRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
            NSLog(@"request-%@",requestUrl);
            NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"fileId":fileId};
            NSLog(@"params=%@",params);
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
                    }];
                }else{
                    [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _current_Image.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

- (IBAction)saveAction:(id)sender {
    if ([self.myModel isKindOfClass:[NSNull class]] || self.myModel == nil) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}
@end
