//
//  HYCourseCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYCourseModel.h"
#import "Maneger.h"
@interface HYCourseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *courseStatus;
@property (weak, nonatomic) IBOutlet UILabel *courseTime;
@property (weak, nonatomic) IBOutlet UILabel *coursePlace;
@property (weak, nonatomic) IBOutlet UILabel *courseName;
@property (weak, nonatomic) IBOutlet UILabel *courseType;
- (void)setProperty:(HYCourseModel *)model;
@end
