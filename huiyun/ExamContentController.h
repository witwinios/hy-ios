//
//  ExamContentController.h
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillModel.h"
#import "CallModel.h"
#import "ExamPointModel.h"
#import "ExamContentModel.h"
#import "ExamContentCell.h"
#import "MarkTableController.h"
@interface ExamContentController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) SkillModel *skillModel;
@property (strong, nonatomic) CallModel *callModel;
@property (strong, nonatomic) ExamPointModel *examModel;
@end
