//
//  Maneger.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    statusTypeEmail = 0,
    statusTypePhone,
    statusTypeCard,
    statusTypeBankCard
} statusType;
@interface Maneger : NSObject
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) UIView *maskView;
@property(assign, nonatomic) statusType status;
+ (id)shareObject;
/*
 正则表达式
*/
+ (BOOL)checkBankCardNumber:(NSString *)cardNumber;
+ (BOOL)deptNumInputShouldNumber:(NSString *)str;
+ (BOOL)IsPhoneNumber:(NSString *)number;
+ (BOOL) IsEmailAdress:(NSString *)Email;
+ (BOOL) IsIdentityCard:(NSString *)IDCardNumber;
+ (BOOL) IsBankCard:(NSString *)cardNumber;
+ (UIAlertController *)showAlertVC:(NSString *)title Message:(NSString *)message Style:(statusType)status;

+ (BOOL)userCardFomat:(NSNumber *)card;
+ (BOOL)userEmailFomat:(NSString *)email;
+ (BOOL)userPhoneFomat:(NSNumber *)phone;

+ (void)showAlert:(NSString *)message;
+ (void)showAlert:(NSString *)message andCurentVC:(UIViewController *)currentVC;
+ (void)showMessageAlert:(NSString *)message andCurrentVC:(UIViewController *)currentVC;
-(void)setMaskView:(NSString *)message andCurrentVC:(UIViewController *)currentVC;
-(void)hideMaskView;

- (NSString *)timeLineFormatter:(NSString *)time;
- (NSString *)dateToString:(NSDate *)date;
- (NSString *)dateTolineString:(NSDate *)date;
- (NSString *)dateToTimeInternal:(NSDate *)date;
- (NSString *)getCurrentTime;
- (NSString *)timeFormatter:(NSString *)time;
- (NSString *)timeFormatter1:(NSString *)time;
- (NSString *)current;
- (NSString *)currentTimeF;
- (NSString *)nowZeroTime;
- (NSString *)zeroTime;
- (NSString *)tomorrowTime;
- (NSString *)tommrowZeroTime;
- (NSString *)getTomorow;
- (NSString *)currentLong;
- (CGRect)relativeFrameForScreenWithView:(UIView *)view;
+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString;
-(CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width  andLineSpace:(CGFloat)lineSpace;

/**
 获取label的高度
 */
+(CGFloat)getPonentH:(NSString *)str andFont:(UIFont *)font andWidth:(CGFloat)width;
+(CGFloat)heightWithFont:(UIFont *)font width:(CGFloat)width string:(NSString *)string;
@end
