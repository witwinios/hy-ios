//
//  DrawController.m
//  yun
//
//  Created by MacAir on 2017/8/3.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "DrawController.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface DrawController ()

@end

@implementation DrawController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:navigationView];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(WIDTH/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(WIDTH/2, leftBtn.center.y);
    titleLab.text = @"签名";
    titleLab.font = [UIFont systemFontOfSize:18];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    UIButton *switchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    switchBtn.frame = CGRectMake(WIDTH-80, 27, 80, 30);
    [switchBtn setTitle:@"撤销" forState:0];
    switchBtn.layer.cornerRadius = 5;
    switchBtn.layer.masksToBounds = YES;
    switchBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    switchBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    switchBtn.layer.borderWidth = 1;
    [switchBtn addTarget:self action:@selector(cancaelAction:) forControlEvents:UIControlEventTouchUpInside];
    [switchBtn setTitleColor:[UIColor whiteColor] forState:0];
    [navigationView addSubview:switchBtn];
    //
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(0, HEIGHT-45, WIDTH, 45);
    saveBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    [saveBtn setTitle:@"保存" forState:0];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:0];
    [saveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    saveBtn.layer.cornerRadius = 5;
    saveBtn.layer.masksToBounds = YES;
    [saveBtn addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)cancaelAction:(UIButton *)btn{
    [self.drawView clear];
}
- (void)saveAction:(UIButton *)btn{
    [MBProgressHUD showHUDAndMessage:@"保存中" toView:nil];
    
    UIGraphicsBeginImageContext(self.drawView.frame.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    [self.drawView.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

//    self.imgBlock(newImage);
    
    //提交后台 1.先转成nsdata
    NSData *data = UIImageJPEGRepresentation(newImage, 0.5f);
    
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *imageStr = [NSString stringWithFormat:@"data:image/png;base64,%@",encodedImageStr];
   
    ControlManeger *VCManeger = [ControlManeger share];
    
    NSString *Url = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/setSignName/%@",LocalIP,VCManeger.ContentVC.skillModel.skillID,VCManeger.ContentVC.callModel.callModelStationId,LocalUserId];
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:@{@"signName":imageStr} Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        NSLog(@"result=%@",result);
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"签名成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"签名保存失败" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
    }];
}
@end
