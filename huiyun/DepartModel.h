//
//  DepartModel.h
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartModel : NSObject

@property (strong, nonatomic) NSString *departStudentName;
@property (strong, nonatomic) NSString *departStatus;
@property (strong, nonatomic) NSString *departName;
@property (strong, nonatomic) NSNumber *departIn;
@property (strong, nonatomic) NSNumber *departOut;

@end
