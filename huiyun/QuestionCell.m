//
//  QuestionCell.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "QuestionCell.h"
#define SCREEN_W [UIScreen mainScreen].bounds.size.width
@implementation QuestionCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.quesStatus.layer.cornerRadius = 5;
    self.quesStatus.layer.masksToBounds = YES;
    self.quesStatus.textAlignment = 1;
    self.quesStatus.font = [UIFont systemFontOfSize:10];
    //
    self.teacherTab.delegate = self;
    self.teacherTab.dataSource = self;
    self.teacherTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.teacherTab.layer.cornerRadius = 2;
    self.teacherTab.layer.borderWidth = 1;
    self.teacherTab.layer.masksToBounds = YES;
    self.teacherTab.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.teacherTab.bounces = NO;
    //
    self.quesDes.layer.cornerRadius = 2;
    self.quesDes.layer.borderWidth = 1;
    self.quesDes.layer.masksToBounds = YES;
    self.quesDes.editable = NO;
    self.quesDes.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //
}
- (void)setModel:(MyQuesModel *)model andCurrentVC:(UIViewController *)currentVC{
    self.superVC = currentVC;
    //
    self.quesName.text = model.quesTitle;
    self.memNum.text = [NSString stringWithFormat:@"%ld人",(unsigned long)model.memberArray.count];
    self.quesDes.text = model.quesDes;
    self.quesStatus.text = model.status;
    if ([model.status isEqualToString:@"未解决"]) {
        self.quesStatus.backgroundColor = [UIColor redColor];
    }else{
        self.quesStatus.backgroundColor = [UIColor greenColor];
    }
    //
    self.tribeArray = model.memberArray;
    [self.teacherTab reloadData];
}
#pragma -aciton
- (void)closeView:(UIButton *)btn{
    [btn.superview removeFromSuperview];
//    [_superVC.view removeFromSuperview];
}
- (void)phoneAction:(UIButton *)btn{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"操作"
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"呼叫", @"发信息",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:_superVC.view];
}
#pragma -协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tribeArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lab = [UILabel new];
    lab.frame = CGRectMake(0, 0, self.contentView.frame.size.width, 20);
    lab.textAlignment = 1;
    lab.text = @"老师列表";
    lab.backgroundColor = [UIColor lightGrayColor];
    return lab;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 20.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.showView = [UIView new];
    self.showView.backgroundColor = UIColorFromHex(0x66CDAA);
    self.showView.frame = CGRectMake(0, 0, SCREEN_W-100, 200);
    self.showView.center = CGPointMake(_superVC.view.frame.size.width/2, _superVC.view.frame.size.height/2);
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(SCREEN_W-160, 0, 60, 30);
    closeBtn.backgroundColor = [UIColor lightGrayColor];
    [closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.showView addSubview:closeBtn];
//    @property (copy, nonatomic) NSString *fullName;
//    @property (copy, nonatomic) NSString *email;
//    @property (strong, nonatomic) NSNumber *phone;
//    @property (copy, nonatomic) NSString *school;
    TribeMem *tribeModel = self.tribeArray[indexPath.row];
    
    UILabel *nameLabel = [UILabel new];
    nameLabel.frame = CGRectMake(5, 35, 40, 30);
    nameLabel.text = @"姓名:";
    UILabel *nameContent = [UILabel new];
    nameContent.frame = CGRectMake(50, 35, SCREEN_W-205, 30);
    nameContent.text = tribeModel.fullName;
    [self.showView addSubview:nameContent];
    [self.showView addSubview:nameLabel];
    
    UILabel *emailLab = [UILabel new];
    emailLab.frame = CGRectMake(5, 70, 40, 30);
    emailLab.text = @"邮箱:";
    UILabel *emailContent = [UILabel new];
    emailContent.frame = CGRectMake(50, 70, SCREEN_W-205, 30);
    emailContent.text = tribeModel.email;
    emailContent.adjustsFontSizeToFitWidth = YES;
    [self.showView addSubview:emailContent];
    [self.showView addSubview:emailLab];
    
    UILabel *phoneLab = [UILabel new];
    phoneLab.frame =CGRectMake(5, 105, 40, 30);
    phoneLab.text = @"手机:";
    UILabel *phoneContent = [UILabel new];
    phoneContent.frame = CGRectMake(50, 105, SCREEN_W-150, 30);
    phoneContent.adjustsFontSizeToFitWidth = YES;
    phoneContent.text = [NSString stringWithFormat:@"%@",tribeModel.phone];
    self.phoneNumber = tribeModel.phone;
    UIButton *phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    phoneBtn.frame = CGRectMake(SCREEN_W-150, 105, 40, 30);
    phoneBtn.layer.cornerRadius =2;
    phoneBtn.layer.masksToBounds = YES;
    [phoneBtn addTarget:self action:@selector(phoneAction:) forControlEvents:UIControlEventTouchUpInside];
    phoneBtn.backgroundColor = [UIColor blueColor];
    [phoneBtn setTitle:@"通话" forState:0];
    [self.showView addSubview:phoneBtn];
    [self.showView addSubview:phoneContent];
    [self.showView addSubview:phoneLab];
    
    //学校
    UILabel *schoolLab = [UILabel new];
    schoolLab.frame = CGRectMake(5, 140, 40, 30);
    schoolLab.text = @"学校:";
    UILabel *schoolContent = [UILabel new];
    schoolContent.frame = CGRectMake(50, 140, 150, 30);
    schoolContent.text = tribeModel.school;
    [self.showView addSubview:schoolLab];
    [self.showView addSubview:schoolContent];
    
    [_superVC.view addSubview:self.showView];
//    [_superVC.view bringSubviewToFront:self.showView];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    TribeMem *mem = self.tribeArray[indexPath.row];
    cell.textLabel.text = mem.fullName;
    cell.textLabel.textAlignment = 1;
    return cell;
}
#pragma -actionsheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex ==0) {
        UIWebView * callWebview = [[UIWebView alloc]init];
        
        [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.phoneNumber]]]];
        
        [[UIApplication sharedApplication].keyWindow addSubview:callWebview];
    }else if(buttonIndex == 1){
        UIWebView * callWebview = [[UIWebView alloc]init];
        
        [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms:%@",self.phoneNumber]]]];
        
        [[UIApplication sharedApplication].keyWindow addSubview:callWebview];
    }
}
@end
