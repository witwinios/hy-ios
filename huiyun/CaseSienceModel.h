//
//  CaseSienceModel.h
//  huiyun
//
//  Created by MacAir on 2017/11/1.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface CaseSienceModel : NSObject
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSString *caseSienceTitle;
@property (strong, nonatomic) NSString *caseSienceStatus;
@property (strong, nonatomic) NSString *caseSienceHeader;
@property (strong, nonatomic) NSString *caseSienceRole;
@property (strong, nonatomic) NSNumber *caseSienceDate;
@property (strong, nonatomic) NSNumber *caseSienceTeacherId;
@property (strong, nonatomic) NSString *caseSienceTeacher;
@property (strong, nonatomic) NSString *caseSienceComepleteDes;
@property (strong, nonatomic) NSString *caseSienceAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@end
