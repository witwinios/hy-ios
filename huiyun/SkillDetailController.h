//
//  SkillDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillModel.h"
#import "osceDetailCellStyle1.h"
@interface SkillDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) SkillModel *model;
@end
