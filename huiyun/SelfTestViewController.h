//
//  SelfTestViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfListCell.h"
#import "LoadingView.h"
#import "Maneger.h"
#import "SelfTestModel.h"
#import "SelfPaperController.h"
#import "AddTestController.h"
#import "RequestTools.h"
@interface SelfTestViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@end
