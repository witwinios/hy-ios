//
//  TStudentModel.m
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TStudentModel.h"

@implementation TStudentModel
- (void)setTStudentModelImage:(NSString *)TStudentModelImage{
    if ([TStudentModelImage isKindOfClass:[NSNull class]]) {
        _TStudentModelImage = @"暂无";
    }else{
        _TStudentModelImage = TStudentModelImage;
    }
}
- (void)setTStudentModelProfessional:(NSString *)TStudentModelProfessional{
    if ([TStudentModelProfessional isKindOfClass:[NSNull class]]||[TStudentModelProfessional isEqualToString:@""]) {
        _TStudentModelProfessional = @"暂无";
    }else{
        _TStudentModelProfessional = TStudentModelProfessional;
    }
}
- (void)setTStudentModelXuehao:(NSNumber *)TStudentModelXuehao{
    if ([TStudentModelXuehao isKindOfClass:[NSNull class]] || [TStudentModelXuehao isEqual:@""]) {
        _TStudentModelXuehao = [NSNumber numberWithInt:0];
    }else{
        _TStudentModelXuehao = TStudentModelXuehao;
    }
}
@end
