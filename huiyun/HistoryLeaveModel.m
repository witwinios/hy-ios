//
//  HistoryLeaveModel.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "HistoryLeaveModel.h"

@implementation HistoryLeaveModel

- (void)setLeaveRequestId:(NSNumber *)leaveRequestId{
    if ([leaveRequestId isKindOfClass:[NSNull class]]) {
        _leaveRequestId = [NSNumber numberWithInt:0];
    }else{
        _leaveRequestId = leaveRequestId;
    }
}
//@property (strong, nonatomic) NSString *approver;
- (void)setApprover:(NSString *)approver{
    if ([approver isKindOfClass:[NSNull class]]) {
        _approver = @"暂无";
    }else{
        _approver = approver;
    }
}
//@property (strong, nonatomic) NSString *approveStatus;
- (void)setApproveStatus:(NSString *)approveStatus{
    if ([approveStatus isKindOfClass:[NSNull class]]) {
        _approveStatus = @"无状态";
    }else if ([approveStatus isEqualToString:@"waiting_approval"]){
        _approveStatus = @"待审批";
    }else if ([approveStatus isEqualToString:@"approved"]){
        _approveStatus = @"同意";
    }else if([approveStatus isEqualToString:@"rejected"]){
        _approveStatus = @"拒绝";
    }
}
//@property (strong, nonatomic) NSNumber *leaveStartTime;
- (void)setLeaveStartTime:(NSNumber *)leaveStartTime{
    if ([leaveStartTime isKindOfClass:[NSNull class]]) {
        _leaveStartTime = [NSNumber numberWithInt:0];
    }else{
        _leaveStartTime = leaveStartTime;
    }
}
//@property (strong, nonatomic) NSNumber *leaveEndTime;
- (void)setLeaveEndTime:(NSNumber *)leaveEndTime{
    if ([leaveEndTime isKindOfClass:[NSNull class]]) {
        _leaveEndTime = [NSNumber numberWithInt:0];
    }else{
        _leaveEndTime = leaveEndTime;
    }
}
//@property (strong, nonatomic) NSString *leaveType;
- (void)setLeaveType:(NSString *)leaveType{
    if ([leaveType isKindOfClass:[NSNull class]]) {
        _leaveType = @"暂无";
    }else if([leaveType isEqualToString:@"SICK_LEAVE"]){
        _leaveType = @"病假";
    }else if ([leaveType isEqualToString:@"PERSONAL_LEAVE"]){
        _leaveType = @"事假";
    }else if ([leaveType isEqualToString:@"MATERNITY_LEAVE"]){
        _leaveType = @"产假";
    }else if ([leaveType isEqualToString:@"DAY_OFF"]){
        _leaveType = @"休假";
    }
}
//@property (strong, nonatomic) NSString *leaveReason;
- (void)setLeaveReason:(NSString *)leaveReason{
    if ([leaveReason isKindOfClass:[NSNull class]] || [leaveReason isEqualToString:@""]) {
        _leaveReason = @"暂无";
    }else{
        _leaveReason = leaveReason;
    }
}
//@property (strong, nonatomic) NSNumber *fileId;
- (void)setFileId:(NSNumber *)fileId{
    if ([fileId isKindOfClass:[NSNull class]]) {
        _fileId = [NSNumber numberWithInt:0];
    }else{
        _fileId = fileId;
    }
}
- (void)setReviewComments:(NSString *)reviewComments{
    if ([reviewComments isKindOfClass:[NSNull class]]) {
        _reviewComments = @"暂无";
    }else{
        _reviewComments = reviewComments;
    }
}
@end
