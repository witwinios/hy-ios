//
//  LeaveController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "LeaveController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface LeaveController ()
{
    //    UIView *mohuView;
    //
    UITableView *leaveTable;
    UITextView *textView;
    //
    UIDatePicker *startPick;
    UIDatePicker *endPick;
    YCPickerView *typePick;
    YCPickerView *teacherPick;
    //
    UIView *pickView;
    UIDatePicker *datePick;
    UITextField *dateDisplay;
    //
    YCPickerView *modelPick;
    YCPickerView *modelPock1;
    //获取textView
    UITextView *reason;
    //提交的数据
    NSMutableDictionary *submitDic;
    NSMutableArray *submitImage;
    //选择几行
    NSInteger selectIndex;
    UITextView *currentTextView;
    //
    CGRect originImageRect;
    //pickview上的time
    NSDate *startDate;
    NSDate *endDate;
}
@end

@implementation LeaveController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
    [self setCamera];
    [self createDatePick];
    [self loadTeacher];
}
//获取老师信息
- (void)loadTeacher{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/users/teachers?pageSize=999&accountStatus=active",LocalIP];
    NSLog(@"request=%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取老师中" Success:^(NSDictionary *result) {
        NSMutableArray *teacher = [NSMutableArray new];
        NSArray *teacherArray = [[result objectForKey:@"responseBody"]objectForKey:@"result"];
        NSLog(@"array = %@",teacherArray);
        for (int i=0; i<teacherArray.count; i++) {
            NSDictionary *teacherModel = [teacherArray objectAtIndex:i];
            NSMutableDictionary *teacherDic = [NSMutableDictionary new];
            [teacherDic setObject:[teacherModel objectForKey:@"userId"] forKey:@"teacherId"];
            [teacherDic setObject:[teacherModel objectForKey:@"fullName"] forKey:@"teacherName"];
            [teacher addObject:teacherDic];
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:teacher forKey:@"teacher"];
        [defaults synchronize];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"获取失败,请重新进入!" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
}
- (void)setCamera{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
}
- (void)setObj{
    self.localImage = [UIImageView new];
    //默认选取的是第一行
    selectIndex = 0;
    leaveTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH , HEIGHT-64) style:UITableViewStylePlain];
    leaveTable.dataSource = self;
    leaveTable.delegate = self;
    [leaveTable registerNib:[UINib nibWithNibName:@"LeaveCellStyle1" bundle:nil] forCellReuseIdentifier:@"leaveCell1"];
    [leaveTable registerNib:[UINib nibWithNibName:@"LeaveCellStyle2" bundle:nil] forCellReuseIdentifier:@"leaveCell2"];
    leaveTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:leaveTable];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, HEIGHT-50, WIDTH, 50);
    [button setTitle:@"提交" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:button];
    //模糊视图
    _mohuView = [UIView new];
    _mohuView.frame = CGRectMake(0, 64, WIDTH, HEIGHT-274);
    _mohuView.backgroundColor = [UIColor lightGrayColor];
    _mohuView.alpha = 0.5;
    modelPick = [[YCPickerView alloc]initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 250)];
    modelPock1 = [[YCPickerView alloc]initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 250)];
    [self.view addSubview:modelPick];
    [self.view addSubview:modelPock1];
    
    //
    submitImage = [NSMutableArray new];
    submitDic = [NSMutableDictionary new];
}
- (void)changeUI:(NSIndexPath*)indexPath andContent:(NSDate *)str{
    if (indexPath.row != 3) {
        LeaveCellStyle1 *cell = [leaveTable cellForRowAtIndexPath:indexPath];
        cell.content.text = [[Maneger shareObject] dateToString:str];
        [submitDic setObject:[[Maneger shareObject] dateToTimeInternal:str] forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
}
- (void)changeUI1:(NSIndexPath*)indexPath andContent:(NSString *)str{
    if (indexPath.row != 3) {
        LeaveCellStyle1 *cell = [leaveTable cellForRowAtIndexPath:indexPath];
        cell.content.text = str;
        [submitDic setObject:str forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
}

- (void)createDatePick{
    pickView = [UIView new];
    pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 210);
    pickView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickView];
    datePick = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 150)];
    [datePick setTimeZone:[NSTimeZone localTimeZone]];
    [datePick setDate:[NSDate date]];
    datePick.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_ch"];
    datePick.timeZone = [NSTimeZone timeZoneWithName:@"GTM+8"];
    [datePick setDatePickerMode:UIDatePickerModeDateAndTime];
    [datePick addTarget:self action:@selector(timeChanged:) forControlEvents:UIControlEventValueChanged];
    [pickView addSubview:datePick];
    //dateDisplay
    dateDisplay = [[UITextField alloc]initWithFrame:CGRectMake(WIDTH/6, 170, Swidth/2, 30)];
    dateDisplay.backgroundColor = [UIColor whiteColor];
    dateDisplay.textAlignment = 1;
    datePick.minimumDate = [NSDate new];
    dateDisplay.userInteractionEnabled = NO;
    dateDisplay.layer.cornerRadius = 5;
    dateDisplay.backgroundColor = UIColorFromHex(0x46bec8);
    dateDisplay.textColor = UIColorFromHex(0xffffff);
    //设置默认时间
    NSDateFormatter *fomatter = [NSDateFormatter new];
    [fomatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateDisplay.text = [fomatter stringFromDate:[datePick date]];
    NSLog(@"datePick=%@",datePick.date);
    //
    [pickView addSubview:dateDisplay];
    //
    UIButton *alphaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [alphaButton setTitle:@"确定" forState:UIControlStateNormal];
    alphaButton.backgroundColor = UIColorFromHex(0x46bec8);
    alphaButton.frame = CGRectMake(Swidth/6*4, 170, Swidth/6, 30);
    alphaButton.layer.cornerRadius = 5;
    alphaButton.titleLabel.textAlignment = 1;
    alphaButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [alphaButton addTarget:self action:@selector(timeSelect:) forControlEvents:UIControlEventTouchUpInside];
    [pickView addSubview:alphaButton];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"图标"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(leaveHistory1:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"请假条";
    [backNavigation addSubview:titleLab];
}
#pragma -Action
- (void)leaveHistory1:(UIButton *)btn{
    LeaveHistoryController *historyVC = [LeaveHistoryController shareInstance];
    [self.navigationController pushViewController:historyVC animated:YES];
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
//点击pickView上的确定按钮
- (void)timeSelect:(UIButton *)btn{
    [UIView animateWithDuration:1 animations:^{
        [_mohuView removeFromSuperview];
        pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 210);
        _myBlock(datePick.date);
    }];
}
//pickView滑动停止
- (void)timeChanged:(UIDatePicker *)pick{
    NSDateFormatter *fomatter = [NSDateFormatter new];
    [fomatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateDisplay.text = [fomatter stringFromDate:[pick date]];
}
- (void)submitImage{
    
    RequestTools *tool = [RequestTools new];
    NSDictionary *dic = @{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"};
    [tool postFile:self.localImage andParams:dic andURL:[NSString stringWithFormat:@"%@/files",LocalIP]];
    tool.responseBlock=^(NSDictionary *response){
        NSNumber *fileId= [[response objectForKey:@"responseBody"] objectForKey:@"fileId"];
        [self submitLeave:fileId];
    };
}
- (void)submitLeave:(NSNumber *)fileId{
    //判断条件
    if (submitDic.count == 4) {
        NSString *startTime = submitDic[@"0"];
        NSString *endTime = submitDic[@"1"];
        NSString *leaveReason = reason.text;
        NSDictionary *keyValue = @{@"病假":@"SICK_LEAVE",@"事假":@"PERSONAL_LEAVE",@"产假":@"MATERNITY_LEAVE",@"休假":@"DAY_OFF"};
        NSString *leaveType = keyValue[submitDic[@"2"]];
        //获取老师ID
        NSString *teacherId;
        NSArray *array = LocalTeacher;
        for (int i =0; i<array.count; i++) {
            NSDictionary *dic = array[i];
            if ([[dic objectForKey:@"teacherName"] isEqualToString:submitDic[@"5"]]) {
                teacherId = [dic objectForKey:@"teacherId"];
                break;
            }
        }
        NSDictionary *param = @{@"userId":LocalUserId,@"leaveTimeStart":startTime,@"leaveTimeEnd":endTime,@"leaveReason":leaveReason,@"reviewerId":teacherId,@"fileId":[NSString stringWithFormat:@"%@",fileId],@"leaveType":leaveType};
        
        RequestTools *tool = [RequestTools new];
        [tool postRequestPrams:param andURL:[NSString stringWithFormat:@"%@/attendance/leaveRequests",LocalIP]];
        tool.responseBlock=^(NSDictionary *response){
            
            [MBProgressHUD hideHUDForView:nil];
            if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"申请成功,请等待老师审核。" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alertView show];
                [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [alertView dismissWithClickedButtonIndex:0 animated:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                if ([[response objectForKey:@"errorCode"] isEqualToString:@"duplicate_leave_request"]) {
                    [Maneger showAlert:@"该段时间已经请假!" andCurentVC:self];
                }
            }
        };
    }else{
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"信息不完整!" places:0 toView:nil];
    }
}
- (void)submitLeave{
    NSLog(@"submitDic=%@",submitDic);
    //判断条件
    if (submitDic.count == 4) {
        NSString *startTime = submitDic[@"0"];
        NSString *endTime = submitDic[@"1"];
        
        NSLog(@"start=%@end=%@",startTime,endTime);
        
        NSString *leaveReason = reason.text;
        NSString *leaveType;
        if ([submitDic[@"2"] isEqualToString:@"病假"]) {
            leaveType = @"SICK_LEAVE";
        }else if([submitDic[@"2"] isEqualToString:@"事假"]){
            leaveType = @"PERSONAL_LEAVE";
        }else if ([submitDic[@"2"] isEqualToString:@"产假"]){
            leaveType = @"MATERNITY_LEAVE";
        }else{
            leaveType = @"DAY_OFF";
        }
        //获取老师ID
        NSString *teacherId;
        NSArray *array = LocalTeacher;
        for (int i =0; i<array.count; i++) {
            NSDictionary *dic = array[i];
            if ([[dic objectForKey:@"teacherName"] isEqualToString:submitDic[@"5"]]) {
                teacherId = [dic objectForKey:@"teacherId"];
                break;
            }
        }
        NSDictionary *param = @{@"userId":LocalUserId,@"leaveTimeStart":startTime,@"leaveTimeEnd":endTime,@"leaveReason":leaveReason,@"reviewerId":teacherId,@"leaveType":leaveType};
        RequestTools *tool = [RequestTools new];
        [tool postRequestPrams:param andURL:[NSString stringWithFormat:@"%@/attendance/leaveRequests",LocalIP]];
        tool.errorBlock = ^(NSString *code){
            if ([code isEqualToString:@"200"]) {
                [MBProgressHUD hideHUDForView:nil];
                [Maneger showAlert:@"请求错误,请检查你的网络或重新请假!" andCurentVC:self];
                return ;
            }
        };
        tool.responseBlock=^(NSDictionary *response){
            
            if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD hideHUDForView:nil];
                [Maneger showAlert:@"申请成功,请等待老师审核!" andCurentVC:self];
                [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                if ([[response objectForKey:@"errorCode"] isEqualToString:@"duplicate_leave_request"]) {
                    [MBProgressHUD hideHUDForView:nil];
                    [Maneger showAlert:@"该段时间已经请假!" andCurentVC:self];
                }
            }
        };
    }else{
        [MBProgressHUD hideHUDForView:nil];
        [Maneger showAlert:@"信息不完整!" andCurentVC:self];
    }
}

- (void)submitAction:(UIButton *)btn{
    //
    
    NSString *startTime = submitDic[@"0"];
    NSString *endTime = submitDic[@"1"];
    
    NSLog(@"start=%@ end=%@",submitDic[@"0"],submitDic[@"1"]);
    
    if ([startTime compare:endTime]>0 || [startTime compare:endTime]==0) {
        
        [MBProgressHUD showToastAndMessage:@"请假时间错误" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"请假中..." toView:nil];
    NSLog(@"start=%@end=%@",startTime,endTime);
    
    //
    if ([self.localImage.image isKindOfClass:[NSNull class]] || self.localImage.image == NULL) {
        [self submitLeave];
    }else{
        [self submitImage];
    }
}
#pragma -协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        return 121.f;
    }else{
        if (indexPath.row == 4 && self.localImage.image != nil) {
            return 114.f;
        }
        return 44.f;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectIndex = indexPath.row;
    
    if (indexPath.row == 0) {
        [UIView animateWithDuration:0.5 animations:^{
            [self.view addSubview:_mohuView];
            pickView.frame = CGRectMake(0, HEIGHT-210, WIDTH, 210);
        }];
        __block LeaveController *leaveVC = self;
        self.myBlock = ^(NSDate *result){
            [leaveVC.mohuView removeFromSuperview];
            [leaveVC changeUI:indexPath andContent:result];
        };
    }else if (indexPath.row == 1){
        [UIView animateWithDuration:0.5 animations:^{
            [self.view addSubview:_mohuView];
            pickView.frame = CGRectMake(0, HEIGHT-210, WIDTH, 210);
        }];
        __block LeaveController *leaveVC = self;
        self.myBlock = ^(NSDate *result){
            [leaveVC.mohuView removeFromSuperview];
            [leaveVC changeUI:indexPath andContent:result];
        };
        
    }else if (indexPath.row == 2){
        [self.view addSubview:_mohuView];
        modelPick.arrPickerData = @[@"病假",@"事假",@"产假",@"休假"];
        [modelPick.pickerView reloadAllComponents];
        __block LeaveController *leaveVC = self;
        modelPick.selectBlock = ^(NSString *result){
            [leaveVC.mohuView removeFromSuperview];
            [leaveVC changeUI1:indexPath andContent:result];
        };
        [modelPick popPickerView];
    }else if (indexPath.row == 4){
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
        [sheet showInView:self.view];
    }else{
        [self.view addSubview:_mohuView];
        NSMutableArray *array = [[NSUserDefaults standardUserDefaults]objectForKey:@"teacher"];
        NSMutableArray *data = [NSMutableArray new];
        for (int i=0; i<array.count; i++) {
            [data addObject:[array[i] objectForKey:@"teacherName"]];
        }
        modelPock1.arrPickerData = data;
        [modelPock1.pickerView reloadAllComponents];
        __block LeaveController *leaveVC = self;
        modelPock1.selectBlock = ^(NSString *result){
            [leaveVC.mohuView removeFromSuperview];
            [leaveVC changeUI1:indexPath andContent:result];
        };
        [modelPock1 popPickerView];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        LeaveCellStyle2 *cell2 = [tableView dequeueReusableCellWithIdentifier:@"leaveCell2"];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        reason = cell2.leaveReason;
        return cell2;
    }else{
        LeaveCellStyle1 *cell1 = [tableView dequeueReusableCellWithIdentifier:@"leaveCell1"];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            cell1.labelText.text = @"开始时间:";
        }else if (indexPath.row == 1){
            cell1.labelText.text = @"结束时间:";
        }else if (indexPath.row == 2){
            cell1.labelText.text = @"请假类型:";
        }
        else if (indexPath.row == 4){
            cell1.labelText.text = @"上传图片";
            cell1.content.text = @"可选择";
            //创建图片
            if (self.localImage != nil) {
                UIImageView *imgView = [UIImageView new];
                imgView.frame = CGRectMake(15, 50, 60, 60);
                imgView.image = self.localImage.image;
                [cell1.contentView addSubview:imgView];
                imgView.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bigger:)];
                tap.numberOfTapsRequired = 1;
                [imgView addGestureRecognizer:tap];
            }
        }else{
            cell1.labelText.text = @"审批老师:";
        }
        return cell1;
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [textView resignFirstResponder];
}
- (void)bigger:(UITapGestureRecognizer *)tap{
    UIImageView *imgView = (UIImageView *)tap.view;
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    view.backgroundColor = [UIColor lightGrayColor];
    UIImageView *baseView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    baseView.userInteractionEnabled = YES;
    baseView.image = imgView.image;
    UITapGestureRecognizer *tapAgin = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(small:)];
    tapAgin.numberOfTapsRequired = 1;
    [baseView addGestureRecognizer:tapAgin];
    [view addSubview:baseView];
    [self.view addSubview:view];
}
- (void)small:(UITapGestureRecognizer *)tap{
    [[tap.view superview] removeFromSuperview];
}
- (void)photoAction{
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
#pragma UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
#pragma UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.localImage.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    [leaveTable reloadData];
}
@end
