//
//  RoundViewCell.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RoundViewCell.h"

@implementation RoundViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self layoutSubviews];
}
- (void)layoutSubviews{
    self.recordName.frame = CGRectMake(0, 15, self.contentView.frame.size.width/4, 21);
    self.passLab.frame = CGRectMake(self.contentView.frame.size.width/4, 15, self.contentView.frame.size.width/4, 21);
    self.referLab.frame = CGRectMake(self.contentView.frame.size.width/2, 15, self.contentView.frame.size.width/4, 21);
    self.pendLab.frame = CGRectMake(self.contentView.frame.size.width/4*3, 15, self.contentView.frame.size.width/4, 21);
    self.line.frame = CGRectMake(0, 49, self.contentView.frame.size.width, 1);
}
@end
