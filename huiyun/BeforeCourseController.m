//
//  BeforeCourseController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/8.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "BeforeCourseController.h"

@interface BeforeCourseController ()
{
    UITableView *beforeTable;
    NSMutableArray *dataArray;
    NSMutableArray *heightArray;
}
@end

@implementation BeforeCourseController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [beforeTable headerBeginRefreshing];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 34.5, 15, 15);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课前答题";
    [backNavigation addSubview:titleLab];
    
    dataArray = [NSMutableArray new];
    heightArray = [NSMutableArray new];
    beforeTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    beforeTable.delegate = self;
    beforeTable.dataSource = self;
    beforeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    beforeTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [beforeTable registerNib:[UINib nibWithNibName:@"BeforeCourseCell" bundle:nil] forCellReuseIdentifier:@"beforeCell"];
    [self.view addSubview:beforeTable];
    //添加下拉刷新
    [beforeTable addHeaderWithTarget:self action:@selector(refresh)];
    //设置文字
    beforeTable.headerPullToRefreshText = @"下拉刷新";
    beforeTable.headerReleaseToRefreshText = @"松开进行刷新";
    beforeTable.headerRefreshingText = @"刷新中。。。";
}
- (void)refresh{
    RequestTools *tool = [RequestTools new];
    [tool getRequest:[NSString stringWithFormat:@"%@/courseSchedules/%@/answerStatics?pageStart=1&pageSize=999&courseQuestionType=preview_question",LocalIP,self.model.scheduleID]];
    NSLog(@"beforeAPI=%@",[NSString stringWithFormat:@"%@/courseSchedules/%@/answerStatics?pageStart=1&pageSize=999&courseQuestionType=preview_question",LocalIP,self.model.scheduleID]);
   
    tool.errorBlock = ^(NSString *code){
        if (![code isEqualToString:@"200"]) {
            [beforeTable headerEndRefreshing];
            [Maneger showAlert:@"请求错误!" andCurentVC:self];
            return ;
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        NSLog(@"before=%@",response);
         [beforeTable headerEndRefreshing];
        [dataArray removeAllObjects];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[response objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [beforeTable headerEndRefreshing];
            [self.navigationController popToRootViewControllerAnimated:YES];
            return;
        }
       
        NSArray *resultArray = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        if (resultArray.count == 0) {
            [Maneger showAlert:@"没内容!" andCurentVC:self];
            return;
        }
        for (int i=0; i<resultArray.count; i++) {
            NSDictionary *responseDic = resultArray[i];
            BeforeCellModel *cellModel = [BeforeCellModel new];
            cellModel.title = [[responseDic objectForKey:@"question"] objectForKey:@"questionTitle"];
            cellModel.correctNum = [responseDic objectForKey:@"correct"];
            cellModel.inCorrectNum = [responseDic objectForKey:@"incorrect"];
            cellModel.weiNum = [responseDic objectForKey:@"unfinished"];
            [dataArray addObject:cellModel];
            //计算高度
            [heightArray addObject:[NSString stringWithFormat:@"%f",[Maneger getPonentH:cellModel.title andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-70]]];
            
        }
        
        [beforeTable reloadData];
    };
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -dataSourse delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.5f+[heightArray[indexPath.row] integerValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BeforeCourseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"beforeCell"];
    [cell setProperty:dataArray[indexPath.row]];
    return cell;
}
@end
