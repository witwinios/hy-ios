//
//  HistoryCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "HistoryCell.h"

@implementation HistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.approveStatus.layer.cornerRadius = 5;
    self.approveStatus.layer.masksToBounds = YES;
}
- (void)setProperty:(HistoryLeaveModel *)model{
    self.approver.text = model.approver;
    self.approveStatus.text = model.approveStatus;
    if ([model.approveStatus isEqualToString:@"同意"]) {
        self.approveStatus.backgroundColor = [UIColor greenColor];
    }else if ([model.approveStatus isEqualToString:@"待审批"]){
        self.approveStatus.backgroundColor = [UIColor orangeColor];
    }else if ([model.approveStatus isEqualToString:@"拒绝"]){
        self.approveStatus.backgroundColor = [UIColor redColor];
    }
    self.leaveTime.text = [[Maneger shareObject] timeFormatter:model.leaveStartTime.stringValue];
    self.approveType.text = model.leaveType;
}
@end
