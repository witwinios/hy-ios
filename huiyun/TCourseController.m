//
//  CourseViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "TCourseController.h"
#import "PopViewController.h"
#import "MyCell.h"
#import "TCourseAddViewController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface TCourseController ()
{
    //
    UITableView *courseTable;
    //
    NSMutableArray *dataArray;
    NSMutableArray *resultArray;
    NSMutableArray *NewArray;
    //
    NSMutableArray *historyArray;
    //
    int currentPage;
    PopViewController *_popVC;
    
    
    UIView *footer;
    UILabel *footerLabel;
}
@property(nonatomic,assign,getter=isFirstResponder)BOOL footerfrefrshing;

@property(nonatomic,strong)UISearchController *searchController;

@property(nonatomic,strong)NSMutableArray *listFilterList;

@end

@implementation TCourseController




- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];
    [self setObj];
    
    
    
    //添加上拉加载刷新
    [self setUpRefresh];

 
}
- (void)setObj{
    currentPage = 1;
    //
    dataArray = [NSMutableArray new];
    resultArray = [NSMutableArray new];
    NewArray=[NSMutableArray new];
    //
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    [courseTable registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellReuseIdentifier:@"tcourseCell"];
    [self.view addSubview:courseTable];
    
}
- (void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
        courseTable.footerPullToRefreshText = @"上拉加载";
        courseTable.footerReleaseToRefreshText = @"松开进行加载";
        courseTable.footerRefreshingText = @"加载中。。。";
 
    
    

}

-(void)FilterContentForSearchText:(NSString *)searchText scope:(NSUInteger)scope{
    
    
    if([searchText length] == 0){
        self.listFilterList=[NSMutableArray arrayWithArray:dataArray];
        return ;
        
    }
    
    
}



- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"Add"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(AddBtn) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程安排";
    [backNavigation addSubview:titleLab];
    
    
    UIButton *searchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame=CGRectMake(self.view.frame.size.width-75, 29.5, 25, 25);
    [searchBtn setImage:[UIImage imageNamed:@"Seach"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:searchBtn];
    
    
    
}
- (void)downRefresh{
   

    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=",LocalIP,LocalUserId,currentPage];
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
                        [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
                        [courseTable headerEndRefreshing];
                        return;
                    }
                    [dataArray removeAllObjects];
                    [resultArray removeAllObjects];
        
        
                    //获取数据源
                    if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
                        if (array.count ==0) {
                            [Maneger showAlert:@"无课程!" andCurentVC:self];
                            [courseTable headerEndRefreshing];
                        }else{
                            for (int i=0; i<array.count; i++) {
                                NSDictionary *dictionary = [array objectAtIndex:i];
                                TCourseModel *model = [TCourseModel new];
                                model.scheduleID = [dictionary objectForKey:@"scheduleId"];
                                model.courseDes = [dictionary objectForKey:@"courseDescription"];
                                model.courseSubjec = [dictionary objectForKey:@"subjectName"];
                                model.startTime = [dictionary objectForKey:@"startTime"];
                                model.endTime = [dictionary objectForKey:@"endTime"];
                                model.scheduleTyoe = [dictionary objectForKey:@"scheduleType"];
                                model.scheduleStatus = [dictionary objectForKey:@"scheduleStatus"];
                                model.registNum = [dictionary objectForKey:@"registeredStudentsNum"];
                                model.courseName = [dictionary objectForKey:@"courseName"];
                                model.coursePlace = [dictionary objectForKey:@"classroomName"];
                                model.teacherName = [dictionary objectForKey:@"teacherName"];
            
                                [dataArray addObject:model];
                                [resultArray addObject:model];
                            }
                            NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                            [courseTable reloadData];
                            [courseTable headerEndRefreshing];
                        }
                    }
                    else{
                        [Maneger showAlert:@"请重新登录!" andCurentVC:self];
                    }
        
        
    } failed:^(NSString *result) {
        
        if (![result isEqualToString:@"200"]) {
                        [courseTable headerEndRefreshing];
                    }
                    return;
    }];
    
}


- (void)loadRefresh{
    
    currentPage++;
    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=10&pageStart=%d&scheduleStatus=",LocalIP,LocalUserId,currentPage];
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
         [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];

        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无课程!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    TCourseModel *model = [TCourseModel new];
                    model.scheduleID = [dictionary objectForKey:@"scheduleId"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.courseSubjec = [dictionary objectForKey:@"subjectName"];
                    model.startTime = [dictionary objectForKey:@"startTime"];
                    model.endTime = [dictionary objectForKey:@"endTime"];
                    model.scheduleTyoe = [dictionary objectForKey:@"scheduleType"];
                    model.scheduleStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.registNum = [dictionary objectForKey:@"registeredStudentsNum"];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.teacherName = [dictionary objectForKey:@"teacherName"];
                    
                    [newData addObject:model];
                }
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [courseTable reloadData];
            }
        }
        else{
           [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
        }
        
        
    } failed:^(NSString *result) {
        
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}


- (void)selectAction:(UIButton *)button{
    _popVC = [PopViewController share];
    _popVC.modalPresentationStyle = UIModalPresentationPopover;
    //可以指示小箭头颜色
    _popVC.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    //设置属性
    _popVC.keyArray = @[@"计划中",@"已发布",@"正在上课",@"已经完成"];
    //content尺寸
    _popVC.preferredContentSize = CGSizeMake(400, 400);
    //pop方向
    _popVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    //delegate
    _popVC.popoverPresentationController.delegate = self;
    //点击的按钮
    __block PopViewController *weakSelf = _popVC;
    __block UITableView *weakCourse = courseTable;
    __block NSMutableArray *weakDataArray = dataArray;
    __block NSMutableArray *weakResultArray = resultArray;
    _popVC.indexBlock = ^(NSString *result){
        //清除数据,重新装
        [weakDataArray removeAllObjects];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        if ([result isEqualToString:@"计划中"]) {
            for (int s=0; s<weakResultArray.count; s++) {
                TCourseModel *model = weakResultArray[s];
                if ([model.scheduleStatus isEqualToString:@"计划中"]) {
                    [weakDataArray addObject:model];
                }
            }
        }else if ([result isEqualToString:@"正在上课"]){
            for (int s=0; s<weakResultArray.count; s++) {
                TCourseModel *model = weakResultArray[s];
                if ([model.scheduleStatus isEqualToString:@"已开始"]) {
                    [weakDataArray addObject:model];
                }
            }
        }else if ([result isEqualToString:@"已发布"]){
            for (int s=0; s<weakResultArray.count; s++) {
                TCourseModel *model = weakResultArray[s];
                if ([model.scheduleStatus isEqualToString:@"已发布"]) {
                    [weakDataArray addObject:model];
                }
            }
        }else{
            for (int s=0; s<weakResultArray.count; s++) {
                TCourseModel *model = weakResultArray[s];
                if ([model.scheduleStatus isEqualToString:@"已完成"]) {
                    [weakDataArray addObject:model];
                }
            }
        }
        [weakCourse reloadData];
    };
    
    //设置依附的按钮
    _popVC.popoverPresentationController.barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    [self presentViewController:_popVC animated:YES completion:nil];
}

-(void)AddBtn{
    [self.navigationController pushViewController:[TCourseAddViewController new] animated:YES];
}


#pragma -action

- (void)back :(UIButton *)button{
    if (_popVC != nil) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -tableView协议

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 176.f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TCourseDetailController *detailVC = [TCourseDetailController new];
    detailVC.model = dataArray[indexPath.section];
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tcourseCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setProperty:[dataArray objectAtIndex:indexPath.section]];
    return cell;
    
}



#pragma mark 监听滚动：
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//
//    if(courseTable.contentSize.height == 0) return;
//
//    if(self.isFirstResponder) return;
//
//    CGFloat ofsetY= courseTable.contentSize.height+courseTable.contentInset.bottom-courseTable.frame.size.height;
//
//    if(courseTable.contentOffset.y >= ofsetY){
//        footerLabel.text=@"正在加载更多数据...";
//        self.footerfrefrshing=YES;
//        //发送请求：
//
//        NSLog(@"currentPage:%d",currentPage);
//
//        //请求服务器：
//       // [self footerRefereshing];
//
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//            [courseTable reloadData];
//            //结束刷新：
//            self.footerfrefrshing=NO;
//            footerLabel.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.5];
//            footerLabel.text=@"上拉加载更多";
//            footerLabel.textColor=[UIColor whiteColor];
//
//
//
//        });
//
//    }
//
//}





#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}


#pragma mark 搜索栏代理：
//-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
//    [self updateSearchResultsForSearchController:self.searchController];
//}
//-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
//    NSString *seachString = searchController.searchBar.text;
//
//    [self FilterContentForSearchText:seachString scope:searchController.searchBar.selectedScopeButtonIndex];
//    [courseTable reloadData];
//}

@end
