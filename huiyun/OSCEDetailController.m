//
//  OSCEDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/1/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OSCEDetailController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface OSCEDetailController ()
{
    UITableView *osceTable;
    CGFloat desH;
}
@end

@implementation OSCEDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    //计算描述高度
    desH = [Maneger getPonentH:self.model.des andFont:[UIFont systemFontOfSize:17] andWidth:WIDTH-129];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *navigationView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    navigationView.userInteractionEnabled = YES;
    [self.view addSubview:navigationView];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    UILabel *title = [UILabel new];
    title.frame = CGRectMake(WIDTH/2-50, 34.5, 200, 30);
    title.font = [UIFont systemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    title.text = @"OSCE详情";
    [navigationView addSubview:title];
    //创建视图
    [self createUI];
}
- (void)createUI{
    [self.model stationName];
    [self.model osceContent];
    [[self.model osceTime] stringValue];
    [[Maneger shareObject] timeFormatter:[[self.model osceTime] stringValue]];
    [self.model createTime];
    [self.model createName];
    [self.model subjectName];
    [[self.model difficulty] stringValue];
    [self.model des];
    [[Maneger shareObject] timeFormatter:[[self.model createTime] stringValue]];
    //
    osceTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT) style:UITableViewStylePlain];
    osceTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    osceTable.delegate = self;
    osceTable.dataSource = self;
    osceTable.backgroundColor = UIColorFromHex(0xf5f5f5);
    [self.view addSubview:osceTable];
    //注册cell
    [osceTable registerNib:[UINib nibWithNibName:@"osceDetailCellStyle1" bundle:nil] forCellReuseIdentifier:@"style1"];
    [osceTable registerNib:[UINib nibWithNibName:@"osceDatailCellStyle2" bundle:nil] forCellReuseIdentifier:@"style2"];
}
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
        view.backgroundColor = UIColorFromHex(0xf5f5f5);
        return view;
    }else{
        return nil;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 40.f;
    }
    return 0.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 5) {
        return desH+23;
    }
    return 44.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 6;
    }else{
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    osceDetailCellStyle1 *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
        if (indexPath.row == 0) {
            cell.biaoti.text = @"考试名称:";
            cell.content.text = [self.model osceName];
        }else if (indexPath.row == 1){
            cell.biaoti.text = @"考试科目:";
            cell.content.text = [self.model subjectName];
        }else if (indexPath.row == 2){
            cell.biaoti.text = @"考试总分:";
            cell.content.text = [self.model totalScore].stringValue;
        }else if (indexPath.row == 3){
            cell.biaoti.text = @"考试分类:";
            cell.content.text = [self.model osceCategory];
        }else if (indexPath.row == 4){
            cell.biaoti.text = @"考试时间:";
            cell.content.text = [[Maneger shareObject] timeFormatter:[self.model osceTime].stringValue];
        }else{
            cell.biaoti.text = @"OSCE简介:";
            cell.content.text = [self.model des];
        }
        return cell;
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
        if (indexPath.row == 0) {
            cell.biaoti.text = @"难度:";
            cell.content.text = [self.model difficulty].stringValue;
        }else if (indexPath.row == 1){
            cell.biaoti.text = @"站点数:";
            cell.content.text = [self.model osceStationNums].stringValue;
        }
        return cell;
    }
}
@end
