//
//  ExamPointCell.h
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExamPointCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *internal;
@property (weak, nonatomic) IBOutlet UILabel *stationName;
@property (weak, nonatomic) IBOutlet UILabel *subject;
@property (weak, nonatomic) IBOutlet UILabel *category;
@property (weak, nonatomic) IBOutlet UILabel *uesds;
@property (weak, nonatomic) IBOutlet UILabel *pointStatus;

@end
