//
//  LeaveCellStyle1.h
//  xiaoyun
//
//  Created by MacAir on 17/2/23.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaveCellStyle1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@end
