//
//  SDeaprtCell.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDeaprtCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *departName;
@property (weak, nonatomic) IBOutlet UILabel *departStatus;
@property (weak, nonatomic) IBOutlet UILabel *departTime;

@end
