//
//  CaseAwardModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseAwardModel.h"

@implementation CaseAwardModel
//@property (strong, nonatomic) NSString *caseAwardModelStatus;
- (void)setCaseAwardModelStatus:(NSString *)caseAwardModelStatus{
    if ([caseAwardModelStatus isKindOfClass:[NSNull class]]) {
        _caseAwardModelStatus = @"";
    }else if ([caseAwardModelStatus isEqualToString:@"waiting_approval"]){
        _caseAwardModelStatus = @"待审核";
    }else if ([caseAwardModelStatus isEqualToString:@"approved"]){
        _caseAwardModelStatus = @"已通过";
    }else{
        _caseAwardModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseAwardModelTitle;
- (void)setCaseAwardModelTitle:(NSString *)caseAwardModelTitle{
    if ([caseAwardModelTitle isKindOfClass:[NSNull class]]) {
        _caseAwardModelTitle = @"";
    }else{
        _caseAwardModelTitle = caseAwardModelTitle;
    }
}
//@property (strong, nonatomic) NSString *caseAwardModelDegree;
- (void)setCaseAwardModelDegree:(NSString *)caseAwardModelDegree{
    if ([caseAwardModelDegree isKindOfClass:[NSNull class]]) {
        _caseAwardModelDegree = @"";
    }else{
        _caseAwardModelDegree = caseAwardModelDegree;
    }
}
//@property (strong, nonatomic) NSNumber *caseAwardModelDate;
- (void)setCaseAwardModelDate:(NSNumber *)caseAwardModelDate{
    if ([caseAwardModelDate isKindOfClass:[NSNull class]]) {
        _caseAwardModelDate = @0;
    }else{
        _caseAwardModelDate = caseAwardModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseAwardModelTeacher;
- (void)setCaseAwardModelTeacher:(NSString *)caseAwardModelTeacher{
    if ([caseAwardModelTeacher isKindOfClass:[NSNull class]]) {
        _caseAwardModelTeacher = @"";
    }else{
        _caseAwardModelTeacher = caseAwardModelTeacher;
    }
}
//@property (strong, nonatomic) NSNumber *caseAwardModelTeacherId;
- (void)setCaseAwardModelTeacherId:(NSNumber *)caseAwardModelTeacherId{
    if ([caseAwardModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseAwardModelTeacherId = @0;
    }else{
        _caseAwardModelTeacherId = caseAwardModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseAwardModelDescroption;
- (void)setCaseAwardModelDescroption:(NSString *)caseAwardModelDescroption{
    if ([caseAwardModelDescroption isKindOfClass:[NSNull class]]) {
        _caseAwardModelDescroption = @"";
    }else{
        _caseAwardModelDescroption = caseAwardModelDescroption;
    }
}
//@property (strong, nonatomic) NSString *caseAwardModelAdvice;
- (void)setCaseAwardModelAdvice:(NSString *)caseAwardModelAdvice{
    if ([caseAwardModelAdvice isKindOfClass:[NSNull class]]) {
        _caseAwardModelAdvice = @"";
    }else{
        _caseAwardModelAdvice = caseAwardModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
