//
//  BlankCardController.m
//  yun
//
//  Created by MacAir on 2017/8/29.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "BlankCardController.h"
@interface BlankCardController ()
{
    NSArray *titleArray;
    NSArray *contentArray;
    Edit status;
    UIButton *rightBtn;
    NSMutableDictionary *updateDic;
}
@end

@implementation BlankCardController

- (void)viewDidLoad {
    [super viewDidLoad];
    titleArray = @[@"开户地址:",@"银行卡号:"];
    contentArray = @[self.entity.userBlankcardOutlets,[NSString stringWithFormat:@"%@",self.entity.userHideBlankCard]];
    updateDic = [NSMutableDictionary dictionaryWithDictionary:self.entity.responseDic];
    [self setUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    status = EditNo;
    rightBtn.selected = NO;
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xBEBEBE);
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-60, 29.5, 60, 25);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn setTitle:@"保存" forState:UIControlStateSelected];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
    [rightBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"银行卡";
    [backNavigation addSubview:titleLab];
    //tableView
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-164) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView registerNib:[UINib nibWithNibName:@"AccountCell1" bundle:nil] forCellReuseIdentifier:@"account1"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)editAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        [MBProgressHUD showToastAndMessage:@"进入编辑模式~" places:0 toView:nil];
        status = EditYes;
    }else{
        status = EditNo;
        //修改
        [self updateInfo:0];
    }
}
- (void)updateInfo:(int)s{
    //s = 1 返回
    NSString *requesUrl = [NSString stringWithFormat:@"%@/users/%@",LocalIP,self.entity.userID];
    [RequestTools RequestWithURL:requesUrl Method:@"put" Params:updateDic Message:@"修改中" Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"成功!" places:0 toView:nil];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败!" places:0 toView:nil];
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titleArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (status == EditYes) {
        AccountCell1 *cell = [tableView cellForRowAtIndexPath:indexPath];
        __weak NSMutableDictionary *response = updateDic;
        if (indexPath.row == 0) {
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                _entity.userBlankcardOutlets = str;
                [response setObject:str forKey:@"bankOutlets"];
            };
            [self.view addSubview:alert];
        }else{
            AlertView *alert = [[AlertView alloc]init];
            alert.inType = @"bank";
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                _entity.userBlankcardOutlets = str;
                [response setObject:str forKey:@"bankCardNumber"];
            };
            [self.view addSubview:alert];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountCell1 *cell1 = [tableView dequeueReusableCellWithIdentifier:@"account1"];
    cell1.arrow.hidden = YES;
    cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    cell1.title.text = titleArray[indexPath.row];
    cell1.content.text = contentArray[indexPath.row];
    return cell1;
}
@end
