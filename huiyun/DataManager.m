
//
//  DataManager.m
//  NSURLConnection
//
//  Created by qianfeng on 15-8-6.
//  Copyright (c) 2015年 ZYK. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

-(id)init
{
    self=[super init];
    if (self) {
        _mutData=[[NSMutableData alloc]init];
    }
    return self;
}

//加载数据方法
-(void)loadWithURL:(NSString *)url
{
    NSURL *ul =[NSURL URLWithString:url];
    NSURLRequest *request=[[NSURLRequest alloc]initWithURL:ul];
    _connection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
}

#pragma -mark NSURLConnectionDataDelegate
//接收到服务器响应，调用该方法
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *r=(NSHTTPURLResponse *)response;
    if (r.statusCode == 200) {
        NSLog(@"连接成功");
    }else{
        NSLog(@"连接失败");
    }
}

//接到服务器数据时，调用该方法
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_mutData appendData:data];
}

//接收数据完成时，调用该方法
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //接到服务器数据
    id obj=[NSJSONSerialization JSONObjectWithData:_mutData options:NSJSONReadingMutableContainers error:nil];
    //obj传到需要使用数据的地方
    //通过调用block 传送obj参数
    self.sBlock(obj);
    
}
@end
