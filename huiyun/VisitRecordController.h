//
//  VisitRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiseaseView.h"
@interface VisitRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *oneField;

@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) DiseaseView *listDiseaseView;
@property (strong, nonatomic)UIImagePickerController *imagePickController;

- (IBAction)saveAction:(id)sender;

- (IBAction)fileAction:(id)sender;
- (IBAction)dateAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;
@property (strong, nonatomic) CaseReuestModel *model;
- (IBAction)oneFieldAction:(id)sender;
@property (strong, nonatomic) CaseTreatModel *myModel;
@end
