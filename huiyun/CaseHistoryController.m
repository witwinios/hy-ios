//
//  CaseHistoryController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseHistoryController.h"

@interface CaseHistoryController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    
    int selectRecord;
}
@end

@implementation CaseHistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
+ (instancetype)shareObject{
    static CaseHistoryController *hisVC = nil;
    if (hisVC == nil) {
        hisVC = [CaseHistoryController new];
    }
    return hisVC;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    selectRecord = [recordVC.selectRecordIndex intValue];
    if (selectRecord < 3) {
        self.departModel = recordVC.departModel;
    }
    dataArray = [NSMutableArray new];
    [self loadData];
}
- (void)loadData{
    NSString *url;
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    if (selectRecord == 0) {
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/medicalCaseRecords?pageStart=1&pageSize=999&requirementId=%@",LocalIP,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        NSLog(@"url=%@",url);
    }else if(selectRecord == 1){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords?pageStart=1&pageSize=999&requirementId=%@",LocalIP,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
    }else if (selectRecord == 2){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/activityRecords?pageStart=1&pageSize=999&requirementId=%@",LocalIP,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
    }else if (selectRecord == 3){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/tutoringRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 4){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/researchRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 5){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/awardRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 6){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/essayRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 7){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/rescueRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 8){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/treatmentRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }else if (selectRecord == 9){
        url = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/malpracticeRecords?pageStart=1&pageSize=999",LocalIP,recordVC.departModel.SDepartRecordId];
    }
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
        [dataArray removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (selectRecord == 0) {
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseMedicalModel *model = [CaseMedicalModel new];
                model.recordId = resDic[@"recordId"];
                model.requestmentId = resDic[@"requirementId"];
                model.caseOne = resDic[@"patientName"];
                model.caseTwo = resDic[@"reviewStatus"];
                model.caseThree = resDic[@"caseNo"];
                model.caseFour = resDic[@"reviewerFullName"];
                model.caseFive = resDic[@"caseTime"];
                model.mainIos = resDic[@"principalDiagnosis"];
                model.secondIos = resDic[@"secondaryDiagnosis"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 1){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseOperateModel *model = [CaseOperateModel new];
                model.recordId = resDic[@"recordId"];
                model.requestmentId = resDic[@"requirementId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseResult = resDic[@"result"];
                model.casePatientName = resDic[@"patientName"];
                model.caseNo = resDic[@"caseNo"];
                model.caseDate = resDic[@"operationTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseReason = resDic[@"analysis"];
                model.caseDescription = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 2){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseActivityModel *model = [CaseActivityModel new];
                model.recordId = resDic[@"recordId"];
                model.requestmentId = resDic[@"requirementId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseActivityName = resDic[@"activityName"];
                model.casePersion = resDic[@"organizerFullName"];
                model.caseInternal = resDic[@"duration"];
                model.caseDate = resDic[@"activityTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseActivityContent = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 3){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseTeachModel *model = [CaseTeachModel new];
                model.recordId = resDic[@"recordId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseProject = resDic[@"tutoringProject"];
                model.caseTeachObject = resDic[@"tutoringObject"];
                model.caseNums = resDic[@"headCount"];
                model.caseStartTime = resDic[@"tutoringStartTime"];
                model.caseEndTime = resDic[@"tutoringEndTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseTeachContent = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 4){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseSienceModel *model = [CaseSienceModel new];
                model.recordId = resDic[@"recordId"];
                model.caseSienceTitle = resDic[@"researchTask"];
                model.caseSienceHeader = resDic[@"topicHead"];
                model.caseSienceRole = resDic[@"playRole"];
                model.caseSienceDate = resDic[@"researchStartTime"];
                model.caseSienceStatus = resDic[@"reviewStatus"];
                model.caseSienceTeacherId = resDic[@"reviewerId"];
                model.caseSienceTeacher = resDic[@"reviewerFullName"];
                model.caseSienceComepleteDes = resDic[@"description"];
                model.caseSienceAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 5){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseAwardModel *model = [CaseAwardModel new];
                model.recordId = resDic[@"recordId"];
                model.caseAwardModelStatus = resDic[@"reviewStatus"];
                model.caseAwardModelTitle = resDic[@"awardTitle"];
                model.caseAwardModelDegree = resDic[@"awardLevel"];
                model.caseAwardModelDate = resDic[@"awardTime"];
                model.caseAwardModelTeacherId = resDic[@"reviewerId"];
                model.caseAwardModelTeacher = resDic[@"reviewerFullName"];
                model.caseAwardModelDescroption = resDic[@"description"];
                model.caseAwardModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 6){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseEssayModel *model = [CaseEssayModel new];
                model.recordId = resDic[@"recordId"];
                model.caseEssayModelStatus = resDic[@"reviewStatus"];
                model.caseEssayModelTitle = resDic[@"paperTitle"];
                model.caseEssayModelDegree = resDic[@"paperType"];
                model.caseEssayModelCategory = resDic[@"articleType"];
                model.caseEssayModelAuthor = resDic[@"authorRank"];
                model.caseEssayModelEdtion = resDic[@"publishedJournal"];
                model.caseEssayModelDate = resDic[@"publishedTime"];
                model.caseEssayModelTeacher = resDic[@"reviewerFullName"];
                model.caseEssayModelTeacherId = resDic[@"reviewerId"];
                model.caseEssayModelDescription = resDic[@"description"];
                model.caseEssayModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 7){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseRescueModel *model = [CaseRescueModel new];
                model.recordId = resDic[@"recordId"];
                model.caseRescueModelStatus = resDic[@"reviewStatus"];
                model.caseRescueModelDiseaseName = resDic[@"diseaseName"];
                model.caseRescueModelDiseaserName = resDic[@"patientName"];
                model.caseRescueModelDiseaseNum = resDic[@"caseNo"];
                model.caseRescueModelBackDescription = resDic[@"outcome"];
                model.caseRescueModelDate = resDic[@"rescueTime"];
                model.caseRescueModelTeacher = resDic[@"reviewerFullName"];
                model.caseRescueModelTeacherId = resDic[@"reviewerId"];
                model.caseRescueModelDescription = resDic[@"description"];
                model.caseRescueModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 8){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseTreatModel *model = [CaseTreatModel new];
                model.recordId = resDic[@"recordId"];
                model.caseTreatModelStatus = resDic[@"reviewStatus"];
                model.caseTreatModelCategory = resDic[@"diseaseICD"][@"diseaseICDName"];
                model.caseTreatModelDate = resDic[@"visitTime"];
                model.caseTreatModelTeacher = resDic[@"reviewerFullName"];
                model.caseTreatModelTeacherId = resDic[@"reviewerId"];
                model.caseTreatModelDescription = resDic[@"description"];
                model.caseTreatModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 9){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseMistakeModel *model = [CaseMistakeModel new];
                model.recordId = resDic[@"recordId"];
                model.caseMistakeModelStatus = resDic[@"reviewStatus"];
                model.caseMistakeModelCategory = resDic[@"accidentType"];
                model.caseMistakeModelDegree = resDic[@"accidentLevel"];
                model.caseMistakeModelDate = resDic[@"accidentTime"];
                model.caseMistakeModelTeacher = resDic[@"reviewerFullName"];
                model.caseMistakeModelTeacherId = resDic[@"reviewerId"];
                model.caseMistakeModelViaDescription = resDic[@"description"];
                model.caseMistakeModelReason = resDic[@"rootCause"];
                model.caseMistakeModelLession = resDic[@"lesson"];
                model.caseMistakeModelAdvice = resDic[@"reviewerComments"];
                model.caseMistakeModelLeader = resDic[@"departmentHead"];
                model.caseMistakeModelLeaderAdvice = resDic[@"departmentHeadComments"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }
    } failed:^(NSString *result) {
        
    }];
}
- (void)setUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = recordVC.departModel;
    if ([departModel.SDepartStatus isEqualToString:@"在科"]) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
        [rightBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    }
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"病例记录";
    [backNavigation addSubview:titleLab];
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    [table registerNib:[UINib nibWithNibName:@"RecordCell" bundle:nil] forCellReuseIdentifier:@"recordCell"];
    [self.view addSubview:table];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    int select = recordVC.selectRecordIndex.intValue;
    if (select == 0) {
        SCaseRecordController *recordAddVC = [SCaseRecordController new];
        recordAddVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:recordAddVC animated:YES];
    }else if (select == 1){
        OperateRecordController *operateVC = [OperateRecordController new];
        operateVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:operateVC animated:YES];
    }else if (select == 2){
        ActivityRecordController *activityVC = [ActivityRecordController new];
        activityVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:activityVC animated:YES];
    }else if (select == 3){
        TeachRecordController *teachVC = [TeachRecordController new];
        teachVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:teachVC animated:YES];
    }else if (select == 4){
        ScienceRecordController *scienceVC = [ScienceRecordController new];
        scienceVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:scienceVC animated:YES];
    }else if (select == 5){
        RewardRecordController *rewardVC = [RewardRecordController new];
        rewardVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:rewardVC animated:YES];
    }else if (select == 6){
        PaperRecordController *paperVC = [PaperRecordController new];
        paperVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:paperVC animated:YES];
    }else if (select == 7){
        RescueRecordController *rescueVC = [RescueRecordController new];
        rescueVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:rescueVC animated:YES];
    }else if (select == 8){
        VisitRecordController *visitVC = [VisitRecordController new];
        visitVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:visitVC animated:YES];
    }else if (select == 9){
        MistakeRecordController *misVC = [MistakeRecordController new];
        misVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:misVC animated:YES];
    }





}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
    if (index ==0) {
        
    }else if (index == 1){
        
    }else if (index == 2){
        
    }else if (index == 3){

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    int select = recordVC.selectRecordIndex.intValue;
    if (select == 0) {
        DetailRecordController *errorVC = [DetailRecordController new];
        errorVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:errorVC animated:YES];
    }else if (select == 1){
        OperateDetailController *operateVC = [OperateDetailController new];
        operateVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:operateVC animated:YES];
    }else if (select == 2){
        ActivityDetailController *activityVC = [ActivityDetailController new];
        activityVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:activityVC animated:YES];
    }else if (select == 3){
        TeachDetailController *teachVC = [TeachDetailController new];
        teachVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:teachVC animated:YES];
    }else if (select == 4){
        SienceDetailController *sienceVC = [SienceDetailController new];
        sienceVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:sienceVC animated:YES];
    }else if (select == 5){
        AwardDetailController *awardVC = [AwardDetailController new];
        awardVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:awardVC animated:YES];
    }else if (select == 6){
        EssayDetailController *essayVC = [EssayDetailController new];
        essayVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:essayVC animated:YES];
    }else if (select == 7){
        RescueDetailController *rescueVC = [RescueDetailController new];
        rescueVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:rescueVC animated:YES];
    }else if (select == 8){
        TreatDetailController *treatVC = [TreatDetailController new];
        treatVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:treatVC animated:YES];
    }else{
        MistakeDetailController *mistakeVC = [MistakeDetailController new];
        mistakeVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:mistakeVC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    int select = recordVC.selectRecordIndex.intValue;
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recordCell"];
    if (select == 0) {
        CaseMedicalModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"病人姓名:";
        cell.twolineLab.text = @"病案号:";
        cell.threelineLab.text = @"带教老师:";
        cell.dateLab.text = @"病案时间:";
        cell.onelineContent.text = model.caseOne;
        cell.status.text = model.caseTwo;
        cell.twolineContent.text = model.caseThree;
        cell.threelineContent.text = model.caseFour;
        NSLog(@"caseFive=%@",model.caseFive);
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseFive.stringValue];
    }else if (select == 1){
        CaseOperateModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"操作结果:";
        cell.twolineLab.text = @"病人姓名:";
        cell.threelineLab.text = @"病案号:";
        cell.dateLab.text = @"操作时间:";
        cell.onelineContent.text = model.caseResult;
        cell.twolineContent.text = model.casePatientName;
        cell.status.text = model.caseStatus;
        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseNo];
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseDate.stringValue];
    }else if (select == 2){
        CaseActivityModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"活动记录:";
        cell.twolineLab.text = @"主讲人:";
        cell.threelineLab.text = @"活动时长:";
        cell.dateLab.text = @"活动时间:";
        cell.status.text = model.caseStatus;
        cell.onelineContent.text = model.caseActivityName;
        cell.twolineContent.text = model.casePersion;
        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseInternal];
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseDate.stringValue];
    }else if (select == 3){
        CaseTeachModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"教学项目:";
        cell.twolineLab.text = @"带教对象:";
        cell.threelineLab.text = @"人数:";
        cell.dateLab.text = @"时间:";
        cell.status.text = model.caseStatus;
        cell.onelineContent.text = model.caseTeachContent;
        cell.twolineContent.text = model.caseTeachObject;
        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseNums];
        NSString *start = [[Maneger shareObject] timeFormatter1:model.caseStartTime.stringValue];
        NSString *end = [[Maneger shareObject] timeFormatter1:model.caseEndTime.stringValue];
        cell.dateLab.text = [NSString stringWithFormat:@"开始:%@-结束:%@",start,end];
    }else if (select == 4){
        CaseSienceModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"题目:";
        cell.twolineLab.text = @"课程负责人:";
        cell.threelineLab.text = @"参与角色:";
        cell.dateLab.text = @"活动时间:";
        cell.onelineContent.text = model.caseSienceTitle;
        cell.twolineContent.text = model.caseSienceHeader;
        cell.threelineLab.text = model.caseSienceRole;
        cell.status.text = model.caseSienceStatus;
        NSString *time = [[Maneger shareObject] timeFormatter1:model.caseSienceDate.stringValue];
        cell.dateContent.text = time;
    }else if (select == 5){
        CaseAwardModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"获奖名称:";
        cell.twolineLab.text = @"获奖级别:";
        cell.threelineLab.text = @"带教老师:";
        cell.dateLab.text = @"获奖时间:";
        cell.onelineContent.text = model.caseAwardModelTitle;
        cell.twolineContent.text = model.caseAwardModelDegree;
        cell.threelineContent.text = model.caseAwardModelTeacher;
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseAwardModelDate.stringValue];
        cell.status.text = model.caseAwardModelStatus;
    }else if (select == 6){
        CaseEssayModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"论文题目:";
        cell.twolineLab.text = @"论文级别:";
        cell.threelineLab.text = @"论文类别:";
        cell.dateLab.text = @"发表时间:";
        cell.onelineContent.text = model.caseEssayModelTitle;
        cell.twolineContent.text = model.caseEssayModelDegree;
        cell.threelineContent.text = model.caseEssayModelCategory;
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseEssayModelDate.stringValue];
        cell.status.text = model.caseEssayModelStatus;
    }else if (select == 7){
        CaseRescueModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"疾病名称:";
        cell.twolineLab.text = @"病人姓名:";
        cell.threelineLab.text = @"转归情况:";
        cell.dateLab.text = @"抢救时间:";
        cell.status.text = model.caseRescueModelStatus;
        cell.onelineContent.text = model.caseRescueModelDiseaseName;
        cell.twolineContent.text = model.caseRescueModelDiseaserName;
        cell.threelineContent.text = model.caseRescueModelBackDescription;
    }else if (select == 8){
        CaseTreatModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"疾病种类:";
        cell.twolineLab.text = @"带教老师:";
        cell.threelineLab.hidden = YES;
        cell.threelineContent.hidden = YES;
        cell.status.text = model.caseTreatModelStatus;
        cell.dateLab.text = @"出诊时间:";
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseTreatModelDate.stringValue];
        cell.onelineContent.text = model.caseTreatModelCategory;
        cell.twolineContent.text = model.caseTreatModelTeacher;
        
    }else{
        CaseMistakeModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"类别:";
        cell.twolineLab.text = @"等级:";
        cell.threelineLab.text = @"院领导:";
        cell.dateLab.text = @"发生时间:";
        cell.status.text = model.caseMistakeModelStatus;
        cell.onelineContent.text = model.caseMistakeModelCategory;
        cell.twolineContent.text = model.caseMistakeModelDegree;
        cell.threelineContent.text = model.caseMistakeModelLeader;
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseMistakeModelDate.stringValue];
    }
    return cell;
}
- (void)statusAction:(UIButton *)btn{
    
}
@end

