//
//  DrawController.h
//  yun
//
//  Created by MacAir on 2017/8/3.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMGDrawView.h"
#import "ControlManeger.h"
typedef void (^ImageBlock) (UIImage *image);
@interface DrawController : UIViewController
@property (weak, nonatomic) IBOutlet XMGDrawView *drawView;
@property (copy, nonatomic) ImageBlock imgBlock;
@end
