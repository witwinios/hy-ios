//
//  QuestionController.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionCell.h"
#import "MyQuesModel.h"
#import "TribeMem.h"
#import "QuesDetailController.h"
#import "PopViewController.h"
#import "TribeHiostory.h"
#import "CustomPopview.h"
typedef void (^BackAction) ();

@interface QuestionController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UIPopoverPresentationControllerDelegate,selectIndexPathDelegate>
@property (strong, nonatomic) BackAction backAction;
@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (strong, nonatomic, readwrite) YWTribe *tribe;
@end
