//
//  RescueRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RescueRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;

@property (weak, nonatomic) IBOutlet UITextField *twoField;

@property (weak, nonatomic) IBOutlet UIImageView *currentImage;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview1;
@property (weak, nonatomic) IBOutlet UITextField *threeField;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
- (IBAction)fileAction:(id)sender;
- (IBAction)saveAction:(id)sender;
- (IBAction)dateAction:(id)sender;
@property (strong, nonatomic)UIImagePickerController *imagePickController;

@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseRescueModel *myModel;
@end
