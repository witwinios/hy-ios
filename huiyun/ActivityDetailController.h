//
//  ActivityDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCellOne.h"
#import "DetailCellTwo.h"
#import "CaseActivityModel.h"
@interface ActivityDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) CaseActivityModel *model;
@end
