//
//  PersonEntity.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonEntity : NSObject<NSCoding>
@property (strong, nonatomic) NSString *userAccount;
@property (strong, nonatomic) NSString *userPassword;
@property (strong, nonatomic) NSNumber *userID;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userFullName;
@property (strong, nonatomic) NSString *userEmail;
@property (strong, nonatomic) NSString *personRole;
@property (strong, nonatomic) NSString *userAccountIM;
@property (strong, nonatomic) NSString *userPasswordIM;
@property (strong, nonatomic) NSNumber *roleId;
@property (strong, nonatomic) NSString *fileUrl;
@end

