//
//  MarkCell.h
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *answerLab;
@property (weak, nonatomic) IBOutlet UILabel *markDesLab;
@property (weak, nonatomic) IBOutlet UILabel *keyTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *keyType;
@property (weak, nonatomic) IBOutlet UILabel *keyLab;
@property (weak, nonatomic) IBOutlet UILabel *key;
@property (weak, nonatomic) IBOutlet UILabel *markDes;
@property (weak, nonatomic) IBOutlet UILabel *answer;
@property (weak, nonatomic) IBOutlet UILabel *scoreValue;
@property (weak, nonatomic) IBOutlet UITextField *koufenView;
@property (weak, nonatomic) IBOutlet UITextView *koufenReason;
@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (strong, nonatomic) NSNumber *indexTAG;
@property (weak, nonatomic) IBOutlet UILabel *scoreValueLab;
@property (weak, nonatomic) IBOutlet UILabel *koufenViewLab;
@property (weak, nonatomic) IBOutlet UILabel *koufenReasonLab;
@property (weak, nonatomic) IBOutlet UIView *headView;

@end
