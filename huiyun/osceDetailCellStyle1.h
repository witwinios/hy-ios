//
//  osceDetailCellStyle1.h
//  xiaoyun
//
//  Created by MacAir on 17/2/7.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface osceDetailCellStyle1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgShow;
@property (weak, nonatomic) IBOutlet UILabel *biaoti;
@property (weak, nonatomic) IBOutlet UILabel *content;
@end
