//
//  PaperDeatailController.m
//  yun
//
//  Created by MacAir on 2017/8/15.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "PaperDeatailController.h"

@interface PaperDeatailController ()
{
    NSMutableArray *answerArray;
    //
    //所有父题
    NSMutableArray *parrenArray;
    //所有子题
    NSMutableArray *childArray;
    //所有单选
    NSMutableArray *singleArray;
    
    
    //
    UIScrollView *mainScroll;
    NSInteger pageNums;
    //题型
    UILabel *typeLabel;
    UILabel *indexLabel;
    UILabel *timeLabel;
}
@end

@implementation PaperDeatailController

- (void)viewDidLoad {
    [super viewDidLoad];
    answerArray = [NSMutableArray new];
    singleArray = [NSMutableArray new];
    parrenArray = [NSMutableArray new];
    childArray = [NSMutableArray new];
    
    [self setUI];
    [self loadData];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if (![self.paperModel.paperStatus isEqualToString:@"完成"]) {
        UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        submitBtn.frame = CGRectMake(Swidth-90, 25, 50, 30);
        submitBtn.center = CGPointMake(submitBtn.center.x, leftBtn.center.y);
        [submitBtn setTitle:@"交卷" forState:0];
        submitBtn.layer.cornerRadius = 5;
        submitBtn.layer.masksToBounds = YES;
        submitBtn.layer.borderWidth = 1;
        submitBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        [submitBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:submitBtn];
    }
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 25, 25, 25);
    rightBtn.center = CGPointMake(rightBtn.center.x, leftBtn.center.y);
    [rightBtn setImage:[UIImage imageNamed:@"切换"] forState:0];

    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [rightBtn addTarget:self action:@selector(moveTo) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 150, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = _paperModel.paperName;
    [backNavigation addSubview:titleLab];
    //
    typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, Swidth/3, 40)];
    [self.view addSubview:typeLabel];
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, Swidth/3, 40)];
    timeLabel.text = @"00:00";
    timeLabel.center = CGPointMake(Swidth/2, timeLabel.center.y);
    [self.view addSubview:timeLabel];
    indexLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-70, 65, 65, 40)];
    [self.view addSubview:indexLabel];
    UIView *lineView = [[UILabel alloc]initWithFrame:CGRectMake(0,106, Swidth, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:lineView];
    //
    mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 106, Swidth, Sheight-106)];
    [self.view addSubview:mainScroll];
}
- (void)loadData{
    NSString *requestUrl;
    //未做
    if (![self.paperModel.paperStatus isEqualToString:@"完成"]) {
        requestUrl = [NSString stringWithFormat:@"%@/testPapers/%@/questions?showAnswers=true&pageSize=999",LocalIP,self.paperModel.paperID];
        //改变试卷状态
        [RequestTools RequestWithURL:[NSString stringWithFormat:@""] Method:@"get" Params:nil Success:^(NSDictionary *result) {
            NSLog(@"改变试卷状态");
        }];
        [RequestTools RequestWithURL:requestUrl Method:@"GET" Params:nil Message:@"制作中" Success:^(NSDictionary *result) {
            NSArray *array = result[@"responseBody"][@"result"];
            for (NSDictionary *dictionary in array) {
                QuestionModel *quesModel = [QuestionModel new];
                quesModel.questionId = dictionary[@"questionId"];
                quesModel.questionType = dictionary[@"questionType"];
                quesModel.questTitle = dictionary[@"questionTitle"];
                quesModel.choiceOptions = dictionary[@"choiceOptions"];
                //正确答案
                NSArray *correctArray = dictionary[@"answersBean"];
                NSMutableArray *tempCorrect = [NSMutableArray new];
                for (int s=0; s<correctArray.count; s++) {
                    NSDictionary *dic = correctArray[s];
                    [tempCorrect addObject:[dic objectForKey:@"answer"]];
                }
                quesModel.correctAnswer = tempCorrect;

                if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                    quesModel.parrentId = [NSNumber numberWithInt:0];
                }else{
                    quesModel.parrentId = dictionary[@"parentQuestionId"];
                }
                quesModel.questionDes = dictionary[@"analysis"];
                quesModel.childNum = dictionary[@"childrenQuestionsNum"];
                //获取图片
                if (![dictionary[@"questionFile"] isKindOfClass:[NSNull class]]) {
                    quesModel.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,dictionary[@"questionFile"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
                }else{
                    quesModel.questionFile = @"";
                }
                
                int childNums = [dictionary[@"childrenQuestionsNum"] intValue];
                //组装
                if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums == 0) {
                    //单独选择题
                    [singleArray addObject:quesModel];
                    
                }else if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums != 0){
                    //父题
                    [parrenArray addObject:quesModel];
                }else if (![dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]]){
                    [childArray addObject:quesModel];
                }

            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"制作失败!" places:0 toView:nil];
            [NSTimer timerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    }else{
        requestUrl = [NSString stringWithFormat:@"%@/selfTests/%@/testPapers/%@/responseAnswers",LocalIP,self.testModel.testId,self.paperModel.paperID];
        [RequestTools RequestWithURL:requestUrl Method:@"GET" Params:nil Message:@"制作中" Success:^(NSDictionary *result) {
            NSArray *array = result[@"responseBody"][@"result"];
            for (NSDictionary *dictionary in array) {
                QuestionModel *quesModel = [QuestionModel new];
                quesModel.questionId = dictionary[@"questionId"];
                quesModel.questionType = dictionary[@"questionType"];
                quesModel.questTitle = dictionary[@"questionTitle"];
                quesModel.choiceOptions = dictionary[@"choiceOptions"];
                //你的答案
                quesModel.responseAnswer = dictionary[@"responseAnswer"];
                //正确答案
                NSArray *correctArray = dictionary[@"answersBean"];
                NSMutableArray *tempCorrect = [NSMutableArray new];
                for (int s=0; s<correctArray.count; s++) {
                    NSDictionary *dic = correctArray[s];
                    [tempCorrect addObject:[dic objectForKey:@"answer"]];
                }
                quesModel.correctAnswer = tempCorrect;
                
                if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                    quesModel.parrentId = [NSNumber numberWithInt:0];
                }else{
                    quesModel.parrentId = dictionary[@"parentQuestionId"];
                }
                quesModel.questionDes = dictionary[@"analysis"];
                quesModel.childNum = dictionary[@"childrenQuestionsNum"];
                //获取图片
                if (![dictionary[@"questionFile"] isKindOfClass:[NSNull class]]) {
                    quesModel.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,dictionary[@"questionFile"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
                }else{
                    quesModel.questionFile = @"";
                }
                
                int childNums = [dictionary[@"childrenQuestionsNum"] intValue];
                //组装
                if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums == 0) {
                    //单选
                    [singleArray addObject:quesModel];
                }else if ([dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]] && childNums != 0){
                    //父题
                    [parrenArray addObject:quesModel];
                }else if (![dictionary[@"parentQuestionId"] isKindOfClass:[NSNull class]]){
                    [childArray addObject:quesModel];
                }
                
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"制作失败!" places:0 toView:nil];
            [NSTimer timerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }];
    }
}
- (void)createPaperUI:(NSArray *)array{
    pageNums = 0;
    NSInteger totalPages = singleArray.count + parrenArray.count;
    mainScroll.contentSize = CGSizeMake(Swidth * totalPages, Sheight - 106);
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
//定位题目
- (void)moveTo{
    
}
- (void)submitAction{
    
}
@end
