//
//  CaseTeachModel.m
//  huiyun
//
//  Created by MacAir on 2017/10/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseTeachModel.h"

@implementation CaseTeachModel
//@property (strong, nonatomic) NSNumber *recordId;

//@property (strong, nonatomic) NSString *caseStatus;
- (void)setCaseStatus:(NSString *)caseStatus{
    if ([caseStatus isKindOfClass:[NSNull class]]) {
        _caseStatus = @"";
    }else if ([caseStatus isEqualToString:@"waiting_approval"]){
        _caseStatus = @"待审核";
    }else if ([caseStatus isEqualToString:@"approved"]){
        _caseStatus = @"已通过";
    }else{
        _caseStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseProject;
- (void)setCaseProject:(NSString *)caseProject{
    if ([caseProject isKindOfClass:[NSNull class]]) {
        _caseProject = @"";
    }else{
        _caseProject = caseProject;
    }
}
//@property (strong, nonatomic) NSString *caseTeachObject;
- (void)setCaseTeachObject:(NSString *)caseTeachObject{
    if ([caseTeachObject isKindOfClass:[NSNull class]]) {
        _caseTeachObject = @"";
    }else{
        _caseTeachObject = caseTeachObject;
    }
}
//@property (strong, nonatomic) NSNumber *caseNums;
- (void)setCaseNums:(NSNumber *)caseNums{
    if ([caseNums isKindOfClass:[NSNull class]]) {
        _caseNums = @0;
    }else{
        _caseNums = caseNums;
    }
}
//@property (strong, nonatomic) NSNumber *caseStartTime;
- (void)setCaseStartTime:(NSNumber *)caseStartTime{
    if ([caseStartTime isKindOfClass:[NSNull class]]) {
        _caseStartTime = @0;
    }else{
        _caseStartTime = caseStartTime;
    }
}
//@property (strong, nonatomic) NSNumber *caseEndTime;
- (void)setCaseEndTime:(NSNumber *)caseEndTime{
    if ([caseEndTime isKindOfClass:[NSNull class]]) {
        _caseEndTime = @0;
    }else{
        _caseEndTime = caseEndTime;
    }
}
//@property (strong, nonatomic) NSString *caseTeacher;
- (void)setCaseTeacher:(NSString *)caseTeacher{
    if ([caseTeacher isKindOfClass:[NSNull class]]) {
        _caseTeacher = @"";
    }else{
        _caseTeacher = caseTeacher;
    }
}
//@property (strong, nonatomic) NSString *caseTeachContent;
- (void)setCaseTeachContent:(NSString *)caseTeachContent{
    if ([caseTeachContent isKindOfClass:[NSNull class]]) {
        _caseTeachContent = @"";
    }else{
        _caseTeachContent = caseTeachContent;
    }
}
//@property (strong, nonatomic) NSString *caseAdvice;
- (void)setCaseAdvice:(NSString *)caseAdvice{
    if ([caseAdvice isKindOfClass:[NSNull class]]) {
        _caseAdvice = @"";
    }else{
        _caseAdvice = caseAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
