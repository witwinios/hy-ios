//
//  CaseTeachModel.h
//  huiyun
//
//  Created by MacAir on 2017/10/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//  教学记录

#import <Foundation/Foundation.h>

@interface CaseTeachModel : NSObject
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSString *caseStatus;
@property (strong, nonatomic) NSString *caseProject;
@property (strong, nonatomic) NSString *caseTeachObject;
@property (strong, nonatomic) NSNumber *caseNums;
@property (strong, nonatomic) NSNumber *caseStartTime;
@property (strong, nonatomic) NSNumber *caseEndTime;
@property (strong, nonatomic) NSString *caseTeacher;
@property (strong, nonatomic) NSString *caseTeachContent;
@property (strong, nonatomic) NSString *caseAdvice;
@property (strong, nonatomic) NSString *fileUrl;
@end
