//
//  OperateDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OperateDetailController.h"

@interface OperateDetailController ()
{
    NSArray *heightArr;
    NSArray *dataArray;
    NSArray *titleArray;
}
@end

@implementation OperateDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"103",@"44"];
    dataArray = @[_model.caseStatus,_model.caseResult,_model.casePatientName,[NSString stringWithFormat:@"%@",_model.caseNo],[[Maneger shareObject]timeFormatter1:_model.caseDate.stringValue],_model.caseTeacher,_model.caseReason,_model.caseDescription,_model.caseAdvice,@"查看"];
    titleArray = @[@"审核状态:",@"审核结果:",@"病人姓名:",@"病案号:",@"操作时间",@"带教老师:",@"成功/失败原因:",@"描述:",@"指导意见:",@"附件"];
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if ([self.model.caseStatus isEqualToString:@"待审核"]) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"修改" forState:0];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    }

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"操作记录";
    [backNavigation addSubview:titleLab];
    _tableView.bounces = NO;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellOne" bundle:nil] forCellReuseIdentifier:@"one"];
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellTwo" bundle:nil] forCellReuseIdentifier:@"two"];
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellThree" bundle:nil] forCellReuseIdentifier:@"three"];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)btn{
    OperateRecordController *recordVC = [OperateRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}

#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *oneCell = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *twoCell = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *threeCell = [tableView dequeueReusableCellWithIdentifier:@"three"];
    
    if (indexPath.row == 9) {
        threeCell.titleLab.text = titleArray[indexPath.row];
        [threeCell.contentBtn setTitle:dataArray[indexPath.row] forState:0];
        return threeCell;
    }else if (indexPath.row == 8 || indexPath.row == 7 || indexPath.row == 6){
        twoCell.titleText.text = titleArray[indexPath.row];
        twoCell.contentText.text = dataArray[indexPath.row];
        return twoCell;
    }else{
        oneCell.titleLab.text = titleArray[indexPath.row];
        oneCell.contentLab.text = dataArray[indexPath.row];
        return oneCell;
    }
    return twoCell;
}
@end
