//
//  TimeBlockController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/9.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TimeBlockController.h"

@interface TimeBlockController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
}
@end

@implementation TimeBlockController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 34.5, 15, 15);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"时间段";
    [backNavigation addSubview:titleLab];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    [tabView addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";

    [tabView registerNib:[UINib nibWithNibName:@"TimeBlockCell" bundle:nil] forCellReuseIdentifier:@"blockCell"];
    [self.view addSubview:tabView];
}
//下拉刷新
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/timeBlocks",LocalIP,self.testId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        [tabView reloadData];
        [tabView headerEndRefreshing];
    } failed:^(NSString *result) {
        [tabView headerEndRefreshing];
    }];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -delegate dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeBlockCell *cell = [tabView dequeueReusableCellWithIdentifier:@"blockCell"];
    return cell;
}
@end
