//
//  AlertView.m
//  huiyun
//
//  Created by MacAir on 2017/9/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AlertView.h"

@implementation AlertView
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, Swidth, Sheight);
        UIColor *color = [UIColor lightGrayColor];
        self.backgroundColor = [color colorWithAlphaComponent:0.5];
        [self loadView];
        
        //使用NSNotificationCenter 鍵盤出現時
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillChangeFrameNotification object:nil];
        //使用NSNotificationCenter 鍵盤隐藏時
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    UIView *contentView = [self viewWithTag:100];
    if (contentView.frame.origin.y < Sheight - keyBoardHeight) {
        [UIView animateWithDuration:0.5 animations:^{
            contentView.frame = CGRectMake(contentView.frame.origin.x, Sheight-keyBoardHeight-contentView.frame.size.height, contentView.frame.size.width, contentView.frame.size.height);
        }];
    }
}
//当键盘退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification{
    
}
- (void)loadView{
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 250)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 5;
    contentView.center = self.center;
    contentView.tag = 100;
    contentView.layer.masksToBounds = YES;
    [self addSubview:contentView];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 290, 80)];
    titleLab.font = [UIFont systemFontOfSize:25];
    titleLab.text = @"请输入";
    titleLab.textAlignment = 1;
    titleLab.textColor = [UIColor blackColor];
    [contentView addSubview:titleLab];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 85, Swidth, 0.5)];
    line.backgroundColor = [UIColor blackColor];
    [contentView addSubview:line];
    
    _msgLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 90, 290, 50)];
    _msgLab.textColor = [UIColor redColor];
    _msgLab.textAlignment = 1;
    [contentView addSubview:_msgLab];
    
    _contentField = [[UITextField alloc]initWithFrame:CGRectMake(5, 140, 290, 50)];
    _contentField.layer.cornerRadius = 5;
    _contentField.layer.masksToBounds = YES;
    _contentField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _contentField.layer.borderWidth = 1;
    _contentField.delegate = self;
    [contentView addSubview:_contentField];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:0];
    [cancelBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[UIColor blueColor] forState:0];
    cancelBtn.frame = CGRectMake(5, 200, 140, 50);
    cancelBtn.layer.cornerRadius = 3;
    cancelBtn.layer.masksToBounds = YES;
    cancelBtn.layer.borderWidth = 1;
    cancelBtn.layer.borderColor = [UIColor grayColor].CGColor;
    [contentView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBtn setTitle:@"确定" forState:0];
    [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitleColor:[UIColor blueColor] forState:0];
    sureBtn.frame = CGRectMake(155, 200, 140, 50);
    sureBtn.layer.cornerRadius = 3;
    sureBtn.layer.masksToBounds = YES;
    sureBtn.layer.borderWidth = 1;
    sureBtn.layer.borderColor = [UIColor grayColor].CGColor;
    [contentView addSubview:sureBtn];
}
- (void)cancelAction:(UIButton *)btn{
    [self removeFromSuperview];
}
- (void)sureAction:(UIButton *)btn{
    BOOL isSure;
    if ([_inType isEqualToString:@"phone"]) {
        isSure = [Maneger IsPhoneNumber:self.contentField.text];
    }else if ([_inType isEqualToString:@"email"]){
        isSure = [Maneger IsEmailAdress:self.contentField.text];
    }else if ([_inType isEqualToString:@"card"]){
        isSure = [Maneger IsIdentityCard:self.contentField.text];
    }else if ([_inType isEqualToString:@"bank"]){
        isSure = [Maneger IsBankCard:self.contentField.text];
    }else if ([_inType isEqualToString:@"number"]){
        isSure = [Maneger deptNumInputShouldNumber:self.contentField.text];
    }else if ([_inType isEqualToString:@"bankNumber"]){
        isSure = [Maneger deptNumInputShouldNumber:self.contentField.text];
    }else{
        if ([self.contentField.text isEqualToString:@""]) {
            _msgLab.text = @"不能为空";
            return;
        }else{
            //传值
            _backBlock(self.contentField.text);
            [self removeFromSuperview];
            return;
        }
    }
    //
    if (!isSure) {
        _msgLab.text = @"格式错误";
    }else{
        //传值
        _backBlock(self.contentField.text);
        [self removeFromSuperview];
    }
}
#pragma -delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    _msgLab.text = @"";
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _msgLab.text = @"";
    return YES;
}
@end
