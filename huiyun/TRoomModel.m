//
//  TRoomModel.m
//  yun
//
//  Created by MacAir on 2017/7/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TRoomModel.h"

@implementation TRoomModel
- (void)setTRoomType:(NSString *)TRoomType{
    if ([TRoomType isEqualToString:@"osce_room"]) {
        _TRoomType = @"osce教室";
    }else if ([TRoomType isEqualToString:@"training_room"]){
        _TRoomType = @"训练室";
    }else if ([TRoomType isEqualToString:@"computer_room"]){
        _TRoomType = @"电教室";
    }else if ([TRoomType isEqualToString:@"lecture_room"]){
        _TRoomType = @"上课室";
    }else{
        _TRoomType = @"PBL室";
    }
}
@end
