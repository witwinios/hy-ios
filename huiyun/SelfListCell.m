//
//  SelfListCell.m
//  xiaoyun
//
//  Created by MacAir on 16/12/29.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "SelfListCell.h"

@implementation SelfListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.paperDes.editable = NO;
    self.statusLab.layer.cornerRadius = 5;
    self.statusLab.layer.masksToBounds = YES;
    
}
@end
