//
//  BaseviewController.h
//  yun
//
//  Created by MacAir on 2017/6/7.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ActionBlock)();
@interface BaseviewController : UIViewController
@property (nonatomic, copy) ActionBlock leftAction;
@property (nonatomic, copy) ActionBlock rightAction;
- (IBAction)leftBtnAction:(id)sender;
- (IBAction)rightBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *leftTitle;
@property (weak, nonatomic) IBOutlet UIButton *rightitle;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UITableView *tabView;


@end
