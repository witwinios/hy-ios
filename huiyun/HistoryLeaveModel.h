//
//  HistoryLeaveModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryLeaveModel : NSObject
@property (strong, nonatomic) NSNumber *leaveRequestId;
@property (strong, nonatomic) NSString *approver;
@property (strong, nonatomic) NSNumber *approverId;
@property (strong, nonatomic) NSString *approveStatus;
@property (strong, nonatomic) NSNumber *leaveStartTime;
@property (strong, nonatomic) NSNumber *leaveEndTime;
@property (strong, nonatomic) NSString *leaveType;
@property (strong, nonatomic) NSString *leaveReason;
@property (strong, nonatomic) NSNumber *fileId;
@property (strong, nonatomic) NSString *fileUrl;
@property (strong, nonatomic) NSString *requestName;
@property (strong, nonatomic) NSString *reviewComments;
@property (strong, nonatomic) UIImageView *submitImage;
@end
