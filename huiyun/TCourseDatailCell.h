//
//  TCourseDatailCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
@interface TCourseDatailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *TStatus;

@end
