//
//  ExamPointModel.h
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExamPointModel : NSObject
@property (strong, nonatomic) NSString *ExamPointModelStatus;
@property (strong, nonatomic) NSNumber *ExamPointModelStationId;
@property (strong, nonatomic) NSString *ExamPointModelSubject;
@property (strong, nonatomic) NSString *ExamPointModelCategory;
@property (strong, nonatomic) NSString *ExamPointModelStationName;
@property (strong, nonatomic) NSNumber *ExamPointModelUseds;
@property (strong, nonatomic) NSNumber *ExamPointModelInternal;
@end
