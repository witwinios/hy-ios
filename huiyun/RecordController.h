//
//  RecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordCell.h"
#import "DetailRecordController.h"
#import "ErrorRecordController.h"
@interface RecordController : UIViewController<UITableViewDelegate,UITableViewDataSource,selectIndexPathDelegate>

@end
