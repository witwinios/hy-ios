//
//  RescueDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RescueDetailController.h"

@interface RescueDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation RescueDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"抢救记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";

    if ([self.model.caseRescueModelStatus isEqualToString:@"待审核"]) {
        RescueDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            RescueRecordController *reueVC = [RescueRecordController new];
            reueVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:reueVC animated:YES];
        };
    }

    titleArray = @[@"审核状态:",@"疾病名称:",@"病人姓名:",@"病案号:",@"转归情况:",@"抢救日期:",@"带教老师:",@"说明:",@"指导意见:",@"附件:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"103",@"44",@"44",@"103",@"103",@"44"];
    self.dataArray = @[_model.caseRescueModelStatus,_model.caseRescueModelDiseaseName,_model.caseRescueModelDiseaserName,_model.caseRescueModelDiseaseNum,_model.caseRescueModelBackDescription,[[Maneger shareObject] timeFormatter1:_model.caseRescueModelDate.stringValue],_model.caseRescueModelTeacher,_model.caseRescueModelDescription,_model.caseRescueModelAdvice,_model.fileUrl];
    
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 4 || indexPath.row == 7 || indexPath.row == 8) {
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else if (indexPath.row == 9){
        return three;
    }else {
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
@end
