//
//  OperateRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseReuestModel.h"
@interface OperateRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UITextField *twoField;
@property (weak, nonatomic) IBOutlet UITextField *threeField;

@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UITextView *reasonView;
- (IBAction)dateAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
- (IBAction)saveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;
@property (strong, nonatomic)UIImagePickerController *imagePickController;
- (IBAction)fileAction:(id)sender;
@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseOperateModel *myModel;
@end
