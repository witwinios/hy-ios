//
//  ControlManeger.m
//  yun
//
//  Created by MacAir on 2017/7/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ControlManeger.h"

@implementation ControlManeger
+ (id)share{
    static ControlManeger *maneger = nil;
    if (maneger == nil) {
        maneger = [[ControlManeger alloc]init];
    }
    return maneger;
}
@end
