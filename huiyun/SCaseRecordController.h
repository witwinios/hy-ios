//
//  SCaseRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseReuestModel.h"

@interface SCaseRecordController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *diseaseNameField;
@property (weak, nonatomic) IBOutlet UITextField *diseaseNumField;
@property (weak, nonatomic) IBOutlet UITextView *mainText;
@property (weak, nonatomic) IBOutlet UITextView *secodText;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveAction:(id)sender;
- (IBAction)dateAction:(id)sender;
@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseMedicalModel *myModel;
@end
