//
//  MistakeDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MistakeDetailController.h"

@interface MistakeDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation MistakeDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"差错记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";

    if ([self.model.caseMistakeModelStatus isEqualToString:@"待审核"]) {
        MistakeDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            MistakeRecordController *misVC = [MistakeRecordController new];
            misVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:misVC animated:YES];
        };
    }

    titleArray = @[@"审核状态:",@"类别:",@"等级:",@"发生时间:",@"带教老师:",@"详细经过:",@"原因:",@"教训:",@"处理意见:",@"院领导:",@"院领导意见:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"103",@"103",@"44",@"103"];
    self.dataArray = @[_model.caseMistakeModelStatus,_model.caseMistakeModelCategory,_model.caseMistakeModelDegree,[[Maneger shareObject] timeFormatter1:_model.caseMistakeModelDate.stringValue],_model.caseMistakeModelTeacher,_model.caseMistakeModelViaDescription,_model.caseMistakeModelReason,_model.caseMistakeModelLession,_model.caseMistakeModelAdvice,_model.caseMistakeModelLeader,_model.caseMistakeModelLeaderAdvice];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    if (indexPath.row == 6 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 10) {
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else {
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
@end
