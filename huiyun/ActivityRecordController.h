//
//  ActivityRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityRecordController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UITextField *threeField;
- (IBAction)dateAction:(id)sender;

- (IBAction)saveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UITextField *twoField;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseActivityModel *myModel;
@end
