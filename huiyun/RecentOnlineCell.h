//
//  RecentOnlineCell.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentOnlineCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *onlineRoom;
@property (weak, nonatomic) IBOutlet UILabel *onlineName;
@property (weak, nonatomic) IBOutlet UILabel *onlineTime;
@property (weak, nonatomic) IBOutlet UILabel *onlineInternal;

@end
