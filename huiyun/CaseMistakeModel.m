//
//  CaseMistakeModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseMistakeModel.h"

@implementation CaseMistakeModel
//@property (strong, nonatomic) NSString *caseMistakeModelStatus;
- (void)setCaseMistakeModelStatus:(NSString *)caseMistakeModelStatus{
    if ([caseMistakeModelStatus isKindOfClass:[NSNull class]]) {
        _caseMistakeModelStatus = @"";
    }else if([caseMistakeModelStatus isEqualToString:@"waiting_approval"]){
        _caseMistakeModelStatus = @"待审核";
    }else if ([caseMistakeModelStatus isEqualToString:@"approved"]){
        _caseMistakeModelStatus = @"已通过";
    }else{
        _caseMistakeModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelCategory;
- (void)setCaseMistakeModelCategory:(NSString *)caseMistakeModelCategory{
    if ([caseMistakeModelCategory isKindOfClass:[NSNull class]]) {
        _caseMistakeModelCategory = caseMistakeModelCategory;
    }else{
        _caseMistakeModelCategory = caseMistakeModelCategory;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelDegree;
- (void)setCaseMistakeModelDegree:(NSString *)caseMistakeModelDegree{
    if ([caseMistakeModelDegree isKindOfClass:[NSNull class]]) {
        _caseMistakeModelDegree = @"";
    }else{
        _caseMistakeModelDegree = caseMistakeModelDegree;
    }
}
//@property (strong, nonatomic) NSNumber *caseMistakeModelDate;
- (void)setCaseMistakeModelDate:(NSNumber *)caseMistakeModelDate{
    if ([caseMistakeModelDate isKindOfClass:[NSNull class]]) {
        _caseMistakeModelDate = @0;
    }else{
        _caseMistakeModelDate = caseMistakeModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelTeacher;
- (void)setCaseMistakeModelTeacher:(NSString *)caseMistakeModelTeacher{
    if ([caseMistakeModelTeacher isKindOfClass:[NSNull class]]) {
        _caseMistakeModelTeacher = @"";
    }else{
        _caseMistakeModelTeacher = caseMistakeModelTeacher;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelTeacherId;
- (void)setCaseMistakeModelTeacherId:(NSString *)caseMistakeModelTeacherId{
    if ([caseMistakeModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseMistakeModelTeacherId = @"";
    }else{
        _caseMistakeModelTeacherId = caseMistakeModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelViaDescription;
- (void)setCaseMistakeModelViaDescription:(NSString *)caseMistakeModelViaDescription{
    if ([caseMistakeModelViaDescription isKindOfClass:[NSNull class]]) {
        _caseMistakeModelViaDescription = @"";
    }else{
        _caseMistakeModelViaDescription = caseMistakeModelViaDescription;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelReason;
- (void)setCaseMistakeModelReason:(NSString *)caseMistakeModelReason{
    if ([caseMistakeModelReason isKindOfClass:[NSNull class]]) {
        _caseMistakeModelReason = @"";
    }else{
        _caseMistakeModelReason = caseMistakeModelReason;
    }
}
//教训
//@property (strong, nonatomic) NSString *caseMistakeModelLession;
- (void)setCaseMistakeModelLession:(NSString *)caseMistakeModelLession{
    if ([caseMistakeModelLession isKindOfClass:[NSNull class]]) {
        _caseMistakeModelLession = @"";
    }else{
        _caseMistakeModelLession = caseMistakeModelLession;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelAdvice;
- (void)setCaseMistakeModelAdvice:(NSString *)caseMistakeModelAdvice{
    if ([caseMistakeModelAdvice isKindOfClass:[NSNull class]]) {
        _caseMistakeModelAdvice = @"";
    }else{
        _caseMistakeModelAdvice = caseMistakeModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelLeader;
- (void)setCaseMistakeModelLeader:(NSString *)caseMistakeModelLeader{
    if ([caseMistakeModelLeader isKindOfClass:[NSNull class]]) {
        _caseMistakeModelLeader = @"";
    }else{
        _caseMistakeModelLeader = caseMistakeModelLeader;
    }
}
//@property (strong, nonatomic) NSString *caseMistakeModelLeaderAdvice;
- (void)setCaseMistakeModelLeaderAdvice:(NSString *)caseMistakeModelLeaderAdvice{
    if ([caseMistakeModelLeaderAdvice isKindOfClass:[NSNull class]]) {
        _caseMistakeModelLeaderAdvice = @"";
    }else{
        _caseMistakeModelLeaderAdvice = caseMistakeModelLeaderAdvice;
    }
}
@end
