//
//  MenuView.m
//  
//
//  Created by 郑敏捷 on 17/3/1.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import "MenuView2.h"

#import "Macro.h"

@interface MenuView2 ()

@property (strong, nonatomic) UIView *lineView;

@end

@implementation MenuView2

- (instancetype)init {
    
    self= [super init];
    if (self) {
        
        self.backgroundColor = AllBackColor;
        self.frame           = CGRectMake(0, 64, ScreenWidth, MenuHeight);
    }
    return self;
}

- (void)setTitleArray:(NSArray *)titleArray {
    
    _titleArray = titleArray;
    
    [self initView];
}

- (void)initView {
    
    for (int count = 0; count < _titleArray.count; count ++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame     = CGRectMake((ScreenWidth / _titleArray.count) *count, 0, ScreenWidth / _titleArray.count, MenuHeight);
        button.tag       = count + 1;
        [button setTitle:_titleArray[count] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [button setTitleColor:AllTextColor forState:UIControlStateNormal];
        button.titleLabel.font = ZmjFontSize(13.0);
        button.adjustsImageWhenHighlighted = NO;
        [button addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        if (count == 0) button.selected = YES;
    }
    
    [self addSubview:self.lineView];
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView                 = [[UIView alloc]init];
        _lineView.frame           = CGRectMake(0, MenuHeight - 1, ScreenWidth / _titleArray.count, 1);
        _lineView.backgroundColor = [UIColor redColor];
    }
    return _lineView;
}

- (void)btnAction:(UIButton *)button {
    
    if (self.vcBtnBlock) {
        
        self.vcBtnBlock(button.tag);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
