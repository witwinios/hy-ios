//
//  ErrorCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ErrorCell.h"
@implementation ErrorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.testStatus.layer.cornerRadius = 5;
    self.testStatus.layer.masksToBounds = YES;
}
- (void)setProperty:(ErrorModel *)model{
    self.testName.text = model.testName;
    self.testStatus.text = model.testStatus;
    self.testCategory.text = model.testCategory;
    self.testSubject.text = model.testSubject;
    self.testPaper.text = model.testPaper.stringValue;
}
@end
