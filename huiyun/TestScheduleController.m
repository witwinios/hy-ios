//
//  SelfTestViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "TestScheduleController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface TestScheduleController ()
{
    UITableView *selfTable;
    NSMutableArray *dataArray;
    int currentPage;
}
@end

@implementation TestScheduleController

- (void)viewDidLoad {
    [super viewDidLoad];
    //简单对象初始化
    [self objectInit];
    //创建导航栏
    [self buildNavigation];
    //创建表
    [self createTable];
    [self setUpRefresh];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [selfTable headerBeginRefreshing];
}
- (void)objectInit{
    currentPage = 1;
    dataArray = [NSMutableArray new];
}
- (void)createTable{
    selfTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64) style:UITableViewStyleGrouped];
    selfTable.delegate = self;
    selfTable.dataSource = self;
    [selfTable registerNib:[UINib nibWithNibName:@"SelfListCell" bundle:nil] forCellReuseIdentifier:@"selfListCell"];
    [self.view addSubview:selfTable];
    //
}
- (void)setUpRefresh{
    [selfTable addHeaderWithTarget:self action:@selector(downRefresh)];
    [selfTable addFooterWithTarget:self action:@selector(loadMoreRefresh)];
    //设置文字
    selfTable.headerPullToRefreshText = @"下拉刷新";
    selfTable.headerReleaseToRefreshText = @"松开进行刷新";
    selfTable.headerRefreshingText = @"刷新中。。。";
    
    selfTable.footerPullToRefreshText = @"上拉加载";
    selfTable.footerReleaseToRefreshText = @"松开进行加载";
    selfTable.footerRefreshingText = @"加载中。。。";
}
- (void)downRefresh{
    //调用api----------------------------------------------------------------------
    NSString *requestUrl = [NSString stringWithFormat:@"%@/selfTests?pageStart=1&pageSize=15&studentId=%@&selfType=SELF_TEST",LocalIP,LocalUserId];
    NSDictionary *params = @{};
    NSString *method = @"get";
    //-----------------------------------------
    
    
    [RequestTools RequestWithURL:requestUrl Method:method Params:params Success:^(NSDictionary *responseDic) {
        [selfTable headerEndRefreshing];
        //获取数据源
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            //移除所有元素
            [dataArray removeAllObjects];
            //数据装配------------------------------------
            NSArray *data = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
            if (data.count ==0) {
                [MBProgressHUD showToastAndMessage:@"暂无自测安排!" places:0 toView:nil];
            }else{
                for (int i = 0; i<data.count; i++) {
                    SelfTestModel *model = [SelfTestModel new];
                    NSDictionary *object = [data objectAtIndex:i];
                    model.testStatus = [object objectForKey:@"testStatus"];
                    model.testName = [object objectForKey:@"testName"];
                    model.pageNum = [object objectForKey:@"testPapersNum"];
                    model.testDes = [object objectForKey:@"description"];
                    model.testId = [object objectForKey:@"testId"];
                    [dataArray addObject:model];
                }
                [selfTable reloadData];
            }
            //------------------------------需要置换
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [selfTable headerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)loadMoreRefresh{
    currentPage ++;
    //--------------------------------
    NSString *requestUrl = [NSString stringWithFormat:@"%@/selfTests?pageStart=%d&pageSize=15&studentId=%@&selfType=SELF_TEST",LocalIP,currentPage,LocalUserId];
    //--------------------------------
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
        [selfTable footerEndRefreshing];
        //获取数据源
        if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
            //--------------------------------
            NSArray *data = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
            if (data.count == 0) {
                [Maneger showAlert:@"暂无更多自测安排" andCurentVC:self];
            }else{
                NSMutableArray *newData = [NSMutableArray new];
                for (int i = 0; i<data.count; i++) {
                    SelfTestModel *model = [SelfTestModel new];
                    NSDictionary *object = [data objectAtIndex:i];
                    model.testStatus = [object objectForKey:@"testStatus"];
                    model.testName = [object objectForKey:@"testName"];
                    model.pageNum = [object objectForKey:@"testPapersNum"];
                    model.testDes = [object objectForKey:@"description"];
                    model.testId = [object objectForKey:@"testId"];
                    [newData addObject:model];
                }
                NSRange range = NSMakeRange(dataArray.count,newData.count);
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [selfTable reloadData];
            }
            //--------------------------------
        }else{
            [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        currentPage--;
        [selfTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)buildNavigation{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(addAction1:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"测试安排";
    [backNavigation addSubview:titleLab];
    //
}
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)addAction1:(UIButton *)btn{
    AddTestController *addVC = [AddTestController new];
    [self.navigationController pushViewController:addVC animated:YES];
}
//
#pragma tableview协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    SelfPaperController *selfVC = [SelfPaperController new];
//    selfVC.model = [dataArray objectAtIndex:indexPath.section];
//    [self.navigationController pushViewController:selfVC animated:YES];
    [self.navigationController pushViewController:[TestController new] animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelfListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selfListCell"];
    SelfTestModel *model = [dataArray objectAtIndex:indexPath.section];
    cell.statusLab.text = model.testStatus;
    cell.testName.text = model.testName;
    cell.pageNum.text = [model.pageNum stringValue];
    cell.paperDes.text = model.testDes;
    return cell;
}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma -UIAlertViewDelegate
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end

