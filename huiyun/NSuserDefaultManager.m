//
//  NSuserDefaultManager.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "NSuserDefaultManager.h"

@implementation NSuserDefaultManager
+ (id)share{
    static NSuserDefaultManager *maneger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (maneger == nil) {
            maneger = [[NSuserDefaultManager alloc]init];
        }
    });
    return maneger;
}
+ (void)saveHeadImage:(UIImage *)image byAccount:(NSNumber *)account{
    NSData *imageData = [NSKeyedArchiver archivedDataWithRootObject:image];
    NSString *imageKey = [NSString stringWithFormat:@"%@-image",account];
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:imageKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (UIImage *)readHeadImageByAccount:(NSNumber *)account{
    NSString *imageKey = [NSString stringWithFormat:@"%@-image",account];
    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:imageKey];
    if (imageData != nil) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:imageData];
    }
    return nil;
}
- (void)saveCurrentUser:(PersonEntity *)person{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [_userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:person] forKey:@"witwin_current_account_test2"];
    [_userDefaults synchronize];
}
- (PersonEntity *)readCurrentUser{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *persionData = [_userDefaults objectForKey:@"witwin_current_account_test2"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:persionData];
}
//保存当前账户
- (void)saveCurrentAccount:(AccountEntity *)entity{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [_userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:entity] forKey:@"witwin_current_account"];
    [_userDefaults synchronize];
}
//读取当前账户
- (AccountEntity *)readCurrentAccount{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *persionData = [_userDefaults objectForKey:@"witwin_current_account"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:persionData];
}
- (void)saveLastAccount:(PersonEntity *)person{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [_userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:person] forKey:@"witwin_last_account_test5"];
    [_userDefaults synchronize];
}
- (PersonEntity *)readLastAccount{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *persionData = [_userDefaults objectForKey:@"witwin_last_account_test5"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:persionData];
}
- (void)saveHospital:(HospitalEntity *)entity{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [_userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:entity] forKey:@"witwin_hospital_test2"];
}
- (HospitalEntity *)readHospital{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    HospitalEntity *hosEntity = [NSKeyedUnarchiver unarchiveObjectWithData:[_userDefaults objectForKey:@"witwin_hospital_test2"]];
    return hosEntity;
}
@end

