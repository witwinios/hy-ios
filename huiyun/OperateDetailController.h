//
//  OperateDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCellOne.h"
#import "DetailCellTwo.h"
#import "DetailCellThree.h"
#import "CaseOperateModel.h"
@interface OperateDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) CaseOperateModel *model;
@end
