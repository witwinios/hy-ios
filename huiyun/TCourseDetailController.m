//
//  CourseDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseDetailController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface TCourseDetailController ()
{
    UITableView *teachTable;
    NSArray *titleArray;
    NSArray *contentArray;
}
@end

@implementation TCourseDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程详情";
    [backNavigation addSubview:titleLab];
    
}
-(void)setUI{
    teachTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-114) style:UITableViewStylePlain];
    teachTable.delegate = self;
    teachTable.dataSource = self;
    teachTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [teachTable registerNib:[UINib nibWithNibName:@"TCourseDatailCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [teachTable registerNib:[UINib nibWithNibName:@"DesCell" bundle:nil] forCellReuseIdentifier:@"cellStyle2"];
    [self.view addSubview:teachTable];
    
    if ([self.model.scheduleStatus isEqualToString:@"已发布"] || [self.model.scheduleStatus isEqualToString:@"计划中"]) {
        NSLog(@"startTime=%@ current=%@",self.model.startTime,[[Maneger shareObject] currentLong]);
        if (self.model.startTime.longValue > [[[Maneger shareObject] currentLong] longLongValue]) {
            UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            submitBtn.frame = CGRectMake(0, Sheight-50, Swidth, 50);
            submitBtn.backgroundColor = [UIColor blueColor];
            [submitBtn setTitle:@"开始上课" forState:UIControlStateNormal];
            [submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
            [submitBtn addTarget:self action:@selector(beginAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:submitBtn];
        }
    }
    NSLog(@"time=%@",self.model.startTime);
    NSLog(@"timeFomatter=%@",[[Maneger shareObject] timeFormatter:self.model.startTime.stringValue]);
    NSString *time = [NSString stringWithFormat:@"%@ 到 %@",[[Maneger shareObject] timeFormatter:self.model.startTime.stringValue],[[Maneger shareObject] timeFormatter:self.model.endTime.stringValue]];
    NSString *start =[[Maneger shareObject] timeFormatter:self.model.startTime.stringValue];
    NSString *end = [[Maneger shareObject] timeFormatter:self.model.endTime.stringValue];
    titleArray = @[@"课程名称:",@"课程科目:",@"课程类型:",@"开始时间:",@"结束时间:",@"上课老师:",@"报名上限:",@"课程简介:"];
    contentArray = @[self.model.courseName,self.model.courseSubjec,self.model.scheduleTyoe,start,end,self.model.teacherName,self.model.registNum.stringValue,self.model.courseDes];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
//开始上课
- (void)beginAction:(UIButton *)btn{
    [MBProgressHUD showHUDAndMessage:@"上课中..." toView:nil];
    if (btn.enabled == YES) {
        RequestTools *tool = [RequestTools new];
        [tool postRequestPrams:nil andURL:[NSString stringWithFormat:@"%@/courseSchedules/%@/start",LocalIP,self.model.scheduleID]];
        NSLog(@"beginAPI=%@",[NSString stringWithFormat:@"%@/courseSchedules/%@/start",LocalIP,self.model.scheduleID]);
        tool.errorBlock = ^(NSString *code){
            [MBProgressHUD hideHUDForView:nil];
            if (![code isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"加载失败!" places:0 toView:nil];
            }
        };
        tool.responseBlock = ^(NSDictionary *result){
            [MBProgressHUD hideHUDForView:nil];
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                [btn setTitle:@"上课中" forState:0];
                btn.enabled=NO;
                [MBProgressHUD showToastAndMessage:@"上课成功!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"上课失败!" places:0 toView:nil];
            }
        };

    }else{
        [Maneger showAlert:@"不可用" andCurentVC:self];
    }
}
#pragma -delegate&dataSourse
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 8;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row ==7) {
            return 120.f;
        }
        return 44.f;
    }
    return 44.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 20.f;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 20)];
        view.backgroundColor = [UIColor lightGrayColor];
        return view;
    }
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1 && ![self.model.scheduleStatus isEqualToString:@"计划中"]) {
        self.beforeVC = [BeforeCourseController new];
        self.beforeVC.model = self.model;
        [self.navigationController pushViewController:self.beforeVC animated:YES];
    }else if (indexPath.section == 1 && [self.model.scheduleStatus isEqualToString:@"计划中"]){
        [MBProgressHUD showToastAndMessage:@"计划中课程暂无答题统计!" places:0 toView:nil];
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 7) {
            DesCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle2"];
            cell2.detailDes.text = contentArray[indexPath.row];
            return cell2;
        }else{
            TCourseDatailCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            if (indexPath.row == 0) {
                cell1.TStatus.text = self.model.scheduleStatus;
                cell1.TStatus.layer.cornerRadius = 5;
                cell1.TStatus.backgroundColor = UIColorFromHex(0x5CADAD);
                cell1.TStatus.layer.masksToBounds = YES;
                cell1.TStatus.adjustsFontSizeToFitWidth = YES;
            }
            cell1.title.text = titleArray[indexPath.row];
            cell1.content.text = contentArray[indexPath.row];
            return cell1;
        }
    }else{
        TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
        cell.title.text = @"课前答题统计";
        return cell;
    }
}
@end
