//
//  EssayDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "BaseWitwinController.h"
#import "CaseEssayModel.h"
@interface EssayDetailController : BaseWitwinController
@property (strong, nonatomic) CaseEssayModel *model;
@end
