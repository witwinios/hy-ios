//
//  MyAlertView.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MyAlertView.h"
#import "AddStar.h"

@interface MyAlertView()<AddStarViewDelegate>{
    NSString *AddStarString;
}

@end


@implementation MyAlertView


- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        //创建遮罩
        _blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
        _blackView.backgroundColor = [UIColor blackColor];
        _blackView.alpha = 0.5;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(blackClick)];
        [self.blackView addGestureRecognizer:tap];
        [self addSubview:_blackView];
        //创建alert
        self.alertview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 360, 300)];
        self.alertview.center = self.center;
        self.alertview.layer.cornerRadius = 17;
        self.alertview.clipsToBounds = YES;
        self.alertview.backgroundColor = [UIColor whiteColor];
        [self addSubview:_alertview];
        [self exChangeOut:self.alertview dur:0.6];
        
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _tipLable = [[UILabel alloc]initWithFrame:CGRectMake(0,0,360,43)];
    _tipLable.textAlignment = NSTextAlignmentCenter;
    [_tipLable setBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1.0]];
    _tipLable.text = _title;
    [_tipLable setFont:[UIFont systemFontOfSize:18]];
    [_tipLable setTextColor:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]];
    
    [self.alertview addSubview:_tipLable];
    
    
     [self creatViewWithPidAlert];
    AddStarString = @"10";
    [self.delegate ChooseStarNum:AddStarString];
    
    self.alertview.center = CGPointMake(self.center.x, self.center.y);
    
    [self createBtnTitle:_btnTitleArr];
}



- (void)creatViewWithPidAlert
{
    
  
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, Swidth, 30)];
    label.text=@"每一颗星代表两分，满分十分。不可选择半颗星：";
    label.font=[UIFont systemFontOfSize:14];
    [self.alertview addSubview:label];
    
    AddStar *view = [[AddStar alloc]initWithFrame:CGRectMake(5, 100,
                                                             self.alertview.frame.size.width-20,
                                                             50)];
    view.canChoose = YES;
    view.animation = YES;
    view.delegate=self;
   
    
    [self.alertview addSubview:view];
    
    
}

-(void)ChooseStarNum:(NSInteger *)ChooseStarNum{
    AddStarString=[NSString stringWithFormat:@"%ld",ChooseStarNum];
    
    
    
    
    
    
    
   //  NSLog(@"以选择 %@ 颗星星",AddStarString);
    [self.delegate ChooseStarNum:AddStarString];
}





- (void)createBtnTitle:(NSArray *)titleArr
{
    
    CGFloat m = self.alertview.frame.size.width;
    
    for (int i=0; i<_numBtn; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (_numBtn == 1) {
            btn.frame = CGRectMake(20, self.alertview.frame.size.height-48,(m-40), 33);
        }else{
            
            btn.frame = CGRectMake(20+i*(20+(m-60)/2), self.alertview.frame.size.height-48, (m-60)/2, 33);
        }
        
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.tag = 100+i;
        btn.layer.cornerRadius = 5;
        btn.clipsToBounds = YES;
        [btn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:18]];
        if ([titleArr[i] isEqualToString:@"确定"]||[titleArr[i] isEqualToString:@"退出页面"]) {
            //            [btn setBackgroundColor:[UIColor colorWithHexString:[ThemeSingleton sharedInstance].UINavgationBar alpha:1]];
            [btn setBackgroundColor:UIColorFromHex(0x20B2AA)];
        }else{
            
            [btn setBackgroundColor:UIColorFromHex(0x20B2AA)];
        }
        [self.alertview addSubview:btn];
    }
}
- (void)blackClick
{
    [self cancleView];
}
- (void)cancleView
{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alertview = nil;
    }];
    
}
-(void)exChangeOut:(UIView *)changeOutView dur:(CFTimeInterval)dur{
    
    CAKeyframeAnimation * animation;
    
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    
    animation.duration = dur;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 0.9)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    animation.timingFunction = [CAMediaTimingFunction functionWithName: @"easeInEaseOut"];
    [changeOutView.layer addAnimation:animation forKey:nil];
}

-(void)clickButton:(UIButton *)button{
    //    DLog(@"%ld",button.tag);
    if ([self.delegate respondsToSelector:@selector(didClickButtonAtIndex:password:)]) {
        if ([button.titleLabel.text isEqualToString:@"退出页面"]) {
            button.tag = 101;
            
            
            
            [self.delegate ChooseStarNum:@"10"];
            return;
        }
        [self.delegate didClickButtonAtIndex:button.tag password:_password];
    }
    [self cancleView];
}
-(void)initWithTitle:(NSString *) title contentStr:(NSString *)content type:(NSInteger)type btnNum:(NSInteger)btnNum btntitleArr:(NSArray *)btnTitleArr

{
    _title = title;
    _type = type;
    _numBtn = btnNum;
    _btnTitleArr = btnTitleArr;
    _contentStr = content;
}


@end
