//
//  CaseActivityModel.m
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseActivityModel.h"

@implementation CaseActivityModel
//@property (strong, nonatomic) NSString *caseActivityName;
- (void)setCaseActivityName:(NSString *)caseActivityName{
    if ([caseActivityName isKindOfClass:[NSNull class]]) {
        _caseActivityName = @"";
    }else{
        _caseActivityName = caseActivityName;
    }
}
//@property (strong, nonatomic) NSString *caseStatus;
- (void)setCaseStatus:(NSString *)caseStatus{
    if ([caseStatus isKindOfClass:[NSNull class]]) {
        _caseStatus = @"";
    }else if ([caseStatus isEqualToString:@"waiting_approval"]){
        _caseStatus = @"待审核";
    }else if ([caseStatus isEqualToString:@"approved"]){
        _caseStatus = @"已通过";
    }else{
        _caseStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *casePersion;
- (void)setCasePersion:(NSString *)casePersion{
    if ([casePersion isKindOfClass:[NSNull class]]) {
        _casePersion = @"";
    }else{
        _casePersion = casePersion;
    }
}
//@property (strong, nonatomic) NSNumber *caseInternal;
- (void)setCaseInternal:(NSNumber *)caseInternal{
    if ([caseInternal isKindOfClass:[NSNull class]]) {
        _caseInternal = @0;
    }else{
        _caseInternal = caseInternal;
    }
}
//@property (strong, nonatomic) NSNumber *caseDate;
- (void)setCaseDate:(NSNumber *)caseDate{
    if ([caseDate isKindOfClass:[NSNull class]]) {
        _caseDate = @0;
    }else{
        _caseDate = caseDate;
    }
}
//@property (strong, nonatomic) NSString *caseTeacher;
- (void)setCaseTeacher:(NSString *)caseTeacher{
    if ([caseTeacher isKindOfClass:[NSNull class]]) {
        _caseTeacher = @"";
    }else{
        _caseTeacher = caseTeacher;
    }
}
//@property (strong, nonatomic) NSString *caseActivityContent;
- (void)setCaseActivityContent:(NSString *)caseActivityContent{
    if ([caseActivityContent isKindOfClass:[NSNull class]]) {
        _caseActivityContent = @"";
    }else{
        _caseActivityContent = caseActivityContent;
    }
}
//@property (strong, nonatomic) NSString *caseAdvice;
- (void)setCaseAdvice:(NSString *)caseAdvice{
    if ([caseAdvice isKindOfClass:[NSNull class]]) {
        _caseAdvice = @"";
    }else{
        _caseAdvice = caseAdvice;
    }
}
@end
