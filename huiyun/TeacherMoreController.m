//
//  TeacherMoreController.m
//  yun
//
//  Created by MacAir on 2017/8/28.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TeacherMoreController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface TeacherMoreController ()
{
    NSArray *titleArray;
    NSArray *contentArray;
    NSMutableArray *heightArray;
    CGFloat des_h;
    
    EditStay editStay;
    UIButton *rightBtn;
    
    UIView *pickView;
    UIDatePicker *datePick;
    UITextField *dateDisplay;
    
    NSMutableDictionary *updateDic;
}
@property (strong, nonatomic)WXPPickerView * PickerOne;
@end

@implementation TeacherMoreController

- (void)viewDidLoad {
    [super viewDidLoad];
    editStay = EditStayNo;
    heightArray = [NSMutableArray new];
    updateDic = [NSMutableDictionary dictionaryWithDictionary:self.entity.responseDic];
    NSString *start = [[Maneger shareObject] timeFormatter1:self.entity.userWorkStart.stringValue];
    NSString *end = [[Maneger shareObject] timeFormatter1:self.entity.userWorkEnd.stringValue];
    if (self.entity.userWorkStart.intValue == 0) {
        start = @"暂无";
    }
    if (self.entity.userWorkEnd.intValue == 0) {
        end = @"暂无";
    }
    
    titleArray = @[@"最高学历:",@"毕业院校:",@"所在单位:",@"教龄:",@"教师类型:",@"聘用开始时间:",@"聘用结束时间:",@"说明:"];
    contentArray = @[self.entity.userDegree,self.entity.userSchool,self.entity.userSource,[NSString stringWithFormat:@"%@年",self.entity.userCareer],self.entity.userTeacherType,start,end,self.entity.userDescription];
    for (int i=0; i<contentArray.count; i++) {
        [heightArray addObject:[NSString stringWithFormat:@"%f",[Maneger getPonentH:contentArray[i] andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-148]]];
    }
    des_h = [Maneger getPonentH:self.entity.description andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-148];
    [self setUI];
    [self createDatePick];
}
- (void)createDatePick{
    pickView = [UIView new];
    pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 210);
    pickView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickView];
    datePick = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 150)];
    [datePick setTimeZone:[NSTimeZone localTimeZone]];
    [datePick setDate:[NSDate date]];
    datePick.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_ch"];
    datePick.timeZone = [NSTimeZone timeZoneWithName:@"GTM+8"];
    [datePick setDatePickerMode:UIDatePickerModeDate];
    [datePick addTarget:self action:@selector(timeChanged:) forControlEvents:UIControlEventValueChanged];
    [pickView addSubview:datePick];
    //dateDisplay
    dateDisplay = [[UITextField alloc]initWithFrame:CGRectMake(WIDTH/6, 170, Swidth/2, 30)];
    dateDisplay.backgroundColor = [UIColor whiteColor];
    dateDisplay.textAlignment = 1;
    datePick.minimumDate = [NSDate new];
    dateDisplay.userInteractionEnabled = NO;
    dateDisplay.layer.cornerRadius = 5;
    dateDisplay.backgroundColor = UIColorFromHex(0x46bec8);
    dateDisplay.textColor = UIColorFromHex(0xffffff);
    //设置默认时间
    NSDateFormatter *fomatter = [NSDateFormatter new];
    [fomatter setDateFormat:@"yyyy-MM-dd"];
    dateDisplay.text = [fomatter stringFromDate:[datePick date]];
    [pickView addSubview:dateDisplay];
    //
    UIButton *alphaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [alphaButton setTitle:@"确定" forState:UIControlStateNormal];
    alphaButton.backgroundColor = UIColorFromHex(0x46bec8);
    alphaButton.frame = CGRectMake(Swidth/6*4, 170, Swidth/6, 30);
    alphaButton.layer.cornerRadius = 5;
    alphaButton.titleLabel.textAlignment = 1;
    alphaButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [alphaButton addTarget:self action:@selector(timeSelect:) forControlEvents:UIControlEventTouchUpInside];
    [pickView addSubview:alphaButton];
}
- (void)timeSelect:(UIButton *)btn{
    [UIView animateWithDuration:1 animations:^{
        [_mohuView removeFromSuperview];
        pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 210);
        _selfBlock(datePick.date);
    }];
}
- (void)timeChanged:(UIDatePicker *)pick{
    NSDateFormatter *fomatter = [NSDateFormatter new];
    [fomatter setDateFormat:@"yyyy-MM-dd"];
    dateDisplay.text = [fomatter stringFromDate:[pick date]];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xBEBEBE);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-60, 29.5, 60, 25);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn setTitle:@"保存" forState:UIControlStateSelected];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
    [rightBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"更多信息";
    [backNavigation addSubview:titleLab];
    //tableView
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-164) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView registerNib:[UINib nibWithNibName:@"AccountCell1" bundle:nil] forCellReuseIdentifier:@"account1"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
    
    _mohuView = [UIView new];
    _mohuView.frame = CGRectMake(0, 64, WIDTH, HEIGHT-274);
    _mohuView.backgroundColor = [UIColor lightGrayColor];
    _mohuView.alpha = 0.5;
}
- (void)updateInfo:(int)s{
    //s = 1 返回
    NSString *requesUrl = [NSString stringWithFormat:@"%@/users/%@",LocalIP,self.entity.userID];
    [RequestTools RequestWithURL:requesUrl Method:@"put" Params:updateDic Message:@"" Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败!" places:0 toView:nil];
    }];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)editAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        [MBProgressHUD showToastAndMessage:@"进入编辑模式~" places:0 toView:nil];
        editStay = EditStayYes;
    }else{
        editStay = EditStayNo;
        //修改
        [self updateInfo:0];
    }
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.PickerOne close];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat H_line = [heightArray[indexPath.row] floatValue]+23;
    return H_line;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titleArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountCell1 *cell = [tableView cellForRowAtIndexPath:indexPath];
    __block NSMutableDictionary *response = updateDic;
    __block TeacherMoreController *teachVC = self;
    if (editStay == EditStayYes) {
        if (indexPath.row == 0) {
            NSArray *array = @[@"学士",@"硕士",@"博士",@"博士后"];
            NSDictionary *titleDic = @{@"学士":@"bachelor",@"硕士":@"master",@"博士":@"doctorate",@"博士后":@"post_doctorate"};
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                teachVC.entity.userDegree = array[index];
                [response setObject:titleDic[array[index]] forKey:@"degree"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 1){
            NSMutableArray *indexArray = [NSMutableArray new];
            NSMutableArray *nameArray = [NSMutableArray new];
            [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/schools?pageSize=999",LocalIP] Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
                NSArray *arr = result[@"responseBody"][@"result"];
                for (int i=0; i<arr.count; i++) {
                    NSDictionary *dic = arr[i];
                    [indexArray addObject:dic[@"schoolId"]];
                    [nameArray addObject:dic[@"schoolName"]];
                }
                //弹框
                self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:nameArray]];
                self.PickerOne.delegate = self;
                self.PickerOne.rightBtnTitle = @"确认";
                self.PickerOne.backgroundColor = [UIColor whiteColor];
                self.PickerOne.index = ^(int index){
                    cell.content.text = nameArray[index];
                    teachVC.entity.userSchool = nameArray[index];
                    [response setObject:indexArray[index] forKey:@"schoolId"];
                };
                [self.view addSubview:self.PickerOne];
                [self.PickerOne show];
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
        }else if (indexPath.row == 2){
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                _entity.userSource = str;
                [response setObject:str forKey:@"source"];
            };
            [self.view addSubview:alert];
        }else if (indexPath.row == 3){
            //            AlertView *alert = [[AlertView alloc]init];
            //            alert.backBlock = ^(NSString *str){
            //                cell.content.text = str;
            //                NSDictionary *dic = response[@"teachingRecord"];
            //                NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:dic];
            //                _entity.userCareer = @([str integerValue]);
            //                [muDic setObject:str forKey:@"career"];
            //                [response setObject:muDic forKey:@"teachingRecord"];
            //                [[response objectForKey:@"teachingRecord"] setObject:str forKey:@"career"];
            //            };
            //            [self.view addSubview:alert];
            [MBProgressHUD showToastAndMessage:@"不能修改~" places:0 toView:nil];
        }else if (indexPath.row == 4){
            //            NSArray *array = @[@"院内",@"院外"];
            //            NSArray *content = @[@"inside",@"outside"];
            //            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            //            self.PickerOne.delegate = self;
            //            self.PickerOne.rightBtnTitle = @"确认";
            //            self.PickerOne.backgroundColor = [UIColor whiteColor];
            //            self.PickerOne.index = ^(int index){
            //                cell.content.text = array[index];
            //                teachVC.entity.userTeacherType = array[index];
            //                [[NSMutableDictionary dictionaryWithDictionary:response[@"teachingRecord"]] setObject:content[index] forKey:@"teacherType"];
            //
            //            };
            //            [self.view addSubview:self.PickerOne];
            //            [self.PickerOne show];
            [MBProgressHUD showToastAndMessage:@"不能修改~" places:0 toView:nil];
        }else if (indexPath.row == 5){
            //            [UIView animateWithDuration:0.5 animations:^{
            //                [self.view addSubview:_mohuView];
            //                pickView.frame = CGRectMake(0, HEIGHT-210, WIDTH, 210);
            //            }];
            //            self.selfBlock = ^(NSDate *result){
            //                NSDateFormatter *fomatter = [NSDateFormatter new];
            //                [fomatter setDateFormat:@"yyyy-MM-dd"];
            //                cell.content.text = [fomatter stringFromDate:result];
            //                [[NSMutableDictionary dictionaryWithDictionary:response[@"teachingRecord"]] setObject:[NSString stringWithFormat:@"%lld",[teachVC getDateTimeTOMilliSeconds:result]] forKey:@"startDate"];
            //            };
            [MBProgressHUD showToastAndMessage:@"不能修改~" places:0 toView:nil];
        }else if (indexPath.row == 6){
            //            [UIView animateWithDuration:0.5 animations:^{
            //                [self.view addSubview:_mohuView];
            //                pickView.frame = CGRectMake(0, HEIGHT-210, WIDTH, 210);
            //            }];
            //            self.selfBlock = ^(NSDate *result){
            //                NSDateFormatter *fomatter = [NSDateFormatter new];
            //                [fomatter setDateFormat:@"yyyy-MM-dd"];
            //                cell.content.text = [fomatter stringFromDate:result];
            //                [[NSMutableDictionary dictionaryWithDictionary:response[@"teachingRecord"]] setObject:[NSString stringWithFormat:@"%lld",[teachVC getDateTimeTOMilliSeconds:result]] forKey:@"endDate"];
            //            };
            [MBProgressHUD showToastAndMessage:@"不能修改~" places:0 toView:nil];
        }else{
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                _entity.userDescription = str;
                [response setObject:str forKey:@"description"];
            };
            [self.view addSubview:alert];
        }
    }
}
-(long long)getDateTimeTOMilliSeconds:(NSDate *)datetime
{
    NSTimeInterval interval = [datetime timeIntervalSince1970];
    long long totalMilliseconds = interval ;
    NSLog(@"totalMilliseconds=%llu",totalMilliseconds);
    return totalMilliseconds;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"account1"];
    cell.title.text = titleArray[indexPath.row];
    cell.content.text = contentArray[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:17];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 7){
        cell.arrow.hidden = YES;
    }
    return cell;
}
@end

