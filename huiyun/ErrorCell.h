//
//  ErrorCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorModel.h"
@interface ErrorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *testStatus;
@property (weak, nonatomic) IBOutlet UILabel *testName;
@property (weak, nonatomic) IBOutlet UILabel *testSubject;
@property (weak, nonatomic) IBOutlet UILabel *testPaper;
@property (weak, nonatomic) IBOutlet UILabel *testCategory;
- (void)setProperty:(ErrorModel *)model;
@end
