//
//  BeforeCourseCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/8.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeforeCellModel.h"
@interface BeforeCourseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *quesTitle;
@property (weak, nonatomic) IBOutlet UILabel *correctNum;
@property (weak, nonatomic) IBOutlet UILabel *inCorrectNum;
@property (weak, nonatomic) IBOutlet UILabel *weiNum;

- (void)setProperty:(BeforeCellModel *)model;
@end
