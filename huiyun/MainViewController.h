//
//  MainViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYCourseViewController.h"
#import "SelfTestViewController.h"
#import "SelfTestCell.h"
#import "RecentOnlineCell.h"
#import "RecentOSCECell.h"
#import "AFNetworking.h"
#import "Maneger.h"
#import "LoadingView.h"
#import "OSCEModel.h"
#import "CameraScanViewController.h"
#import "Maneger.h"
#import "MainVcCell.h"
#import "ErrorController.h"
#import "LeaveController.h"
#import "RequestTools.h"
#import "ExamController.h"
#import "AddTestController.h"
#import "AddErrorController.h"
#import "AccountController.h"
#import "ScanController.h"
#import "MeetDisplayController.h"
#import "SdepartController.h"
#import "TestScheduleController.h"
@interface MainViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
+(id)shareObject;
@property (strong, nonatomic) HYCourseViewController *courseVC;
@property (strong, nonatomic) ExamController *examVC;
@property (strong, nonatomic) SelfTestViewController *selftestVC;
@property (strong, nonatomic) CameraScanViewController *cameraVC;
@property (strong, nonatomic) ErrorController *errorVC;
@property (strong, nonatomic) LeaveController *leaveVC;
@property (strong, nonatomic) AddTestController *addTestVC;
@property (strong, nonatomic) AddPaperController *addPaperVC;
@property (strong, nonatomic) MeetDisplayController *meetVC;
@property (strong, nonatomic) SdepartController *sdepartVC;
@property (strong, nonatomic) TestScheduleController *testVC;
@property (strong, nonatomic) UIImageView *headImage;
@end
