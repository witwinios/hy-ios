//
//  LiveRoomViewController.h
//  iOSZhongYe
//
//  Created by 郑敏捷 on 2017/5/19.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveRoomViewController : UIViewController

@property (strong, nonatomic) NSString *domain;

@property (strong, nonatomic) NSString *webcastID;

@property (strong, nonatomic) NSString *UserName;

@property (strong, nonatomic) NSString *watchPassword;

@property (strong, nonatomic) NSString *LiveClassName;

@property (strong, nonatomic) NSString *ServiceType;

@end
