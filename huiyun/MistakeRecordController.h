//
//  MistakeRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MistakeRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UITextField *twoField;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview1;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview2;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview3;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic)UIImagePickerController *imagePickController;
- (IBAction)dateAction:(id)sender;
- (IBAction)saveAction:(id)sender;

@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseMistakeModel *myModel;
@end
