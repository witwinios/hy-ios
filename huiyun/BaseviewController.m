//
//  BaseviewController.m
//  yun
//
//  Created by MacAir on 2017/6/7.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "BaseviewController.h"

@interface BaseviewController ()

@end

@implementation BaseviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (IBAction)leftBtnAction:(id)sender {
    self.leftAction();
}

- (IBAction)rightBtnAction:(id)sender {
    self.rightAction();
}
@end
