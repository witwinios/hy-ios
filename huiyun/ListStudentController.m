//
//  ListStudentController.m
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ListStudentController.h"

@interface ListStudentController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    
    NSInteger timerInternal;
    NSTimer *timer;
    
}
@end

@implementation ListStudentController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadStudent];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    [self setUI];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"考生列表";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    UIButton *switchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    switchBtn.frame = CGRectMake(Swidth-80, 27, 80, 30);
    [switchBtn setTitle:@"自动叫号" forState:0];
    switchBtn.layer.cornerRadius = 5;
    switchBtn.layer.masksToBounds = YES;
    switchBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    switchBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    switchBtn.layer.borderWidth = 1;
    [switchBtn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
    [switchBtn setTitleColor:[UIColor whiteColor] forState:0];
    [navigationView addSubview:switchBtn];
    
    [self.view addSubview:navigationView];
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"TStudentCell" bundle:nil] forCellReuseIdentifier:@"studentCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
    [tabView addHeaderWithTarget:self action:@selector(pullStudent)];
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
}
- (void)callAction:(NSNumber *)userId{
    [MBProgressHUD showHUDAndMessage:@"叫号中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/call?studentId=%@",LocalIP,self.skillModel.skillID,self.selectStationId,userId];
    NSLog(@"call=%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        
        if ([result[@"nextStudent"] isKindOfClass:[NSNull class]]) {
            [MBProgressHUD showToastAndMessage:@"该学生已评分或弃考,请重新刷新!" places:0 toView:nil];
            return ;
        }
        if (![result[@"errorCode"] isKindOfClass:[NSNull class]]) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"已经存在考生,是否为存在考生评分?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag = 400;
            [alertView show];
            return ;
        }
        
        CallModel *callModel = [CallModel new];
        NSDictionary *dic = result[@"responseBody"];
        callModel.callModelStationId = dic[@"selectedId"];
        callModel.callModelStationName = dic[@"selectedStationName"];
        callModel.callModelRoomId = dic[@"roomId"];
        callModel.callModelRoomName = dic[@"roomName"];
        callModel.callModelTeacherId = dic[@"teacherIds"];
        callModel.callModelType = dic[@"mode"];
        callModel.selectOsceStationNums = dic[@"selectedCandidateStationsNum"];
        
        NSArray *teacherArray = dic[@"teachers"];
        NSMutableString *teachers = [[NSMutableString alloc]init];

        for (NSDictionary *teacherObj in teacherArray) {
            [teachers appendString:[NSString stringWithFormat:@"%@  ",teacherObj[@"fullName"]]];
        }
        callModel.callModelTeacher = teachers;
        
        callModel.callModelUserId = dic[@"nextStudent"][@"userId"];
        callModel.callModelUserName = dic[@"nextStudent"][@"fullName"];
        callModel.callModelUserNo = dic[@"nextStudent"][@"userNo"];
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.ContentVC = [ContentController new];
        VCManeger.ContentVC.skillModel = self.skillModel;
        VCManeger.ContentVC.callModel = callModel;
        VCManeger.ContentVC.callType = [NSNumber numberWithInt:0];
        [self.navigationController pushViewController:VCManeger.ContentVC animated:YES];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"叫号失败,请重试!" places:0 toView:nil];
    }];
}
- (void)autoCall{
    [MBProgressHUD showHUDAndMessage:@"自动叫号中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/call",LocalIP,self.skillModel.skillID,self.selectStationId];
    NSLog(@"%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        
        if (![result[@"errorCode"] isKindOfClass:[NSNull class]]) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"已经存在考生,是否为存在考生评分?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alertView.tag = 400;
            [alertView show];
            return ;
        }
        CallModel *callModel = [CallModel new];
        NSDictionary *dic = result[@"responseBody"];
        callModel.callModelStationId = dic[@"selectedId"];
        callModel.callModelStationName = dic[@"selectedStationName"];
        callModel.callModelRoomId = dic[@"roomId"];
        callModel.callModelRoomName = dic[@"roomName"];
        callModel.callModelTeacherId = dic[@"teacherIds"];
        callModel.callModelType = dic[@"mode"];
        callModel.selectOsceStationNums = dic[@"selectedCandidateStationsNum"];
        NSArray *teacherArray = dic[@"teachers"];
        NSMutableString *teachers = [[NSMutableString alloc]init];
        NSLog(@"teacherArray=%@",teacherArray);
        for (NSDictionary *teacherObj in teacherArray) {
            [teachers appendString:[NSString stringWithFormat:@"%@  ",teacherObj[@"fullName"]]];
        }
        callModel.callModelTeacher = teachers;
        
        callModel.callModelUserId = dic[@"nextStudent"][@"userId"];
        callModel.callModelUserName = dic[@"nextStudent"][@"fullName"];
        callModel.callModelUserNo = dic[@"nextStudent"][@"userNo"];
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.ContentVC = [ContentController new];
        VCManeger.ContentVC.skillModel = self.skillModel;
        VCManeger.ContentVC.callType = [NSNumber numberWithInt:1];
        VCManeger.ContentVC.callModel = callModel;
        [self.navigationController pushViewController:VCManeger.ContentVC animated:YES];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"叫号失败,请重试!" places:0 toView:nil];
    }];

}
- (void)loadStudent{
    [MBProgressHUD showHUDAndMessage:@"获取考生中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/waitingStudents?pageStart=1&pageSize=999",LocalIP,self.skillModel.skillID,self.selectStationId];
    
    NSLog(@"listStuVC=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [dataArray removeAllObjects];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [tabView reloadData];
            [MBProgressHUD showToastAndMessage:@"当前没有等待考生!" places:0 toView:nil];
            return ;
        }
        
        for (NSDictionary *dic in array) {
            TStudentModel *studentModel = [TStudentModel new];
            studentModel.TStudentModelId = dic[@"userId"];
            studentModel.TStudentModelName = dic[@"fullName"];
            if ([dic[@"picture"] isKindOfClass:[NSNull class]]) {
                studentModel.TStudentModelImage = @"暂无";
            }else{
                studentModel.TStudentModelImage = dic[@"picture"][@"fileUrl"];
            }
            studentModel.TStudentModelXuehao = dic[@"userNo"];
            studentModel.TStudentModelProfessional = dic[@"major"];
            [dataArray addObject:studentModel];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"加载失败,请下拉刷新!" places:0 toView:nil];
    }];
}
- (void)pullStudent{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/waitingStudents",LocalIP,self.skillModel.skillID,self.selectStationId];
    
    NSLog(@"req=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [tabView headerEndRefreshing];
        [dataArray removeAllObjects];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂时没有等待考生!" places:0 toView:nil];
            return ;
        }
        for (NSDictionary *dic in array) {
            TStudentModel *studentModel = [TStudentModel new];
            studentModel.TStudentModelId = dic[@"userId"];
            studentModel.TStudentModelName = dic[@"fullName"];
            if ([dic[@"picture"] isKindOfClass:[NSNull class]]) {
                studentModel.TStudentModelImage = @"暂无";
            }else{
                studentModel.TStudentModelImage = dic[@"picture"][@"fileUrl"];
            }
            studentModel.TStudentModelXuehao = dic[@"userNo"];
            studentModel.TStudentModelProfessional = dic[@"major"];
            [dataArray addObject:studentModel];
        }
        //
        [tabView reloadData];
    } failed:^(NSString *result) {
        [tabView headerEndRefreshing];
    }];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)switchAction:(UIButton *)btn{
    [self autoCall];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TStudentModel *studentModel = dataArray[indexPath.row];
    
    [self callAction:studentModel.TStudentModelId];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TStudentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"studentCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    TStudentModel *studentModel = dataArray[indexPath.row];
    
    cell.TSName.text = studentModel.TStudentModelName;
    cell.TSProfessional.text = studentModel.TStudentModelProfessional;
    if (studentModel.TStudentModelXuehao.integerValue == 0) {
        cell.TSXuehao.text = @"暂无";
    }else{
        cell.TSXuehao.text = [NSString stringWithFormat:@"%@",studentModel.TStudentModelXuehao];
    }
    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 400) {
        if (buttonIndex == 1) {
            [self markCurrent];
        }
    }
}
- (void)markCurrent{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@",LocalIP,self.skillModel.skillID,self.selectStationId];
    
    NSLog(@"current=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取中" Success:^(NSDictionary *result) {
        CallModel *callModel = [CallModel new];
        NSDictionary *dic = result[@"responseBody"];
        
        callModel.callModelStationId = dic[@"selectedId"];
        callModel.callModelStationName = dic[@"selectedStationName"];
        callModel.callModelRoomId = dic[@"roomId"];
        callModel.callModelRoomName = dic[@"roomName"];
        callModel.callModelTeacherId = dic[@"teacherIds"];
        callModel.callModelType = dic[@"mode"];
        callModel.selectOsceStationNums = dic[@"selectedCandidateStationsNum"];
        NSArray *teacherArray = dic[@"teachers"];
        NSMutableString *teachers = [[NSMutableString alloc]init];
    
        for (NSDictionary *teacherObj in teacherArray) {
            [teachers appendString:[NSString stringWithFormat:@"%@  ",teacherObj[@"fullName"]]];
        }
        callModel.callModelTeacher = teachers;
        if ([dic[@"currentStudent"] isKindOfClass:[NSNull class]]) {
            callModel.callModelUserId = dic[@"nextStudent"][@"userId"];
            callModel.callModelUserName = dic[@"nextStudent"][@"fullName"];
            callModel.callModelUserNo = dic[@"nextStudent"][@"userNo"];
        }else{
            callModel.callModelUserId = dic[@"currentStudent"][@"userId"];
            callModel.callModelUserName = dic[@"currentStudent"][@"fullName"];
            callModel.callModelUserNo = dic[@"currentStudent"][@"userNo"];
        }
        
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.ContentVC = [ContentController new];
        VCManeger.ContentVC.skillModel = self.skillModel;
        VCManeger.ContentVC.callType = [NSNumber numberWithInt:1];
        VCManeger.ContentVC.callModel = callModel;
        VCManeger.ContentVC.callType = [NSNumber numberWithInt:0];  
        [self.navigationController pushViewController:VCManeger.ContentVC animated:YES];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"获取失败,请重试!" places:0 toView:nil];
    }];
}
@end
