//
//  AccountCell1.h
//  yun
//
//  Created by MacAir on 2017/8/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@end
