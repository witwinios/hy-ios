//
//  AwardDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AwardDetailController.h"

@interface AwardDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation AwardDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"获奖记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";

    if ([self.model.caseAwardModelStatus isEqualToString:@"待审核"]) {
        AwardDetailController *weakSelf = self;
        self.rightBtn.hidden = NO;
        self.rightBlock = ^(){
            RewardRecordController *rewardVC = [RewardRecordController new];
            rewardVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:rewardVC animated:YES];
        };
    }

    titleArray = @[@"审核状态:",@"获奖名称:",@"获奖级别:",@"获奖时间:",@"带教老师:",@"说明:",@"指导意见:",@"附件:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    self.dataArray = @[_model.caseAwardModelStatus,_model.caseAwardModelTitle,_model.caseAwardModelDegree,[[Maneger shareObject]timeFormatter1:_model.caseAwardModelDate.stringValue],_model.caseAwardModelTeacher,_model.caseAwardModelDescroption,_model.caseAwardModelAdvice,_model.fileUrl];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 7) {
        return three;
    }else if (indexPath.row == 6 || indexPath.row == 5){
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else{
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
    return one;
    
}
@end
