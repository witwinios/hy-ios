//
//  OperateRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OperateRecordController.h"
#import "THDatePickerView.h"
@interface OperateRecordController ()
{
    NSString *current_date;
    UIView *btnBack;
    NSNumber *has_image;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;
@end

@implementation OperateRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    has_image = @0;
    current_date = @"";
    
    NSLog(@"myModel=%@",self.myModel);
    if (![self.myModel isKindOfClass:[NSNull class]] || self.model != nil) {
        self.oneField.text = self.myModel.caseResult;
        self.twoField.text = self.myModel.casePatientName;
        self.threeField.text = [NSString stringWithFormat:@"%@",self.myModel.caseNo];
        self.reasonView.text = self.myModel.caseReason;
        self.descriptionView.text = self.myModel.caseDescription;
        if (self.myModel.caseDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter:self.myModel.caseDate.stringValue] forState:0];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardDidShowNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [self.dateBtn setTitle:date forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"操作记录";
    [backNavigation addSubview:titleLab];
    //style
    self.oneField.layer.cornerRadius = 5;
    self.oneField.layer.masksToBounds = YES;
    self.oneField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.oneField.layer.borderWidth = 1;
    self.twoField.layer.cornerRadius = 5;
    self.twoField.layer.masksToBounds = YES;
    self.twoField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.twoField.layer.borderWidth = 1;
    self.threeField.layer.cornerRadius = 5;
    self.threeField.layer.masksToBounds = YES;
    self.threeField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.threeField.layer.borderWidth = 1;
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.threeField resignFirstResponder];
    [self.reasonView resignFirstResponder];
    [self.descriptionView resignFirstResponder];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doneButtonshow: (NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    CGFloat keyBoardHeight = keyboardRect.size.height;
    
    if (btnBack == nil) {
        btnBack = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight-keyBoardHeight-35, Swidth, 35)];
    }
    btnBack.backgroundColor = UIColorFromHex(0xDBDBDB);
    UIButton *doneButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(Swidth-70, 0, 70, 35);
    [doneButton setTitle:@"完成" forState: UIControlStateNormal];
    [doneButton addTarget:self action:@selector(hideKeyboard:) forControlEvents: UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor blueColor] forState:0];
    [btnBack addSubview:doneButton];
    [self.view addSubview:btnBack];
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
}
- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结果不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.reasonView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.descriptionView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"描述不能为空~" places:0 toView:nil];
        return;
    }
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
        NSLog(@"request=%@",requestUrl);
        NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":_oneField.text,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"requirementId":self.myModel.requestmentId,@"fileId":@""};
        NSLog(@"params=%@",params);
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            self.myModel = nil;
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            has_image = @0;
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords/%@",LocalIP,departModel.SDepartRecordId,self.myModel.recordId];
            NSLog(@"request=%@",requestUrl);
            NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":_oneField.text,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"requirementId":self.myModel.requestmentId,@"fileId":fileId};
            NSLog(@"params=%@",params);
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                self.myModel = nil;
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
                    }];
                }else{
                    [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结果不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.reasonView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.descriptionView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"描述不能为空~" places:0 toView:nil];
        return;
    }
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = departVC.departModel;
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords",LocalIP,departModel.SDepartRecordId];
        NSLog(@"request=%@",requestUrl);
        NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":_oneField.text,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"requirementId":self.model.CaseReuestModelRequirementId,@"fileId":@""};
        NSLog(@"params=%@",params);
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords/%@/operationRecords",LocalIP,departModel.SDepartRecordId];
            NSLog(@"request=%@",requestUrl);
            NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":_oneField.text,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"requirementId":self.model.CaseReuestModelRequirementId,@"fileId":fileId};
            NSLog(@"params=%@",params);
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (IBAction)saveAction:(id)sender {
    if ([self.myModel isKindOfClass:[NSNull class]] || self.myModel == nil) {
        [self saveAction];
    }else{
        [self updateAction];
    }

}

#pragma -action


#pragma UIImagePickerControllerDelegate
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _currentImage.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

@end
