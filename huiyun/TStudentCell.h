//
//  TStudentCell.h
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TStudentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *TSImage;
@property (weak, nonatomic) IBOutlet UILabel *TSName;
@property (weak, nonatomic) IBOutlet UILabel *TSProfessional;
@property (weak, nonatomic) IBOutlet UILabel *TSXuehao;

@end
