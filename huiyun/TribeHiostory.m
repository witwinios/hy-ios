//
//  TribeHiostory.m
//  yun
//
//  Created by MacAir on 2017/6/6.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TribeHiostory.h"

@interface TribeHiostory ()
{
    NSDictionary *currentTribe;
    NSMutableArray *dataArray;
    NSMutableArray *heightArray;
}
@end

@implementation TribeHiostory

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self downRefresh];
}
- (void)setUI{
    dataArray = [NSMutableArray new];
    
    [self.rightitle setTitle:@"问题" forState:0];
    self.titleLab.text = @"聊天记录";
    __weak typeof(self) weakSelf = self;
    self.leftAction = ^(){
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };

    __block NSDictionary *tribeDic = currentTribe;
    self.rightAction = ^(){
        UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:@"问题描述" message:[tribeDic objectForKey:@"questionDescription"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *canCel = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alerVC addAction:canCel];
        [weakSelf presentViewController:alerVC animated:YES completion:nil];
    };
    self.tabView.delegate = self;
    self.tabView.dataSource = self;
   
    [self.tabView addHeaderWithTarget:self action:@selector(downRefresh)];
    [self.tabView addFooterWithTarget:self action:@selector(loadRefresh)];
    [self.tabView registerNib:[UINib nibWithNibName:@"TribeHistoryCellStyleLeft" bundle:nil] forCellReuseIdentifier:@"messageCell"];
    //设置文字
    self.tabView.headerPullToRefreshText = @"下拉刷新";
    self.tabView.headerReleaseToRefreshText = @"松开进行刷新";
    self.tabView.headerRefreshingText = @"刷新中。。。";
    self.tabView.footerPullToRefreshText = @"上拉加载";
    self.tabView.footerReleaseToRefreshText = @"松开进行加载";
    self.tabView.footerRefreshingText = @"加载中。。。";
}
//下拉刷新
- (void)downRefresh{
    NSString *requestURL = [NSString stringWithFormat:@"%@/QAGroup?createdBy=%@&status=SOLVED",LocalIP,LocalUserId];
    [RequestTools RequestWithURL:requestURL Method:@"get" Params:nil Message:@"加载中..." Success:^(NSDictionary *result) {
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        for (int i=0; i<array.count; i++) {
            NSDictionary *responseDic = array[i];
            if ([responseDic objectForKey:@"tribeId"] == self.model.tribeId) {
                currentTribe = responseDic;
                NSString *message = [responseDic objectForKey:@"messages"];
                NSDictionary *dictonary = [Maneger parseJSONStringToNSDictionary:message];
                NSArray *messageArray = [[[[dictonary objectForKey:@"openim_tribelogs_get_response"] objectForKey:@"data"] objectForKey:@"messages"] objectForKey:@"tribe_message"];
                for (int s=0; s<messageArray.count; s++) {
                    NSDictionary *messageDic = messageArray[s];
                    NSArray *contentArray = [[messageDic objectForKey:@"content"] objectForKey:@"message_item"];
                    for (int k=0; k<contentArray.count; k++) {
                        NSDictionary *contentDic = contentArray[k];
                        MessageModel *msgModel = [MessageModel new];
                        msgModel.time = [messageDic objectForKey:@"time"];
                        msgModel.name = [[messageDic objectForKey:@"from_id"] objectForKey:@"uid"];
                        msgModel.meesage = [contentDic objectForKey:@"value"];
                        [dataArray addObject:msgModel];
                    }
                }
                break;
            }
        }
        [self.tabView reloadData];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"加载失败!" places:0 toView:nil];
    }];
}
//上拉加载
- (void)loadRefresh{
    NSString *requestURL = [NSString stringWithFormat:@"%@/QAGroup?createdBy=%@&status=SOLVED",LocalIP,LocalUserId];
    
    [RequestTools RequestWithURL:requestURL Method:@"get" Params:nil Message:@"加载中..." Success:^(NSDictionary *result) {
        
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"加载失败!" places:0 toView:nil];
    }];

}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:@"BaseviewController" bundle:nibBundleOrNil]) {
    }
    return self;
}
#pragma -delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, Swidth, 10);
    view.backgroundColor = [UIColor lightGrayColor];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 49.5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TribeHistoryCellStyleLeft *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell"];
    MessageModel *msgModel = dataArray[indexPath.section];
    cell.userName.text = msgModel.name;
    CGFloat time = msgModel.time.floatValue * 1000;
    cell.messageTime.text = [[Maneger shareObject]timeFormatter:[NSString stringWithFormat:@"%f",time]];
    cell.messageContent.text = [NSString stringWithFormat:@"    %@",msgModel.meesage];
 
    return cell;
}
@end
