//
//  TMarkController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TMarkController.h"

//typedef NS_ENUM(NSInteger, ExamType) {
//     examTypeOsce = 0,
//     examTypeOnline,
//     examTypeSkill
//};

@interface TMarkController ()
{
    UISearchBar *searchBar;
    UITableView *tabView;
    UISegmentedControl *segController;
    
    NSMutableArray *skillArray;
    
    //
    UIView *backView;
    UILabel *timeLabel;
    SkillModel *currentModel;
    UILabel *startLabel;
}
@end

@implementation TMarkController
- (void)viewDidLoad {
    [super viewDidLoad];
    skillArray = [NSMutableArray new];

    [self loadExam];
    [self setUI];
}
//获取数据
- (void)loadExam{
    [MBProgressHUD showHUDAndMessage:@"加载中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/listSkillTestsAPP?testTimeFrom=%@&testTimeTo=%@&teacherId=%@&pageStart=1&pageSize=999",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId];
    
    NSLog(@"markVC=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [skillArray removeAllObjects];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂时没有安排!" places:0 toView:nil];
            return ;
        }
        for (NSDictionary *dic in array) {
            SkillModel *model = [SkillModel new];
            model.skillID = dic[@"testId"];
            model.skillStatus = dic[@"testStatus"];
            model.skillName = dic[@"testName"];
            model.skillCategory = dic[@"categoryName"];
            model.skillSubject = dic[@"subjectName"];
            model.skillStartTime = dic[@"startTime"];
            model.skillDes = dic[@"description"];
            model.skillStationNums = dic[@"selectedStationsNum"];
            model.timeBlock = dic[@"testTimeBlocksNum"];
            [skillArray addObject:model];
        }
        [tabView reloadData];
        }failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"加载失败,请下拉刷新!" places:0 toView:nil];
    }];
}
- (void)pullRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/listSkillTestsAPP?testTimeFrom=%@&testTimeTo=%@&teacherId=%@&pageStart=1&pageSize=999",LocalIP,[[Maneger shareObject] zeroTime],[[Maneger shareObject] tomorrowTime],LocalUserId];
    
    NSLog(@"markVC-pull=%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [skillArray removeAllObjects];
        [tabView headerEndRefreshing];
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂时没有安排!" places:0 toView:nil];
            return ;
        }
        for (NSDictionary *dic in array) {
            SkillModel *model = [SkillModel new];
            model.skillID = dic[@"testId"];
            model.skillStatus = dic[@"testStatus"];
            model.skillName = dic[@"testName"];
            model.skillCategory = dic[@"categoryName"];
            model.skillSubject = dic[@"subjectName"];
            model.skillStartTime = dic[@"startTime"];
            model.skillDes = dic[@"description"];
            model.skillStationNums = dic[@"selectedStationsNum"];
            model.timeBlock = dic[@"testTimeBlocksNum"];
            [skillArray addObject:model];
        }
        [tabView reloadData];
    }failed:^(NSString *result) {
        [tabView headerEndRefreshing];
    }];
}
- (void)showBack{
    startLabel.text = [[Maneger shareObject]timeFormatter:currentModel.skillStartTime.stringValue];
    float internal = currentModel.skillStartTime.floatValue/1000 - [[[Maneger shareObject] currentLong] floatValue]/1000;
    int internalS = (int)internal;
    timeLabel.text = [NSString stringWithFormat:@"%.2d 小时 %.2d 分 %.2d 秒",internalS/60/60,internalS/60%60,internalS%60];
    
    __block int inter = internalS;
    
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSString *timerStr = [NSString stringWithFormat:@"%.2d 小时 %.2d 分 %.2d 秒",inter/60/60,inter/60%60,inter%60];
        timeLabel.text = timerStr;
        inter = inter-1 ;
        if (inter/60/60 == 0 && inter/60%60==0 && inter%60==0) {
            [timer invalidate];
            [self findStatus];
        }
    }];
    
    backView.frame = CGRectMake(0, 0, Swidth, Sheight);
}
- (void)hidBack{
    backView.frame = CGRectMake(0, -Sheight, Swidth, Sheight);
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"技能考试";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    [self.view addSubview:navigationView];
    //搜索框🔍
    searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 64, Swidth, 45)];
    searchBar.placeholder = @"请输入考试名称";
    [self.view addSubview:searchBar];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 109, Swidth, Sheight-109) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"OsceCell" bundle:nil] forCellReuseIdentifier:@"examCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
    [tabView addHeaderWithTarget:self action:@selector(pullRefresh)];
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
    //
    //背景层
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, -Sheight, Swidth, Sheight)];
    backView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 150, Swidth, 80)];
    label.text = @"考试暂时没有开始!";
    label.textAlignment = 1;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:25];
    [backView addSubview:label];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(5, 20, 60, 30);
    cancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    cancelBtn.layer.borderWidth = 1;
    cancelBtn.layer.cornerRadius = 5;
    cancelBtn.layer.masksToBounds = YES;
    [cancelBtn setTitle:@"关闭" forState:0];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
    [cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:cancelBtn];
    
    UILabel *startLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    startLab.textColor = [UIColor whiteColor];
    startLab.text = @"开始时间:";
    startLab.font = [UIFont systemFontOfSize:14];
    startLab.center = CGPointMake(Swidth/2-40, Sheight/2-20);
    [backView addSubview:startLab];
    
    startLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, Sheight/2-40, 100, 40)];
    startLabel.font = [UIFont systemFontOfSize:14];
    startLabel.textColor = [UIColor whiteColor];
    startLabel.adjustsFontSizeToFitWidth = YES;
    [backView addSubview:startLabel];
    
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    timeLab.text = @"倒计时:";
    timeLab.textColor = [UIColor whiteColor];
    timeLab.font = [UIFont systemFontOfSize:14];
    timeLab.center = CGPointMake(Swidth/2-40, Sheight/2+20);
    [backView addSubview:timeLab];
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, Sheight/2, Swidth/2, 40)];
    timeLabel.adjustsFontSizeToFitWidth = YES;
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont systemFontOfSize:14];
    [backView addSubview:timeLabel];
    
    [self.view addSubview:backView];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)cancelView{
    [self hidBack];
}
//查询当场考试
- (void)findStatus{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@",LocalIP,currentModel.skillID];
    NSLog(@"%@",requestUrl);
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"查询中" Success:^(NSDictionary *result) {
        NSLog(@"status=%@",result[@"responseBody"][@"testStatus"]);
        if ([result[@"responseBody"][@"testStatus"] isEqualToString:@"STARTED"] || [result[@"responseBody"][@"testStatus"] isEqualToString:@"started"]) {
            ControlManeger *VCManeger = [ControlManeger share];
            VCManeger.TClassVC = [TClassController new];
            VCManeger.TClassVC.skillModel = currentModel;
            [self.navigationController pushViewController:VCManeger.TClassVC animated:YES];
        }else{
            if (currentModel.skillStartTime.longLongValue > [[[Maneger shareObject]currentLong] longLongValue]) {
                [self showBack];
            }else{
                [MBProgressHUD showToastAndMessage:@"请联系管理员开始考试,考试时间已到!" places:0 toView:nil];
            }
        }
    } failed:^(NSString *result) {
        
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return skillArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //
    currentModel = skillArray[indexPath.row];
    [self findStatus];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OsceCell *cell = [tabView dequeueReusableCellWithIdentifier:@"examCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell set:skillArray[indexPath.row]];
    return cell;
}
@end
