//
//  TestController.h
//  huiyun
//
//  Created by MacAir on 2017/11/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaperCellModel.h"
#import "QuestionModel.h"

@interface TestController : UIViewController
@property (strong, nonatomic) PaperCellModel *paperModel;
@property (strong, nonatomic) SelfTestModel *testModel;
@end
