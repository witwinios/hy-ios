//
//  ContentController.h
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//  身份确认界面

#import <UIKit/UIKit.h>
#import "CallModel.h"
#import "SkillModel.h"
#import "ExamPointController.h"
@interface ContentController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *roomName;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *xuehao;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *stationName;
@property (weak, nonatomic) IBOutlet UIView *infomationView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *givenUp;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (strong, nonatomic) NSNumber *callType;
@property (strong, nonatomic) CallModel *callModel;
@property (strong, nonatomic) SkillModel *skillModel;
@end
