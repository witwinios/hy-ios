//
//  DetailRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DetailRecordController.h"

@interface DetailRecordController ()
{
    UILabel *statusLab;
    UILabel *patientNameLab;
    UILabel *caseNoLab;
    UILabel *caseDateLab;
    UILabel *caseTeacherLab;
    UITextView *mainText;
    UITextView *secondText;
    UITextView *adviceText;
}
@end

@implementation DetailRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if ([self.model.caseTwo isEqualToString:@"待审核"]) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"修改" forState:0];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    }
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"操作记录";
    [backNavigation addSubview:titleLab];
    //
    UIScrollView *scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    //
    UILabel *status = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 80, 20)];
    status.text = @"审核状态:";
    status.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:status];
    statusLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 5, Swidth-95, 20)];
    statusLab.font = [UIFont systemFontOfSize:17];
    statusLab.text = _model.caseTwo;
    [scrollView addSubview:statusLab];
    //
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 80, 20)];
    name.font = [UIFont systemFontOfSize:17];
    name.text = @"病人姓名";
    [scrollView addSubview:name];
    patientNameLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 30, Swidth-95, 20)];
    patientNameLab.font = [UIFont systemFontOfSize:17];
    patientNameLab.text = _model.caseOne;
    [scrollView addSubview:patientNameLab];
    //
    UILabel *caseNo = [[UILabel alloc]initWithFrame:CGRectMake(5, 55, 80, 20)];
    caseNo.text = @"病案号:";
    caseNo.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseNo];
    caseNoLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 55, Swidth-95, 20)];
    caseNoLab.text = _model.caseThree;
    caseNoLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseNoLab];
    //
    UILabel *dates = [[UILabel alloc]initWithFrame:CGRectMake(5, 80, 80, 20)];
    dates.font = [UIFont systemFontOfSize:17];
    dates.text = @"病例时间:";
    [scrollView addSubview:dates];
    caseDateLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 80, Swidth-95, 20)];
    caseDateLab.text = [[Maneger shareObject] timeFormatter1:_model.caseFive.stringValue];
    caseDateLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseDateLab];
    //
    UILabel *teacher = [[UILabel alloc]initWithFrame:CGRectMake(5, 105, 80, 20)];
    teacher.font = [UIFont systemFontOfSize:17];
    teacher.text = @"带教老师:";
    [scrollView addSubview:teacher];
    caseTeacherLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 105, Swidth-95, 20)];
    caseTeacherLab.text = _model.caseFour;
    caseTeacherLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseTeacherLab];
    //
    UILabel *main = [[UILabel alloc]initWithFrame:CGRectMake(5, 130, 80, 20)];
    main.text = @"主要诊断:";
    main.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:main];
    //
    mainText = [[UITextView alloc]initWithFrame:CGRectMake(5, 155, Swidth-10, 60)];
    mainText.layer.borderColor = [UIColor grayColor].CGColor;
    mainText.layer.borderWidth = 1;
    mainText.font = [UIFont systemFontOfSize:15];
    mainText.text =[NSString stringWithFormat:@"    %@",_model.mainIos];
    [scrollView addSubview:mainText];
    //
    UILabel *second = [[UILabel alloc]initWithFrame:CGRectMake(5, 220, 80, 20)];
    second.text = @"次要诊断";
    second.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:second];
    secondText = [[UITextView alloc]initWithFrame:CGRectMake(5, 245, Swidth-10, 60)];
    secondText.layer.borderColor = [UIColor grayColor].CGColor;
    secondText.layer.borderWidth = 1;
    secondText.font = [UIFont systemFontOfSize:15];
    secondText.text = [NSString stringWithFormat:@"    %@",_model.secondIos];
    [scrollView addSubview:secondText];
    //
    //
    UILabel *advice = [[UILabel alloc]initWithFrame:CGRectMake(5, 315, 80, 20)];
    advice.text = @"审核意见:";
    advice.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:advice];
    adviceText = [[UITextView alloc]initWithFrame:CGRectMake(5, 360, Swidth-10, 60)];
    adviceText.layer.borderColor = [UIColor grayColor].CGColor;
    adviceText.layer.borderWidth = 1;
    adviceText.text = _model.advice;
    adviceText.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:adviceText];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)choose:(UIButton *)btn{
    btn.selected = !btn.selected;
    UIButton *agree = [self.view viewWithTag:100];
    UIButton *disagree = [self.view viewWithTag:101];
    if (btn.selected) {
        if (btn == agree) {
            disagree.selected = NO;
        }else{
            agree.selected = NO;
        }
    }
}
//修改
- (void)selectAction:(UIButton *)btn{
    SCaseRecordController *recordVC = [SCaseRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}
@end
