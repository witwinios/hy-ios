//
//  onlineModel.m
//  xiaoyun
//
//  Created by MacAir on 17/1/3.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "onlineModel.h"

@implementation onlineModel


//
- (void)setOnlineTime:(NSNumber *)onlineTime{
    if ([onlineTime isKindOfClass:[NSNull class]]) {
        _onlineTime = [NSNumber numberWithInt:1];
    }else{
        _onlineTime = onlineTime;
    }
}
- (void)setOnlineInternal:(NSNumber *)onlineInternal{
    if ([onlineInternal isKindOfClass:[NSNull class]]) {
        _onlineInternal = [NSNumber numberWithInt:1];
    }else{
        _onlineInternal = onlineInternal;
    }
}
- (void)setOnlineName:(NSString *)onlineName{
    if ([onlineName isKindOfClass:[NSNull class]]) {
        _onlineName = @"暂无";
    }else{
        _onlineName = onlineName;
    }
}
- (void)setOnlineRoom:(NSString *)onlineRoom{
    if ([onlineRoom isKindOfClass:[NSNull class]]) {
        _onlineRoom = @"暂无";
    }else{
        _onlineRoom = onlineRoom;
    }
}
- (void)setDifficulty:(NSNumber *)difficulty{
    if ([difficulty isKindOfClass:[NSNull class]]) {
        _difficulty = [NSNumber numberWithInt:-1];
    }else{
        _difficulty = difficulty;
    }
}
- (void)setCreateTime:(NSNumber *)createTime{
    if ([createTime isKindOfClass:[NSNull class]]) {
        _createTime = 0;
    }else{
        _createTime = createTime;
    }
}
- (void)setCreateName:(NSString *)createName{
    if ([createName isKindOfClass:[NSNull class]]) {
        _createName = @"暂无";
    }else{
        _createName = createName;
    }
}
//    planning
//    started
//    completed
//    released
//    preordered
- (void)setOnlinePages:(NSNumber *)onlinePages{
    if ([onlinePages isKindOfClass:[NSNull class]]) {
        _onlinePages = [NSNumber numberWithInt:0];
    }else{
        _onlinePages = onlinePages;
    }
}
- (void)setOnlineDes:(NSString *)onlineDes{
    if ([onlineDes isKindOfClass:[NSNull class]]) {
        _onlineDes = @"无简介";
    }else{
        _onlineDes = onlineDes;
    }
}
- (void)setTimeBlock:(NSNumber *)timeBlock{
    if ([timeBlock isKindOfClass:[NSNull class]]) {
        _timeBlock = [NSNumber numberWithInt:0];
    }else{
        _timeBlock = timeBlock;
    }
}
- (void)setOnlineStatus:(NSString *)onlineStatus{
    if ([onlineStatus isEqualToString:@"planning"]) {
        _onlineStatus = @"计划中";
    }else if ([onlineStatus isEqualToString:@"started"]){
        _onlineStatus = @"已开始";
    }else if ([onlineStatus isEqualToString:@"completed"]){
        _onlineStatus = @"已完成";
    }else if ([onlineStatus isEqualToString:@"released"]){
        _onlineStatus = @"已发布";
    }else if ([onlineStatus isEqualToString:@"preordered"]){
        _onlineStatus = @"预排";
    }else if ([onlineStatus isEqualToString:@"arranged"]){
        _onlineStatus = @"已安排";
    }else if ([onlineStatus isEqualToString:@"reviewed"]){
        _onlineStatus = @"已评测";
    }else if ([onlineStatus isEqualToString:@"marked"]){
        _onlineStatus = @"评卷中";
    }
}
#pragma -mark
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.onlineInternal forKey:@"onlineInternal"];
    [aCoder encodeObject:self.onlineName forKey:@"onlineName"];
    [aCoder encodeObject:self.onlineRoom forKey:@"onlineRoom"];
    [aCoder encodeObject:self.onlineTime forKey:@"onlineTime"];
    [aCoder encodeObject:self.difficulty forKey:@"difficulty"];
    [aCoder encodeObject:self.createTime forKey:@"createTime"];
    [aCoder encodeObject:self.createName forKey:@"createName"];
    [aCoder encodeObject:self.type forKey:@"type"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.onlineTime = [aDecoder decodeObjectForKey:@"onlineTime"];
        self.onlineRoom = [aDecoder decodeObjectForKey:@"onlineRoom"];
        self.onlineName = [aDecoder decodeObjectForKey:@"onlineName"];
        self.onlineInternal = [aDecoder decodeObjectForKey:@"onlineInternal"];
        self.difficulty = [aDecoder decodeObjectForKey:@"difficulty"];
        self.createTime = [aDecoder decodeObjectForKey:@"createTime"];
        self.createName = [aDecoder decodeObjectForKey:@"createName"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
    }
    return self;
}
@end
