//
//  CourseDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
#import "TCourseDatailCell.h"
#import "DesCell.h"
#import "BeforeCourseController.h"
@interface TCourseDetailController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) TCourseModel *model;
@property (strong, nonatomic) BeforeCourseController *beforeVC;
@end
