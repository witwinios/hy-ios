//
//  RecentCell.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RecentOSCECell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *recentName;
@property (weak, nonatomic) IBOutlet UILabel *recentStation;
@property (weak, nonatomic) IBOutlet UILabel *recentExamName;
@property (weak, nonatomic) IBOutlet UILabel *recentTime;
@property (weak, nonatomic) IBOutlet UILabel *testType;
@end
