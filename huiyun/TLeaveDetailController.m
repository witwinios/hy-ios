//
//  TLeaveDetailController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TLeaveDetailController.h"

@interface TLeaveDetailController ()

@end

@implementation TLeaveDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    self.naviView.backgroundColor = UIColorFromHex(0x20B2AA);
    self.leaveReason.layer.borderWidth =1;
    self.leaveReason.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.leaveReason.editable=NO;
    
    self.leaveStatus.layer.cornerRadius = 5;
    self.leaveStatus.layer.masksToBounds = YES;
    //设置颜色
    if ([self.model.approveStatus isEqualToString:@"待审批"]) {
        self.leaveStatus.backgroundColor = [UIColor orangeColor];
    }else if ([self.model.approveStatus isEqualToString:@"同意"]){
        self.leaveStatus.backgroundColor = [UIColor greenColor];
    }else{
        self.leaveStatus.backgroundColor = [UIColor redColor];
    }
    
    self.imageBtn.layer.cornerRadius = 5;
    self.imageBtn.layer.masksToBounds = YES;
    
    if (![_model.approveStatus isEqualToString:@"待审批"]) {
        _agreeBtn.alpha = 0;
        _disagreeBtn.alpha = 0;
    }
    
    self.leaveName.text = self.model.requestName;
    self.leaveStatus.text = self.model.approveStatus;
    self.leaveTime.text = [[Maneger shareObject] timeFormatter:self.model.leaveStartTime.stringValue];
    self.leaveType.text = self.model.leaveType;
    self.leaveReason.text = self.model.leaveReason;
    if ([self.model.fileUrl isEqualToString:@""]) {
        [self.imageBtn setTitle:@"暂无" forState:UIControlStateNormal];
    }else{
        [self.imageBtn addTarget:self action:@selector(imageAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}
- (void)changeLeaveActionWithStyle:(NSInteger)style andDes:(NSString *)des{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/attendance/leaveRequests/%@/review",LocalIP,self.model.leaveRequestId];
    
    if (style == 0) {
        NSDictionary *params = @{@"reviewerComments":des,@"approvalStatus":@"approved",@"reviewerId":[NSString stringWithFormat:@"%@",LocalUserId]};
        //同意请求
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Message:@"审批中..." Success:^(NSDictionary *result) {
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"已批准!" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"审批失败!" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
        }];
    }else{
        NSDictionary *params = @{@"reviewerComments":des,@"approvalStatus":@"rejected",@"reviewerId":[NSString stringWithFormat:@"%@",LocalUserId]};
        //拒绝请求
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Message:@"审批中..." Success:^(NSDictionary *result) {
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"已拒绝!" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                [MBProgressHUD showToastAndMessage:@"审批失败!" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
        }];
        
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma -action
- (void)imageAction:(UIButton *)btn{
    NSString *imgStr = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,self.model.fileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    NSString *imageStr =  [imgStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",imgStr);
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    UIColor *color = [UIColor lightGrayColor];
    view.backgroundColor = [color colorWithAlphaComponent:0.5];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, Swidth/2, Sheight/2)];
    imgView.layer.cornerRadius = 5;
    imgView.layer.masksToBounds = YES;
    imgView.center = view.center;
    [view addSubview:imgView];
    __block UIProgressView *pv;
    __weak UIImageView *weakImageView = imgView;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imageStr]
               placeholderImage:[UIImage imageNamed:@"holdimage"]
                        options:SDWebImageCacheMemoryOnly
                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                           if (!pv) {
                               [weakImageView addSubview:pv = [UIProgressView.alloc initWithProgressViewStyle:UIProgressViewStyleDefault]];
                               pv.frame = CGRectMake(0, 0, Swidth/2, 40);
                           }
                           CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 8.0f);
                           pv.transform = transform;
                           float showProgress = (float)receivedSize/(float)expectedSize;
                           NSLog(@"进度:%f",showProgress);
                           [pv setProgress:showProgress];
                       }
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          [pv removeFromSuperview];
                          pv = nil;
                      }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeAction:)];
    tap.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tap];
    
    [self.view addSubview:view];
}
- (void)removeAction:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    [view removeFromSuperview];
}
- (IBAction)agreeAction:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"审批意见" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"请输入审批意见";
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *leaveDes = alertController.textFields.firstObject;
        [self changeLeaveActionWithStyle:0 andDes:leaveDes.text];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)disagreeAction:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"审批意见" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"请输入审批意见";
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *leaveDes = alertController.textFields.firstObject;
        [self changeLeaveActionWithStyle:1 andDes:leaveDes.text];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

