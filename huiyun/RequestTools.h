//
//  RequestTools.h
//  xiaoyun
//
//  Created by MacAir on 2017/3/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Reachability/Reachability.h>
#import "Reachability.h"
typedef void (^MyBasicBlock)(id result);
typedef void (^FaildBlock) (NSString *result);
typedef void (^SucessBlock) (NSDictionary *result);
typedef void (^ProgressBlock) (NSProgress *progress);
@interface RequestTools : NSObject
@property (nonatomic, copy) MyBasicBlock responseBlock;
@property (nonatomic, copy) MyBasicBlock errorBlock;

+ (void)autoLogin:(NSDictionary *)dic Success:(SucessBlock)success failed:(FaildBlock)faild;
/**
 创建单例
 */

+ (id)shareInstance;
/**
 带加载框的请求
 Params: 
    url:请求URL
 method:请求方式
 params:请求参数
 message:加载框提示信息
 success:成功回调Block
 faild:失败回调Block
 */
+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Message:(NSString *)message Success:(SucessBlock)success failed:(FaildBlock)faild;
/**
 普通请求
 Params:
 url:请求URL
 method:请求方式
 params:请求参数
 success:成功回调Block
 faild:失败回调Block
 */
+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success failed:(FaildBlock)faild;

+ (void)RequestWithURL:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success;
+ (void)RequestWithFile:(UIImage *)image andParams:(NSDictionary *)dic andUrl:(NSString *)Url Success:(SucessBlock)success failed:(FaildBlock)faild;

+ (void)RequestFile:(UIImage *)image andParams:(NSDictionary *)dic andUrl:(NSString *)Url Progress:(ProgressBlock)progress Success:(SucessBlock)success failed:(FaildBlock)faild;
+ (void)simpleRequestUrl:(NSString *)url Method:(NSString *)method Params:(id)params Success:(SucessBlock)success;

- (void)getRequest:(NSString *)url;
- (void)getRequest:(NSString *)url andParams:(NSDictionary *)params;

- (void)postRequestPrams:(NSDictionary *)params andURL:(NSString *)url;

- (void)putRequest:(NSDictionary *)params andURL:(NSString *)url;

- (void)deleteRequest:(NSString *)url;

- (void)getSingleRequest:(NSString *)str;

- (void)postFile:(UIImageView *)imgView andParams:(NSDictionary *)prams andURL:(NSString *)url;
@end
