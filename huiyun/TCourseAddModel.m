//
//  TCourseAddModel.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseAddModel.h"

@implementation TCourseAddModel

//课程名称：
-(void)setTCourseName:(NSString *)TCourseName{
    if ([TCourseName isKindOfClass:[NSNull class]]) {
        _TCourseName=@"暂无";
    }else{
        _TCourseName=TCourseName;
    }
}

//课程类型：
-(void)setCategoryId:(NSNumber *)CategoryId{
    if([CategoryId isKindOfClass:[NSNull class]]){
        _CategoryId=[NSNumber numberWithInt:0];
    }else{
        _CategoryId=CategoryId;
    }
}


//课程科目：





//课程描述：

//开始时间：

//结束时间：

//上课地点：
-(void)setTCourseLocation:(NSString *)TCourseLocation{
    if ([TCourseLocation isKindOfClass:[NSNull class]]) {
        _TCourseLocation=@"暂无";
    }else{
        _TCourseLocation=TCourseLocation;
    }
}




@end
