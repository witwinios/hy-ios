//
//  CourseViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HYCourseCell.h"
#import "LoadingView.h"
#import "HYCourseDetailController.h"
#import "HYStudyHistoryViewController.h"
#import "RequestTools.h"
@interface HYCourseViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@end
