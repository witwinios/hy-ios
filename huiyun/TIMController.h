//
//  TIMController.h
//  yun
//
//  Created by MacAir on 2017/5/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherCell.h"
#import "MyQuesModel.h"
#import "SPKitExample.h"
#import "TeacherController.h"
@interface TIMController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (strong, nonatomic, readwrite) YWTribe *tribe;

@end
