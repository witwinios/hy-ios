//
//  TeachDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/10/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachDetailController.h"

@interface TeachDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
}
@end

@implementation TeachDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak UIViewController *weakSelf = self;
    self.leftBlock = ^(){
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";
    if ([self.model.caseStatus isEqualToString:@"待审核"]) {
        self.rightBtn.hidden = NO;
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        TeachDetailController *weakSelf = self;
        self.rightBlock = ^(){
            TeachRecordController *teachVC = [TeachRecordController new];
            teachVC.myModel = weakSelf.model;
            [weakSelf.navigationController pushViewController:teachVC animated:YES];
        };
    }
    self.titles = @"教学记录";
    titleArray = @[@"审核状态:",@"教学项目:",@"带教对象:",@"人数:",@"开始时间:",@"结束时间:",@"带教老师:",@"教学内容:",@"指导意见:",@"附件:"];
    self.dataArray = @[_model.caseStatus,_model.caseProject,_model.caseTeachObject,[NSString stringWithFormat:@"%@",_model.caseNums],[NSString stringWithFormat:@"%@",_model.caseStartTime],[NSString stringWithFormat:@"%@",_model.caseEndTime],_model.caseTeacher,_model.caseTeachContent,_model.caseAdvice,_model.fileUrl];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 9) {
        three.titleLab.text = titleArray[indexPath.row];
        return three;
    }else if (indexPath.row == 8 || indexPath.row == 7){
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else{
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
@end
